//
//  BlogAddCategoryViewController.m
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "BlogAddCategoryViewController.h"
#import "GlobalData.h"
#import "AddCategoryCell.h"
#import "CategoryListCell.h"

@interface BlogAddCategoryViewController ()

@end

@implementation BlogAddCategoryViewController

GlobalData *globalObjectBlogAddCategory;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    strAddCategoryText = @"";
    
    globalObjectBlogAddCategory = [[GlobalData alloc] init];
    globalObjectBlogAddCategory.delegate = self;
    [globalObjectBlogAddCategory language];
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    wantChangePassword = 0;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"Add New Category" value:@"" table:nil];
    
    
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    
    if(savedSessionId == nil)
        [self loginRequest];
    else{
        whichApiDataToProcess = @"getcategories";
        [self callingHttppApi];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData
{
    isAlertVisible = 1;
    collection = collectionData ;
    
    if([collection[@"success"] integerValue] == 5)
    {
        [self loginRequest];
    }
    else
    {
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted
{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}


-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectBlogAddCategory callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi
{
    // http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/getcategories
    // customerId
    
    [GlobalData alertController:currentWindow msg:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToProcess isEqual: @"getcategories"])
    {
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", customerId];
        
        [globalObjectBlogAddCategory callHTTPPostMethodForBlog:post api:@"getcategories" signal:@"HttpPostMetod"];
        globalObjectBlogAddCategory.delegate = self;
    }
    else if ([whichApiDataToProcess isEqual: @"addcategory"])
    {
        /*
         http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/addcategory
         
         -     - userid
         -     - cname (customer name)
         -     - cemail (customer email)
         -     - category (Category name)
         */
        
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"userid=%@&", customerId];
       // [post appendFormat:@"postid=%@&", [dictBlog valueForKey:@"id"]];
        [post appendFormat:@"category=%@&", strAddCategoryText];
        
        [globalObjectBlogAddCategory callHTTPPostMethodForBlog:post api:@"addcategory" signal:@"HttpPostMetod"];
        globalObjectBlogAddCategory.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult
{
    if(isAlertVisible == 1)
    {
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    if([whichApiDataToProcess isEqual: @"getcategories"])
    {
        arrCategoryList = (NSArray *)collection[@"categories"];
        
        if (arrCategoryList.count > 0)
        {
            [tblView reloadData];
        }
    }
    else if ([whichApiDataToProcess isEqual: @"addcategory"])
    {
        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        AddCategoryCell *cell = (AddCategoryCell *)[tblView cellForRowAtIndexPath:selectedIndexPath];
        
        cell.txtFCategory.text = @"";
        
        whichApiDataToProcess = @"getcategories";
        [self callingHttppApi];
    }
    else
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"message" value:@"" table:nil] message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogAddCategory.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    
    //  _mainContainerHeightConstraint.constant = SCREEN_HEIGHT-64;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader;
    
    if (section == 1)
    {
        if (arrCategoryList.count > 0)
        {
            viewHeader = [[UIView alloc] init];
            viewHeader.frame = CGRectMake(0, 0, tblView.frame.size.width, 30);
            viewHeader.backgroundColor = [UIColor clearColor];
            
            UILabel  *lblComment = [[UILabel alloc] init];
            lblComment.frame = CGRectMake(10, 0, tblView.frame.size.width-20, 30);
            lblComment.backgroundColor = [UIColor clearColor];
            lblComment.text = @"CATEGORY LIST";
            lblComment.font = [UIFont boldSystemFontOfSize:17.0];
            
            [viewHeader addSubview:lblComment];
        }
    }
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        if (arrCategoryList.count > 0)
        {
            return 30.0;
        }
    }
    return 0.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int row = 0;
    
    if (section == 0)
    {
        return 1;
    }
    else if (section == 1)
    {
        return arrCategoryList.count;
    }
    else if (section == 2)
    {
        return 1;
    }
    
    return row;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 142.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *simpleTableIdentifier = @"AddCategoryCell";
        
        AddCategoryCell *cell = (AddCategoryCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddCategoryCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
        cell.txtFCategory.delegate = self;
        
        [cell.btnAddCategory addTarget:self action:@selector(btnAddCategoryPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [cell reloadAddCategoryCell];

        
        return cell;
    }
    else if (indexPath.section == 1)
    {
        static NSString *simpleTableIdentifier = @"CategoryListCell";
        
        CategoryListCell *cell = (CategoryListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
        cell.dictCategory = [arrCategoryList objectAtIndex:indexPath.row];
        
        [cell reloadCategoryListCell];
        
        return cell;
    }
    
    UITableViewCell *cell;
    return cell;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)btnAddCategoryPressed
{
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    AddCategoryCell *cell = (AddCategoryCell *)[tblView cellForRowAtIndexPath:selectedIndexPath];
    
    strAddCategoryText = cell.txtFCategory.text;
    
    if ([strAddCategoryText isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please provide required detail." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }
    else
    {
        whichApiDataToProcess = @"addcategory";
        [self callingHttppApi];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
