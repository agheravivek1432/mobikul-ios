//
//  CategoryListCell.h
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryListCell : UITableViewCell

{
    IBOutlet UIView *viewCategory;
    IBOutlet UILabel *lblCategoryName;
    IBOutlet UILabel *lblDate;
}

@property (nonatomic, retain) NSDictionary *dictCategory;

-(void)reloadCategoryListCell;

@end
