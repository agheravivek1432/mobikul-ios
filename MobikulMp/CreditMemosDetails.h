//
//  CreditMemosDetails.h
//  MobikulMp
//
//  Created by kunal prasad on 01/07/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditMemosDetails : UIViewController{
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSString *sessionId, *message, *dataFromApi;
    id mainCollection,collection;
    NSString *whichApiDataToProcess;
    UIWindow *currentWindow;
}
@property (nonatomic) NSString *incrementId;
@property (nonatomic) NSString *customerId;
@property (nonatomic) NSString *creditMemoId;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;
@end
