//
//  MyOrders.h
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrders : UIViewController <NSXMLParserDelegate,UIScrollViewDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id collection;
    NSString *incrementId;
    UIWindow *currentWindow;
    UIView *paintView;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSIndexPath *indexPathValue;
    NSArray *arrayMainCollection;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
    NSString *reloadPageData;
}

@property (weak, nonatomic) IBOutlet UIView *allOrderContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *myOrderScrollView;

@end
