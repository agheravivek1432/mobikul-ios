//
//  MakeReviewData.m
//  MobikulMp
//
//  Created by kunal prasad on 27/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "MakeReviewData.h"
#import "GlobalData.h"
#import "ToastView.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)


@interface MakeReviewData ()

@end

GlobalData *globalObjectMakerReviewData;

@implementation MakeReviewData

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectMakerReviewData = [[GlobalData alloc] init];
    globalObjectMakerReviewData.delegate = self;
    [globalObjectMakerReviewData language];
    isAlertVisible = 0;
   [self registerForKeyboardNotifications];
    whichApiDataToprocess = @"";
    ratingsDict = [NSMutableDictionary dictionary];
    feedBackData = [NSMutableDictionary dictionary];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    _backPressedButton.target = self;
    _backPressedButton.action = @selector(backPressed);
    currentWindow = [UIApplication sharedApplication].keyWindow;
    keyboardSetFlag = 1;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

- (void) backPressed{
    if([_popUpView isEqualToString:@"1"]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
    UINavigationController *navController = self.navigationController;
    [navController popViewControllerAnimated:YES];
    }
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectMakerReviewData callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMakerReviewData.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectMakerReviewData.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectMakerReviewData.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMakerReviewData.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectMakerReviewData.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *customerId = [preferences objectForKey:@"sellerId"];
    NSString *customerEmail = [preferences objectForKey:@"customerEmail"];
     NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    if([whichApiDataToprocess isEqual:@"saveRating"]){
        
        [post appendFormat:@"feedprice=%@&", [ratingsDict objectForKey:@"100"]];
        [post appendFormat:@"feedvalue=%@&", [ratingsDict objectForKey:@"101"]];
        [post appendFormat:@"feedquality=%@&", [ratingsDict objectForKey:@"102"]];
        [post appendFormat:@"profileUrl=%@&", _profileUrl];
        [post appendFormat:@"feednickname=%@&",[feedBackData objectForKey:@"nickName"]];
        [post appendFormat:@"feedsummary=%@&", [feedBackData objectForKey:@"thoughts"]];
        [post appendFormat:@"feedreview=%@&",[feedBackData objectForKey:@"summary"]];
        [post appendFormat:@"userid=%@&", [preferences objectForKey:@"customerId"]];
        [post appendFormat:@"useremail=%@&", customerEmail];
        [post appendFormat:@"proownerid=%@", customerId];
        
        
        //[dictionary setObject:[ratingsDict objectForKey:@"100"] forKey:@"feedprice"];
        //[dictionary setObject:[ratingsDict objectForKey:@"101"] forKey:@"feedvalue"];
        //[dictionary setObject:[ratingsDict objectForKey:@"102"] forKey:@"feedquality"];
        //[dictionary setObject:_profileUrl forKey:@"profileUrl"];
        //[dictionary setObject:[feedBackData objectForKey:@"nickName"] forKey:@"feednickname"];
        //[dictionary setObject:[feedBackData objectForKey:@"thoughts"] forKey:@"feedsummary"];
        //[dictionary setObject:[feedBackData objectForKey:@"summary"] forKey:@"feedreview"];
        //[dictionary setObject:[preferences objectForKey:@"customerId"] forKey:@"userid"];
        //[dictionary setObject:customerEmail forKey:@"useremail"];
        //[dictionary setObject:customerId forKey:@"proownerid"];

        [globalObjectMakerReviewData callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/saveRating" signal:@"HttpPostMetod"];
        globalObjectMakerReviewData.delegate = self;
        
    
    }
    else{
     [post appendFormat:@"width=%@&", screenWidth];
        [post appendFormat:@"customerId=%@&", customerId];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"profileUrl=%@", _profileUrl];
    [globalObjectMakerReviewData callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getreviewData" signal:@"HttpPostMetod"];
    globalObjectMakerReviewData.delegate = self;
    }
}



//-(void)callingApi{
//    NSString *prefSessionId = [preferences objectForKey:@"sessionId"];
//    NSString *apiName = @"";
//    NSString *nameSpace = @"urn:Magento";
//    NSError *error;
//    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//    NSString *storeId = [preferences objectForKey:@"storeId"];
//    NSString *customerId = [preferences objectForKey:@"sellerId"];
//    NSString *customerEmail = [preferences objectForKey:@"customerEmail"];
//    NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
//    
//    if([whichApiDataToprocess isEqual:@"saveRating"]){
//        apiName =@"mobikulmpMarketplaceSaveRating";
//        [dictionary setObject:[ratingsDict objectForKey:@"100"] forKey:@"feedprice"];
//        [dictionary setObject:[ratingsDict objectForKey:@"101"] forKey:@"feedvalue"];
//        [dictionary setObject:[ratingsDict objectForKey:@"102"] forKey:@"feedquality"];
//        [dictionary setObject:_profileUrl forKey:@"profileUrl"];
//        [dictionary setObject:[feedBackData objectForKey:@"nickName"] forKey:@"feednickname"];
//        [dictionary setObject:[feedBackData objectForKey:@"thoughts"] forKey:@"feedsummary"];
//        [dictionary setObject:[feedBackData objectForKey:@"summary"] forKey:@"feedreview"];
//        [dictionary setObject:[preferences objectForKey:@"customerId"] forKey:@"userid"];
//        [dictionary setObject:customerEmail forKey:@"useremail"];
//        [dictionary setObject:customerId forKey:@"proownerid"];
//        
//    }
//    else {
//    apiName = @"mobikulmpMarketplaceGetreviewData";
//    [dictionary setObject:customerId forKey:@"customerId"];
//    [dictionary setObject:storeId forKey:@"storeId"];
//    [dictionary setObject:screenWidth forKey:@"width"];
//    [dictionary setObject:_profileUrl forKey:@"profileUrl"];
//    }
//    
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *jsonString;
//    if(!jsonData){
//       // NSLog(@"Got an error: %@", error);
//    }
//    else
//        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    NSString *parameters = [NSString stringWithFormat:@"<sessionId xsi:type=\"xsd:string\">%@</sessionId><attributes xsi:type=\"xsd:string\">%@</attributes>", prefSessionId, jsonString];
//    alert = [UIAlertController alertControllerWithTitle: @"Please Wait..." message: nil preferredStyle: UIAlertControllerStyleAlert];
//    NSString *envelope = [GlobalData createEnvelope:apiName  forNamespace:nameSpace forParameters:parameters currentView:self isAlertVisiblef:0 alertf:alert];
//    isAlertVisible = 1;
//    [globalObjectMakerReviewData sendingApiData:envelope];
//    globalObjectMakerReviewData.delegate = self;
//}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    float mainContainerY = 0;
    float internalY = 0;
    if([whichApiDataToprocess isEqual:@"saveRating" ]){
        if(collection[@"result"][@"message"])
         [ToastView showToastInParentView:self.view withText:collection[@"result"][@"message"]  withStatus:@"sucess" withDuaration:3.0];
        else{
         [ToastView showToastInParentView:self.view withText:@"Try again Later"  withStatus:@"error" withDuaration:3.0];
        }
    }
    else{
    
    UIView *bannerContainer = [[UIView alloc] initWithFrame:CGRectMake(5, 0 ,_mainView.frame.size.width-10, SCREEN_WIDTH/2)];
    bannerContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    bannerContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:bannerContainer];
    
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(0,internalY , SCREEN_WIDTH/4, SCREEN_WIDTH/4)];
    logo.image = [UIImage imageNamed:@"ic_placeholder.png"];
    logo.userInteractionEnabled = YES;
    UIImage *image = [imageCache objectForKey:mainCollection[@"logo"]];
    if(image)
        logo.image = image;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"logo"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        logo.image = image;
        [imageCache setObject:image forKey:mainCollection[@"logo"]];
    }
    [bannerContainer addSubview:logo];
    UIImageView *worldMap = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4 +5, internalY ,bannerContainer.frame.size.width-logo.frame.size.width-5, SCREEN_WIDTH/4)];
    [worldMap setImage:[UIImage imageNamed:@"ic_world.png"]];
    [bannerContainer addSubview:worldMap];
    
    internalY +=SCREEN_WIDTH/4 + 10;
    
    NSDictionary *reqAttributesforProfileTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]};
    CGSize reqStringSizeProfileTitle = [mainCollection[@"shopTitle"] sizeWithAttributes:reqAttributesforProfileTitle];
    CGFloat reqStringWidthProfileTitle = reqStringSizeProfileTitle.width;
    UILabel *profileTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, internalY , reqStringWidthProfileTitle, 30)];
    [profileTitleLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [profileTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [profileTitleLabel setText:mainCollection[@"shopTitle"]];
    [bannerContainer addSubview:profileTitleLabel];
    
    internalY += 35;

    UIImageView *countryFlag = [[UIImageView alloc] initWithFrame:CGRectMake(20, internalY , 2*SCREEN_WIDTH/16, SCREEN_WIDTH/16)];
    countryFlag.image = [UIImage imageNamed:@"ic_placeholder.png"];
    logo.userInteractionEnabled = YES;
    UIImage *countryImage = [imageCache objectForKey:mainCollection[@"countryflagUrl"]];
    if(image)
        countryFlag.image = countryImage;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"countryflagUrl"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        countryFlag.image = image;
        [imageCache setObject:image forKey:mainCollection[@"countryflagUrl"]];
    }
    [bannerContainer addSubview:countryFlag];
    
    internalY +=SCREEN_WIDTH/16 +10;
    
    NSDictionary *reqAttributesForAddress = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeAddress = [mainCollection[@"address"] sizeWithAttributes:reqAttributesForAddress];
    CGFloat reqStringWidthAddress = reqStringSizeAddress.width;
    CGFloat reqStringHeightaddress = reqStringSizeAddress.height ;
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, internalY, reqStringWidthAddress, reqStringHeightaddress)];
    [addressLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [addressLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [addressLabel setText:mainCollection[@"address"]];
    [bannerContainer addSubview:addressLabel ];
    
    internalY +=reqStringHeightaddress;
    CGRect bannerNewContainerFrame = bannerContainer.frame;
    bannerNewContainerFrame.size.height =internalY +5;
    bannerContainer.frame = bannerNewContainerFrame;
    
    mainContainerY +=internalY +10;
    
    UIButton *buttonGiveFeedBack = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonGiveFeedBack addTarget:self action:@selector(buttonHandlerForFeedback :) forControlEvents:UIControlEventTouchUpInside];
    [buttonGiveFeedBack setTitle:@"GIVE A FEEDBACK" forState:UIControlStateNormal];
    buttonGiveFeedBack.frame = CGRectMake(8,internalY + 5 ,_mainView.frame.size.width-16, 40.0);
    [buttonGiveFeedBack setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buttonGiveFeedBack setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [buttonGiveFeedBack setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:buttonGiveFeedBack ];
    
    mainContainerY +=45;
    internalY = 0;
    
    UIView *reviewListContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY ,_mainView.frame.size.width-10, 300)];
    reviewListContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    reviewListContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:reviewListContainer];
    
    UILabel *reviewDetails = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,reviewListContainer.frame.size.width, 30)];
    [reviewDetails setTextColor:[UIColor blackColor]];
    [reviewDetails setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [reviewDetails setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
    [reviewDetails setText:@"  Review List"];
    [reviewListContainer addSubview:reviewDetails];
    
    mainContainerY += 40;
    internalY = 40;
    float Y = 5;
    for(int i=0;i<[mainCollection[@"allReviews"] count];i++ ){
        NSDictionary *recentRating = [mainCollection[@"allReviews"] objectAtIndex:i];
        
        UIView *ratingDataContainer = [[UIView alloc] initWithFrame:CGRectMake(10,internalY ,reviewListContainer.frame.size.width-20 , 100)];
        ratingDataContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        ratingDataContainer.layer.borderWidth = 2.0f;
        [reviewListContainer addSubview:ratingDataContainer];
        
        
        NSDictionary *reqAttributesforReviewRatingSummary = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeReviewratingSummary = [recentRating[@"summary"] sizeWithAttributes:reqAttributesforReviewRatingSummary];
        CGFloat reqStringWidthReviewRatingSummary = reqStringSizeReviewratingSummary.width;
        UILabel *reviewRatingSummary = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingSummary, 25)];
        [reviewRatingSummary setTextColor:[UIColor blackColor]];
        [reviewRatingSummary setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [reviewRatingSummary setText:recentRating[@"summary"]];
        [ratingDataContainer addSubview:reviewRatingSummary];
        
        
        
        Y = 35;
        for(int j=0;j<[recentRating[@"rating"] count];j++){
            NSDictionary *recentRatingValue = [recentRating[@"rating"] objectAtIndex:j];
            NSDictionary *reqAttributesforReviewRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeReviewratingTitle = [recentRatingValue[@"label"] sizeWithAttributes:reqAttributesforReviewRatingTitle];
            CGFloat reqStringWidthReviewRatingTitle = reqStringSizeReviewratingTitle.width;
            UILabel *reviewRating = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingTitle, 25)];
            [reviewRating setTextColor:[UIColor blackColor]];
            [reviewRating setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
            [reviewRating setText:recentRatingValue[@"label"]];
            [ratingDataContainer addSubview:reviewRating];
            
            
            
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(100,Y,120,24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            
            double percent = 24 * [recentRatingValue[@"value"] doubleValue];
            
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [ratingDataContainer addSubview:ratingContainer];
            
            
            
            Y +=25;
            
        }
        Y +=5;
        NSDictionary *reqAttributesforReviewRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeReviewratingTitle = [recentRating[@"title"] sizeWithAttributes:reqAttributesforReviewRatingTitle];
        CGFloat reqStringWidthReviewRatingTitle = reqStringSizeReviewratingTitle.width;
        UILabel *reviewRatingTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingTitle, 25)];
        [reviewRatingTitle setTextColor:[UIColor blackColor]];
        [reviewRatingTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [reviewRatingTitle setText:recentRating[@"title"]];
        [ratingDataContainer addSubview:reviewRatingTitle];
        Y += 25;
        
        CGRect  ratingDataNewFrame = ratingDataContainer.frame;
        ratingDataNewFrame.size.height = Y +5 ;
        ratingDataContainer.frame = ratingDataNewFrame;
        
        internalY +=Y +10;
        
        Y = 5;
    }

    CGRect  reviewListNewFrame = reviewListContainer.frame;
    reviewListNewFrame.size.height = internalY +5 ;
    reviewListContainer.frame = reviewListNewFrame;

    mainContainerY +=internalY;
    
    _mainViewHeightConstraints.constant = mainContainerY - 35;
    }
}
-(void)buttonHandlerForFeedback:(UIButton*)button{
    
    ReviewData = @[@"Price", @"Value",@"Quality"];
    
    self.view.backgroundColor = [UIColor clearColor];
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 888;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    float Y = 0;
    CGFloat maxRatingCodeWidth = 0;
    
    float height= [[UIScreen mainScreen] bounds].size.height;
    
    for(int i=0; i<ReviewData.count; i++) {
        NSDictionary *ratingCodeAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]};
        CGSize ratingCodeStringSize = [ReviewData[i] sizeWithAttributes:ratingCodeAttributes];
        CGFloat ratingCodeStringWidth = ratingCodeStringSize.width;
        if(ratingCodeStringWidth > maxRatingCodeWidth)
            maxRatingCodeWidth = ratingCodeStringWidth;
    }
    
    
    ratingContainerBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5, 900)];
    [ratingContainerBlock setBackgroundColor : [UIColor whiteColor]];
    ratingContainerBlock.layer.cornerRadius = 4;
    ratingContainerBlock.tag = 999;
    
    ratingScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0  ,SCREEN_WIDTH/2+SCREEN_WIDTH/2.5,height-150)];
    //[ratingScrollView setBackgroundColor: [UIColor grayColor]];
    ratingScrollView.userInteractionEnabled = YES;
    [ratingScrollView  addSubview:ratingContainerBlock];
    
    UILabel *ratingTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 32)];      // heading
    [ratingTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [ratingTitle setBackgroundColor : [UIColor clearColor]];
    [ratingTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [ratingTitle setText : @"WRITE YOUR OWN REVIEW"];
    ratingTitle.adjustsFontSizeToFitWidth = YES;
    ratingTitle.textAlignment = NSTextAlignmentCenter;
    ratingTitle.autoresizesSubviews = "YES";
    [ratingContainerBlock addSubview : ratingTitle];
    
    Y +=35;
    
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, Y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 1)];           // line1
    [line1 setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [ratingContainerBlock addSubview : line1];
    
    Y +=2;
    
    for(int i =0 ; i<3 ; i++){
        
    UILabel *ratingHead = [[UILabel alloc] initWithFrame:CGRectMake(5, Y ,maxRatingCodeWidth , 25)];
    [ratingHead setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [ratingHead setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    [ratingHead setText:ReviewData[i]];
    [ratingContainerBlock addSubview:ratingHead];
      
        for(int j=0; j<5; j++) {
            UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(maxRatingCodeWidth +10 , Y, 25, 25)];
            radioBtnContainer.tag = 100+i;
            radioBtnContainer.layer.cornerRadius = 13;
            radioBtnContainer.layer.masksToBounds = YES;
            radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
            radioBtnContainer.layer.borderWidth = 2.0f;
        
        
            UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
            [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            radioBtn.tag = 1+j;
            radioBtn.layer.cornerRadius = 13;
            radioBtn.layer.masksToBounds = YES;
            radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
            radioBtn.layer.borderWidth = 5.0f;
            UITapGestureRecognizer *addReviewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectThisOption:)];
            addReviewGesture.numberOfTapsRequired = 1;
            [radioBtn addGestureRecognizer:addReviewGesture];
            [radioBtnContainer addSubview:radioBtn];
            [ratingContainerBlock addSubview:radioBtnContainer];
            
            
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake( maxRatingCodeWidth+40, Y, 120, 24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            double percent = 24 * (j+1);
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [ratingContainerBlock addSubview:ratingContainer];
            Y +=30;
    }
        
    Y +=30;
        
 }
    
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, Y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 1)];           // line2
    [line2 setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [ratingContainerBlock addSubview : line2];
    
    Y +=2;
    
    NSDictionary *reviewDetailAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};   // your thought
    CGSize reviewDetailStringSize = [@"Let us know your thoughts" sizeWithAttributes:reviewDetailAttributes];
    CGFloat reviewDetailWidth = reviewDetailStringSize.width;
    UILabel *reviewDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, Y, reviewDetailWidth, 25)];
    [reviewDetailLabel setTextColor: [GlobalData colorWithHexString:@"555555"]];
    [reviewDetailLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [reviewDetailLabel setText:@"Let us know your thoughts"];
    [reviewDetailLabel setBackgroundColor:[UIColor clearColor]];
    [ratingContainerBlock addSubview:reviewDetailLabel];
    
    Y +=30;
    
    UITextView *textArea = [[UITextView alloc] initWithFrame:CGRectMake(5, Y, ratingContainerBlock.frame.size.width - 10, 100)];
    textArea.layer.borderWidth = 1.0f;
    textArea.textColor = [UIColor blackColor];
    textArea.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textArea.font = [UIFont fontWithName: @"Trebuchet MS" size: 16.0f];
    textArea.tag = 10001;
    [ratingContainerBlock addSubview:textArea ];
    
    Y +=110;
    
    UITextField *summaryField = [[UITextField alloc] initWithFrame:CGRectMake(5, Y, ratingContainerBlock.frame.size.width - 10, 40)];
    summaryField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    summaryField.textColor = [UIColor blackColor];
    [summaryField setPlaceholder:@"Summary Your Review"];
    summaryField.layer.borderWidth = 1.0f;
    summaryField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    summaryField.tag =10002;
    [summaryField setKeyboardType:UIKeyboardTypePhonePad];
    summaryField.textAlignment = NSTextAlignmentLeft;
    summaryField.borderStyle = UITextBorderStyleRoundedRect;
    [ratingContainerBlock addSubview:summaryField];
    
    Y +=50;
    
    UITextField *nickNameField = [[UITextField alloc] initWithFrame:CGRectMake(5, Y, ratingContainerBlock.frame.size.width - 10, 40)];
    nickNameField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    nickNameField.textColor = [UIColor blackColor];
    [nickNameField setPlaceholder:@"What is Your Nick Name"];
    nickNameField.layer.borderWidth = 1.0f;
    nickNameField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    nickNameField.tag =10003;
    [nickNameField setKeyboardType:UIKeyboardTypePhonePad];
    nickNameField.textAlignment = NSTextAlignmentLeft;
    nickNameField.borderStyle = UITextBorderStyleRoundedRect;
    [ratingContainerBlock addSubview:nickNameField];
    
    Y +=50;
    
    
    UIView *requestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,Y , ratingContainerBlock.frame.size.width, 30)];
    requestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    requestContainer.layer.borderWidth = 0.0f;
    
    [ratingContainerBlock addSubview:requestContainer];
    
    
    NSDictionary *reqAttributesforCancelButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforCancelButtonText = [@"Cancel" sizeWithAttributes:reqAttributesforCancelButtonText];
    CGFloat reqStringWidthVCancelButtonText = reqStringSizeforCancelButtonText.width;
    UIButton  *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(reviewCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0,0,reqStringWidthVCancelButtonText+20, 40);
    [cancelButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [cancelButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [cancelButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    
    [requestContainer addSubview:cancelButton];
    
    NSDictionary *reqAttributesforSubmitButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforSubmitButtonText = [@"Submit" sizeWithAttributes:reqAttributesforSubmitButtonText];
    CGFloat reqStringWidthSubmitButtonText = reqStringSizeforSubmitButtonText.width;
    UIButton  *SubmitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [SubmitButton addTarget:self action:@selector(reviewSubmitButton :) forControlEvents:UIControlEventTouchUpInside];
    [SubmitButton setTitle:@"Submit" forState:UIControlStateNormal];
    SubmitButton.frame = CGRectMake(reqStringWidthVCancelButtonText +20 +10,0,reqStringWidthSubmitButtonText+20, 40);
    [SubmitButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [SubmitButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [SubmitButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [requestContainer addSubview:SubmitButton];
    
    CGRect  newFrame = requestContainer.frame;
    newFrame.size.width = reqStringWidthVCancelButtonText +20 +10 + reqStringWidthSubmitButtonText+20;
    requestContainer.frame = newFrame;
    
    
    UIView *finalrequestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,Y , ratingContainerBlock.frame.size.width, 30)];
    finalrequestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    finalrequestContainer.layer.borderWidth = 0.0f;
    requestContainer.center = CGPointMake(finalrequestContainer.frame.size.width  / 2,
                                          finalrequestContainer.frame.size.height / 2);
    [finalrequestContainer addSubview:requestContainer];
    
    [ratingContainerBlock addSubview:finalrequestContainer];
   
   
    Y +=50;
    
    CGRect  ratingContainerNewFrame = ratingContainerBlock.frame;
    ratingContainerNewFrame.size.height = Y;
    ratingContainerBlock.frame = ratingContainerNewFrame;
    
    
    ratingScrollView.contentSize = CGSizeMake(50,Y);
    
    ratingScrollView.center = blurEffectView.center;
    [blurEffectView addSubview:ratingScrollView];
    [self.view addSubview:blurEffectView];
}

-(void)reviewCancelButton :(UIButton*)button{
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    UIView *parent3 = parent2.superview;
    [parent3.superview removeFromSuperview];
 
}
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    if(keyboardSetFlag == 1){
        keyboardSetFlag = 0;
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    ratingScrollView.contentInset = contentInsets;
    ratingScrollView.scrollIndicatorInsets = contentInsets;
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
   if (!CGRectContainsPoint(aRect, self.view.frame.origin) ) {
        [ratingScrollView scrollRectToVisible:self.view.frame animated:YES];
    }
    }
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    if(keyboardSetFlag == 0){
        keyboardSetFlag = 1;
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    ratingScrollView.contentInset = contentInsets;
    ratingScrollView.scrollIndicatorInsets = contentInsets;
    }
}

-(void)selectThisOption:(UITapGestureRecognizer *)recognizer{
    for(UIView *subViews in ratingContainerBlock.subviews){
        if(subViews.tag == recognizer.view.superview.tag){
            for(UIView *subViews1 in subViews.subviews)
                [subViews1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    [ratingsDict setObject:[NSString stringWithFormat:@"%d", (int)recognizer.view.tag] forKey:[NSString stringWithFormat:@"%d", (int)recognizer.view.superview.tag]];
}
-(void)reviewSubmitButton :(UIButton*)button{
    NSString *errorMessage;
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    UIView *parent3 = parent2.superview;
    
    UITextView *textarea = [parent3 viewWithTag:10001];
    UITextField *summary = [parent3 viewWithTag:10002];
    UITextField *nickName = [parent3 viewWithTag:10003];
    
    isValid = 1;
    
    if ([nickName.text isEqualToString:@""]){
        isValid = 0;
        errorMessage = @"Please enter the Nick Name";
    }
    if ([summary.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Summary";
    }
    if ([textarea.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter your Thoughts";
    }
    if( ![ratingsDict objectForKey:@"102"]){
        
        isValid =0;
        errorMessage = @"Please Rate Quality";
    }
    if( ![ratingsDict objectForKey:@"101"]){
        
        isValid =0;
        errorMessage = @"Please Rate Value";
    }
    if( ![ratingsDict objectForKey:@"100"]){
        
        isValid =0;
        errorMessage = @"Please Rate Price";
    }
    if(isValid == 1){
        
         [feedBackData setObject:nickName.text forKey:@"nickName"];
         [feedBackData setObject:summary.text forKey:@"summary"];
         [feedBackData setObject:textarea.text forKey:@"thoughts"];
         isValid = 2;
    }
    
    
    if(isValid == 0){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }

    if(isValid == 2){
        
    whichApiDataToprocess = @"saveRating";
    [parent3.superview removeFromSuperview];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
        }
}




@end
