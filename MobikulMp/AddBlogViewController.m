//
//  AddBlogViewController.m
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AddBlogViewController.h"
#import "GlobalData.h"

@interface AddBlogViewController ()

@end

@implementation AddBlogViewController

GlobalData *globalObjectBlogAddBlog;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    strCategoryID = @"";
    
    globalObjectBlogAddBlog = [[GlobalData alloc] init];
    globalObjectBlogAddBlog.delegate = self;
    [globalObjectBlogAddBlog language];
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    wantChangePassword = 0;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"Add New Blog" value:@"" table:nil];
    
    
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    
    if(savedSessionId == nil)
        [self loginRequest];
    else
    {
        whichApiDataToProcess = @"getcategories";
        [self callingHttppApi];
    }
    
    btnCategory.layer.borderColor = [UIColor blackColor].CGColor;
    btnCategory.layer.borderWidth = 1.0;
    
    txtVMetaKeyword.layer.borderColor = [UIColor blackColor].CGColor;
    txtVMetaKeyword.layer.borderWidth = 1.0;
    
    txtVMetaDescription.layer.borderColor = [UIColor blackColor].CGColor;
    txtVMetaDescription.layer.borderWidth = 1.0;
    
    txtVContent.layer.borderColor = [UIColor blackColor].CGColor;
    txtVContent.layer.borderWidth = 1.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData
{
    isAlertVisible = 1;
    collection = collectionData ;
    
    if([collection[@"success"] integerValue] == 5)
    {
        [self loginRequest];
    }
    else
    {
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted
{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}


-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectBlogAddBlog callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi
{
    // http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/getcategories
    // customerId
    
    [GlobalData alertController:currentWindow msg:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToProcess isEqual: @"getcategories"])
    {
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", customerId];
        
        [globalObjectBlogAddBlog callHTTPPostMethodForBlog:post api:@"getcategories" signal:@"HttpPostMetod"];
        globalObjectBlogAddBlog.delegate = self;
    }
    else if ([whichApiDataToProcess isEqual: @"addblog"])
    {
        /*
         http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/addblog
         
         -     - userid
         -     - cname (Ankit Patel)
         -     - cemail (ankitpatel2341@gmail.com)
         -     - category (100)
         -     - subject (Change store landing page)
         -     - metakeywords (Text)
         -     - metadescription (Text)
         -     - content (content)
         -     - tag (Text)
         */
        
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"userid=%@&", customerId];
        [post appendFormat:@"category=%@&", strCategoryID];
        [post appendFormat:@"subject=%@&", txtFTitle.text];
        [post appendFormat:@"metakeywords=%@&", txtVMetaKeyword.text];
        [post appendFormat:@"metadescription=%@&", txtVMetaDescription.text];
        [post appendFormat:@"content=%@&", txtVContent.text];
        [post appendFormat:@"tag=%@&", txtFTag.text];
        
        [globalObjectBlogAddBlog callHTTPPostMethodForBlog:post api:@"addblog" signal:@"HttpPostMetod"];
        globalObjectBlogAddBlog.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult
{
    if(isAlertVisible == 1)
    {
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    if([whichApiDataToProcess isEqual: @"getcategories"])
    {
        arrCategoryList = (NSArray *)collection[@"categories"];
        
        if (arrCategoryList.count > 0)
        {
            [pickerCategory reloadAllComponents];
        }
    }
    else if ([whichApiDataToProcess isEqual: @"addblog"])
    {
        [self performSegueWithIdentifier:@"bloglistview" sender:self];
    }
    else
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"message" value:@"" table:nil] message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogAddBlog.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    
    //  _mainContainerHeightConstraint.constant = SCREEN_HEIGHT-64;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(IBAction)btnCategoryPressed:(id)sender
{
    UIAlertController *actionSheet;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    
    if(result.height == 480)
    {
        actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
    }
    else
    {
        actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
    }
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [btnCategory setTitle:[NSString stringWithFormat:@"%@",[[arrCategoryList objectAtIndex:selectedRow] valueForKey:@"category"]] forState:UIControlStateNormal];
                                     strCategoryID = [NSString stringWithFormat:@"%@",[[arrCategoryList objectAtIndex:selectedRow] valueForKey:@"id"]];
                                     
                                     [actionSheet dismissViewControllerAnimated:YES completion:nil];
                                 }];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [actionSheet dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    [actionSheet addAction:doneAction];
    [actionSheet addAction:cancelAction];
    
    if(result.height == 480)
    {
        pickerCategory = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 25, 300, 70)];
    }
    else
    {
        pickerCategory = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, actionSheet.view.frame.size.width, 150)];
    }
    pickerCategory.delegate = self;
    pickerCategory.dataSource = self;
  //  pickerCategory.center = CGPointMake(150, 100);
    pickerCategory.showsSelectionIndicator = YES;
    
    UILabel *lblType = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, actionSheet.view.frame.size.width, 30)];
    lblType.text = @"Select Blog Category";
    lblType.textAlignment = NSTextAlignmentCenter;
    lblType.font = [UIFont systemFontOfSize:22.0];
    lblType.backgroundColor = pickerCategory.backgroundColor;
    
    [actionSheet.view addSubview:lblType];
    [actionSheet.view addSubview:pickerCategory];
    [self presentViewController:actionSheet animated:YES completion:nil];

}

#pragma For PickerView With ActionSheet START--------------------------------------------------------------------
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger row = 0;
    
    if (component == 0)
    {
        row = [arrCategoryList count];
    }
    return row;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *strTitle = @"";
    
    if (component == 0)
    {
        strTitle = [[arrCategoryList objectAtIndex:row] valueForKey:@"category"];
    }
    return strTitle;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    CGFloat height = 0.0;
    
    if (component == 0)
    {
        height = 40;
    }
    return height;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        selectedRow = row;
    }
}
#pragma For PickerView With ActionSheet END-----------------------------------------------------------------------

- (IBAction)btnSubmitBlogPressed:(id)sender
{
    if ([txtFTitle.text isEqualToString:@""] || [txtFTag.text isEqualToString:@""] || [txtVContent.text isEqualToString:@""] || [txtVMetaDescription.text isEqualToString:@""]|| [txtVMetaKeyword.text isEqualToString:@""] || [strCategoryID isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please provide required detail." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }
    else
    {
        whichApiDataToProcess = @"addblog";
        [self callingHttppApi];
    }
}

@end
