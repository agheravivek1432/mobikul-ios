//
//  SearchTerms.h
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTerms : UIViewController<NSXMLParserDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSDictionary *searchData;
    NSInteger isAlertVisible;
    id collection;
    UIWindow *currentWindow;
}

@property (weak, nonatomic) IBOutlet UITableView *searchTermTable;

@end
