//
//  OrderSuccess.m
//  Mobikul
//
//  Created by Ratnesh on 04/05/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "OrderSuccess.h"
#import "Home.h"
#import "GlobalData.h"


#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectOrderSuccess;

@implementation OrderSuccess

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectOrderSuccess = [[GlobalData alloc] init];
    globalObjectOrderSuccess.delegate = self;
    [globalObjectOrderSuccess language];
    languageCode = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:@"00CBDF"];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationItem.title = [globalObjectOrderSuccess.languageBundle localizedStringForKey:@"orderPlaced" value:@"" table:nil];
    [_actionLabel setText:[NSString stringWithFormat:@"Your Order id is #%@", _incrementId]];
    _mainHeading.text = [globalObjectOrderSuccess.languageBundle localizedStringForKey:@"orderRecieved" value:@"" table:nil];
    _subHeading.text = [globalObjectOrderSuccess.languageBundle localizedStringForKey:@"yourPurchase" value:@"" table:nil];
    _noticeLabel.text = [globalObjectOrderSuccess.languageBundle localizedStringForKey:@"noticeLabel" value:@"" table:nil];
    [_continueShopping setTitle: [globalObjectOrderSuccess.languageBundle localizedStringForKey:@"continueShopping" value:@"" table:nil] forState: UIControlStateNormal];
}

- (IBAction)continueShopping:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    tabBarController.selectedIndex = 0;
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        [[tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:nil];
    else
        [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:nil];
    
}

@end
