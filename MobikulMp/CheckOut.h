//
//  CheckOut.h
//  Mobikul
//
//  Created by Ratnesh on 15/04/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BraintreeUI.h>

@interface CheckOut : UIViewController <NSXMLParserDelegate, UIPickerViewDelegate, UITabBarControllerDelegate, BTDropInViewControllerDelegate>
{
    NSMutableData *webData;
    NSUserDefaults *preferences;
    NSString *gotSessionId, *gotMessage, *gotCode, *gotDataFromApi;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSMutableString *finalData;
    NSInteger isAlertVisible, selectedCountryIndex, SIselectedCountryIndex;
    NSString *whichApiDataToprocess, *shipToThisOrDifferentAddress, *choosingNewBillingAddress, *selectedbillingId, *heightModificationDoneOfBillingInfo, *savenewBillingAddress, *billingCountryId, *billingStateId, *isBillingStateId, *sizeModified, *forgotPasswordEmail, *checkoutAsGuest;
    NSString *choosingNewShippingAddress, *selectedshippingId, *heightModificationDoneOfShippingInfo, *savenewShippingAddress, *shippingCountryId, *shippingStateId, *isShippingStateId, *useBillingAddress, *selectedShippingMethod, *selectedPaymentMethod, *isBillingFormOpen, *isShippingFormOpen;
    id mainCollection, stepThreenFourCollection, orderReviewCollection, saveOrderCollection, loginCollection, forgotPasswordCollection,collection;
    int mainContainerY, stepCount, NBAY, NSAY, lowerActionsY, SIlowerActionsY, currentStep, SMY, PIY, ORY;
    UIView *guestRegisterBlock, *newBillingAddressContainer, *billingInformationContainer, *newShippingAddressContainer, *shippingInformationContainer;
    NSMutableArray *addressPickerData, *countryPickerData, *statePickerData, *SIcountryPickerData, *SIstatePickerData;
    NSString *shippingSelect, *statePickerState, *billingFormState;
    UIWindow *currentWindow;
    
    id brainTreePaymentScccess;
    BTAPIClient *braintreeClient;
    NSString *strToken;
    BTDropInViewController *dropInViewController;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMargin;
@property (strong, nonatomic) IBOutlet UIView *mainBaseView;


@property (weak, nonatomic) IBOutlet UIView *guestRegisterLoginContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *guestRegisterLoginContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *GRLHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *GRLInnerMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *GRLInnerMainContainerHeightConstraint;


@property (weak, nonatomic) IBOutlet UIView *BIContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *BIHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *BIInternalMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIInternalMainContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *BIInternalHeadingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIAdreessFormTop;
@property (weak, nonatomic) IBOutlet UIPickerView *BIAddressPicker;
@property (weak, nonatomic) IBOutlet UIView *BIAddressForm;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIAddressFormHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *BIActionContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIActionContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIInternalHeadingHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BIAddressPickerHeightConstraint;




@property (weak, nonatomic) IBOutlet UIView *SIContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SIContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *SIHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *SIInternalMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SIInternalMainContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *SIInternalHeadingLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *SIAddressPicker;
@property (weak, nonatomic) IBOutlet UIView *SIAddressForm;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SIAddressFormHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *SIActionContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SIActionContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SIInternalHeadingHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SIAddressPickerHeightConstraint;


















@property (weak, nonatomic) IBOutlet UIView *SMContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SMContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *SMHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *SMInternalMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SMInternalMainContainerHeightConstraint;


@property (weak, nonatomic) IBOutlet UIView *PIContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PIContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *PIHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *PIInternalMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PIInternalMainContainerHeightConstraint;


@property (weak, nonatomic) IBOutlet UIView *ORContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ORContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *ORHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *ORInternalMainContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ORInternalMainContainerHeightConstraint;

@end