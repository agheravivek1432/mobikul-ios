//
//  SellerDashBoardData.m
//  MobikulMp
//
//  Created by kunal prasad on 23/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "SellerDashBoardData.h"
#import "GlobalData.h"
#import "JBLineChartView.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

typedef NS_ENUM(NSInteger, JBLineChartLine){
    JBLineChartLineSolid,
    kJBLineChartViewControllerChartHeaderPadding,
    kJBColorLineChartBackground
//    JBLineChartLineDashed,
//    JBLineChartLineCount
};

@interface SellerDashBoardData ()

@end

GlobalData *globalObjectSellerDashBoardData;


@implementation SellerDashBoardData

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectSellerDashBoardData = [[GlobalData alloc] init];
    globalObjectSellerDashBoardData.delegate = self;
    [globalObjectSellerDashBoardData language];
    isAlertVisible = 0;
    graphdata = [[NSMutableArray alloc] init];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

- (BOOL)lineChartView:(JBLineChartView *)lineChartView showsDotsForLineAtLineIndex:(NSUInteger)lineIndex;{
    return  YES;
}

- (BOOL)lineChartView:(JBLineChartView *)lineChartView smoothLineAtLineIndex:(NSUInteger)lineIndex{
    return YES;
}
- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForDotAtHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex{
    return [UIColor darkGrayColor];
}
- (CGFloat)lineChartView:(JBLineChartView *)lineChartView dotRadiusForDotAtHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex{
    return 7.5;
}
- (JBLineChartViewLineStyle)lineChartView:(JBLineChartView *)lineChartView lineStyleForLineAtLineIndex:(NSUInteger)lineIndex{
    return JBLineChartViewLineStyleDashed;
}



- (BOOL)lineChartView:(JBLineChartView *)lineChartView shouldHideDotViewOnSelectionAtHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex{
    return  YES;
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView selectionFillColorForLineAtLineIndex:(NSUInteger)lineIndex{
    
    return [UIColor blueColor];
}
- (NSUInteger)numberOfLinesInLineChartView:(JBLineChartView *)lineChartView{
    return 1; // number of lines in chart
}

- (NSUInteger)lineChartView:(JBLineChartView *)lineChartView numberOfVerticalValuesAtLineIndex:(NSUInteger)lineIndex{
    return [graphdata count]; // number of values for a line
}
- (CGFloat)lineChartView:(JBLineChartView *)lineChartView verticalValueForHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex{
    NSNumber *value = (NSNumber *)[graphdata objectAtIndex:horizontalIndex];
    return [value floatValue];
}


- (void)lineChartView:(JBLineChartView *)lineChartView didSelectLineAtIndex:(NSUInteger)lineIndex horizontalIndex:(NSUInteger)horizontalIndex touchPoint:(CGPoint)touchPoint{
    
    float result = [[graphdata objectAtIndex:horizontalIndex] floatValue];
    NSString *str = [NSString stringWithFormat:@"%.2f", result];
    [tooltipData setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [tooltipData setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [tooltipData setText:str];
    [_mainView addSubview: tooltipData];
    
    [tooltipMonth setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [tooltipMonth setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [tooltipMonth setText:[self monthName:horizontalIndex]];
    [_mainView addSubview: tooltipMonth];

}

-(NSString*) monthName:(NSInteger) value{
    NSString *result;
    switch(value){
        case 0: result = @"JANUARY";break;
        case 1: result = @"FEBRUARY";break;
        case 2: result = @"MARCH";break;
        case 3: result = @"APRIL";break;
        case 4: result = @"MAY";break;
        case 5: result = @"JUNE";break;
        case 6: result = @"JULY";break;
        case 7: result = @"AUGUST";break;
        case 8: result = @"SEPTEMBER";break;
        case 9: result = @"OCTOBER";break;
        case 10: result = @"NOVEMBER";break;
        case 11: result = @"DECEMBER";break;
    }
    return  result;
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectSellerDashBoardData callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectSellerDashBoardData.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectSellerDashBoardData.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectSellerDashBoardData.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectSellerDashBoardData.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectSellerDashBoardData.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
     NSString *customerId = [preferences objectForKey:@"customerId"];
    [post appendFormat:@"customerId=%@", customerId];
    
    [globalObjectSellerDashBoardData callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getsellerdashboardData" signal:@"HttpPostMetod"];
    globalObjectSellerDashBoardData.delegate = self;



}
-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    mainContainerY = 0;
    float internalY = 0;
    
    for(int i=0;i<[mainCollection[@"graphData"] count];i++ ){
        NSDictionary  *value = [mainCollection[@"graphData"] objectAtIndex:i];
        [graphdata addObject:value];
    }
    
    if(graphdata.count > 0){
        
        UITableView *graphContainer = [[UITableView alloc] initWithFrame:CGRectMake(5,mainContainerY, _mainView.frame.size.width-10, 300)];
        graphContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        graphContainer.layer.borderWidth = 2.0f;
        [_mainView addSubview:graphContainer];
        lineChartView = [[JBLineChartView alloc] initWithFrame:CGRectMake(5,100, _mainView.frame.size.width-20, 190)];
        lineChartView.dataSource = self;
        lineChartView.showsLineSelection = YES;
        lineChartView.showsVerticalSelection = NO; // it shows vertical shade in background
        lineChartView.delegate = self;
        lineChartView.headerPadding = kJBLineChartViewControllerChartHeaderPadding;
        [lineChartView reloadData];
        [graphContainer addSubview:lineChartView];
        tooltipData = [[UILabel alloc] initWithFrame:CGRectMake(10,mainContainerY +20 ,100 , 25)];
        tooltipMonth = [[UILabel alloc] initWithFrame:CGRectMake(120,mainContainerY +20 ,200 , 25)];
        
        mainContainerY +=310;
    }
    
    UIView *priceContainer = [[UIView alloc] initWithFrame:CGRectMake(5, 310 , _mainView.frame.size.width-10, SCREEN_WIDTH/2)];
    priceContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    priceContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:priceContainer];
 
    // today price
    
    UIView *priceContainer1 = [[UIView alloc] initWithFrame:CGRectMake(5, 5 ,priceContainer.frame.size.width/2 ,65)];
    priceContainer1.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    priceContainer1.layer.borderWidth = 2.0f;
    [priceContainer addSubview:priceContainer1];
    
    
    UILabel *todayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, priceContainer1.frame.size.width, 25)];
    [todayLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [todayLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [todayLabel setText:mainCollection[@"today"][@"label"]];
    todayLabel.textAlignment = NSTextAlignmentCenter;
    [todayLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [priceContainer1 addSubview:todayLabel];
    
    UILabel *todayLabelPrice = [[UILabel alloc] initWithFrame:CGRectMake(0,35, priceContainer1.frame.size.width, 25)];
    [todayLabelPrice setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [todayLabelPrice setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [todayLabelPrice setText:mainCollection[@"today"][@"amount"]];
    todayLabelPrice.textAlignment = NSTextAlignmentCenter;
    [priceContainer1 addSubview:todayLabelPrice];
    
    
    //week price
    
    UIView *priceContainer2 = [[UIView alloc] initWithFrame:CGRectMake(priceContainer1.frame.size.width +5, 5 ,priceContainer.frame.size.width/2-10 ,65)];
    priceContainer2.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    priceContainer2.layer.borderWidth = 2.0f;
    [priceContainer addSubview:priceContainer2];
    
    
    UILabel *weekLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, priceContainer2.frame.size.width, 25)];
    [weekLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [weekLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [weekLabel setText:mainCollection[@"week"][@"label"]];
    weekLabel.textAlignment = NSTextAlignmentCenter;
    [weekLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [priceContainer2 addSubview:weekLabel];
    
    UILabel *weekLabelPrice = [[UILabel alloc] initWithFrame:CGRectMake(0,35, priceContainer2.frame.size.width, 25)];
    [weekLabelPrice setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [weekLabelPrice setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [weekLabelPrice setText:mainCollection[@"week"][@"amount"]];
    weekLabelPrice.textAlignment = NSTextAlignmentCenter;
    [priceContainer2 addSubview:weekLabelPrice];
    
    
    internalY += 75;
    
    //month price
    
    
    UIView *priceContainer3 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,priceContainer.frame.size.width/2 ,65)];
    priceContainer3.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    priceContainer3.layer.borderWidth = 2.0f;
    [priceContainer addSubview:priceContainer3];
    
    
    UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, priceContainer3.frame.size.width, 25)];
    [monthLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [monthLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [monthLabel setText:mainCollection[@"month"][@"label"]];
    monthLabel.textAlignment = NSTextAlignmentCenter;
    [monthLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [priceContainer3 addSubview:monthLabel];
    
    UILabel *monthLabelPrice = [[UILabel alloc] initWithFrame:CGRectMake(0,35, priceContainer3.frame.size.width, 25)];
    [monthLabelPrice setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [monthLabelPrice setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [monthLabelPrice setText:mainCollection[@"month"][@"amount"]];
    monthLabelPrice.textAlignment = NSTextAlignmentCenter;
    [priceContainer3 addSubview:monthLabelPrice];
    
    //remaining amount
    
    UIView *priceContainer4 = [[UIView alloc] initWithFrame:CGRectMake(priceContainer3.frame.size.width +5, internalY ,priceContainer.frame.size.width/2-10 ,65)];
    priceContainer4.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    priceContainer4.layer.borderWidth = 2.0f;
    [priceContainer addSubview:priceContainer4];
    
    
    UILabel *remainLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, priceContainer4.frame.size.width, 25)];
    [remainLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [remainLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [remainLabel setText:mainCollection[@"remaining"][@"label"]];
    remainLabel.textAlignment = NSTextAlignmentCenter;
    [remainLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    remainLabel.adjustsFontSizeToFitWidth = YES;
    [priceContainer4 addSubview:remainLabel];
    
    UILabel *remainLabelPrice = [[UILabel alloc] initWithFrame:CGRectMake(0,35, priceContainer4.frame.size.width, 25)];
    [remainLabelPrice setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [remainLabelPrice setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [remainLabelPrice setText:mainCollection[@"remaining"][@"amount"]];
    remainLabelPrice.textAlignment = NSTextAlignmentCenter;
    [priceContainer4 addSubview:remainLabelPrice];

    
    internalY +=70;
    
    // total amount
    
    UIView *priceContainer5 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,priceContainer.frame.size.width-10 ,65)];
    priceContainer5.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    priceContainer5.layer.borderWidth = 2.0f;
    [priceContainer addSubview:priceContainer5];

    
    UILabel *totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, priceContainer5.frame.size.width, 25)];
    [totalLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [totalLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [totalLabel setText:mainCollection[@"payout"][@"label"]];
    totalLabel.textAlignment = NSTextAlignmentCenter;
    [totalLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [priceContainer5 addSubview:totalLabel];
    
    UILabel *totalLabelPrice = [[UILabel alloc] initWithFrame:CGRectMake(0,35, priceContainer5.frame.size.width, 25)];
    [totalLabelPrice setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [totalLabelPrice setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [totalLabelPrice setText:mainCollection[@"payout"][@"amount"]];
    totalLabelPrice.textAlignment = NSTextAlignmentCenter;
    [priceContainer5 addSubview:totalLabelPrice];
    
    internalY += 70;
    
    CGRect priceConrtainernewFrame = priceContainer.frame;
    priceConrtainernewFrame.size.height = internalY;
    priceContainer.frame = priceConrtainernewFrame;
   
    mainContainerY +=internalY;
    internalY = mainContainerY;
    

    internalY +=10;
    float  y = 0;
    
    
    for(int i=0 ;i<[mainCollection[@"orderList"] count];i++ ){
     
        NSDictionary *orderList = [mainCollection[@"orderList"] objectAtIndex:i];
        
        UIView *orderContainer = [[UIView alloc] initWithFrame:CGRectMake(5,internalY,_mainView.frame.size.width-10,100 )];
        orderContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        orderContainer.layer.borderWidth = 2.0f;
        orderContainer.tag = i;
        [_mainView addSubview:orderContainer];
        
        
        UILabel *orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, orderContainer.frame.size.width, 25)];
        [orderLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [orderLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [orderLabel setText:orderList[@"label"]];
        orderLabel.textAlignment = NSTextAlignmentCenter;
        [orderLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [orderContainer addSubview:orderLabel];
        
        y = 35;
        
        for(int j=0; j<[orderList[@"orderList"]count]; j++){
            
            NSDictionary *orderName = [orderList[@"orderList"] objectAtIndex:j];
            
            UILabel *orderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0,y, orderContainer.frame.size.width, 25)];
            [orderLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
            [orderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [orderLabelName setText:orderName[@"name"]];
            orderLabelName.textAlignment = NSTextAlignmentCenter;
            [orderContainer addSubview:orderLabelName];
            y +=30;
        }
        
        y+=10;
        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,y, orderContainer.frame.size.width, 25)];
        [statusLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [statusLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [statusLabel setText:orderList[@"status"]];
        [orderContainer addSubview:statusLabel];
        
        y+=35;
        
        UILabel *summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,y, orderContainer.frame.size.width-10, 25)];
        [summaryLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [summaryLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [summaryLabel setText:orderList[@"summary"]];
        summaryLabel.adjustsFontSizeToFitWidth = YES;
        [orderContainer addSubview:summaryLabel];
        
        y+=35;
        
        UILabel *orderTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,y, orderContainer.frame.size.width, 25)];
        [orderTotalLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [orderTotalLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [orderTotalLabel setText:orderList[@"orderTotal"]];
        [orderContainer addSubview:orderTotalLabel];
        
        y+=35;
        
        CGRect orderConrtainernewFrame = orderContainer.frame;
        orderConrtainernewFrame.size.height = y;
        orderContainer.frame = orderConrtainernewFrame;
        
        
        internalY +=y +10;
        
        
    }
    
    
    mainContainerY = internalY;
    
    
    
    internalY = 0;
    UIView *reviewListContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY, _mainView.frame.size.width-10, 300)];
    reviewListContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    reviewListContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:reviewListContainer];
    
    
    UILabel *reviewDetails = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,_mainView.frame.size.width-10, 30)];
    [reviewDetails setTextColor:[UIColor blackColor]];
    [reviewDetails setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [reviewDetails setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
    [reviewDetails setText:@"  Review List"];
    [reviewListContainer addSubview:reviewDetails];
    
    mainContainerY += 40;
    internalY = 40;
    float Y = 5;
    for(int i=0;i<[mainCollection[@"recentReviews"] count];i++ ){
        NSDictionary *recentRating = [mainCollection[@"recentReviews"] objectAtIndex:i];
        
        UIView *ratingDataContainer = [[UIView alloc] initWithFrame:CGRectMake(10,internalY ,reviewListContainer.frame.size.width-20 , 100)];
        ratingDataContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        ratingDataContainer.layer.borderWidth = 2.0f;
        [reviewListContainer addSubview:ratingDataContainer];
        
        
        NSDictionary *reqAttributesforReviewRatingSummary = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeReviewratingSummary = [recentRating[@"summary"] sizeWithAttributes:reqAttributesforReviewRatingSummary];
        CGFloat reqStringWidthReviewRatingSummary = reqStringSizeReviewratingSummary.width;
        UILabel *reviewRatingSummary = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingSummary, 25)];
        [reviewRatingSummary setTextColor:[UIColor blackColor]];
        [reviewRatingSummary setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [reviewRatingSummary setText:recentRating[@"summary"]];
        [ratingDataContainer addSubview:reviewRatingSummary];
        
        
        
        Y = 35;
        for(int j=0;j<[recentRating[@"rating"] count];j++){
            NSDictionary *recentRatingValue = [recentRating[@"rating"] objectAtIndex:j];
            NSDictionary *reqAttributesforReviewRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeReviewratingTitle = [recentRatingValue[@"label"] sizeWithAttributes:reqAttributesforReviewRatingTitle];
            CGFloat reqStringWidthReviewRatingTitle = reqStringSizeReviewratingTitle.width;
            UILabel *reviewRating = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingTitle, 25)];
            [reviewRating setTextColor:[UIColor blackColor]];
            [reviewRating setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
            [reviewRating setText:recentRatingValue[@"label"]];
            [ratingDataContainer addSubview:reviewRating];
            
            
            
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(100,Y,120,24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            
            double percent = 24 * [recentRatingValue[@"value"] doubleValue];
            
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [ratingDataContainer addSubview:ratingContainer];
            
            
            
            Y +=25;
            
        }
        Y +=5;
        NSDictionary *reqAttributesforReviewRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeReviewratingTitle = [recentRating[@"title"] sizeWithAttributes:reqAttributesforReviewRatingTitle];
        CGFloat reqStringWidthReviewRatingTitle = reqStringSizeReviewratingTitle.width;
        UILabel *reviewRatingTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingTitle, 25)];
        [reviewRatingTitle setTextColor:[UIColor blackColor]];
        [reviewRatingTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [reviewRatingTitle setText:recentRating[@"title"]];
        [ratingDataContainer addSubview:reviewRatingTitle];
        Y += 25;
        
        CGRect  ratingDataNewFrame = ratingDataContainer.frame;
        ratingDataNewFrame.size.height = Y +5 ;
        ratingDataContainer.frame = ratingDataNewFrame;
        
        internalY +=Y +10;
        
        Y = 5;
    }
    
    CGRect  reviewListNewFrame = reviewListContainer.frame;
    reviewListNewFrame.size.height = internalY +5 ;
    reviewListContainer.frame = reviewListNewFrame;
    
    mainContainerY += internalY;
    mainContainerY -= 20;
    
//    for(int i=0;i<[mainCollection[@"graphData"] count];i++ ){
//        NSDictionary  *value = [mainCollection[@"graphData"] objectAtIndex:i];
//       [graphdata addObject:value];
//    }
//    
//    if(graphdata.count > 0){
//    
//    UITableView *graphContainer = [[UITableView alloc] initWithFrame:CGRectMake(5,mainContainerY, SCREEN_WIDTH-10, 300)];
//    graphContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
//    graphContainer.layer.borderWidth = 2.0f;
//    [_mainView addSubview:graphContainer];
//    
//    
//    
//    lineChartView = [[JBLineChartView alloc] initWithFrame:CGRectMake(5,100, SCREEN_WIDTH-20, 190)];
//    lineChartView.dataSource = self;
//    lineChartView.showsLineSelection = YES;
//    lineChartView.showsVerticalSelection = NO; // it shows vertical shade in background
//    lineChartView.delegate = self;
//    lineChartView.headerPadding = kJBLineChartViewControllerChartHeaderPadding;
//    [lineChartView reloadData];
//    [graphContainer addSubview:lineChartView];
//    tooltipData = [[UILabel alloc] initWithFrame:CGRectMake(10,mainContainerY +20 ,100 , 25)];
//    tooltipMonth = [[UILabel alloc] initWithFrame:CGRectMake(120,mainContainerY +20 ,200 , 25)];
//    
//    mainContainerY +=310;
//    }
    
    _mainViewHeightConstraints.constant = mainContainerY;
    
}






@end
