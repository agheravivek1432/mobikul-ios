//
//  AdvancedSearchResults.m
//  Mobikul
//
//  Created by Ratnesh on 30/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AdvancedSearchResults.h"
#import "CollectionViewCell.h"
#import "GlobalData.h"
#import "CatalogProduct.h"
#import "ToastView.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
GlobalData *globalObjectAdvanceSearchResult;
@implementation AdvancedSearchResults

- (void)viewDidLoad {
    [super viewDidLoad];
    whichApiDataToprocess = @"";
    isAlertVisible = 0;
    globalObjectAdvanceSearchResult =[ [GlobalData alloc] init];
    globalObjectAdvanceSearchResult.delegate = self;
    [globalObjectAdvanceSearchResult language];
    self.navigationItem.title = [globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"advancedSearchResult" value:@"" table:nil];
    sortSignal = 0;
    _sortByView.hidden = YES;
    pageNumber = 1;
    reloadPageData = @"false";
    sortItem = @"relevance";
    sortDir = @"0";
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newProductShowOptions:)];
    [_productCollectionView addGestureRecognizer:tap];
    sortDirection = [[NSMutableArray alloc] init];
    
    UITapGestureRecognizer *sortByTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sortByTapped:)];
    sortByTap.numberOfTapsRequired = 1;
    [_sortByView addGestureRecognizer:sortByTap];
    
    finalData = [[NSMutableString alloc] initWithString:@""];
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    preferences = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    _emptyView.layer.cornerRadius = 2;
    _emptyView.layer.shadowOffset = CGSizeMake(0, 0);
    _emptyView.layer.shadowRadius = 3;
    _emptyView.layer.shadowOpacity = 0.5;
    _emptyLabel.text = [globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"nosearchresult" value:@"" table:nil];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    loadPageRequestFlag = 1;
    if(savedSessionId == nil){
        [self loginRequest];
        
    }
    else{
        [self callingHttppApi];
    }
}



-(void) callingHttppApi{
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToprocess isEqual:@"addToWishlist"]){
        [GlobalData alertController:currentWindow msg:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        //if(customerId != nil)
        [post appendFormat:@"customerId=%@&",customerId];
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectAdvanceSearchResult callHTTPPostMethod:post api:@"mobikulhttp/catalog/addtoWishlist" signal:@"HttpPostMetod"];
    }
    else if([whichApiDataToprocess isEqual: @"addToCart"]){
        [GlobalData alertController:currentWindow msg:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", customerId];
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId != nil)
            [post appendFormat:@"quoteId=%@&", quoteId];
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectAdvanceSearchResult callHTTPPostMethod:post api:@"mobikulhttp/checkout/addtoCart" signal:@"HttpPostMetod"];
        globalObjectAdvanceSearchResult.delegate = self;
        
    }
    else{
        if(pageNumber == 1)
            [GlobalData alertController:currentWindow msg:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        else{
            [GlobalData loadingController:currentWindow];
        }
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@&", screenWidth];
        NSArray *sortData = @[sortItem, sortDir];
        NSError *error = nil;
        NSData *jsonSortData = [NSJSONSerialization dataWithJSONObject:sortData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonSortString = [[NSString alloc] initWithData:jsonSortData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"sortData=%@&",jsonSortString];
        NSData *queryStringJsonData = [NSJSONSerialization dataWithJSONObject:_queryString options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonQueryString = [[NSString alloc] initWithData:queryStringJsonData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"queryString=%@&",jsonQueryString];
        [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
        [globalObjectAdvanceSearchResult callHTTPPostMethod:post api:@"mobikulhttp/catalog/getadvancedsearchResult" signal:@"HttpPostMetod"];
        globalObjectAdvanceSearchResult.delegate = self;
    }
}
#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}


-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self callingHttppApi];
}


-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}
-(void)loginRequestCall
{
    [self loginRequest];
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    
    [globalObjectAdvanceSearchResult callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    _sortByView.hidden = NO;
    if([whichApiDataToprocess isEqual: @"addToWishlist"]){
        addToWishListCollection = collection;
        if([addToWishListCollection[@"status"] boolValue])
            [ToastView showToastInParentView:self.view withText:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"productAdded" value:@"" table:nil] withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"somethingWrong" value:@"" table:nil] withStatus:@"error" withDuaration:5.0];
    }
    else
        if([whichApiDataToprocess isEqual: @"addToCart"]){
            addToCartCollection = collection;
            if([addToCartCollection objectForKey:@"quoteId"]){
                [preferences setObject:addToCartCollection[@"quoteId"] forKey:@"quoteId"];
                [preferences synchronize];
            }
            if([addToCartCollection[@"error"] boolValue])
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"error" withDuaration:5.0];
            else
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"success" withDuaration:5.0];
            UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
            [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
        }
        else if([reloadPageData isEqualToString:@"true"]){
            [[currentWindow viewWithTag:313131] removeFromSuperview];
            loadPageRequestFlag = 1;
            NSArray *newData = collection[@"productCollection"];
            arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
            mainCollection = collection;
            for(int i=0; i<[arrayMainCollection count]; i++)
                productCurtainOpenSignal[i] = @"0";
            if(sortSignal == 0) {
                for(int i=0; i<[collection[@"sortingData"] count]; i++) {
                    if(i == 0)
                        [sortDirection addObject:@"1"];
                    else
                        [sortDirection addObject:@"0"];
                }
                sortSignal++;
            }
            [_productCollectionView reloadData];
            [self scrollAutomatically];
            
            
        }
        else{
            NSArray *newData = collection[@"productCollection"];
            arrayMainCollection = newData;
            totalCount = [collection[@"totalCount"] integerValue];
            mainCollection = collection;
            productCurtainOpenSignal = [[NSMutableArray alloc] initWithCapacity:[mainCollection[@"productCollection"] count]];
            for (int i=0; i<[mainCollection[@"productCollection"] count]; i++)
                productCurtainOpenSignal[i] = @"0";
            if(sortSignal == 0) {
                for(int i=0; i<[mainCollection[@"sortingData"] count]; i++) {
                    if(i == 0)
                        [sortDirection addObject:@"1"];
                    else
                        [sortDirection addObject:@"0"];
                }
                sortSignal++;
            }
            [_productCollectionView reloadData];
        }
}
-(void)scrollAutomatically{
    
    float width = CGRectGetWidth(_productCollectionView.frame);
    float height = CGRectGetHeight(_productCollectionView.frame);
    float newPosition = contentHeight;
    
    CGRect toVisible = CGRectMake(newPosition, 0, width, height);
    [_productCollectionView scrollRectToVisible:toVisible animated:YES];
}



- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if([arrayMainCollection count] > 0){
        _emptyView.hidden = YES;
        _sortByView.backgroundColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    }
    else{
        _emptyView.hidden = NO;
        _sortByView.backgroundColor = [UIColor whiteColor];
        
    }
    return [arrayMainCollection count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if([arrayMainCollection count] == totalCount)
        return CGSizeMake(0,0);
    else
        return CGSizeMake(0,45);
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // NSDictionary *productCollectionDictionary = [mainCollection[@"productCollection"] objectAtIndex:[indexPath row]];
    NSDictionary *productCollectionDictionary = [arrayMainCollection objectAtIndex:[indexPath row]];
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.layer.cornerRadius = 3.0f;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    cell.layer.shadowRadius = 3.0f;
    cell.layer.shadowOpacity = 0.5;
    cell.layer.masksToBounds = NO;
    cell.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds cornerRadius:cell.contentView.layer.cornerRadius].CGPath;
    
    for(UIView *lbl in cell.contentView.subviews)
        [lbl removeFromSuperview];
    float x = (((SCREEN_WIDTH/2)-10) - SCREEN_WIDTH/2.5)/2;
    float y = 10;
    UIView *productImageBlock = [[UIView alloc]initWithFrame:CGRectMake(x, y, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
    productImageBlock.clipsToBounds = YES;
    
    UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
    productImage.userInteractionEnabled = YES;
    productImage.tag = [indexPath row];
    [productImageBlock addSubview:productImage];
    productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
    UIImage *image = [imageCache objectForKey:productCollectionDictionary[@"thumbNail"]];
    if (image)
        productImage.image = image;
    else{
        [_queue addOperationWithBlock:^{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: productCollectionDictionary[@"thumbNail"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            if(image){
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    productImage.image = image;
                }];
                [imageCache setObject:image forKey:productCollectionDictionary[@"thumbNail"]];
            }
        }];
    }
    UITapGestureRecognizer *openProductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewProduct:)];
    openProductGesture.numberOfTapsRequired = 1;
    [productImage addGestureRecognizer:openProductGesture];
    
    
    UIView *productImageCurtain;
    if([productCurtainOpenSignal[[indexPath row]] isEqual: @"1"])
        productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
    else
        productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
    [productImageCurtain setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [productImageBlock addSubview:productImageCurtain];
    
    
    float subWish = 64;
    float subAdd = 32;
    
    if(SCREEN_WIDTH < 500){
        subWish = 54;
        subAdd = 22;
    }
    
    UIImageView *wishlistButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)-subWish, (SCREEN_WIDTH/2.5)-40, 32, 32)];
    [wishlistButton setImage:[UIImage imageNamed:@"ic_addtowishlist.png"]];
    wishlistButton.tag = [indexPath row];
    wishlistButton.userInteractionEnabled = YES;
    UITapGestureRecognizer *addtowishlistGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addtoWishlist:)];
    addtowishlistGesture.numberOfTapsRequired = 1;
    [wishlistButton addGestureRecognizer:addtowishlistGesture];
    [productImageCurtain addSubview:wishlistButton];
    if([productCollectionDictionary[@"hasOptions"] isEqual: @"1"] || [productCollectionDictionary[@"typeId"] isEqual: @"configurable"] || [productCollectionDictionary[@"typeId"] isEqual: @"bundle"] || [productCollectionDictionary[@"typeId"] isEqual: @"grouped"]){
        UIImageView *viewproductButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
        [viewproductButton setImage:[UIImage imageNamed:@"ic_viewproduct.png"]];
        viewproductButton.tag = [indexPath row];
        viewproductButton.userInteractionEnabled = YES;
        UITapGestureRecognizer *viewproductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewProduct:)];
        viewproductGesture.numberOfTapsRequired = 1;
        [viewproductButton addGestureRecognizer:viewproductGesture];
        [productImageCurtain addSubview:viewproductButton];
    }
    else{
        UIImageView *addtocartButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
        [addtocartButton setImage:[UIImage imageNamed:@"ic_addtocart.png"]];
        addtocartButton.tag = [indexPath row];
        addtocartButton.userInteractionEnabled = YES;
        UITapGestureRecognizer *addtocartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addtoCart:)];
        addtocartGesture.numberOfTapsRequired = 1;
        [addtocartButton addGestureRecognizer:addtocartGesture];
        [productImageCurtain addSubview:addtocartButton];
    }
    [cell.contentView addSubview:productImageBlock];
    
    y += SCREEN_WIDTH/2.5+5;
    UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(10, y, (SCREEN_WIDTH/2)-50, 25)];
    [productName setTextColor:[GlobalData colorWithHexString:@"555555"]];
    productName.tag = [indexPath row];
    [productName setBackgroundColor:[UIColor clearColor]];
    [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [productName setText:productCollectionDictionary[@"name"]];
    [cell.contentView addSubview:productName];
    
    int t = y;
    if(SCREEN_WIDTH >500){
        t = y+60;
    }
    
    UIImageView *productOption = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2)-42, t, 32, 32)];
    [productOption setImage:[UIImage imageNamed:@"ic_pro_option.png"]];
    productOption.userInteractionEnabled = YES;
    productOption.tag = [indexPath row];
    [cell.contentView addSubview:productOption];
    
    if(SCREEN_WIDTH >500){
        y += 30;
        if([productCollectionDictionary[@"typeId"] isEqual: @"grouped"]){
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 90, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [preString setText:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"starting" value:@"" table:nil]];
            [cell.contentView addSubview:preString];
            
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(95, y, 70, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [price setText:productCollectionDictionary[@"groupedPrice"]];
            [cell.contentView addSubview:price];
        }
        else
            if([productCollectionDictionary[@"typeId"] isEqual: @"bundle"]){
                UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 50, 25)];
                [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [from setBackgroundColor:[UIColor clearColor]];
                [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [from setText:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"from" value:@"" table:nil]];
                [cell.contentView addSubview:from];
                
                UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(55, y, 60, 25)];
                [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [minPrice setBackgroundColor:[UIColor clearColor]];
                [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [minPrice setText:productCollectionDictionary[@"formatedMinPrice"]];
                [cell.contentView addSubview:minPrice];
                
                UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(125, y, 25, 25)];
                [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [to setBackgroundColor:[UIColor clearColor]];
                [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [to setText:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"to" value:@"" table:nil]];
                // [cell.contentView addSubview:to];
                
                UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(147, y, 60, 25)];
                [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [maxPrice setBackgroundColor:[UIColor clearColor]];
                [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [maxPrice setText:productCollectionDictionary[@"formatedMaxPrice"]];
                
                float widthX = cell.frame.size.width - 125;
                
                if(widthX > 90){
                    [cell.contentView addSubview:to];
                    [cell.contentView addSubview:maxPrice];
                }else{
                    UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(125, y, widthX, 25)];
                    [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [dot setBackgroundColor:[UIColor clearColor]];
                    [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [dot setText:[[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"to" value:@"" table:nil] stringByAppendingString:@" ..."]];
                    [cell.contentView addSubview:dot];
                }
                // [cell.contentView addSubview:maxPrice];
            }
            else{
                if(![productCollectionDictionary[@"specialPrice"] isEqual:[NSNull null]] && [productCollectionDictionary[@"isInRange"] boolValue]){
                    UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 70, 25)];
                    [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [regularprice setBackgroundColor:[UIColor clearColor]];
                    [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:productCollectionDictionary[@"formatedPrice"]];
                    [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                    [regularprice setAttributedText:attributeString];
                    [cell.contentView addSubview:regularprice];
                    
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 85, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [price setText:productCollectionDictionary[@"formatedSpecialPrice"]];
                    [cell.contentView addSubview:price];
                }
                else{
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 60, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [price setText:productCollectionDictionary[@"formatedPrice"]];
                    [cell.contentView addSubview:price];
                }
            }
        if([productCollectionDictionary[@"hasTierPrice"] isEqual:@"true"]){
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 90, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"cf5050"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [preString setText:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil]];
            //[cell.contentView addSubview:preString];
            
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(165, y, 60, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [price setText:productCollectionDictionary[@"tierPrice"]];
            
            float widthX = cell.frame.size.width - 80;
            
            if(widthX > 150){
                [cell.contentView addSubview:preString];
                [cell.contentView addSubview:price];
            }
            else{
                UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(80, y, widthX, 25)];
                [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [dot setBackgroundColor:[UIColor clearColor]];
                [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [dot setText:[[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil] stringByAppendingString:@" ..."]];
                [cell.contentView addSubview:dot];
            }
            
            //[cell.contentView addSubview:price];
        }
    }
    y += 30;
    UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(10,y,120,24)];
    UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
    [ratingContainer addSubview:grayContainer];
    UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
    gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI1];
    UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
    gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI2];
    UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
    gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI3];
    UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
    gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI4];
    UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
    gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI5];
    
    double percent = 24 * [productCollectionDictionary[@"rating"] doubleValue];
    UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
    blueContainer.clipsToBounds = YES;
    [ratingContainer addSubview:blueContainer];
    UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
    bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI1];
    UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
    bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI2];
    UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
    bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI3];
    UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
    bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI4];
    UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
    bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI5];
    [cell.contentView addSubview:ratingContainer];
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float productBlockHeight;
    if(SCREEN_WIDTH > 500){
        productBlockHeight = (SCREEN_WIDTH/2.5)+117;
    }
    else{
        productBlockHeight = (SCREEN_WIDTH/2.5)+75;
    }
    return CGSizeMake((SCREEN_WIDTH/2)-10, productBlockHeight);
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@" %ld",[self.categoryProductCollectionView numberOfItemsInSection:0]-1);
    NSInteger currentCellCount = [self.productCollectionView numberOfItemsInSection:0];
    //NSLog(@"current cell count %ld   %ld",currentCellCount,totalCount);
    for (UICollectionViewCell *cell in [self.productCollectionView visibleCells]) {
        indexPathValue = [self.productCollectionView indexPathForCell:cell];
        if([indexPathValue row] == [self.productCollectionView numberOfItemsInSection:0]-1){
            if(totalCount > currentCellCount && loadPageRequestFlag){
                whichApiDataToprocess = @"";
                reloadPageData = @"true";
                loadPageRequestFlag = 0;
                pageNumber +=1;
                contentHeight = _productCollectionView.contentOffset.y;
                NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                if(savedSessionId == nil)
                    [self loginRequest];
                else
                    [self callingHttppApi];
            }
        }
    }
}


-(void)applySort:(UITapGestureRecognizer *)recognizer{
    int viewIndex = (int)(2*recognizer.view.tag)+2;
    int index = (int)recognizer.view.tag;
    UIView *currentView = [recognizer.view.superview.subviews objectAtIndex:viewIndex];
    UIImageView *dir = [currentView.subviews objectAtIndex:1];
    if([[sortDirection objectAtIndex:index] isEqual: @"0"]){
        [sortDirection replaceObjectAtIndex:index withObject:@"1"];
        dir.image = [UIImage imageNamed:@"ic-down.png"];
        sortDir = @"1";
    }
    else{
        [sortDirection replaceObjectAtIndex:index withObject:@"0"];
        dir.image = [UIImage imageNamed:@"ic-up.png"];
        sortDir = @"0";
    }
    NSDictionary *sortingDict = [mainCollection[@"sortingData"] objectAtIndex:recognizer.view.tag];
    sortItem = sortingDict[@"code"];
    UIVisualEffectView *toRemove = (UIVisualEffectView *)[self.view viewWithTag:999];
    [toRemove removeFromSuperview];
    finalData = [[NSMutableString alloc] initWithString: @""];
    NSError *error = nil;
    NSData *objectData = [finalData dataUsingEncoding:NSUTF8StringEncoding];
    mainCollection = [NSJSONSerialization JSONObjectWithData:objectData options:0 error:&error];
    [_productCollectionView reloadData];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    pageNumber = 1;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)sortByTapped:(UITapGestureRecognizer *)recognizer{
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 999;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UIView *sortBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
    [sortBlock setBackgroundColor : [UIColor whiteColor]];
    sortBlock.layer.cornerRadius = 4;
    
    float y = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 32)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [sortTitle setText : @"Sort By"];
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [sortBlock addSubview : sortTitle];
    
    y += 42;
    UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 1)];
    [hr setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [sortBlock addSubview : hr];
    
    for(int i=0; i<[mainCollection[@"sortingData"] count]; i++) {
        NSDictionary *sortingDict = [mainCollection[@"sortingData"] objectAtIndex:i];
        y += 6;
        UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 32)];
        [containerView setBackgroundColor:[UIColor whiteColor]];
        containerView.tag = i;
        containerView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(applySort:)];
        tap.numberOfTapsRequired = 1;
        [containerView addGestureRecognizer:tap];
        
        UILabel *sortTerm = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, (SCREEN_WIDTH/2.5)-47, 32)];
        [sortTerm setTextColor:[GlobalData colorWithHexString : @"555555"]];
        [sortTerm setBackgroundColor:[UIColor clearColor]];
        [sortTerm setFont:[UIFont fontWithName:@"Trebuchet MS" size:22.0f]];
        [sortTerm setText: sortingDict[@"label"]];
        [containerView addSubview:sortTerm];
        
        UIImageView *directionIcon = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)-47, 0, 32,32)];
        if([[sortDirection objectAtIndex:i] isEqual: @"0"]){
            directionIcon.image = [UIImage imageNamed:@"ic-down.png"];
            sortDir = @"1";
        }
        else{
            directionIcon.image = [UIImage imageNamed:@"ic-up.png"];
            sortDir = @"0";
        }
        [containerView addSubview:directionIcon];
        [sortBlock addSubview:containerView];
        
        y += 37;
        if(i < [mainCollection[@"sortingData"] count]-1){
            UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 1)];
            [hr setBackgroundColor:[GlobalData colorWithHexString : @"555555"]];
            [sortBlock addSubview : hr];
        }
    }
    CGRect newFrame = sortBlock.frame;
    newFrame.size.height = y+5;
    sortBlock.frame = newFrame;
    sortBlock.center = blurEffectView.center;
    
    UIImageView *closeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-52, 82, 32,32)];
    closeIcon.image = [UIImage imageNamed:@"ic_close.png"];
    closeIcon.userInteractionEnabled = YES;
    [blurEffectView addSubview:closeIcon];
    UITapGestureRecognizer *tapClose = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeSortView:)];
    tapClose.numberOfTapsRequired = 1;
    [closeIcon addGestureRecognizer:tapClose];
    [blurEffectView addSubview:sortBlock];
    [self.view addSubview:blurEffectView];
}

-(void)closeSortView:(UITapGestureRecognizer *)recognizer{
    [recognizer.view.superview removeFromSuperview];
}

-(void)newProductShowOptions:(UITapGestureRecognizer *)recognizer{
    CGPoint tapLocation = [recognizer locationInView:_productCollectionView];
    NSIndexPath *indexPath = [_productCollectionView indexPathForItemAtPoint:tapLocation];
    CollectionViewCell *cell = (CollectionViewCell *)[_productCollectionView cellForItemAtIndexPath:indexPath];
    UIImageView *productOption = [cell.contentView.subviews objectAtIndex:2];
    UIView *containerView = [cell.contentView.subviews objectAtIndex:0];
    UIView *curtainView = [containerView.subviews objectAtIndex:1];
    CGRect productOptionCordinate = [_productCollectionView convertRect:productOption.frame fromView:cell];
    if(CGRectContainsPoint(productOptionCordinate, tapLocation)){
        if([productCurtainOpenSignal[productOption.tag] isEqual: @"0"]) {
            [UIView animateWithDuration:0.5 delay:0.1 options: UIViewAnimationOptionCurveEaseOut animations:^{
                curtainView.frame = CGRectMake(0, 0, curtainView.frame.size.width, curtainView.frame.size.height);
            }
                             completion:^(BOOL finished){
                                 if (finished)
                                     productCurtainOpenSignal[productOption.tag] = @"1";
                             }];
        }
        else{
            [UIView animateWithDuration:0.5 delay:0.1 options: UIViewAnimationOptionCurveEaseOut animations:^{
                curtainView.frame = CGRectMake(0, curtainView.frame.size.height, curtainView.frame.size.width, curtainView.frame.size.height);
            }
                             completion:^(BOOL finished){
                                 if (finished)
                                     productCurtainOpenSignal[productOption.tag] = @"0";
                             }];
        }
    }
}

-(void)addtoWishlist:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId == nil){
        UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"logInWishList" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          [self performSegueWithIdentifier:@"addtoWishlistLoginSegue" sender:self];
                                                      }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearchResult.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [AC addAction:okBtn];
        [AC addAction:noBtn];
        [self.parentViewController presentViewController:AC animated:YES completion:nil];
    }
    else{
        NSDictionary *tempDictionary = [[NSDictionary alloc] init];
        //tempDictionary = [mainCollection[@"productCollection"] objectAtIndex:recognizer.view.tag];
        tempDictionary = [arrayMainCollection objectAtIndex:recognizer.view.tag];
        whichApiDataToprocess = @"addToWishlist";
        productIdForApiCall = tempDictionary[@"entityId"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)viewProduct:(UITapGestureRecognizer *)recognizer{
    productDictionary = [[NSDictionary alloc] init];
    productDictionary = [arrayMainCollection objectAtIndex:recognizer.view.tag];
    [self performSegueWithIdentifier:@"openProductFromAdvancedSearchPageSegue" sender:self];
}

-(void)addtoCart:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary *tempDictionary = [[NSDictionary alloc] init];
    //tempDictionary = [mainCollection[@"productCollection"] objectAtIndex:recognizer.view.tag];
    tempDictionary = [arrayMainCollection objectAtIndex:recognizer.view.tag];
    whichApiDataToprocess = @"addToCart";
    productIdForApiCall = tempDictionary[@"entityId"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openProductFromAdvancedSearchPageSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = productDictionary[@"entityId"];
        destViewController.productName = productDictionary[@"name"];
        destViewController.productType = productDictionary[@"typeId"];
    }
}


@end
