//
//  groupProducts.h
//  MobikulMp
//
//  Created by kunal prasad on 20/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerProducts : UIViewController
{
    NSInteger isAlertVisible ,sortSignal;
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *dataFromApi ;
    UIAlertController *alert;
    NSCache *imageCache;
    NSMutableArray *productCurtainOpenSignal, *sortDirection;
    id mainCollection , addToCartCollection,addToWishListCollection,collection;
    NSString *sortItem, *sortDir,*name,*entityId ,*typeId , *whichApiDataToprocess,*productIdForApiCall,*reloadPageData;
    NSMutableString *finalData;
    UIWindow *currentWindow;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSIndexPath *indexPathValue;
    NSArray *arrayMainCollection;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
}
@property (nonatomic) NSString *profileUrl;
@property (weak, nonatomic) IBOutlet UIView *sortbyView;
@property (weak, nonatomic) IBOutlet UICollectionView *categoryProductCollectionView;
@property (nonatomic, strong) NSOperationQueue *queue;

@end
