//
//  BlogAddCategoryViewController.h
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogAddCategoryViewController : UIViewController <NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate>

{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *firstName, *lastName, *emailAddress, *currentPassword, *password, *confirmPassword, *wantChangePassword, *whichApiDataToProcess;
    NSInteger isAlertVisible;
    UIAlertController *alert;
    id collection;
    UIWindow *currentWindow;
    
    IBOutlet UITableView *tblView;
    
    NSArray *arrCategoryList;
    
    NSString *strAddCategoryText;
}

@end
