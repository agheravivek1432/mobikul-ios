//
//  ToastView.h
//  Mobikul
//
//  Created by Ratnesh on 20/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToastView : UIView

@property (strong, nonatomic) NSString *text;

+ (void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withStatus:(NSString *)status withDuaration:(float)duration;

@end