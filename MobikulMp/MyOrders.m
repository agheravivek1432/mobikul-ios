//
//  MyOrders.m
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "MyOrders.h"
#import "GlobalData.h"
#import "CustomerOrderDetails.h"

GlobalData *globalObjectMyOrder;

@implementation MyOrders

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectMyOrder=[[GlobalData alloc] init];
    globalObjectMyOrder.delegate = self;
    [globalObjectMyOrder language];
    arrayMainCollection = [[NSArray alloc] init];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    self.navigationItem.title = [globalObjectMyOrder.languageBundle localizedStringForKey:@"myOrders" value:@"" table:nil];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    _myOrderScrollView.delegate = self;
    pageNumber = 1;
    loadPageRequestFlag = 1;
    contentHeight = 0;
    reloadPageData = @"false";
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger currentCellCount = [arrayMainCollection count];
    // NSLog(@" called   %ld   %ld",totalCount,currentCellCount);
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    // NSLog(@" called   %f   %f",bottomEdge,scrollView.contentSize.height);
    if (bottomEdge >= scrollView.contentSize.height-50) {
        // NSLog(@" called bottom   %f    %f",bottomEdge,scrollView.contentSize.height);
        if( (totalCount > currentCellCount) && loadPageRequestFlag ){
            reloadPageData = @"true";
            pageNumber +=1;
            loadPageRequestFlag = 0;
            contentHeight = _myOrderScrollView.contentOffset.y;
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else{
                [self callingHttppApi];
            }
        }
    }
    
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    NSInteger currentCellCount = [arrayMainCollection count];
//   // NSLog(@" called   %ld   %ld",totalCount,currentCellCount);
//    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
//    NSLog(@" called   %f   %f",bottomEdge,scrollView.contentSize.height);
//    if (bottomEdge >= scrollView.contentSize.height-70) {
//        NSLog(@" called bottom   %f    %f",bottomEdge,scrollView.contentSize.height);
//        if( (totalCount > currentCellCount) && loadPageRequestFlag ){
//            reloadPageData = @"true";
//            pageNumber +=1;
//            loadPageRequestFlag = 0;
//            contentHeight = _myOrderScrollView.contentOffset.y;
//            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
//            if(savedSessionId == nil)
//                [self loginRequest];
//            else{
//                [self callingHttppApi];
//            }
//        }
//    }
//}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectMyOrder callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMyOrder.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectMyOrder.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectMyOrder.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMyOrder.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
-(void) callingHttppApi{
    if(pageNumber == 1)
        [GlobalData alertController:currentWindow msg:[globalObjectMyOrder.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    else{
        [GlobalData loadingController:currentWindow];
        [self scrollAutomatically];
    }
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    [post appendFormat:@"websiteId=%@&",websiteId];
    NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
    [post appendFormat:@"customerEmail=%@&",prefCustomerEmail];
    [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
    [globalObjectMyOrder callHTTPPostMethod:post api:@"mobikulhttp/customer/getallOrders" signal:@"HttpPostMetod"];
    globalObjectMyOrder.delegate = self;
}
-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    loadPageRequestFlag = 1;
    NSArray *newData = collection[@"allOrders"];
    arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
    
    totalCount = [collection[@"totalCount"] integerValue];
    for(int i=0; i<[arrayMainCollection count]; i++){
        NSDictionary *dict = [arrayMainCollection objectAtIndex:i];
        paintView =[[UIView alloc]initWithFrame:CGRectMake(2, (i*130)+(2*(i+1)), (_allOrderContainer.frame.size.width)-4, 130)];
        [paintView setBackgroundColor:[UIColor whiteColor]];
        paintView.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        paintView.tag = i;
        
        paintView.layer.borderWidth = 2.0f;
        [_allOrderContainer addSubview:paintView];
        _allOrderContainer.tag = i;
        UILabel *orderId = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (_allOrderContainer.frame.size.width)-4, 20)];
        [orderId setTextColor:[UIColor blackColor]];
        [orderId setBackgroundColor:[UIColor clearColor]];
        [orderId setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *orderIdText = [NSString stringWithFormat:@"Order ID : %@",dict[@"order_id"]];
        [orderId setText:orderIdText];
        [paintView addSubview:orderId];
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(5, 30, (_allOrderContainer.frame.size.width)-4, 20)];
        [date setTextColor:[UIColor blackColor]];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *dateText = [NSString stringWithFormat:@"Date : %@",dict[@"date"]];
        [date setText:dateText];
        [paintView addSubview:date];
        
        UILabel *shipTo = [[UILabel alloc] initWithFrame:CGRectMake(5, 55, (_allOrderContainer.frame.size.width)-4, 20)];
        [shipTo setTextColor:[UIColor blackColor]];
        [shipTo setBackgroundColor:[UIColor clearColor]];
        [shipTo setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *shipToText = [NSString stringWithFormat:@"Ship To : %@",dict[@"ship_to"]];
        [shipTo setText:shipToText];
        [paintView addSubview:shipTo];
        
        UILabel *orderTotal = [[UILabel alloc] initWithFrame:CGRectMake(5, 80, (_allOrderContainer.frame.size.width)-4, 20)];
        [orderTotal setTextColor:[UIColor blackColor]];
        [orderTotal setBackgroundColor:[UIColor clearColor]];
        [orderTotal setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *orderTotalText = [NSString stringWithFormat:@"Order Total : %@",dict[@"order_total"]];
        [orderTotal setText:orderTotalText];
        [paintView addSubview:orderTotal];
        
        UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(5, 105, (_allOrderContainer.frame.size.width)-4, 20)];
        [status setTextColor:[UIColor blackColor]];
        [status setBackgroundColor:[UIColor clearColor]];
        [status setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *statusText = [NSString stringWithFormat:@"Status : %@",dict[@"status"]];
        [status setText:statusText];
        [paintView addSubview:status];
        
        NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforViewAllButtonText = [@"View Order" sizeWithAttributes:reqAttributesforViewAllButtonText];
        CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
        UIButton *buttonViewAllDesc = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buttonViewAllDesc.tag = i;
        [buttonViewAllDesc addTarget:self action:@selector(buttonHandlerForViewAllOrderDetails :) forControlEvents:UIControlEventTouchUpInside];
        [buttonViewAllDesc setTitle:@"View Order" forState:UIControlStateNormal];
        buttonViewAllDesc.frame = CGRectMake(paintView.frame.size.width-reqStringWidthViewAllButtonText-30,75 ,reqStringWidthViewAllButtonText + 20, 50.0);
        [buttonViewAllDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [buttonViewAllDesc setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
        [buttonViewAllDesc setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [paintView addSubview:buttonViewAllDesc ];
        
        
    }
    if([arrayMainCollection count] == totalCount)
        _heightConstraint.constant = (132*[arrayMainCollection count]) + 68;
    else{
        _heightConstraint.constant = (132*[arrayMainCollection count]) + 68+50;
    }// to give space for display a loader alert
    [_allOrderContainer layoutIfNeeded];
    //    [self scrollAutomatically];
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}
-(void)scrollAutomatically{
    //    float width = CGRectGetWidth(_myOrderScrollView.frame);
    //    float height = CGRectGetHeight(_myOrderScrollView.frame);
    //    float newPosition = contentHeight-50;
    ////    NSLog(@" %f",newPosition);
    //  //  CGRect toVisible = CGRectMake(0, newPosition, width, height+50000);
    //
    //    _myOrderScrollView.contentSize = CGSizeMake(width, newPosition);
    //    // [_myOrderScrollView scrollRectToVisible:toVisible animated:YES];
}


-(void)buttonHandlerForViewAllOrderDetails:(UIButton*)button{
    NSDictionary *dict = [arrayMainCollection objectAtIndex:button.tag];
    incrementId = dict[@"order_id"];
    [self performSegueWithIdentifier:@"myOrdersToOrderDetailsSegue" sender:self];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"myOrdersToOrderDetailsSegue"]) {
        CustomerOrderDetails *destViewController = segue.destinationViewController;
        destViewController.incrementId = incrementId;
    }
}

@end