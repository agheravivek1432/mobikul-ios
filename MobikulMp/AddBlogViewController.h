//
//  AddBlogViewController.h
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBlogViewController : UIViewController <NSXMLParserDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *firstName, *lastName, *emailAddress, *currentPassword, *password, *confirmPassword, *wantChangePassword, *whichApiDataToProcess;
    NSInteger isAlertVisible;
    UIAlertController *alert;
    id collection;
    UIWindow *currentWindow;
    
    UIPickerView *pickerCategory;
    NSArray *arrCategoryList;
    NSInteger selectedRow;
    NSString *strCategoryID;
    
    IBOutlet UIScrollView *scrolView;
    IBOutlet UIView *viewScrol;
    IBOutlet UILabel *lblPostDetail;
    IBOutlet UIView *viewAddBlog;
    IBOutlet UILabel *lblCategory;
    IBOutlet UIButton *btnCategory;
    IBOutlet UILabel *lblTitle;
    IBOutlet UITextField *txtFTitle;
    IBOutlet UILabel *lblMetaKeyword;
    IBOutlet UITextView *txtVMetaKeyword;
    IBOutlet UILabel *lblMetaDescription;
    IBOutlet UITextView *txtVMetaDescription;
    IBOutlet UILabel *lblContent;
    IBOutlet UITextView *txtVContent;
    IBOutlet UILabel *lblTag;
    IBOutlet UITextField *txtFTag;
    IBOutlet UIButton *btnSubmitBlog;
}

-(IBAction)btnCategoryPressed:(id)sender;
- (IBAction)btnSubmitBlogPressed:(id)sender;


@end
