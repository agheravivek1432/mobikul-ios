//
//  ViewController.h
//  Mobikul
//
//  Created by Ratnesh on 02/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerLogin : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *enteredEmail, *enteredPassword;
    UIAlertController *alert;
    id collection;
    NSString *whichApiToCall, *userEmail;
    NSInteger isAlertVisible;
    NSUserDefaults *languageCode;
    UIWindow *currentWindow ;
    NSInteger flag;

}
@property (weak, nonatomic) IBOutlet UILabel *requiredField;
@property (weak, nonatomic) IBOutlet UILabel *emailAddress;
@property (weak, nonatomic) IBOutlet UILabel *password;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet UIButton *createAccount;
@property (weak, nonatomic) IBOutlet UILabel *alreadyRegistered;
- (IBAction)forgotPasswordButton:(id)sender;
- (IBAction)submitButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)openCreateAccount:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *subHeading;
@property (nonatomic) NSString *catalogProductFlag;

@end

