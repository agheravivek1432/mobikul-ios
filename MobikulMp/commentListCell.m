//
//  commentListCell.m
//  MobikulMp
//
//  Created by Apple on 19/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "commentListCell.h"

@implementation commentListCell

@synthesize dictComment;

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reloadCommentListCell
{
    imgViewUser.layer.cornerRadius = imgViewUser.frame.size.width/2;
    imgViewUser.clipsToBounds = YES;
    imgViewUser.backgroundColor = [UIColor redColor];
    
    lblUserName.text = [dictComment valueForKey:@"cname"];
    
    NSString * htmlString = [dictComment valueForKey:@"comment"];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    lblComment.attributedText = attrStr;
}

@end
