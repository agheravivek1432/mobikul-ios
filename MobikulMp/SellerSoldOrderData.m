//
//  SellerOrderDetails.m
//  MobikulMp
//
//  Created by kunal prasad on 25/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "SellerSoldOrderData.h"
#import "GlobalData.h"
#import "SellerOrderDetails.h"
#import "CatalogProduct.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)



@interface SellerSoldOrderData ()
@end

GlobalData *globalObjectSoldOrderDetails;


@implementation SellerSoldOrderData

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectSoldOrderDetails = [[GlobalData alloc] init];
    globalObjectSoldOrderDetails.delegate = self;
    isAlertVisible = 0;
    arrayMainCollection = [[NSArray alloc] init];
    _sellerSoldOrderDetailsScrollView.delegate = self;
    [globalObjectSoldOrderDetails language];
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"cccc, MMM d, hh:mm aa"]; //yyyy-MM-dd"
    preferences = [NSUserDefaults standardUserDefaults];
    orderStatus =  [[NSMutableArray alloc] initWithObjects:@"All", @"Cancelled",@"Closed",@"Complete",@"Suspected Fraud",@"On Hold",@"Payment Review",@"PayPal Canceled Reversal",@"PayPal Reversed",@"Pending",@"Pending Payment",@"Pending PayPal",@"Processing",nil];
    orderStatusSend = [[NSMutableArray alloc] initWithObjects:@"",@"canceled",@"closed",@"complete",@"fraud",@"holded",@"payment_review",@"paypal_canceled_reversal",@"paypal_reversed",@"pending",@"pending_payment",@"pending_paypal",@"processing", nil];
    orderListData =[[NSMutableDictionary alloc] init];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    pageNumber = 1;
    loadPageRequestFlag = 1;
    contentHeight = 0;
    reloadPageData = @"false";
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];

}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectSoldOrderDetails callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectSoldOrderDetails.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectSoldOrderDetails.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectSoldOrderDetails.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectSoldOrderDetails.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void) callingHttppApi{
    if(pageNumber == 1)
        [GlobalData alertController:currentWindow msg:[globalObjectSoldOrderDetails.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    else{
        [GlobalData loadingController:currentWindow];
     //   [self scrollAutomatically];
    }
    preferences = [NSUserDefaults standardUserDefaults];
     NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *customerid = [preferences objectForKey:@"customerId"];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if([whichApiDataToprocess isEqual:@"searchOrders"]){
     
     
     if([orderListData objectForKey:@"orderStatus"]){
          [post appendFormat:@"orderStatus=%@&", [orderListData objectForKey:@"orderStatus"]];
     }
     // NSString *orderid = [orderListData objectForKey:@"orderId"];
     if([orderListData objectForKey:@"orderId"]){
         [post appendFormat:@"orderId=%@&", [orderListData objectForKey:@"orderId"]];
         //[dictionary setObject:[orderListData objectForKey:@"orderId"] forKey:@"orderId"];
     }
     if([orderListData objectForKey:@"setDateFrom"]){
         [post appendFormat:@"fromDate=%@&", [orderListData objectForKey:@"setDateFrom"]];
         //[dictionary setObject:[orderListData objectForKey:@"setDateFrom"] forKey:@"fromDate"];
     }if([orderListData objectForKey:@"setDateTo"]){
         [post appendFormat:@"toDate=%@&", [orderListData objectForKey:@"setDateTo"] ];
         
         //[dictionary setObject:[orderListData objectForKey:@"setDateTo"] forKey:@"toDate"];
     }
     }
    
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    [post appendFormat:@"customerId=%@&", customerid];
    [post appendFormat:@"storeId=%@&", storeId];
    [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
    [globalObjectSoldOrderDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getmysoldordersData" signal:@"HttpPostMetod"];
    globalObjectSoldOrderDetails.delegate = self;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger currentCellCount = [arrayMainCollection count];
   // NSLog(@" called   %ld   %ld",totalCount,currentCellCount);
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height-50) {
        if( (totalCount > currentCellCount) && loadPageRequestFlag ){
            reloadPageData = @"true";
            pageNumber +=1;
            loadPageRequestFlag = 0;
            contentHeight = _sellerSoldOrderDetailsScrollView.contentOffset.y;
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else{
                [self callingHttppApi];
            }
        }}
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    float mainContainerY = 0;
    float internalY = 5;
    float y = 0;
    
    for(UIView *subViews in _mainView.subviews){
        [subViews removeFromSuperview];
    }
    [orderListData removeAllObjects];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    loadPageRequestFlag = 1;
    NSArray *newData = collection[@"orderList"];
    arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
    
    totalCount = [collection[@"totalCount"] integerValue];
    
    UIButton  *filterButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [filterButton addTarget:self action:@selector(buttonHandlerForFilter :) forControlEvents:UIControlEventTouchUpInside];
    [filterButton setTitle:@"FILTER YOUR ORDER'S" forState:UIControlStateNormal];
    filterButton.tag = 1000;
    filterButton.frame = CGRectMake(8,0 ,_mainView.frame.size.width-16, 50.0);
    [filterButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [filterButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [filterButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:filterButton ];
    
    internalY +=55;
    
    if([arrayMainCollection count]>0){
    for(int i=0 ;i<[arrayMainCollection count];i++ ){
        
        NSDictionary *orderList = [arrayMainCollection objectAtIndex:i];
        
        UIView *orderContainer = [[UIView alloc] initWithFrame:CGRectMake(5,internalY,_mainView.frame.size.width-10,100 )];
        orderContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        orderContainer.layer.borderWidth = 2.0f;
        orderContainer.tag = i;
        [_mainView addSubview:orderContainer];
        
        
        UILabel *orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, orderContainer.frame.size.width, 25)];
        [orderLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [orderLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [orderLabel setText:orderList[@"label"]];
        orderLabel.textAlignment = NSTextAlignmentCenter;
        [orderLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [orderContainer addSubview:orderLabel];
        
        y = 35;
        
        for(int j=0; j<[orderList[@"orderList"]count]; j++){
            
            NSDictionary *orderName = [orderList[@"orderList"] objectAtIndex:j];
            
            NSMutableString *productName = orderName[@"name"];
            NSString *qty = [NSString stringWithFormat:@"%d",[orderName[@"qty"]intValue]];
            NSString *modified = [productName stringByAppendingString:@"  X "];
            NSDictionary *reqAttributesforOrderName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeforOrderName = [[modified stringByAppendingString:qty] sizeWithAttributes:reqAttributesforOrderName];
            UILabel *orderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,y, reqStringSizeforOrderName.width, 25)];
            orderLabelName.tag = j;
            [orderLabelName setTextColor:[GlobalData colorWithHexString:@"1e7ec8"]];
            [orderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [orderLabelName setText:[modified stringByAppendingString:qty]];
            orderLabelName.textAlignment = NSTextAlignmentCenter;
            productTitleTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callProductTitleTap:)];
            orderLabelName.userInteractionEnabled = "YES";
            productTitleTap.numberOfTapsRequired = 1;
            [orderLabelName addGestureRecognizer:productTitleTap];

            
            [orderContainer addSubview:orderLabelName];
            y +=30;
        }
        
        y+=10;
        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,y, orderContainer.frame.size.width, 25)];
        [statusLabel setTextColor:[GlobalData colorWithHexString:@"FF0000"]];
        [statusLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        
        NSString *status = orderList[@"status"];
        if([status isEqualToString :@"PENDING"]){
          [statusLabel setTextColor:[GlobalData colorWithHexString:@"f8af2e"]];
        }
        if([status isEqualToString :@"PROCESSING"]){
          [statusLabel setTextColor:[GlobalData colorWithHexString:@"4db6ac"]];
        }
        if([status isEqualToString :@"COMPLETE"]){
          [statusLabel setTextColor:[GlobalData colorWithHexString:@"00FF00"]];
        }
        if([status isEqualToString :@"CLOSED"]){
            [statusLabel setTextColor:[GlobalData colorWithHexString:@"FF0000"]];
        }
        
        
        [statusLabel setText:orderList[@"status"]];
        [orderContainer addSubview:statusLabel];
        
        
        
        NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforViewAllButtonText = [@"View Order" sizeWithAttributes:reqAttributesforViewAllButtonText];
        CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
        UIButton *buttonViewAllDesc = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buttonViewAllDesc.tag = i;
        [buttonViewAllDesc addTarget:self action:@selector(buttonHandlerForViewAllOrderDetails :) forControlEvents:UIControlEventTouchUpInside];
        [buttonViewAllDesc setTitle:@"View Order" forState:UIControlStateNormal];
        buttonViewAllDesc.frame = CGRectMake(orderContainer.frame.size.width-reqStringWidthViewAllButtonText-30,y-5 ,reqStringWidthViewAllButtonText + 20, 40.0);
        [buttonViewAllDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [buttonViewAllDesc setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
        [buttonViewAllDesc setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [orderContainer addSubview:buttonViewAllDesc ];
        
        y+=35;
        
        UILabel *summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,y, orderContainer.frame.size.width, 25)];
        [summaryLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [summaryLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [summaryLabel setText:orderList[@"summary"]];
        [orderContainer addSubview:summaryLabel];
        
        y+=35;
        
        UILabel *orderTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,y, orderContainer.frame.size.width-10, 25)];
        [orderTotalLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [orderTotalLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [orderTotalLabel setText:orderList[@"orderTotal"]];
        orderTotalLabel.textAlignment =   NSTextAlignmentRight;
        [orderContainer addSubview:orderTotalLabel];
        
        y+=35;
        
        CGRect orderConrtainernewFrame = orderContainer.frame;
        orderConrtainernewFrame.size.height = y;
        orderContainer.frame = orderConrtainernewFrame;
        internalY +=y +10;
        
    }
    mainContainerY = internalY;
    }
    else{
        
        mainContainerY = internalY;
        UIView *emptyContainer = [[UIView alloc]initWithFrame:CGRectMake(5, internalY, _mainView.frame.size.width-10, 40)];
        emptyContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
        emptyContainer.layer.borderWidth = 2.0f;
        emptyContainer.backgroundColor =[GlobalData colorWithHexString:@"7F7F7F"];
        [_mainView addSubview:emptyContainer];
        
        UILabel *defaultTItleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0 , _mainView.frame.size.width-10 ,40)];
        [defaultTItleLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [defaultTItleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [defaultTItleLabel setText:@"No Items Found For Requested Value... Please Search with different Value."];
        defaultTItleLabel.textAlignment = NSTextAlignmentCenter;
        [emptyContainer addSubview:defaultTItleLabel];
        internalY += 100;
        mainContainerY = internalY ;
    }

    
    _mainViewHeightConstraints.constant = mainContainerY;
}


-(void)buttonHandlerForViewAllOrderDetails:(UIButton*)button{
     NSDictionary *result = [arrayMainCollection objectAtIndex:button.tag];
     incrementId = result[@"incrementId"];
     NSString *customerid = [preferences objectForKey:@"customerId"];
     customerId = customerid;
     [self performSegueWithIdentifier:@"soldOrderToOrderDetailsSegue" sender:self];
}

-(void) callProductTitleTap:(UITapGestureRecognizer *)recognize {
    NSDictionary *orderList = [arrayMainCollection objectAtIndex:recognize.view.superview.tag];
    NSDictionary *orderName = [orderList[@"orderList"] objectAtIndex:recognize.view.tag ];
    productId = orderName[@"id"];
    name    = orderName[@"name"];
    type = orderName[@"type"];
 [self performSegueWithIdentifier:@"sellerSoldOrderToProductPageSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"soldOrderToOrderDetailsSegue"]) {
        SellerOrderDetails *destViewController = segue.destinationViewController;
        destViewController.incrementId = incrementId;
        destViewController.customerId = customerId;
    }
   else if([segue.identifier isEqualToString:@"sellerSoldOrderToProductPageSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
       destViewController.productId = productId;
       destViewController.productName = name;
       destViewController.productType = type;
       destViewController.parentClass = @"SellerOrder";
    }
}

-(void)buttonHandlerForFilter :(UIButton*)button{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 888;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UIView *contactBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5, SCREEN_WIDTH/2+SCREEN_WIDTH/4)];
    [contactBlock setBackgroundColor : [UIColor whiteColor]];
    contactBlock.layer.cornerRadius = 4;
    
    float y = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 32)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [sortTitle setText : @"FILTER YOUR ORDER LIST"];
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [contactBlock addSubview : sortTitle];
    
    y += 42;
    UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 1)];
    [hr setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [contactBlock addSubview : hr];
    y +=10;
    UITextField *nameField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width-20 , 40)];
    nameField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    nameField.textColor = [UIColor blackColor];
    [nameField setPlaceholder:@"Enter Order Id"];
    nameField.layer.borderWidth = 1.0f;
    nameField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    nameField.tag = 1;
    [nameField setKeyboardType:UIKeyboardTypePhonePad];
    nameField.textAlignment = NSTextAlignmentLeft;
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:nameField];
    
    y += 50;

    
    UILabel *dateFrom = [[UILabel alloc] initWithFrame:CGRectMake(10,y, contactBlock.frame.size.width-20, 25)];
    [dateFrom setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [dateFrom setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [dateFrom setText:@"Set Date From"];
    dateFrom.textAlignment = NSTextAlignmentCenter;
    [dateFrom setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [contactBlock addSubview:dateFrom];
    
    y +=25;
    
    datePickerFrom = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width-10, 50)];
    datePickerFrom.tag =2;
    [datePickerFrom setDatePickerMode:UIDatePickerModeDate];
    [contactBlock addSubview:datePickerFrom];
    [datePickerFrom addTarget:self action:@selector(setDateFromMethod:) forControlEvents:UIControlEventValueChanged];
    
    
    y +=50;
    
    UILabel *dateTo = [[UILabel alloc] initWithFrame:CGRectMake(10,y, contactBlock.frame.size.width-20, 25)];
    [dateTo setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [dateTo setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [dateTo setText:@"Set Date To"];
    dateTo.textAlignment = NSTextAlignmentCenter;
    [dateTo setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [contactBlock addSubview:dateTo];
    
    y +=25;
    
    datePickerTo = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width-10, 50)];
    datePickerTo.tag =3;
    [datePickerTo setDatePickerMode:UIDatePickerModeDate];
    [contactBlock addSubview:datePickerTo];
    [datePickerTo addTarget:self action:@selector(setDateToMethod:) forControlEvents:UIControlEventValueChanged];
    
    y += 50;
    
    UILabel *orderStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,y, contactBlock.frame.size.width-20, 25)];
    [orderStatusLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [orderStatusLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [orderStatusLabel setText:@"Order Status"];
    orderStatusLabel.textAlignment = NSTextAlignmentCenter;
    [orderStatusLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [contactBlock addSubview:orderStatusLabel];
    
    
    y +=25;
    
    dropDown = [[UIPickerView alloc] initWithFrame:CGRectMake(5,y ,contactBlock.frame.size.width-10 , 50)];
    dropDown.tag = 4;
    dropDown.showsSelectionIndicator = YES;
    dropDown.hidden = NO;
    dropDown.delegate = self;
    dropDown.dataSource = self;
    [contactBlock addSubview:dropDown];
    
    
    
    y += 80;
    
    UIView *requestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , contactBlock.frame.size.width, 30)];
    requestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    requestContainer.layer.borderWidth = 0.0f;
    [contactBlock addSubview:requestContainer];
    
    
    NSDictionary *reqAttributesforCancelButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforCancelButtonText = [@"Cancel" sizeWithAttributes:reqAttributesforCancelButtonText];
    CGFloat reqStringWidthVCancelButtonText = reqStringSizeforCancelButtonText.width;
    UIButton  *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(contactCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
   // cancelButton.frame = CGRectMake(SCREEN_WIDTH/2,y,reqStringWidthVCancelButtonText+20, 40);
    cancelButton.frame = CGRectMake(0,0,reqStringWidthVCancelButtonText+20, 40);
    [cancelButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [cancelButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [cancelButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [requestContainer addSubview:cancelButton];
    
    
    NSDictionary *reqAttributesforSubmitButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforSubmitButtonText = [@"Submit" sizeWithAttributes:reqAttributesforSubmitButtonText];
    CGFloat reqStringWidthSubmitButtonText = reqStringSizeforSubmitButtonText.width;
    UIButton  *SubmitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [SubmitButton addTarget:self action:@selector(contactSubmitButton :) forControlEvents:UIControlEventTouchUpInside];
    [SubmitButton setTitle:@"Submit" forState:UIControlStateNormal];
    //SubmitButton.frame = CGRectMake(SCREEN_WIDTH/2 +reqStringWidthVCancelButtonText +20 +10,y,reqStringWidthSubmitButtonText+20, 40);
    SubmitButton.frame = CGRectMake(reqStringWidthVCancelButtonText +20 +10,0,reqStringWidthSubmitButtonText+20, 40);
    [SubmitButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [SubmitButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [SubmitButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
   // [contactBlock addSubview:SubmitButton];
    
    [requestContainer addSubview:SubmitButton];
    
    CGRect  newFrame = requestContainer.frame;
    newFrame.size.width = reqStringWidthVCancelButtonText +20 +10 + reqStringWidthSubmitButtonText+20;
    requestContainer.frame = newFrame;
    
    UIView *finalrequestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y ,contactBlock.frame.size.width, 30)];
    finalrequestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    finalrequestContainer.layer.borderWidth = 0.0f;
    requestContainer.center = CGPointMake(finalrequestContainer.frame.size.width  / 2,
                                          finalrequestContainer.frame.size.height / 2);
    [finalrequestContainer addSubview:requestContainer];
    
    [contactBlock addSubview:finalrequestContainer];
    
    CGRect  contactNewFrame = contactBlock.frame;
    contactNewFrame.size.height = y+60;
    contactBlock.frame = contactNewFrame;
    
    
    
    
    contactBlock.center = blurEffectView.center;
    [blurEffectView addSubview:contactBlock];
    [self.view addSubview:blurEffectView];
    
    

}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    return [orderStatus count];
}
-(NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [orderStatus objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
}
- (IBAction)setDateFromMethod:(id)sender {
     NSString *dateFrom = [dateFormat stringFromDate:datePickerFrom.date];
    [orderListData setObject:dateFrom forKey:@"setDateFrom"];
}
- (IBAction)setDateToMethod:(id)sender {
    NSString *dateTo = [dateFormat stringFromDate:datePickerTo.date];
    [orderListData setObject:dateTo forKey:@"setDateTo"];
}




-(void)contactCancelButton :(UIButton*)button{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    [parent2.superview removeFromSuperview];
}
-(void)contactSubmitButton :(UIButton*)button{
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    UITextField *orderIdField = [parent2 viewWithTag:1];
    if([orderIdField.text isEqualToString:@""] == NO){
    [orderListData setObject:orderIdField.text forKey:@"orderId"];
    }
    UIPickerView *statusPicker = [parent2 viewWithTag:4];
    int row =(int)[statusPicker selectedRowInComponent:0];
    if(row != 0){
    [orderListData setObject:orderStatusSend[row] forKey:@"orderStatus"];
    }
    whichApiDataToprocess = @"searchOrders";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if([orderListData count]>0){
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    pageNumber = 1;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
        [parent2.superview removeFromSuperview];
    }
    else{
        [parent2.superview removeFromSuperview];
    }
  [[self navigationController] setNavigationBarHidden:NO animated:YES];
}



@end
