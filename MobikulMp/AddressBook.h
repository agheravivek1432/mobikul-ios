//
//  AddressBook.h
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface AddressBook : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi, *whichApiResultToProcess;
    NSInteger *addressIdToDelete;
    NSString *addOrEdit, *addressId;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id collection;
    UIWindow *currentWindow;
    
    NSString *STR_SellerStatus;
}
@property (weak, nonatomic) IBOutlet UILabel *defaultShipping;
@property (weak, nonatomic) IBOutlet UILabel *defaultAddresses;
@property (weak, nonatomic) IBOutlet UILabel *defaultBilling;
@property (weak, nonatomic) IBOutlet UILabel *defaultPickUpAddress;
@property (weak, nonatomic) IBOutlet UIButton *changeBilling;
@property (weak, nonatomic) IBOutlet UILabel *additionalAddress;
@property (weak, nonatomic) IBOutlet UIButton *changeShipping;
@property (weak, nonatomic) IBOutlet UILabel *defaultBillingAddress;
@property (weak, nonatomic) IBOutlet UILabel *defaultShippingAddress;
@property (weak, nonatomic) IBOutlet UILabel *defaultPickupAddress;
@property (weak, nonatomic) IBOutlet UIView *additionalAddressContainer;
- (IBAction)addNewAddress:(id)sender;
- (IBAction)changeBillingAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *changeBillingButton;
- (IBAction)changeShippingAction:(id)sender;
- (void)editAdditionalAddress:(UILabel *)label;
- (void)deleteAdditionalAddress:(UILabel *)label;
@property (weak, nonatomic) IBOutlet UIButton *changeShippingButton;
@property (weak, nonatomic) IBOutlet UIButton *changePickupButton;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *additionalAddressContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainContainerHeightConstraint;

@end
