//
//  commentListCell.h
//  MobikulMp
//
//  Created by Apple on 19/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface commentListCell : UITableViewCell

{
    IBOutlet UIView *viewComment;
    IBOutlet UIImageView *imgViewUser;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblComment;
}

@property (nonatomic, retain) NSDictionary *dictComment;

-(void)reloadCommentListCell;

@end
