//
//  CheckOut.m
//  Mobikul
//
//  Created by Ratnesh on 15/04/16.
//  Copyright © 2016 Webkul. All rights reserved.
//


#import "CheckOut.h"
#import "GlobalData.h"
#import "OrderSuccess.h"
#import "ToastView.h"


#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)


GlobalData *globalObjectCheckOut;

@implementation CheckOut

- (void)viewDidLoad
{
    //mobikulhttp/customer/getbraintreetoken
    // mobikulhttp/customer/getbraintreepayment
    
    [super viewDidLoad];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId != NULL || customerId != [NSNull null])
        stepCount = 1;
    else
        stepCount = 2;
    globalObjectCheckOut = [[GlobalData alloc] init];
    globalObjectCheckOut.delegate = self;
    [globalObjectCheckOut language];
    isAlertVisible = 0;currentStep = 0;isBillingFormOpen = @"0";isShippingFormOpen = @"0";
    [self registerForKeyboardNotifications];
    billingFormState = @"0";
    selectedbillingId = @"0";choosingNewBillingAddress = @"0";heightModificationDoneOfBillingInfo = @"0";heightModificationDoneOfShippingInfo = @"0";savenewBillingAddress = @"0";savenewShippingAddress = @"0";sizeModified = @"0";useBillingAddress = @"1";selectedshippingId = @"0";choosingNewShippingAddress = @"0";selectedShippingMethod = @"";selectedPaymentMethod = @"";
    [self.tabBarController setDelegate:self];
    finalData = [[NSMutableString alloc] initWithString: @""];
    whichApiDataToprocess = @"";
    shipToThisOrDifferentAddress = @"1";
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    mainContainerY = 5;checkoutAsGuest = @"";
    whichApiDataToprocess = @"steponetwo";
    currentStep = 1;
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData
{
    isAlertVisible = 1;
    
    if([whichApiDataToprocess isEqual:@"getbraintreetoken"])
    {
        strToken = collectionData[@"token"];
        
        [self doFurtherProcessingWithResult];
    }
    else if ([whichApiDataToprocess isEqual:@"getbraintreepayment"])
    {
        [self doFurtherProcessingWithResult];
    }
    else
    {
        collection = collectionData ;
        
        if([collection[@"success"] integerValue] == 5)
        {
            [self loginRequest];
        }
        else
        {
            [self doFurtherProcessingWithResult];
        }
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}
-(void)loginRequestCall{
    [self loginRequest];
}



-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)initializeCheckout {
    [_BIContainer setValue:@"YES" forKeyPath:@"hidden"];
    [_SIContainer setValue:@"YES" forKeyPath:@"hidden"];
    [_SMContainer setValue:@"YES" forKeyPath:@"hidden"];
    [_PIContainer setValue:@"YES" forKeyPath:@"hidden"];
    [_ORContainer setValue:@"YES" forKeyPath:@"hidden"];
    [_guestRegisterLoginContainer setValue:@"NO" forKeyPath:@"hidden"];
    [_GRLHeadingLabel setText:[NSString stringWithFormat:@"%d CHECKOUT METHOD", 1]];
    int checkoutMethodContainerInternalY = 5;
    ////////////////////////////////////////////// first box in checkout method box  /////////////////////////////////////////
    int guestregisterBlockY = 0;
    guestRegisterBlock = [[UIView alloc]initWithFrame:CGRectMake(5, checkoutMethodContainerInternalY, SCREEN_WIDTH-30, 1)];
    [guestRegisterBlock setBackgroundColor:[UIColor whiteColor]];
    guestRegisterBlock.layer.cornerRadius = 2;
    guestRegisterBlock.layer.shadowOffset = CGSizeMake(0, 0);
    guestRegisterBlock.layer.shadowRadius = 3;
    guestRegisterBlock.layer.shadowOpacity = 0.5;
    [_GRLInnerMainContainer addSubview:guestRegisterBlock];
    UILabel *GRBinternalHeading1 = [[UILabel alloc] initWithFrame:CGRectMake(0, guestregisterBlockY, guestRegisterBlock.frame.size.width, 30)];
    [GRBinternalHeading1 setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [GRBinternalHeading1 setBackgroundColor:[GlobalData colorWithHexString:@"FFC107"]];
    GRBinternalHeading1.textAlignment = NSTextAlignmentCenter;
    [GRBinternalHeading1 setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    [GRBinternalHeading1 setText:@"CHECKOUT AS A GUEST OR REGISTER"];
    [guestRegisterBlock addSubview:GRBinternalHeading1];
    guestregisterBlockY += 35;
    UILabel *GRBinternalHeading2 = [[UILabel alloc] initWithFrame:CGRectMake(5, guestregisterBlockY, guestRegisterBlock.frame.size.width-10, 25)];
    [GRBinternalHeading2 setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [GRBinternalHeading2 setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    [GRBinternalHeading2 setText:@"Register with us for future convenience:"];
    [guestRegisterBlock addSubview:GRBinternalHeading2];
    guestregisterBlockY += 30;
    
    if(![mainCollection[@"isVirtual"] boolValue]){
        UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(5, guestregisterBlockY, 25, 25)];
        radioBtnContainer.layer.cornerRadius = 13;
        radioBtnContainer.tag = 500;
        radioBtnContainer.layer.masksToBounds = YES;
        radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
        radioBtnContainer.layer.borderWidth = 2.0f;
        UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
        radioBtn.tag = 501;
        radioBtn.layer.cornerRadius = 13;
        radioBtn.layer.masksToBounds = YES;
        radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
        radioBtn.layer.borderWidth = 5.0f;
        UITapGestureRecognizer *addReviewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectThisOption:)];
        addReviewGesture.numberOfTapsRequired = 1;
        [radioBtn addGestureRecognizer:addReviewGesture];
        [radioBtnContainer addSubview:radioBtn];
        [guestRegisterBlock addSubview:radioBtnContainer];
        UILabel *checkoutAsGuestLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, guestregisterBlockY, guestRegisterBlock.frame.size.width-40, 25)];
        [checkoutAsGuestLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
        [checkoutAsGuestLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
        [checkoutAsGuestLabel setText:@"Checkout As Guest"];
        [guestRegisterBlock addSubview:checkoutAsGuestLabel];
        guestregisterBlockY += 30;
    }
    
    UIView *radioBtnContainer1 = [[UIView alloc]initWithFrame:CGRectMake(5, guestregisterBlockY, 25, 25)];
    radioBtnContainer1.layer.cornerRadius = 13;
    radioBtnContainer1.tag = 500;
    radioBtnContainer1.layer.masksToBounds = YES;
    radioBtnContainer1.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
    radioBtnContainer1.layer.borderWidth = 2.0f;
    UIView *radioBtn1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [radioBtn1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
    radioBtn1.tag = 502;
    radioBtn1.layer.cornerRadius = 13;
    radioBtn1.layer.masksToBounds = YES;
    radioBtn1.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
    radioBtn1.layer.borderWidth = 5.0f;
    UITapGestureRecognizer *addReviewGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectThisOption:)];
    addReviewGesture1.numberOfTapsRequired = 1;
    [radioBtn1 addGestureRecognizer:addReviewGesture1];
    [radioBtnContainer1 addSubview:radioBtn1];
    [guestRegisterBlock addSubview:radioBtnContainer1];
    UILabel *registernCheckoutLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, guestregisterBlockY, guestRegisterBlock.frame.size.width-40, 25)];
    [registernCheckoutLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [registernCheckoutLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [registernCheckoutLabel setText:@"Register and Checkout"];
    [guestRegisterBlock addSubview:registernCheckoutLabel];
    guestregisterBlockY += 40;
    UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
    continueButton.tag = 801;
    [continueButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
    continueButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
    [continueButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
    [continueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
    continueButton.frame = CGRectMake(5, guestregisterBlockY, guestRegisterBlock.frame.size.width-10, 40);
    [guestRegisterBlock addSubview:continueButton];
    guestregisterBlockY += 45;
    CGRect GRBnewFrame = guestRegisterBlock.frame;
    GRBnewFrame.size.height = guestregisterBlockY;
    guestRegisterBlock.frame = GRBnewFrame;
    //////////////////////////////////////////////////// first box in checkout method box  //////////////////////////////////////////////////
    
    checkoutMethodContainerInternalY += guestregisterBlockY+15;
    //////////////////////////////////////////////////// second box in checkout method box  //////////////////////////////////////////////////
    int loginBlockY = 0;
    UIView *loginBlock = [[UIView alloc]initWithFrame:CGRectMake(5, checkoutMethodContainerInternalY, SCREEN_WIDTH-30, 1)];
    [loginBlock setBackgroundColor:[UIColor whiteColor]];
    loginBlock.layer.cornerRadius = 2;
    loginBlock.layer.shadowOffset = CGSizeMake(0, 0);
    loginBlock.layer.shadowRadius = 3;
    loginBlock.layer.shadowOpacity = 0.5;
    [_GRLInnerMainContainer addSubview:loginBlock];
    UILabel *LBinternalHeading1 = [[UILabel alloc] initWithFrame:CGRectMake(0, loginBlockY, loginBlock.frame.size.width, 30)];
    [LBinternalHeading1 setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [LBinternalHeading1 setBackgroundColor:[GlobalData colorWithHexString:@"FFC107"]];
    [LBinternalHeading1 setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    LBinternalHeading1.textAlignment = NSTextAlignmentCenter;
    [LBinternalHeading1 setText:@"RETURNING CUSTOMERS"];
    [loginBlock addSubview:LBinternalHeading1];
    loginBlockY += 35;
    UILabel *LBinternalHeading2 = [[UILabel alloc] initWithFrame:CGRectMake(5, loginBlockY, loginBlock.frame.size.width-10, 25)];
    [LBinternalHeading2 setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [LBinternalHeading2 setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    [LBinternalHeading2 setText:@"Sign in to speed up your checkout process"];
    [loginBlock addSubview:LBinternalHeading2];
    loginBlockY += 30;
    UILabel *requiredStar;
    NSDictionary *emailLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
    CGSize emailLabelStringSize = [@"Email Address" sizeWithAttributes:emailLabelAttributes];
    CGFloat emailLabelWidth = emailLabelStringSize.width;
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, loginBlockY, emailLabelWidth, 25)];
    [emailLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [emailLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [emailLabel setText:@"Email Address"];
    [loginBlock addSubview:emailLabel];
    requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(emailLabelWidth+8, loginBlockY, 10, 25)];
    [requiredStar setText:@"*"];
    [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
    [loginBlock addSubview:requiredStar];
    loginBlockY += 30;
    UITextField *emailField = [[UITextField alloc] initWithFrame:CGRectMake(5, loginBlockY, loginBlock.frame.size.width-10, 30)];
    emailField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
    emailField.textColor = [UIColor blackColor];
    [emailField setPlaceholder:@"Enter Email"];
    emailField.tag = 600;
    emailField.textAlignment = NSTextAlignmentLeft;
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    [loginBlock addSubview:emailField];
    loginBlockY += 35;
    NSDictionary *passwordAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
    CGSize passwordStringSize = [@"Password" sizeWithAttributes:passwordAttributes];
    CGFloat passwordWidth = passwordStringSize.width;
    UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, loginBlockY, passwordWidth, 25)];
    [passwordLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [passwordLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [passwordLabel setText:@"Password"];
    [loginBlock addSubview:passwordLabel];
    requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(passwordWidth+8, loginBlockY, 10, 25)];
    [requiredStar setText:@"*"];
    [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
    [loginBlock addSubview:requiredStar];
    loginBlockY += 30;
    UITextField *passwordField = [[UITextField alloc] initWithFrame:CGRectMake(5, loginBlockY, loginBlock.frame.size.width-10, 30)];
    passwordField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
    passwordField.textColor = [UIColor blackColor];
    [passwordField setPlaceholder:@"Enter Password"];
    passwordField.tag = 601;
    [passwordField setSecureTextEntry:YES];
    passwordField.textAlignment = NSTextAlignmentLeft;
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    [loginBlock addSubview:passwordField];
    loginBlockY += 35;
    NSDictionary *forgotPasswordAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
    CGSize forgotPasswordStringSize = [@"Forgot your Password ?" sizeWithAttributes:forgotPasswordAttributes];
    CGFloat forgotPasswordWidth = forgotPasswordStringSize.width;
    UILabel *forgotPasswordLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, loginBlockY, forgotPasswordWidth, 25)];
    [forgotPasswordLabel setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
    [forgotPasswordLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [forgotPasswordLabel setText:@"Forgot your Password ?"];
    forgotPasswordLabel.userInteractionEnabled = YES;
    [loginBlock addSubview:forgotPasswordLabel];
    UITapGestureRecognizer *forgotPasswordGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotPassword:)];
    forgotPasswordGesture.numberOfTapsRequired = 1;
    [forgotPasswordLabel addGestureRecognizer:forgotPasswordGesture];
    loginBlockY += 40;
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.tag = 802;
    [loginButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
    loginButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
    [loginButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
    [loginButton setTitle:@"LOGIN" forState:UIControlStateNormal];
    loginButton.frame = CGRectMake(5, loginBlockY, loginBlock.frame.size.width-10, 40);
    [loginBlock addSubview:loginButton];
    loginBlockY += 45;
    CGRect LBnewFrame = loginBlock.frame;
    LBnewFrame.size.height = loginBlockY;
    loginBlock.frame = LBnewFrame;
    //////////////////////////////////////////////////// second box in checkout method box  //////////////////////////////////////////////////
    checkoutMethodContainerInternalY += loginBlockY+5;
    _GRLInnerMainContainerHeightConstraint.constant = checkoutMethodContainerInternalY;
    _guestRegisterLoginContainerHeightConstraint.constant = checkoutMethodContainerInternalY+40;
}

-(void)selectThisOption:(UITapGestureRecognizer *)recognizer{
    for(UIView *subViews in guestRegisterBlock.subviews){
        if(subViews.tag == recognizer.view.superview.tag){
            for(UIView *subViews1 in subViews.subviews)
                [subViews1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    if(recognizer.view.tag == 501)
        checkoutAsGuest = @"1";
    else
        checkoutAsGuest = @"0";
}

-(void)shippingMethodSelection:(UITapGestureRecognizer *)recognizer{
    int thisTag = (int)recognizer.view.tag;
    int parentTag = (int)recognizer.view.superview.tag;
    for(UIView *subViews in _SMInternalMainContainer.subviews){
        if(subViews.tag == 999){
            int i = 0;
            for(UIView *subViews1 in subViews.subviews){
                if(i == 0){
                    for(UIView *subViews2 in subViews1.subviews)
                        [subViews2 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                }
                i++;
                break;
            }
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    NSDictionary *SMDict = [stepThreenFourCollection[@"shippingMethods"] objectAtIndex:parentTag];
    NSDictionary *EachSMDict = [SMDict[@"method"] objectAtIndex:thisTag];
    selectedShippingMethod = EachSMDict[@"code"];
}

-(void)paymentMethodSelection:(UITapGestureRecognizer *)recognizer
{
    int thisTag = (int)recognizer.view.tag;
    
    for(UIView *subViews in _PIInternalMainContainer.subviews)
    {
        if(subViews.tag == recognizer.view.superview.tag)
        {
            for(UIView *subViews1 in subViews.subviews)
            {
                [subViews1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            }
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    NSDictionary *PIDict = [stepThreenFourCollection[@"paymentMethods"] objectAtIndex:thisTag];
    selectedPaymentMethod = PIDict[@"code"];
}

-(IBAction)actionButtonEventHandler: (id)sender{
    
    int currentViewTag = (int)[sender tag];
    if(currentViewTag == 801){
        if([checkoutAsGuest isEqualToString:@"1"]){
            [_BIContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_SIContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_SMContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_PIContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_ORContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_guestRegisterLoginContainer setValue:@"NO" forKeyPath:@"hidden"];
            _guestRegisterLoginContainerHeightConstraint.constant = 35;
            
            isBillingFormOpen = @"1";
            [_BIInternalHeadingLabel setValue:@"YES" forKeyPath:@"hidden"];
            [_BIAddressPicker setValue:@"YES" forKeyPath:@"hidden"];
            _BIAddressPickerHeightConstraint.constant = 0;
            _BIInternalHeadingHeightConstraint.constant = 0;
            [_BIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
            _BIAddressFormHeightConstraint.constant = (NBAY - 40);
            _BIInternalMainContainerHeightConstraint.constant = 8 + (NBAY - 40) + lowerActionsY + 23;
            _BIActionContainerHeightConstraint.constant = lowerActionsY;
            _BIContainerHeightConstraint.constant = 45 + (NBAY - 40) + lowerActionsY + 26;
            
            _SIContainerHeightConstraint.constant = 35;
            _SIInternalMainContainerHeightConstraint.constant = 0;
            _SIAddressFormHeightConstraint.constant = 0;
            _SIActionContainerHeightConstraint.constant = 0;
            
            _SMInternalMainContainerHeightConstraint.constant = 0;
            _SMContainerHeightConstraint.constant = 35;
            
            _PIInternalMainContainerHeightConstraint.constant = 0;
            _PIContainerHeightConstraint.constant = 35;
            
            _ORInternalMainContainerHeightConstraint.constant = 0;
            _ORContainerHeightConstraint.constant = 35;
        }
        else
            if([checkoutAsGuest isEqualToString:@"0"]){
                [self performSegueWithIdentifier:@"createAccountSegue" sender:self];
            }
            else{
                UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Please Choose Action" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                [errorAlert addAction:noBtn];
                [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
            }
        
    }
    else
        if(currentViewTag == 802){
            UITextField *email = [_GRLInnerMainContainer viewWithTag:600];
            UITextField *password = [_GRLInnerMainContainer viewWithTag:601];
            int isValid = 1;
            NSString *errorMessage;
            NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            if([email.text isEqual: @""]){
                isValid = 0;
                errorMessage = @"Email Address is a required field.";
            }
            else
                if(![emailTest evaluateWithObject:email.text]){
                    isValid = 0;
                    errorMessage = @"Please Enter Valid Email Address.";
                }
                else
                    if([password.text isEqual:@""]){
                        isValid = 0;
                        errorMessage = @"Password can not be blank.";
                    }
                    else
                        if([password.text length] < 7){
                            isValid = 0;
                            errorMessage = @"Password should be atleast 7 characters long.";
                        }
            if(isValid == 0){
                UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Validation Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                [errorAlert addAction:noBtn];
                [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
            }
            else{
                whichApiDataToprocess = @"login";
                preferences = [NSUserDefaults standardUserDefaults];
                NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                if(savedSessionId == nil)
                    [self loginRequest];
                else
                    [self callingHttppApi];
            }
            //NSLog(@"Login as Customer");
        }
        else
            if(currentViewTag == 803){
                if([selectedbillingId isEqualToString:@"0"]){
                    int isValid = 1;
                    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                    NSString *errorMessage;
                    UITextField *firstName = [_BIAddressForm viewWithTag:701];
                    UITextField *lastName = [_BIAddressForm viewWithTag:702];
                    UITextField *email = [_BIAddressForm viewWithTag:716];
                    UITextField *street1 = [_BIAddressForm viewWithTag:704];
                    UITextField *city = [_BIAddressForm viewWithTag:706];
                    UITextField *zip = [_BIAddressForm viewWithTag:707];
                    UITextField *phone = [_BIAddressForm viewWithTag:708];
                    UITextField *fax = [_BIAddressForm viewWithTag:709];
                    UITextField *state = [_BIAddressForm viewWithTag:710];
                    UIPickerView *statePicker = [_BIAddressForm viewWithTag:753];
                    [firstName setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [lastName setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [email setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [street1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [city setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [zip setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [phone setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [fax setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    [state setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    if([firstName.text isEqual:@""]){
                        isValid = 0;
                        errorMessage = @"First Name is a required field";
                        [firstName setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                    }
                    else
                        if([lastName.text isEqual:@""]){
                            isValid = 0;
                            errorMessage = @"Last Name is a required field.";
                            [lastName setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                        }
                        else
                            if([email.text isEqual:@""]){
                                isValid = 0;
                                errorMessage = @"Email Address is a required field.";
                                [email setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                            }
                            else
                                if(![emailTest evaluateWithObject:email.text]){
                                    isValid = 0;
                                    errorMessage = @"Please Enter Valid Email Address.";
                                    [email setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                }
                                else
                                    if([street1.text isEqual:@""]){
                                        isValid = 0;
                                        errorMessage = @"Street is a required field.";
                                        [street1 setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                    }
                                    else
                                        if([city.text isEqual:@""]){
                                            isValid = 0;
                                            errorMessage = @"City is a required field.";
                                            [city setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                        }
                                        else
                                            if([zip.text isEqual:@""]){
                                                isValid = 0;
                                                errorMessage = @"Zip Code is a required field.";
                                                [zip setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                            }
                                            else
                                                if([phone.text isEqual:@""]){
                                                    isValid = 0;
                                                    errorMessage = @"Telephone is a required field.";
                                                    [phone setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                                }
                                                else{
                                                    if([isBillingStateId isEqualToString:@"1"]){
                                                    }
                                                    else{
                                                        if([statePickerState isEqualToString:@"1"]){
                                                        }
                                                        else{
                                                            if([state.text isEqual:@""]){
                                                                isValid = 0;
                                                                errorMessage = @"State is a required field.";
                                                                [state setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                                            }
                                                        }
                                                        
                                                    }
                                                }
                    if(isValid == 0){
                        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Validation Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                        [errorAlert addAction:noBtn];
                        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                    }
                    else{
                        if([shipToThisOrDifferentAddress isEqualToString:@"1"]){
                            selectedbillingId = @"0";
                            selectedshippingId = @"0";
                            whichApiDataToprocess = @"stepthreefour";
                            currentStep = 3;
                            if([shipToThisOrDifferentAddress isEqualToString:@"1"])
                                useBillingAddress = @"1";
                            else
                                useBillingAddress = @"0";
                            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                            if(savedSessionId == nil)
                                [self loginRequest];
                            else
                                [self callingHttppApi];
                        }
                        else{
                            currentStep = 2;
                            _BIContainerHeightConstraint.constant = 35;
                            _BIInternalMainContainerHeightConstraint.constant = 0;
                            _BIAddressFormHeightConstraint.constant = 0;
                            _BIActionContainerHeightConstraint.constant = 0;
                            
                            NSString *customerId = [preferences objectForKey:@"customerId"];
                            if(customerId != nil){
                                _SIContainerHeightConstraint.constant = 45 + 118 + SIlowerActionsY + 5;
                                _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                                _SIInternalMainContainerHeightConstraint.constant = 118 + SIlowerActionsY +8;
                                _SIAddressFormHeightConstraint.constant = 0;
                                
                                if([mainCollection[@"address"] count] < 1){
                                    _SIAddressForm.hidden = NO;
                                    isShippingFormOpen = @"1";
                                    _SIInternalHeadingHeightConstraint.constant = 0;
                                    _SIAddressPickerHeightConstraint.constant = 0;
                                    _SIInternalHeadingLabel.hidden = YES;
                                    _SIAddressPicker.hidden = YES;
                                    selectedshippingId = @"0";
                                    choosingNewShippingAddress = @"1";
                                    [_SIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
                                    _SIAddressFormHeightConstraint.constant = NSAY;
                                    _SIInternalMainContainerHeightConstraint.constant = 18 + NSAY + SIlowerActionsY + 8;
                                    _SIContainerHeightConstraint.constant = 45 + 18 + NSAY + SIlowerActionsY + 5;
                                    _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                                }
                            }
                            else{
                                
                                [_SIInternalHeadingLabel setValue:@"YES" forKeyPath:@"hidden"];
                                [_SIAddressPicker setValue:@"YES" forKeyPath:@"hidden"];
                                _SIAddressPickerHeightConstraint.constant = 0;
                                _SIInternalHeadingHeightConstraint.constant = 0;
                                [_SIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
                                _SIAddressFormHeightConstraint.constant = (NSAY - 40);
                                _SIInternalMainContainerHeightConstraint.constant = 8 + (NSAY - 40) + SIlowerActionsY + 23;
                                _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                                _SIContainerHeightConstraint.constant = 45 + (NSAY - 40) + SIlowerActionsY + 26;
                            }
                        }
                    }
                }
                else{
                    if([shipToThisOrDifferentAddress isEqualToString:@"1"]){
                        whichApiDataToprocess = @"stepthreefour";
                        currentStep = 3;
                        useBillingAddress = @"1";
                        selectedshippingId = selectedbillingId;
                        UISwitch *onoff = [_SIActionContainer viewWithTag:612];
                        [onoff setOn:YES animated:NO];
                        int row = 0;
                        for(int i=0; i<[mainCollection[@"address"] count]; i++) {
                            NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:i];
                            if(selectedbillingId == addressDict[@"id"])
                                row = i;
                        }
                        [_SIAddressPicker selectRow:row inComponent:0 animated:NO];
                        [self pickerView:_SIAddressPicker didSelectRow:row inComponent:0];
                        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                        if(savedSessionId == nil)
                            [self loginRequest];
                        else
                            [self callingHttppApi];
                    }
                    else{
                        currentStep = 2;
                        _BIContainerHeightConstraint.constant = 35;
                        _BIInternalMainContainerHeightConstraint.constant = 0;
                        _BIAddressFormHeightConstraint.constant = 0;
                        _BIActionContainerHeightConstraint.constant = 0;
                        _SIContainerHeightConstraint.constant = 45 + 118 + SIlowerActionsY + 5;
                        _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                        _SIInternalMainContainerHeightConstraint.constant = 118 + SIlowerActionsY +8;
                        _SIAddressFormHeightConstraint.constant = 0;
                    }
                }
            }
            else
                if(currentViewTag == 804){
                    if([selectedshippingId isEqualToString:@"0"] && [useBillingAddress isEqualToString:@"0"]){
                        int isValid = 1;
                        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                        NSString *errorMessage;
                        UITextField *firstName = [_SIAddressForm viewWithTag:601];
                        UITextField *lastName = [_SIAddressForm viewWithTag:602];
                        UITextField *street1 = [_SIAddressForm viewWithTag:604];
                        UITextField *email = [_SIAddressForm viewWithTag:616];
                        UITextField *city = [_SIAddressForm viewWithTag:606];
                        UITextField *zip = [_SIAddressForm viewWithTag:607];
                        UITextField *phone = [_SIAddressForm viewWithTag:608];
                        UITextField *fax = [_SIAddressForm viewWithTag:609];
                        UITextField *state = [_SIAddressForm viewWithTag:610];
                        [firstName setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [lastName setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [street1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [email setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [city setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [zip setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [phone setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [fax setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        [state setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        if([firstName.text isEqual:@""]){
                            isValid = 0;
                            errorMessage = @"First Name is a required field";
                            [firstName setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                        }
                        else
                            if([lastName.text isEqual:@""]){
                                isValid = 0;
                                errorMessage = @"Last Name is a required field.";
                                [lastName setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                            }
                            else
                                if([email.text isEqual:@""]){
                                    isValid = 0;
                                    errorMessage = @"Email Address is a required field.";
                                    [email setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                }
                                else
                                    if(![emailTest evaluateWithObject:email.text]){
                                        isValid = 0;
                                        errorMessage = @"Please Enter Valid Email Address.";
                                        [email setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                    }
                                    else
                                        if([street1.text isEqual:@""]){
                                            isValid = 0;
                                            errorMessage = @"Street is a required field.";
                                            [street1 setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                        }
                                        else
                                            if([city.text isEqual:@""]){
                                                isValid = 0;
                                                errorMessage = @"City is a required field.";
                                                [city setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                            }
                                            else
                                                if([zip.text isEqual:@""]){
                                                    isValid = 0;
                                                    errorMessage = @"Zip Code is a required field.";
                                                    [zip setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                                }
                                                else
                                                    if([phone.text isEqual:@""]){
                                                        isValid = 0;
                                                        errorMessage = @"Telephone is a required field.";
                                                        [phone setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                                    }
                                                    else{
                                                        if([statePickerState isEqualToString:@"1"]){
                                                        }
                                                        else{
                                                            if([state.text isEqual:@""]){
                                                                isValid = 0;
                                                                errorMessage = @"State is a required field.";
                                                                [state setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                                                            }
                                                        }
                                                    }
                        
                        if(isValid == 0){
                            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Validation Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                            [errorAlert addAction:noBtn];
                            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                        }
                        else{
                            selectedshippingId = @"0";
                            whichApiDataToprocess = @"stepthreefour";
                            currentStep = 3;
                            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                            if(savedSessionId == nil)
                                [self loginRequest];
                            else
                                [self callingHttppApi];
                        }
                    }
                    else{
                        whichApiDataToprocess = @"stepthreefour";
                        currentStep = 3;
                        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                        if(savedSessionId == nil)
                            [self loginRequest];
                        else
                            [self callingHttppApi];
                    }
                }
                else
                    if(currentViewTag == 805){
                        if([stepThreenFourCollection[@"shippingMethods"] count] == 0){
                            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address." preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                            [errorAlert addAction:noBtn];
                            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                            
                            
                        }
                        else{
                            if([selectedShippingMethod isEqualToString:@""]){
                                UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Validation Error" message:@"Please Choose Shipping Method" preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                [errorAlert addAction:noBtn];
                                [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                            }
                            else{
                                currentStep = 4;
                                _SMContainerHeightConstraint.constant = 35;
                                _SMInternalMainContainerHeightConstraint.constant = 0;
                                _PIContainerHeightConstraint.constant = 35 + PIY + 5;
                                _PIInternalMainContainerHeightConstraint.constant = PIY;
                            }
                        }
                    }
                    else
                        if(currentViewTag == 806){
                            
                            if([stepThreenFourCollection[@"paymentMethods"] count] == 0){
                                UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your order cannot be completed at this time as there is no payment methods available for it. Please make necessary changes in your shipping address." preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                [errorAlert addAction:noBtn];
                                [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                                
                                
                            }
                            else{
                                if([selectedPaymentMethod isEqualToString:@""]){
                                    UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Validation Error" message:@"Please Choose Payment Method" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                    [errorAlert addAction:noBtn];
                                    [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                                }
                                else{
                                    currentStep = 5;
                                    whichApiDataToprocess = @"stepOrderReview";
                                    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                    if(savedSessionId == nil)
                                        [self loginRequest];
                                    else
                                        [self callingHttppApi];
                                }
                            }
                        }
                        else
                            if(currentViewTag == 807)
                            {
//                                whichApiDataToprocess = @"saveOrder";
//                                [sender setValue:@"YES" forKeyPath:@"hidden"];
//                                NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
//                                
//                                if(savedSessionId == nil)
//                                    [self loginRequest];
//                                else
//                                    [self callingHttppApi];
                                
                                //mobikulhttp/customer/getbraintreetoken
                                // mobikulhttp/customer/getbraintreepayment
                                
                                whichApiDataToprocess = @"getbraintreetoken";
                               // [sender setValue:@"YES" forKeyPath:@"hidden"];
                                NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                
                                [GlobalData alertController:currentWindow msg:[globalObjectCheckOut.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
                                NSMutableString *post = [NSMutableString string];
                                NSString *strSessionId = [preferences objectForKey:@"sessionId"];
                                [post appendFormat:@"sessionId=%@&", strSessionId];
                                
                                NSString *customerId = [preferences objectForKey:@"customerId"];
                                
                                [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/customer/getbraintreetoken" signal:@"HttpPostMetod"];
                                globalObjectCheckOut.delegate = self;
                            }
}

-(void)forgotPassword:(UITapGestureRecognizer *)recognizer{
    UIAlertController *forgotpasswordAlert = [UIAlertController alertControllerWithTitle:nil message:@"Please Enter your Email ID" preferredStyle:UIAlertControllerStyleAlert];
    [forgotpasswordAlert addTextFieldWithConfigurationHandler:^(UITextField *enteredEmailFromPromt){
        enteredEmailFromPromt.placeholder = @"Please Enter your Email ID";
    }];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      @try{
                                                          int isValid = 1;
                                                          NSString *errorMessage;
                                                          forgotPasswordEmail = ((UITextField *)[forgotpasswordAlert.textFields objectAtIndex:0]).text;
                                                          NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                                                          NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                                          if([forgotPasswordEmail isEqual:@""]){
                                                              isValid = 0;
                                                              errorMessage = @"Email Address is a required field.";
                                                          }
                                                          else
                                                              if(![emailTest evaluateWithObject:forgotPasswordEmail]){
                                                                  isValid = 0;
                                                                  errorMessage = @"Please Enter Valid Email Address.";
                                                              }
                                                          if(isValid == 0){
                                                              UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Validation Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                                                              UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                                              [errorAlert addAction:noBtn];
                                                              [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                                                          }
                                                          else{
                                                              whichApiDataToprocess = @"forgotpassword";
                                                              NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                              if(savedSessionId == nil)
                                                                  [self loginRequest];
                                                              else
                                                                  [self callingHttppApi];
                                                          }
                                                      }
                                                      @catch (NSException *e) {
                                                          //  NSLog(@"catching %@ reason %@", [e name], [e reason]);
                                                      }
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [forgotpasswordAlert addAction:okBtn];
    [forgotpasswordAlert addAction:noBtn];
    [self.parentViewController presentViewController:forgotpasswordAlert animated:YES completion:nil];
}



-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}


-(void)showConnectionErrorDialogue{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:@"Warning" message:@"ERROR with the Conenction" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}



-(void) callingHttppApi
{
    [GlobalData alertController:currentWindow msg:[globalObjectCheckOut.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    NSString *customerId = [preferences objectForKey:@"customerId"];
    
    if(customerId != nil || customerId != [NSNull null])
    {
        [post appendFormat:@"customerId=%@&", customerId];
        [post appendFormat:@"checkoutMethod=%@&", @"customer"];
    }
    
    NSString *quoteId = [preferences objectForKey:@"quoteId"];
    
    if(quoteId != nil)
    {
        [post appendFormat:@"quoteId=%@&",quoteId];
        [post appendFormat:@"checkoutMethod=%@&",@"guest"];
    }
    
    NSString *storeId = [preferences objectForKey:@"storeId"];
    
    if([whichApiDataToprocess isEqual:@"steponetwo"])
    {
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/checkout/getsteponentwoData" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"stepthreefour"])
    {
        [post appendFormat:@"storeId=%@&", storeId];
        NSMutableDictionary *BDDict = [NSMutableDictionary dictionary];
        NSMutableDictionary *BDNewAddrDict = [NSMutableDictionary dictionary];
        NSMutableArray *BDStreet = [[NSMutableArray alloc] init];
        NSMutableDictionary *SDDict = [NSMutableDictionary dictionary];
        NSMutableDictionary *SDNewAddrDict = [NSMutableDictionary dictionary];
        NSMutableArray *SDStreet = [[NSMutableArray alloc] init];
        
        if(![selectedbillingId isEqualToString:@"0"])
            [BDDict setObject:selectedbillingId forKey:@"addressId"];
        else
        {
            [BDDict setObject:@"New Address" forKey:@"addressId"];
            UITextField *firstName = [_BIAddressForm viewWithTag:701];
            [BDNewAddrDict setObject:firstName.text forKey:@"firstName"];
            UITextField *lastName = [_BIAddressForm viewWithTag:702];
            [BDNewAddrDict setObject:lastName.text forKey:@"lastName"];
            UITextField *company = [_BIAddressForm viewWithTag:703];
            [BDNewAddrDict setObject:company.text forKey:@"company"];
            UITextField *street1 = [_BIAddressForm viewWithTag:704];
            UITextField *street2 = [_BIAddressForm viewWithTag:705];
            [BDStreet addObject:street1.text];
            [BDStreet addObject:street2.text];
            [BDNewAddrDict setObject:BDStreet forKey:@"street"];
            UITextField *city = [_BIAddressForm viewWithTag:706];
            [BDNewAddrDict setObject:city.text forKey:@"city"];
            UITextField *emailAddress = [_BIAddressForm viewWithTag:716];
            [BDNewAddrDict setObject:emailAddress.text forKey:@"emailAddress"];
            UITextField *zip = [_BIAddressForm viewWithTag:707];
            [BDNewAddrDict setObject:zip.text forKey:@"postcode"];
            UITextField *phone = [_BIAddressForm viewWithTag:708];
            [BDNewAddrDict setObject:phone.text forKey:@"telephone"];
            UITextField *fax = [_BIAddressForm viewWithTag:709];
            [BDNewAddrDict setObject:fax.text forKey:@"fax"];
            UITextField *state = [_BIAddressForm viewWithTag:710];
            [BDNewAddrDict setObject:savenewBillingAddress forKey:@"save_in_address_book"];
            
            if([isBillingStateId isEqualToString:@"1"])
            {
                [BDNewAddrDict setObject:billingStateId forKey:@"region_id"];
                [BDNewAddrDict setObject:@"" forKey:@"region"];
            }
            else
            {
                [BDNewAddrDict setObject:@"" forKey:@"region_id"];
                [BDNewAddrDict setObject:state.text forKey:@"region"];
            }
            [BDNewAddrDict setObject:billingCountryId forKey:@"country_id"];
        }
        
        if([shipToThisOrDifferentAddress isEqualToString:@"1"])
            [BDDict setObject:@"1" forKey:@"use_for_shipping"];
        else
            [BDDict setObject:@"0" forKey:@"use_for_shipping"];
        
        [BDDict setObject:BDNewAddrDict forKey:@"newAddress"];
        NSData *jsonBillData = [NSJSONSerialization dataWithJSONObject:BDDict options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonBillString = [[NSString alloc] initWithData:jsonBillData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"billingData=%@&", jsonBillString];
        
        if([selectedshippingId isEqualToString:@"0"] && [shipToThisOrDifferentAddress isEqualToString:@"2"] && [useBillingAddress isEqualToString:@"0"])
        {
            [SDDict setObject:@"New Address" forKey:@"addressId"];
            UITextField *firstName = [_SIAddressForm viewWithTag:601];
            [SDNewAddrDict setObject:firstName.text forKey:@"firstName"];
            UITextField *lastName = [_SIAddressForm viewWithTag:602];
            [SDNewAddrDict setObject:lastName.text forKey:@"lastName"];
            UITextField *company = [_SIAddressForm viewWithTag:603];
            [SDNewAddrDict setObject:company.text forKey:@"company"];
            UITextField *street1 = [_SIAddressForm viewWithTag:604];
            UITextField *street2 = [_SIAddressForm viewWithTag:605];
            [SDStreet addObject:street1.text];
            [SDStreet addObject:street2.text];
            [SDNewAddrDict setObject:SDStreet forKey:@"street"];
            UITextField *city = [_SIAddressForm viewWithTag:606];
            [SDNewAddrDict setObject:city.text forKey:@"city"];
            UITextField *emailAddress = [_SIAddressForm viewWithTag:616];
            [SDNewAddrDict setObject:emailAddress.text forKey:@"emailAddress"];
            UITextField *zip = [_SIAddressForm viewWithTag:607];
            [SDNewAddrDict setObject:zip.text forKey:@"postcode"];
            UITextField *phone = [_SIAddressForm viewWithTag:608];
            [SDNewAddrDict setObject:phone.text forKey:@"telephone"];
            UITextField *fax = [_SIAddressForm viewWithTag:609];
            [SDNewAddrDict setObject:fax.text forKey:@"fax"];
            UITextField *state = [_SIAddressForm viewWithTag:610];
            [SDNewAddrDict setObject:savenewShippingAddress forKey:@"save_in_address_book"];
            
            if([isShippingStateId isEqualToString:@"1"])
            {
                [SDNewAddrDict setObject:shippingStateId forKey:@"region_id"];
                [SDNewAddrDict setObject:@"" forKey:@"region"];
            }
            else
            {
                [SDNewAddrDict setObject:@"" forKey:@"region_id"];
                [SDNewAddrDict setObject:state.text forKey:@"region"];
            }
            [SDNewAddrDict setObject:shippingCountryId forKey:@"country_id"];
        }
        else
            if([useBillingAddress isEqualToString:@"1"] && [isBillingFormOpen isEqualToString:@"1"])
            {
                [SDDict setObject:@"New Address" forKey:@"addressId"];
                UITextField *firstName = [_BIAddressForm viewWithTag:701];
                [SDNewAddrDict setObject:firstName.text forKey:@"firstName"];
                UITextField *lastName = [_BIAddressForm viewWithTag:702];
                [SDNewAddrDict setObject:lastName.text forKey:@"lastName"];
                UITextField *company = [_BIAddressForm viewWithTag:703];
                [SDNewAddrDict setObject:company.text forKey:@"company"];
                UITextField *street1 = [_BIAddressForm viewWithTag:704];
                UITextField *street2 = [_BIAddressForm viewWithTag:705];
                [SDStreet addObject:street1.text];
                [SDStreet addObject:street2.text];
                [SDNewAddrDict setObject:SDStreet forKey:@"street"];
                UITextField *city = [_BIAddressForm viewWithTag:706];
                [SDNewAddrDict setObject:city.text forKey:@"city"];
                UITextField *emailAddress = [_BIAddressForm viewWithTag:716];
                [SDNewAddrDict setObject:emailAddress.text forKey:@"emailAddress"];
                UITextField *zip = [_BIAddressForm viewWithTag:707];
                [SDNewAddrDict setObject:zip.text forKey:@"postcode"];
                UITextField *phone = [_BIAddressForm viewWithTag:708];
                [SDNewAddrDict setObject:phone.text forKey:@"telephone"];
                UITextField *fax = [_BIAddressForm viewWithTag:709];
                [SDNewAddrDict setObject:fax.text forKey:@"fax"];
                UITextField *state = [_BIAddressForm viewWithTag:710];
                [SDNewAddrDict setObject:savenewBillingAddress forKey:@"save_in_address_book"];
                if([isBillingStateId isEqualToString:@"1"]){
                    [SDNewAddrDict setObject:billingStateId forKey:@"region_id"];
                    [SDNewAddrDict setObject:@"" forKey:@"region"];
                }
                else{
                    [SDNewAddrDict setObject:@"" forKey:@"region_id"];
                    [SDNewAddrDict setObject:state.text forKey:@"region"];
                }
                [SDNewAddrDict setObject:billingCountryId forKey:@"country_id"];
                
            }
            else
                [SDDict setObject:selectedshippingId forKey:@"addressId"];
        [SDDict setObject:useBillingAddress forKey:@"same_as_billing"];
        [SDDict setObject:SDNewAddrDict forKey:@"newAddress"];
        if([selectedbillingId isEqualToString:@"0"] && [shipToThisOrDifferentAddress isEqualToString:@"1"])
            [SDDict setObject:@"New Address" forKey:@"addressId"];
        NSData *jsonShipData = [NSJSONSerialization dataWithJSONObject:SDDict options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonShipString = [[NSString alloc] initWithData:jsonShipData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"shippingData=%@", jsonShipString];
        
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/checkout/getstepthreenfourData" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"stepOrderReview"]){
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"shippingMethod=%@&", selectedShippingMethod];
        [post appendFormat:@"method=%@", selectedPaymentMethod];
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/checkout/getorderreviewData" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"saveOrder"]){
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"method=%@", selectedPaymentMethod];
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/checkout/saveOrder" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"login"]){
        [post appendFormat:@"storeId=%@&", storeId];
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        if(websiteId == nil){
            [post appendFormat:@"websiteId=%@&", DEFAULT_WEBSITE_ID];
            [preferences setObject:DEFAULT_WEBSITE_ID forKey:@"websiteId"];
            [preferences synchronize];
        }
        else
            [post appendFormat:@"websiteId=%@&", websiteId];
        UITextField *email = [_GRLInnerMainContainer viewWithTag:600];
        UITextField *password = [_GRLInnerMainContainer viewWithTag:601];
        [post appendFormat:@"username=%@&", email.text];
        [post appendFormat:@"password=%@", password.text];
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/customer/logIn" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
    
    else if([whichApiDataToprocess isEqual:@"forgotpassword"]){
        [post appendFormat:@"storeId=%@&", storeId];
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        if(websiteId == nil){
            [post appendFormat:@"websiteId=%@&", DEFAULT_WEBSITE_ID];
            [preferences setObject:DEFAULT_WEBSITE_ID forKey:@"websiteId"];
            [preferences synchronize];
        }
        else
            [post appendFormat:@"websiteId=%@&", websiteId];
        [post appendFormat:@"emailAddress=%@",forgotPasswordEmail];
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/customer/forgotpasswordPost" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"getbraintreetoken"])
    {
        //mobikulhttp/customer/getbraintreetoken
        // mobikulhttp/customer/getbraintreepayment
        
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/customer/getbraintreetoken" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;

    }
    else if ([whichApiDataToprocess isEqual:@"getbraintreepayment"])
    {
        [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/customer/getbraintreepayment" signal:@"HttpPostMetod"];
        globalObjectCheckOut.delegate = self;
    }
}


-(void)doFurtherProcessingWithResult
{
    if(isAlertVisible == 1)
    {
        isAlertVisible = 0;
        
        [self.view setUserInteractionEnabled:YES];
        
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
        
        NSString *customerId = [preferences objectForKey:@"customerId"];
        
        if([whichApiDataToprocess isEqual:@"steponetwo"])
        {
            [_BIContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_SIContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_SMContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_PIContainer setValue:@"NO" forKeyPath:@"hidden"];
            [_ORContainer setValue:@"NO" forKeyPath:@"hidden"];
            
            [_guestRegisterLoginContainer setValue:@"YES" forKeyPath:@"hidden"];
            mainCollection = collection;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////                                    /////////////////////////////////////////////////////
            //////////////////////////////////////////// Creating Billing Information Block /////////////////////////////////////////////////////
            ////////////////////////////////////////////                                    /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            [_BIHeadingLabel setText:[NSString stringWithFormat:@"%d BILLING INFORMATION", stepCount]];
            _BIHeadingLabel.userInteractionEnabled = YES;
            _BIHeadingLabel.tag = 691;
            _guestRegisterLoginContainerHeightConstraint.constant = 0;
            UITapGestureRecognizer *openStep1Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStep:)];
            openStep1Gesture.numberOfTapsRequired = 1;
            [_BIHeadingLabel addGestureRecognizer:openStep1Gesture];
            stepCount++;
            [_BIInternalHeadingLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [_BIInternalHeadingLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
            [_BIInternalHeadingLabel setText:@"Select a billing address from your address book or enter a new address."];
            addressPickerData = [[NSMutableArray alloc] init];
            
            if([mainCollection[@"address"] count] > 0 && customerId != nil)
            {
                for(int i=0; i<[mainCollection[@"address"] count]; i++)
                {
                    NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:i];
                    
                    if(i == 0)
                        selectedbillingId = addressDict[@"id"];
                    
                    [addressPickerData addObject:addressDict[@"value"]];
                }
                [addressPickerData addObject:@"New Address"];
                _BIAddressPicker.tag = 751;
                _BIAddressPicker.delegate = self;
            }
            
            NBAY = 5;
            UILabel *requiredStar;
            [_BIAddressForm setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            NSDictionary *firstNameLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize firstNameLabelStringSize = [@"First Name" sizeWithAttributes:firstNameLabelAttributes];
            CGFloat firstNameLabelWidth = firstNameLabelStringSize.width;
            UILabel *firstNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, firstNameLabelWidth, 25)];
            [firstNameLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [firstNameLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [firstNameLabel setText:@"First Name"];
            [_BIAddressForm addSubview:firstNameLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(firstNameLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            NBAY += 30;
            UITextField *firstNameField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            firstNameField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            firstNameField.textColor = [UIColor blackColor];
            firstNameField.tag = 701;
            firstNameField.textAlignment = NSTextAlignmentLeft;
            firstNameField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:firstNameField];
            NBAY += 35;
            NSDictionary *lastNameLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize lastNameLabelStringSize = [@"Last Name" sizeWithAttributes:lastNameLabelAttributes];
            CGFloat lastNameLabelWidth = lastNameLabelStringSize.width;
            UILabel *lastNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, lastNameLabelWidth, 25)];
            [lastNameLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [lastNameLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [lastNameLabel setText:@"Last Name"];
            [_BIAddressForm addSubview:lastNameLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(lastNameLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            NBAY += 30;
            UITextField *lastNameField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            lastNameField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            lastNameField.textColor = [UIColor blackColor];
            lastNameField.tag = 702;
            lastNameField.textAlignment = NSTextAlignmentLeft;
            lastNameField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:lastNameField];
            NBAY += 35;
            NSDictionary *companyLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize companyLabelStringSize = [@"Company" sizeWithAttributes:companyLabelAttributes];
            CGFloat companyLabelWidth = companyLabelStringSize.width;
            UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, companyLabelWidth, 25)];
            [companyLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [companyLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [companyLabel setText:@"Company"];
            [_BIAddressForm addSubview:companyLabel];
            NBAY += 30;
            UITextField *companyField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            companyField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            companyField.textColor = [UIColor blackColor];
            companyField.tag = 703;
            companyField.textAlignment = NSTextAlignmentLeft;
            companyField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:companyField];
            
            
            NBAY += 35;
            NSDictionary *emailLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize emailLabelStringSize = [@"Email Address" sizeWithAttributes:emailLabelAttributes];
            CGFloat emailLabelWidth = emailLabelStringSize.width;
            UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, emailLabelWidth, 25)];
            [emailLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [emailLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [emailLabel setText:@"Email Address"];
            [_BIAddressForm addSubview:emailLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(emailLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            NBAY += 30;
            UITextField *emailField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            emailField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            emailField.textColor = [UIColor blackColor];
            emailField.tag = 716;
            emailField.textAlignment = NSTextAlignmentLeft;
            emailField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:emailField];
            
            
            if(customerId != nil)
            {
                [emailField setEnabled: NO];
                emailField.text = [preferences objectForKey:@"customerEmail"];
            }
            
            NBAY += 35;
            NSDictionary *streetAddressLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize streetAddressLabelStringSize = [@"Street Address 1" sizeWithAttributes:streetAddressLabelAttributes];
            CGFloat streetAddressLabelWidth = streetAddressLabelStringSize.width;
            UILabel *streetAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, streetAddressLabelWidth, 25)];
            [streetAddressLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [streetAddressLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [streetAddressLabel setText:@"Street Address 1"];
            [_BIAddressForm addSubview:streetAddressLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(streetAddressLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            
            NBAY += 30;
            UITextField *streetAddressField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            streetAddressField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            streetAddressField.textColor = [UIColor blackColor];
            streetAddressField.tag = 704;
            streetAddressField.textAlignment = NSTextAlignmentLeft;
            streetAddressField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:streetAddressField];
            
            NBAY += 35;
            NSDictionary *streetAddress1LabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize streetAddress1LabelStringSize = [@"Street Address 2" sizeWithAttributes:streetAddress1LabelAttributes];
            CGFloat streetAddress1LabelWidth = streetAddress1LabelStringSize.width;
            UILabel *streetAddress1Label = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, streetAddress1LabelWidth, 25)];
            [streetAddress1Label setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [streetAddress1Label setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [streetAddress1Label setText:@"Street Address 2"];
            [_BIAddressForm addSubview:streetAddress1Label];
            
            NBAY += 30;
            UITextField *streetAddress1Field = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            streetAddress1Field.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            streetAddress1Field.textColor = [UIColor blackColor];
            streetAddress1Field.tag = 705;
            streetAddress1Field.textAlignment = NSTextAlignmentLeft;
            streetAddress1Field.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:streetAddress1Field];
            
            NBAY += 35;
            NSDictionary *cityLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize cityLabelStringSize = [@"City" sizeWithAttributes:cityLabelAttributes];
            CGFloat cityLabelWidth = cityLabelStringSize.width;
            UILabel *cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, cityLabelWidth, 25)];
            [cityLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [cityLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [cityLabel setText:@"City"];
            [_BIAddressForm addSubview:cityLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(cityLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            
            NBAY += 30;
            UITextField *cityField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            cityField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            cityField.textColor = [UIColor blackColor];
            cityField.tag = 706;
            cityField.textAlignment = NSTextAlignmentLeft;
            cityField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:cityField];
            
            NBAY += 35;
            NSDictionary *zipLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize zipLabelStringSize = [@"Zip/Postal Code" sizeWithAttributes:zipLabelAttributes];
            CGFloat zipLabelWidth = zipLabelStringSize.width;
            UILabel *zipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, zipLabelWidth, 25)];
            [zipLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [zipLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [zipLabel setText:@"Zip/Postal Code"];
            [_BIAddressForm addSubview:zipLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(zipLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            
            NBAY += 30;
            UITextField *zipField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            zipField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            zipField.textColor = [UIColor blackColor];
            zipField.tag = 707;
            zipField.textAlignment = NSTextAlignmentLeft;
            zipField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:zipField];
            
            NBAY += 35;
            NSDictionary *phoneLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize phoneLabelStringSize = [@"Telephone" sizeWithAttributes:phoneLabelAttributes];
            CGFloat phoneLabelWidth = phoneLabelStringSize.width;
            UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, phoneLabelWidth, 25)];
            [phoneLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [phoneLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [phoneLabel setText:@"Telephone"];
            [_BIAddressForm addSubview:phoneLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(phoneLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            
            NBAY += 30;
            UITextField *phoneField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            phoneField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            phoneField.textColor = [UIColor blackColor];
            phoneField.tag = 708;
            [phoneField setKeyboardType:UIKeyboardTypePhonePad];
            phoneField.textAlignment = NSTextAlignmentLeft;
            phoneField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:phoneField];
            
            NBAY += 35;
            NSDictionary *faxLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize faxLabelStringSize = [@"Fax" sizeWithAttributes:faxLabelAttributes];
            CGFloat faxLabelWidth = faxLabelStringSize.width;
            UILabel *faxLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, faxLabelWidth, 25)];
            [faxLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [faxLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [faxLabel setText:@"Fax"];
            [_BIAddressForm addSubview:faxLabel];
            
            NBAY += 30;
            UITextField *faxField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            faxField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            faxField.textColor = [UIColor blackColor];
            faxField.tag = 709;
            [faxField setKeyboardType:UIKeyboardTypePhonePad];
            faxField.textAlignment = NSTextAlignmentLeft;
            faxField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:faxField];
            
            NBAY += 35;
            NSDictionary *countryLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize countryLabelStringSize = [@"Country" sizeWithAttributes:countryLabelAttributes];
            CGFloat countryLabelWidth = countryLabelStringSize.width;
            UILabel *countryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, countryLabelWidth, 25)];
            [countryLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [countryLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [countryLabel setText:@"Country"];
            [_BIAddressForm addSubview:countryLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(countryLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            
            NBAY += 7;
            countryPickerData = [[NSMutableArray alloc] init];
            
            for(int i=0; i<[mainCollection[@"countryData"] count]; i++)
            {
                NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:i];
                if(i == 0)
                    billingCountryId = countryDict[@"country_id"];
                [countryPickerData addObject:countryDict[@"name"]];
            }
            
            UIPickerView *countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, NBAY + 5, SCREEN_WIDTH-30, 75)];
            countryPicker.tag = 752;
            countryPicker.showsSelectionIndicator = YES;
            countryPicker.delegate = self;
            [_BIAddressForm addSubview:countryPicker];
            
            NBAY += 80;
            NSDictionary *stateLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize stateLabelStringSize = [@"State/Province" sizeWithAttributes:stateLabelAttributes];
            CGFloat stateLabelWidth = stateLabelStringSize.width;
            UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NBAY, stateLabelWidth, 25)];
            [stateLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [stateLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [stateLabel setText:@"State/Province"];
            [_BIAddressForm addSubview:stateLabel];
            requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(stateLabelWidth+8, NBAY, 10, 25)];
            [requiredStar setText:@"*"];
            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_BIAddressForm addSubview:requiredStar];
            
            NBAY += 5;
            UIPickerView *statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 75)];
            statePicker.tag = 753;
            [statePicker setValue:@"YES" forKeyPath:@"hidden"];
            statePicker.showsSelectionIndicator = YES;
            statePicker.delegate = self;
            [_BIAddressForm addSubview:statePicker];
            
            NBAY += 25;
            UITextField *stateField = [[UITextField alloc] initWithFrame:CGRectMake(0, NBAY, SCREEN_WIDTH-30, 30)];
            stateField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            stateField.textColor = [UIColor blackColor];
            stateField.tag = 710;
            stateField.textAlignment = NSTextAlignmentLeft;
            stateField.borderStyle = UITextBorderStyleRoundedRect;
            [_BIAddressForm addSubview:stateField];
            
            NBAY += 40;
            UISwitch *saveInAddressBook = [[UISwitch alloc] initWithFrame:CGRectMake(0, NBAY, 55, 32)];
            saveInAddressBook.tag = 711;
            [saveInAddressBook addTarget:self action:@selector(saveInAddressBookFlip:) forControlEvents: UIControlEventValueChanged];
            [_BIAddressForm addSubview: saveInAddressBook];
            NSDictionary *saveInAddressBookAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize saveInAddressBookStringSize = [@"Save in address book" sizeWithAttributes:saveInAddressBookAttributes];
            CGFloat saveInAddressBookWidth = saveInAddressBookStringSize.width;
            UILabel *saveInAddressBookLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, NBAY+5, saveInAddressBookWidth, 25)];
            [saveInAddressBookLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [saveInAddressBookLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [saveInAddressBookLabel setText:@"Save in address book"];
            [_BIAddressForm addSubview:saveInAddressBookLabel];
            
            NBAY += 40;
            lowerActionsY = 0;
            [_BIActionContainer setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(0, lowerActionsY, 25, 25)];
            radioBtnContainer.layer.cornerRadius = 13;
            radioBtnContainer.tag = 714;
            radioBtnContainer.layer.masksToBounds = YES;
            radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
            radioBtnContainer.layer.borderWidth = 2.0f;
            UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
            [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
            radioBtn.tag = 712;
            radioBtn.layer.cornerRadius = 13;
            radioBtn.layer.masksToBounds = YES;
            radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
            radioBtn.layer.borderWidth = 5.0f;
            UITapGestureRecognizer *BISIGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(billingInfoShippingInfoSelection:)];
            BISIGesture.numberOfTapsRequired = 1;
            [radioBtn addGestureRecognizer:BISIGesture];
            [radioBtnContainer addSubview:radioBtn];
            [_BIActionContainer addSubview:radioBtnContainer];
            UILabel *shipToThisAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, lowerActionsY, SCREEN_WIDTH-60, 25)];
            [shipToThisAddressLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [shipToThisAddressLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [shipToThisAddressLabel setText:@"Ship to this address"];
            [_BIActionContainer addSubview:shipToThisAddressLabel];
            
            lowerActionsY += 30;
            UIView *radioBtnContainer1 = [[UIView alloc]initWithFrame:CGRectMake(0, lowerActionsY, 25, 25)];
            radioBtnContainer1.layer.cornerRadius = 13;
            radioBtnContainer1.tag = 714;
            radioBtnContainer1.layer.masksToBounds = YES;
            radioBtnContainer1.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
            radioBtnContainer1.layer.borderWidth = 2.0f;
            UIView *radioBtn1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
            [radioBtn1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            radioBtn1.tag = 713;
            radioBtn1.layer.cornerRadius = 13;
            radioBtn1.layer.masksToBounds = YES;
            radioBtn1.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
            radioBtn1.layer.borderWidth = 5.0f;
            UITapGestureRecognizer *BISIGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(billingInfoShippingInfoSelection:)];
            BISIGesture1.numberOfTapsRequired = 1;
            [radioBtn1 addGestureRecognizer:BISIGesture1];
            [radioBtnContainer1 addSubview:radioBtn1];
            [_BIActionContainer addSubview:radioBtnContainer1];
            UILabel *shipToDifferentAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, lowerActionsY, SCREEN_WIDTH-60, 25)];
            [shipToDifferentAddressLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [shipToDifferentAddressLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [shipToDifferentAddressLabel setText:@"Ship to different address"];
            [_BIActionContainer addSubview:shipToDifferentAddressLabel];
            
            lowerActionsY += 35;
            UIView *hr1 = [[UIView alloc]initWithFrame:CGRectMake(0, lowerActionsY, SCREEN_WIDTH-30, 1)];
            [hr1 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
            [_BIActionContainer addSubview:hr1];
            
            lowerActionsY += 11;
            UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
            continueButton.tag = 803;
            [continueButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
            continueButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
            [continueButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
            [continueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
            continueButton.frame = CGRectMake(0, lowerActionsY, SCREEN_WIDTH-30, 40);
            [_BIActionContainer addSubview:continueButton];
            
            lowerActionsY += 40;
            _BIAddressFormHeightConstraint.constant = 0;
            _BIInternalMainContainerHeightConstraint.constant = 118 + 8 + lowerActionsY + 5;
            _BIActionContainerHeightConstraint.constant = lowerActionsY;
            _BIContainerHeightConstraint.constant = 45 + 118 + lowerActionsY + 10;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////                                     //////////////////////////////////////////////////////
            ////////////////////////////////////////// Creating Shipping Information Block //////////////////////////////////////////////////////
            //////////////////////////////////////////                                     //////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            [_SIHeadingLabel setText:[NSString stringWithFormat:@"%d SHIPPING INFORMATION", stepCount]];
            _SIHeadingLabel.userInteractionEnabled = YES;
            _SIHeadingLabel.tag = 692;
            UITapGestureRecognizer *openStep2Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStep:)];
            openStep2Gesture.numberOfTapsRequired = 1;
            [_SIHeadingLabel addGestureRecognizer:openStep2Gesture];
            stepCount++;
            [_SIInternalHeadingLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [_SIInternalHeadingLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
            [_SIInternalHeadingLabel setText:@"Select a shipping address from your address book or enter a new address."];
            addressPickerData = [[NSMutableArray alloc] init];
            
            if([mainCollection[@"address"] count] > 0 && customerId != nil)
            {
                for(int i=0; i<[mainCollection[@"address"] count]; i++)
                {
                    NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:i];
                    if(i == 0)
                        selectedshippingId = addressDict[@"id"];
                    
                    [addressPickerData addObject:addressDict[@"value"]];
                }
                
                [addressPickerData addObject:@"New Address"];
                _SIAddressPicker.tag = 651;
                _SIAddressPicker.delegate = self;
            }
            
            NSAY = 5;
            UILabel *SIrequiredStar;
            [_SIAddressForm setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            NSDictionary *SIfirstNameLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIfirstNameLabelStringSize = [@"First Name" sizeWithAttributes:SIfirstNameLabelAttributes];
            CGFloat SIfirstNameLabelWidth = SIfirstNameLabelStringSize.width;
            UILabel *SIfirstNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIfirstNameLabelWidth, 25)];
            [SIfirstNameLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIfirstNameLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIfirstNameLabel setText:@"First Name"];
            [_SIAddressForm addSubview:SIfirstNameLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIfirstNameLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIfirstNameField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIfirstNameField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIfirstNameField.textColor = [UIColor blackColor];
            SIfirstNameField.tag = 601;
            SIfirstNameField.textAlignment = NSTextAlignmentLeft;
            SIfirstNameField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIfirstNameField];
            
            NSAY += 35;
            NSDictionary *SIlastNameLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIlastNameLabelStringSize = [@"Last Name" sizeWithAttributes:SIlastNameLabelAttributes];
            CGFloat SIlastNameLabelWidth = SIlastNameLabelStringSize.width;
            UILabel *SIlastNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIlastNameLabelWidth, 25)];
            [SIlastNameLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIlastNameLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIlastNameLabel setText:@"Last Name"];
            [_SIAddressForm addSubview:SIlastNameLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIlastNameLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIlastNameField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIlastNameField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIlastNameField.textColor = [UIColor blackColor];
            SIlastNameField.tag = 602;
            SIlastNameField.textAlignment = NSTextAlignmentLeft;
            SIlastNameField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIlastNameField];
            
            NSAY += 35;
            NSDictionary *SIcompanyLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIcompanyLabelStringSize = [@"Company" sizeWithAttributes:SIcompanyLabelAttributes];
            CGFloat SIcompanyLabelWidth = SIcompanyLabelStringSize.width;
            UILabel *SIcompanyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIcompanyLabelWidth, 25)];
            [SIcompanyLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIcompanyLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIcompanyLabel setText:@"Company"];
            [_SIAddressForm addSubview:SIcompanyLabel];
            
            NSAY += 30;
            UITextField *SIcompanyField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIcompanyField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIcompanyField.textColor = [UIColor blackColor];
            SIcompanyField.tag = 603;
            SIcompanyField.textAlignment = NSTextAlignmentLeft;
            SIcompanyField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIcompanyField];
            
            NSAY += 35;
            NSDictionary *SIemailLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIemailLabelStringSize = [@"Email Address" sizeWithAttributes:SIemailLabelAttributes];
            CGFloat SIemailLabelWidth = SIemailLabelStringSize.width;
            UILabel *SIemailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIemailLabelWidth, 25)];
            [SIemailLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIemailLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIemailLabel setText:@"Email Address"];
            [_SIAddressForm addSubview:SIemailLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIemailLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIemailField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIemailField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIemailField.textColor = [UIColor blackColor];
            SIemailField.tag = 616;
            SIemailField.textAlignment = NSTextAlignmentLeft;
            SIemailField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIemailField];
            
            if(customerId != nil)
            {
                [SIemailField setEnabled: NO];
                SIemailField.text = [preferences objectForKey:@"customerEmail"];
            }
            
            NSAY += 35;
            NSDictionary *SIstreetAddressLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIstreetAddressLabelStringSize = [@"Street Address 1" sizeWithAttributes:SIstreetAddressLabelAttributes];
            CGFloat SIstreetAddressLabelWidth = SIstreetAddressLabelStringSize.width;
            UILabel *SIstreetAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIstreetAddressLabelWidth, 25)];
            [SIstreetAddressLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIstreetAddressLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIstreetAddressLabel setText:@"Street Address 1"];
            [_SIAddressForm addSubview:SIstreetAddressLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIstreetAddressLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIstreetAddressField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIstreetAddressField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIstreetAddressField.textColor = [UIColor blackColor];
            SIstreetAddressField.tag = 604;
            SIstreetAddressField.textAlignment = NSTextAlignmentLeft;
            SIstreetAddressField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIstreetAddressField];
            
            NSAY += 35;
            NSDictionary *SIstreetAddress1LabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIstreetAddress1LabelStringSize = [@"Street Address 2" sizeWithAttributes:SIstreetAddress1LabelAttributes];
            CGFloat SIstreetAddress1LabelWidth = SIstreetAddress1LabelStringSize.width;
            UILabel *SIstreetAddress1Label = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIstreetAddress1LabelWidth, 25)];
            [SIstreetAddress1Label setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIstreetAddress1Label setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIstreetAddress1Label setText:@"Street Address 2"];
            [_SIAddressForm addSubview:SIstreetAddress1Label];
            
            NSAY += 30;
            UITextField *SIstreetAddress1Field = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIstreetAddress1Field.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIstreetAddress1Field.textColor = [UIColor blackColor];
            SIstreetAddress1Field.tag = 605;
            SIstreetAddress1Field.textAlignment = NSTextAlignmentLeft;
            SIstreetAddress1Field.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIstreetAddress1Field];
            
            NSAY += 35;
            NSDictionary *SIcityLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIcityLabelStringSize = [@"City" sizeWithAttributes:SIcityLabelAttributes];
            CGFloat SIcityLabelWidth = SIcityLabelStringSize.width;
            UILabel *SIcityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIcityLabelWidth, 25)];
            [SIcityLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIcityLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIcityLabel setText:@"City"];
            [_SIAddressForm addSubview:SIcityLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIcityLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIcityField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIcityField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIcityField.textColor = [UIColor blackColor];
            SIcityField.tag = 606;
            SIcityField.textAlignment = NSTextAlignmentLeft;
            SIcityField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIcityField];
            
            NSAY += 35;
            NSDictionary *SIzipLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIzipLabelStringSize = [@"Zip/Postal Code" sizeWithAttributes:SIzipLabelAttributes];
            CGFloat SIzipLabelWidth = SIzipLabelStringSize.width;
            UILabel *SIzipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIzipLabelWidth, 25)];
            [SIzipLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIzipLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIzipLabel setText:@"Zip/Postal Code"];
            [_SIAddressForm addSubview:SIzipLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIzipLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIzipField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIzipField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIzipField.textColor = [UIColor blackColor];
            SIzipField.tag = 607;
            SIzipField.textAlignment = NSTextAlignmentLeft;
            SIzipField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIzipField];
            
            NSAY += 35;
            NSDictionary *SIphoneLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIphoneLabelStringSize = [@"Telephone" sizeWithAttributes:SIphoneLabelAttributes];
            CGFloat SIphoneLabelWidth = SIphoneLabelStringSize.width;
            UILabel *SIphoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIphoneLabelWidth, 25)];
            [SIphoneLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIphoneLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIphoneLabel setText:@"Telephone"];
            [_SIAddressForm addSubview:SIphoneLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIphoneLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 30;
            UITextField *SIphoneField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIphoneField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIphoneField.textColor = [UIColor blackColor];
            SIphoneField.tag = 608;
            [SIphoneField setKeyboardType:UIKeyboardTypePhonePad];
            SIphoneField.textAlignment = NSTextAlignmentLeft;
            SIphoneField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIphoneField];
            
            NSAY += 35;
            NSDictionary *SIfaxLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIfaxLabelStringSize = [@"Fax" sizeWithAttributes:SIfaxLabelAttributes];
            CGFloat SIfaxLabelWidth = SIfaxLabelStringSize.width;
            UILabel *SIfaxLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIfaxLabelWidth, 25)];
            [SIfaxLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIfaxLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIfaxLabel setText:@"Fax"];
            [_SIAddressForm addSubview:SIfaxLabel];
            
            NSAY += 30;
            UITextField *SIfaxField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIfaxField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIfaxField.textColor = [UIColor blackColor];
            SIfaxField.tag = 609;
            [SIfaxField setKeyboardType:UIKeyboardTypePhonePad];
            SIfaxField.textAlignment = NSTextAlignmentLeft;
            SIfaxField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIfaxField];
            
            NSAY += 35;
            NSDictionary *SIcountryLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIcountryLabelStringSize = [@"Country" sizeWithAttributes:SIcountryLabelAttributes];
            CGFloat SIcountryLabelWidth = SIcountryLabelStringSize.width;
            UILabel *SIcountryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIcountryLabelWidth, 25)];
            [SIcountryLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIcountryLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIcountryLabel setText:@"Country"];
            [_SIAddressForm addSubview:SIcountryLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIcountryLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 7;
            countryPickerData = [[NSMutableArray alloc] init];
            for(int i=0; i<[mainCollection[@"countryData"] count]; i++)
            {
                NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:i];
                if(i == 0)
                    shippingCountryId = countryDict[@"country_id"];
                [countryPickerData addObject:countryDict[@"name"]];
            }
            UIPickerView *SIcountryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, NSAY + 5, SCREEN_WIDTH-30, 75)];
            SIcountryPicker.tag = 652;
            SIcountryPicker.showsSelectionIndicator = YES;
            SIcountryPicker.delegate = self;
            [_SIAddressForm addSubview:SIcountryPicker];
            
            NSAY += 80;
            NSDictionary *SIstateLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIstateLabelStringSize = [@"State/Province" sizeWithAttributes:SIstateLabelAttributes];
            CGFloat SIstateLabelWidth = SIstateLabelStringSize.width;
            UILabel *SIstateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, NSAY, SIstateLabelWidth, 25)];
            [SIstateLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIstateLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIstateLabel setText:@"State/Province"];
            [_SIAddressForm addSubview:SIstateLabel];
            SIrequiredStar = [[UILabel alloc] initWithFrame:CGRectMake(SIstateLabelWidth+8, NSAY, 10, 25)];
            [SIrequiredStar setText:@"*"];
            [SIrequiredStar setTextColor:[GlobalData colorWithHexString:@"df280a"]];
            [_SIAddressForm addSubview:SIrequiredStar];
            
            NSAY += 5;
            UIPickerView *SIstatePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 75)];
            SIstatePicker.tag = 653;
            [SIstatePicker setValue:@"YES" forKeyPath:@"hidden"];
            SIstatePicker.showsSelectionIndicator = YES;
            SIstatePicker.delegate = self;
            [_SIAddressForm addSubview:SIstatePicker];
            
            NSAY += 25;
            UITextField *SIstateField = [[UITextField alloc] initWithFrame:CGRectMake(0, NSAY, SCREEN_WIDTH-30, 30)];
            SIstateField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
            SIstateField.textColor = [UIColor blackColor];
            SIstateField.tag = 610;
            SIstateField.textAlignment = NSTextAlignmentLeft;
            SIstateField.borderStyle = UITextBorderStyleRoundedRect;
            [_SIAddressForm addSubview:SIstateField];
            
            NSAY += 40;
            UISwitch *SIsaveInAddressBook = [[UISwitch alloc] initWithFrame:CGRectMake(0, NSAY, 55, 32)];
            SIsaveInAddressBook.tag = 611;
            [SIsaveInAddressBook addTarget:self action:@selector(saveInAddressBookFlip:) forControlEvents: UIControlEventValueChanged];
            [_SIAddressForm addSubview: SIsaveInAddressBook];
            NSDictionary *SIsaveInAddressBookAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize SIsaveInAddressBookStringSize = [@"Save in address book" sizeWithAttributes:SIsaveInAddressBookAttributes];
            CGFloat SIsaveInAddressBookWidth = SIsaveInAddressBookStringSize.width;
            UILabel *SIsaveInAddressBookLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, NSAY+5, SIsaveInAddressBookWidth, 25)];
            [SIsaveInAddressBookLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [SIsaveInAddressBookLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [SIsaveInAddressBookLabel setText:@"Save in address book"];
            [_SIAddressForm addSubview:SIsaveInAddressBookLabel];
            
            NSAY += 40;
            [_SIActionContainer setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            SIlowerActionsY = 0;
            UISwitch *useBillingAddressSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, SIlowerActionsY, 55, 32)];
            useBillingAddressSwitch.tag = 612;
            [useBillingAddressSwitch addTarget:self action:@selector(useBillingAddressFlip:) forControlEvents: UIControlEventValueChanged];
            [_SIActionContainer addSubview:useBillingAddressSwitch];
            NSDictionary *useBillingAddressAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
            CGSize useBillingAddressStringSize = [@"Use Billing Address" sizeWithAttributes:useBillingAddressAttributes];
            CGFloat useBillingAddressWidth = useBillingAddressStringSize.width;
            UILabel *useBillingAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, SIlowerActionsY+5, useBillingAddressWidth, 25)];
            [useBillingAddressLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [useBillingAddressLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [useBillingAddressLabel setText:@"Use Billing Address"];
            [_SIActionContainer addSubview:useBillingAddressLabel];
            
            SIlowerActionsY += 40;
            UIView *hr2 = [[UIView alloc]initWithFrame:CGRectMake(0, SIlowerActionsY, SCREEN_WIDTH-30, 1)];
            [hr2 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
            [_SIActionContainer addSubview:hr2];
            SIlowerActionsY += 11;
            UIButton *SIcontinueButton = [UIButton buttonWithType:UIButtonTypeCustom];
            SIcontinueButton.tag = 804;
            [SIcontinueButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
            SIcontinueButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
            [SIcontinueButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
            [SIcontinueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
            SIcontinueButton.frame = CGRectMake(0, SIlowerActionsY, SCREEN_WIDTH-30, 40);
            [_SIActionContainer addSubview:SIcontinueButton];
            
            SIlowerActionsY += 45;
            _SIContainerHeightConstraint.constant = 35;
            _SIInternalMainContainerHeightConstraint.constant = 0;
            _SIAddressFormHeightConstraint.constant = 0;
            _SIActionContainerHeightConstraint.constant = 0;
            
            [_SMHeadingLabel setText:[NSString stringWithFormat:@"%d SHIPPING METHOD", stepCount]];
            _SMHeadingLabel.userInteractionEnabled = YES;
            _SMHeadingLabel.tag = 693;
            UITapGestureRecognizer *openStep3Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStep:)];
            openStep3Gesture.numberOfTapsRequired = 1;
            [_SMHeadingLabel addGestureRecognizer:openStep3Gesture];
            stepCount++;
            _SMInternalMainContainerHeightConstraint.constant = 0;
            _SMContainerHeightConstraint.constant = 35;
            
            [_PIHeadingLabel setText:[NSString stringWithFormat:@"%d PAYMENT INFORMATION", stepCount]];
            _PIHeadingLabel.userInteractionEnabled = YES;
            _PIHeadingLabel.tag = 694;
            UITapGestureRecognizer *openStep4Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStep:)];
            openStep4Gesture.numberOfTapsRequired = 1;
            [_PIHeadingLabel addGestureRecognizer:openStep4Gesture];
            stepCount++;
            _PIInternalMainContainerHeightConstraint.constant = 0;
            _PIContainerHeightConstraint.constant = 35;
            
            [_ORHeadingLabel setText:[NSString stringWithFormat:@"%d ORDER REVIEW", stepCount]];
            _ORHeadingLabel.userInteractionEnabled = YES;
            _ORHeadingLabel.tag = 695;
            UITapGestureRecognizer *openStep5Gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStep:)];
            openStep5Gesture.numberOfTapsRequired = 1;
            [_ORHeadingLabel addGestureRecognizer:openStep5Gesture];
            stepCount++;
            _ORInternalMainContainerHeightConstraint.constant = 0;
            _ORContainerHeightConstraint.constant = 35;
            
            if(customerId == nil)
                [self initializeCheckout];
            
            if([mainCollection[@"address"] count] < 1)
            {
                isBillingFormOpen = @"1";
                _BIInternalHeadingHeightConstraint.constant = 0;
                _BIAddressPickerHeightConstraint.constant = 0;
                _BIInternalHeadingLabel.hidden = YES;
                _BIAddressPicker.hidden = YES;
                if([heightModificationDoneOfBillingInfo isEqualToString:@"0"]){
                    heightModificationDoneOfBillingInfo = @"1";
                    selectedbillingId = @"0";
                    choosingNewBillingAddress = @"1";
                    [_BIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
                    //  _BIAdreessFormTop.constant = _BIAdreessFormTop.constant - (45 + 118);
                    _BIAddressFormHeightConstraint.constant = NBAY;
                    _BIInternalMainContainerHeightConstraint.constant = 18 + NBAY + 8 + lowerActionsY + 5;
                    _BIActionContainerHeightConstraint.constant = lowerActionsY;
                    _BIContainerHeightConstraint.constant = 45 + 18 + NBAY + lowerActionsY + 10;
                }
            }
        }
        else
            if([whichApiDataToprocess isEqual:@"stepthreefour"])
            {
                stepThreenFourCollection = collection;
                selectedShippingMethod = @"";selectedPaymentMethod = @"";
                
                if([stepThreenFourCollection[@"error"] boolValue])
                {
                    NSString *notifyMessage = stepThreenFourCollection[@"message"];
                    NSString *newString = [notifyMessage stringByReplacingOccurrencesOfString:@"\\"  withString:@""];
                    NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@"\""  withString:@""];
                    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"["  withString:@""];
                    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"]"  withString:@""];
                    NSString *newString4 = [newString3 stringByReplacingOccurrencesOfString:@","  withString:@"\n"];
                    
                    UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:newString4 message:nil preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                    [errorAlert addAction:noBtn];
                    [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                }
                else
                {
                    for(UIView *subViews in _SMInternalMainContainer.subviews)
                        [subViews removeFromSuperview];
                    for(UIView *subViews in _PIInternalMainContainer.subviews)
                        [subViews removeFromSuperview];
                    _BIContainerHeightConstraint.constant = 35;
                    _BIInternalMainContainerHeightConstraint.constant = 0;
                    _BIAddressFormHeightConstraint.constant = 0;
                    _BIActionContainerHeightConstraint.constant = 0;
                    _SIContainerHeightConstraint.constant = 35;
                    _SIInternalMainContainerHeightConstraint.constant = 0;
                    _SIAddressFormHeightConstraint.constant = 0;
                    _SIActionContainerHeightConstraint.constant = 0;
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////////////////////////////////////////                                //////////////////////////////////////////////////////////
                    //////////////////////////////////////// Creating Shipping Method Block //////////////////////////////////////////////////////////
                    ////////////////////////////////////////                                //////////////////////////////////////////////////////////
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    SMY = 5;
                    
                    if([stepThreenFourCollection[@"shippingMethods"] count] == 0)
                    {
                        NSString *noShippingMethod = @"Sorry, no quotes are available for this order at this time.";
                        UILabel *noShippingMethodLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, SMY, SCREEN_WIDTH - 20, 55)];
                        [noShippingMethodLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        noShippingMethodLabel.lineBreakMode = UILineBreakModeWordWrap;
                        noShippingMethodLabel.numberOfLines = 0;
                        [noShippingMethodLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:20]];
                        [noShippingMethodLabel setText:noShippingMethod];
                        [_SMInternalMainContainer addSubview:noShippingMethodLabel];
                        SMY += 60;
                    }
                    
                    for(int i=0; i<[stepThreenFourCollection[@"shippingMethods"] count]; i++)
                    {
                        NSDictionary *SMDict = [stepThreenFourCollection[@"shippingMethods"] objectAtIndex:i];
                        NSDictionary *shippingMethodMainLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:20]};
                        CGSize shippingMethodMainLabelStringSize = [SMDict[@"title"] sizeWithAttributes:shippingMethodMainLabelAttributes];
                        CGFloat shippingMethodMainLabelWidth = shippingMethodMainLabelStringSize.width;
                        UILabel *shippingMethodMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, SMY, shippingMethodMainLabelWidth, 25)];
                        [shippingMethodMainLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [shippingMethodMainLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:20]];
                        [shippingMethodMainLabel setText:SMDict[@"title"]];
                        [_SMInternalMainContainer addSubview:shippingMethodMainLabel];
                        
                        SMY += 30;
                        
                        for(int j=0; j<[SMDict[@"method"] count]; j++)
                        {
                            UIView *radioBtnHolder = [[UIView alloc]initWithFrame:CGRectMake(15, SMY, SCREEN_WIDTH-40, 25)];
                            radioBtnHolder.tag = 999;
                            NSDictionary *SMEachDict = [SMDict[@"method"] objectAtIndex:j];
                            UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
                            radioBtnContainer.layer.cornerRadius = 13;
                            radioBtnContainer.tag = i;
                            radioBtnContainer.layer.masksToBounds = YES;
                            radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
                            radioBtnContainer.layer.borderWidth = 2.0f;
                            UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
                            [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                            radioBtn.tag = j;
                            radioBtn.layer.cornerRadius = 13;
                            radioBtn.layer.masksToBounds = YES;
                            radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
                            radioBtn.layer.borderWidth = 5.0f;
                            UITapGestureRecognizer *SMSelectionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shippingMethodSelection:)];
                            SMSelectionGesture.numberOfTapsRequired = 1;
                            [radioBtn addGestureRecognizer:SMSelectionGesture];
                            [radioBtnContainer addSubview:radioBtn];
                            [radioBtnHolder addSubview:radioBtnContainer];
                            UILabel *shippingMethodLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, SCREEN_WIDTH-70, 25)];
                            [shippingMethodLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [shippingMethodLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                            [shippingMethodLabel setText:[NSString stringWithFormat:@"%@ %@", SMEachDict[@"label"], SMEachDict[@"price"]]];
                            [radioBtnHolder addSubview:shippingMethodLabel];
                            [_SMInternalMainContainer addSubview:radioBtnHolder];
                            SMY += 30;
                        }
                        if(i<[stepThreenFourCollection[@"shippingMethods"] count]-1)
                            SMY += 20;
                    }
                    UIView *hr3 = [[UIView alloc]initWithFrame:CGRectMake(5, SMY, SCREEN_WIDTH-30, 1)];
                    [hr3 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_SMInternalMainContainer addSubview:hr3];
                    SMY += 11;
                    UIButton *continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    continueButton.tag = 805;
                    [continueButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
                    continueButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
                    [continueButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [continueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
                    continueButton.frame = CGRectMake(5, SMY, SCREEN_WIDTH-30, 40);
                    [_SMInternalMainContainer addSubview:continueButton];
                    SMY += 45;
                    _SMContainerHeightConstraint.constant = 35 + SMY + 5;
                    _SMInternalMainContainerHeightConstraint.constant = SMY;
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////                               ////////////////////////////////////////////////////
                    /////////////////////////////////////////////// Creating Payment Method Block ////////////////////////////////////////////////////
                    ///////////////////////////////////////////////                               ////////////////////////////////////////////////////
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    PIY = 5;
                    if([stepThreenFourCollection[@"paymentMethods"] count] == 0){
                        NSString *noPaymentMethod = @"Sorry, no Payment Methods are available for this order at this time.";
                        UILabel *noPaymentMethodLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, PIY, SCREEN_WIDTH - 20, 55)];
                        [noPaymentMethodLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        noPaymentMethodLabel.lineBreakMode = UILineBreakModeWordWrap;
                        noPaymentMethodLabel.numberOfLines = 0;
                        [noPaymentMethodLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:20]];
                        [noPaymentMethodLabel setText:noPaymentMethod];
                        [_PIInternalMainContainer addSubview:noPaymentMethodLabel];
                        PIY += 60;
                    }
                    
                    NSArray *allowedPaymentMethodCodes = [NSArray arrayWithObjects:@"mpbraintree",@"mpadaptivepayment",nil];
                    
                    for(int i=0; i<[stepThreenFourCollection[@"paymentMethods"] count]; i++)
                    {
                        NSDictionary *PIEachDict = [stepThreenFourCollection[@"paymentMethods"] objectAtIndex:i];
                        
                        if([allowedPaymentMethodCodes containsObject:PIEachDict[@"code"]])
                        {
                            UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(5, PIY, 25, 25)];
                            radioBtnContainer.layer.cornerRadius = 13;
                            radioBtnContainer.tag = 999;
                            radioBtnContainer.layer.masksToBounds = YES;
                            radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
                            radioBtnContainer.layer.borderWidth = 2.0f;
                            UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
                            [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                            radioBtn.tag = i;
                            radioBtn.layer.cornerRadius = 13;
                            radioBtn.layer.masksToBounds = YES;
                            radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
                            radioBtn.layer.borderWidth = 5.0f;
                            UITapGestureRecognizer *SMSelectionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(paymentMethodSelection:)];
                            SMSelectionGesture.numberOfTapsRequired = 1;
                            [radioBtn addGestureRecognizer:SMSelectionGesture];
                            [radioBtnContainer addSubview:radioBtn];
                            [_PIInternalMainContainer addSubview:radioBtnContainer];
                            UILabel *paymentMethodTitle = [[UILabel alloc] initWithFrame:CGRectMake(35, PIY, SCREEN_WIDTH-60, 25)];
                            [paymentMethodTitle setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [paymentMethodTitle setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                            [paymentMethodTitle setText:PIEachDict[@"title"]];
                            [_PIInternalMainContainer addSubview:paymentMethodTitle];
                            PIY += 30;
                        }
                    }
                    
                    UIView *hr4 = [[UIView alloc]initWithFrame:CGRectMake(5, PIY, SCREEN_WIDTH-30, 1)];
                    [hr4 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_PIInternalMainContainer addSubview:hr4];
                    PIY += 11;
                    UIButton *PIcontinueButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    PIcontinueButton.tag = 806;
                    [PIcontinueButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
                    PIcontinueButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
                    [PIcontinueButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [PIcontinueButton setTitle:@"CONTINUE" forState:UIControlStateNormal];
                    PIcontinueButton.frame = CGRectMake(5, PIY, SCREEN_WIDTH-30, 40);
                    [_PIInternalMainContainer addSubview:PIcontinueButton];
                    PIY += 45;
                    _PIContainerHeightConstraint.constant = 35 ;
                    _PIInternalMainContainerHeightConstraint.constant = 0;
                }
            }
            else
                if([whichApiDataToprocess isEqual:@"stepOrderReview"]){
                    orderReviewCollection = collection;
                    _PIContainerHeightConstraint.constant = 35;
                    _PIInternalMainContainerHeightConstraint.constant = 0;
                    ORY = 5;
                    for (long i=_ORInternalMainContainer.subviews.count-1; i>=0; i--)
                        [[_ORInternalMainContainer.subviews objectAtIndex:i] removeFromSuperview];
                    
                    NSDictionary *BAHAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]};
                    CGSize BAHStringSize = [@"BILLING ADDRESS" sizeWithAttributes:BAHAttributes];
                    CGFloat BAHWidth = BAHStringSize.width;
                    UILabel *BAHLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, BAHWidth, 25)];
                    [BAHLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [BAHLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]];
                    [BAHLabel setText:@"BILLING ADDRESS"];
                    [_ORInternalMainContainer addSubview:BAHLabel];
                    ORY += 40;
                    NSString *selectedBillingAddress = @"";
                    if([isBillingFormOpen isEqualToString:@"0"] || ![selectedbillingId isEqualToString:@"0"]){
                        for(int i=0; i<[mainCollection[@"address"] count]; i++) {
                            NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:i];
                            if(selectedbillingId == addressDict[@"id"])
                                selectedBillingAddress = addressDict[@"value"];
                        }
                    }
                    else{
                        UITextField *firstName = [_BIAddressForm viewWithTag:701];
                        UITextField *lastName = [_BIAddressForm viewWithTag:702];
                        UITextField *street1 = [_BIAddressForm viewWithTag:704];
                        UITextField *city = [_BIAddressForm viewWithTag:706];
                        UITextField *zip = [_BIAddressForm viewWithTag:707];
                        UITextField *phone = [_BIAddressForm viewWithTag:708];
                        UITextField *state = [_BIAddressForm viewWithTag:710];
                        NSString *countryName = @"";
                        int row = 0;
                        for(int i=0; i<[mainCollection[@"countryData"] count]; i++) {
                            NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:i];
                            if([billingCountryId isEqualToString:countryDict[@"country_id"]]){
                                countryName = countryDict[@"name"];
                                row = i;
                            }
                        }
                        if([isBillingStateId isEqualToString:@"1"]){
                            NSString *stateName = @"";
                            NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:row];
                            for(int i=0; i<[countryDict[@"states"] count]; i++) {
                                NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:i];
                                if([billingStateId isEqualToString:stateDict[@"region_id"]])
                                    stateName = stateDict[@"name"];
                            }
                            selectedBillingAddress = [NSString stringWithFormat:@"%@ %@ %@, %@, %@, %@  %@ T:%@", firstName.text, lastName.text, street1.text, city.text, stateName, zip.text, countryName, phone.text];
                        }
                        else
                            selectedBillingAddress = [NSString stringWithFormat:@"%@ %@ %@, %@, %@, %@  %@ T:%@", firstName.text, lastName.text, street1.text, city.text, state.text, zip.text, countryName, phone.text];
                    }
                    UILabel *BALabel = [[UILabel alloc] initWithFrame:CGRectMake(15, ORY, SCREEN_WIDTH-40, 25)];
                    [BALabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [BALabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [BALabel setText:selectedBillingAddress];
                    [_ORInternalMainContainer addSubview:BALabel];
                    ORY += 30;
                    UIView *hr1 = [[UIView alloc]initWithFrame:CGRectMake(5, ORY, SCREEN_WIDTH-30, 1)];
                    [hr1 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_ORInternalMainContainer addSubview:hr1];
                    ORY += 16;
                    NSDictionary *SAHAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]};
                    CGSize SAHStringSize = [@"SHIPPING ADDRESS" sizeWithAttributes:SAHAttributes];
                    CGFloat SAHWidth = SAHStringSize.width;
                    UILabel *SAHLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, SAHWidth, 25)];
                    [SAHLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [SAHLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]];
                    [SAHLabel setText:@"SHIPPING ADDRESS"];
                    [_ORInternalMainContainer addSubview:SAHLabel];
                    ORY += 40;
                    NSString *selectedShippingAddress = @"";
                    if([selectedshippingId isEqualToString:@"0"] && [shipToThisOrDifferentAddress isEqualToString:@"2"] && [useBillingAddress isEqualToString:@"0"]){
                        UITextField *firstName = [_SIAddressForm viewWithTag:601];
                        UITextField *lastName = [_SIAddressForm viewWithTag:602];
                        UITextField *street1 = [_SIAddressForm viewWithTag:604];
                        UITextField *city = [_SIAddressForm viewWithTag:606];
                        UITextField *zip = [_SIAddressForm viewWithTag:607];
                        UITextField *phone = [_SIAddressForm viewWithTag:608];
                        UITextField *state = [_SIAddressForm viewWithTag:610];
                        NSString *countryName = @"";
                        int row = 0;
                        for(int i=0; i<[mainCollection[@"countryData"] count]; i++) {
                            NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:i];
                            if([shippingCountryId isEqualToString:countryDict[@"country_id"]]){
                                countryName = countryDict[@"name"];
                                row = i;
                            }
                        }
                        if([isShippingStateId isEqualToString:@"1"]){
                            NSString *stateName = @"";
                            NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:row];
                            for(int i=0; i<[countryDict[@"states"] count]; i++) {
                                NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:i];
                                if([shippingStateId isEqualToString:stateDict[@"region_id"]])
                                    stateName = stateDict[@"name"];
                            }
                            selectedShippingAddress = [NSString stringWithFormat:@"%@ %@ %@, %@, %@, %@  %@ T:%@", firstName.text, lastName.text, street1.text, city.text, stateName, zip.text, countryName, phone.text];
                        }
                        else
                            selectedShippingAddress = [NSString stringWithFormat:@"%@ %@ %@, %@, %@, %@  %@ T:%@", firstName.text, lastName.text, street1.text, city.text, state.text, zip.text, countryName, phone.text];
                    }
                    else
                        if([selectedshippingId isEqualToString:@"0"] && [shipToThisOrDifferentAddress isEqualToString:@"2"] && [useBillingAddress isEqualToString:@"1"]){
                            UITextField *firstName = [_BIAddressForm viewWithTag:701];
                            UITextField *lastName = [_BIAddressForm viewWithTag:702];
                            UITextField *street1 = [_BIAddressForm viewWithTag:704];
                            UITextField *city = [_BIAddressForm viewWithTag:706];
                            UITextField *zip = [_BIAddressForm viewWithTag:707];
                            UITextField *phone = [_BIAddressForm viewWithTag:708];
                            UITextField *state = [_BIAddressForm viewWithTag:710];
                            NSString *countryName = @"";
                            int row = 0;
                            for(int i=0; i<[mainCollection[@"countryData"] count]; i++) {
                                NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:i];
                                if([billingCountryId isEqualToString:countryDict[@"country_id"]]){
                                    countryName = countryDict[@"name"];
                                    row = i;
                                }
                            }
                            if([isShippingStateId isEqualToString:@"1"]){
                                NSString *stateName = @"";
                                NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:row];
                                for(int i=0; i<[countryDict[@"states"] count]; i++) {
                                    NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:i];
                                    if([shippingStateId isEqualToString:stateDict[@"region_id"]])
                                        stateName = stateDict[@"name"];
                                }
                                selectedShippingAddress = [NSString stringWithFormat:@"%@ %@ %@, %@, %@, %@  %@ T:%@", firstName.text, lastName.text, street1.text, city.text, stateName, zip.text, countryName, phone.text];
                            }
                            else
                                selectedShippingAddress = [NSString stringWithFormat:@"%@ %@ %@, %@, %@, %@  %@ T:%@", firstName.text, lastName.text, street1.text, city.text, state.text, zip.text, countryName, phone.text];
                        }
                        else{
                            for(int i=0; i<[mainCollection[@"address"] count]; i++) {
                                NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:i];
                                if(selectedbillingId == addressDict[@"id"])
                                    selectedShippingAddress = addressDict[@"value"];
                            }
                        }
                    UILabel *SALabel = [[UILabel alloc] initWithFrame:CGRectMake(15, ORY, SCREEN_WIDTH-40, 25)];
                    [SALabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [SALabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [SALabel setText:selectedShippingAddress];
                    [_ORInternalMainContainer addSubview:SALabel];
                    ORY += 30;
                    UIView *hr2 = [[UIView alloc]initWithFrame:CGRectMake(5, ORY, SCREEN_WIDTH-30, 1)];
                    [hr2 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_ORInternalMainContainer addSubview:hr2];
                    ORY += 16;
                    NSDictionary *SMHAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]};
                    CGSize SMHStringSize = [@"SHIPPING METHOD" sizeWithAttributes:SMHAttributes];
                    CGFloat SMHWidth = SMHStringSize.width;
                    UILabel *SMHLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, SMHWidth, 25)];
                    [SMHLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [SMHLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]];
                    [SMHLabel setText:@"SHIPPING METHOD"];
                    [_ORInternalMainContainer addSubview:SMHLabel];
                    ORY += 40;
                    NSString *shippingMethodString = @"";
                    for(int i=0; i<[stepThreenFourCollection[@"shippingMethods"] count]; i++) {
                        NSDictionary *SMDict = [stepThreenFourCollection[@"shippingMethods"] objectAtIndex:i];
                        for(int j=0; j<[SMDict[@"method"] count]; j++) {
                            NSDictionary *SMEachDict = [SMDict[@"method"] objectAtIndex:j];
                            if([selectedShippingMethod isEqualToString:SMEachDict[@"code"]])
                                shippingMethodString = [NSString stringWithFormat:@"%@ %@", SMEachDict[@"label"], SMEachDict[@"price"]];
                        }
                    }
                    UILabel *SMLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, ORY, SCREEN_WIDTH-40, 25)];
                    [SMLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [SMLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [SMLabel setText:shippingMethodString];
                    [_ORInternalMainContainer addSubview:SMLabel];
                    ORY += 30;
                    UIView *hr3 = [[UIView alloc]initWithFrame:CGRectMake(5, ORY, SCREEN_WIDTH-30, 1)];
                    [hr3 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_ORInternalMainContainer addSubview:hr3];
                    ORY += 16;
                    NSDictionary *PMHAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]};
                    CGSize PMHStringSize = [@"PAYMENT METHOD" sizeWithAttributes:PMHAttributes];
                    CGFloat PMHWidth = PMHStringSize.width;
                    UILabel *PMHLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, PMHWidth, 25)];
                    [PMHLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [PMHLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:22]];
                    [PMHLabel setText:@"PAYMENT METHOD"];
                    [_ORInternalMainContainer addSubview:PMHLabel];
                    ORY += 40;
                    NSString *paymentMethodString = @"";
                    for(int i=0; i<[stepThreenFourCollection[@"paymentMethods"] count]; i++) {
                        NSDictionary *PIEachDict = [stepThreenFourCollection[@"paymentMethods"] objectAtIndex:i];
                        if([selectedPaymentMethod isEqualToString:PIEachDict[@"code"]])
                            paymentMethodString = PIEachDict[@"title"];
                    }
                    UILabel *PMLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, ORY, SCREEN_WIDTH-40, 25)];
                    [PMLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [PMLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [PMLabel setText:paymentMethodString];
                    [_ORInternalMainContainer addSubview:PMLabel];
                    ORY += 30;
                    UIView *hr4 = [[UIView alloc]initWithFrame:CGRectMake(5, ORY, SCREEN_WIDTH-30, 1)];
                    [hr4 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_ORInternalMainContainer addSubview:hr4];
                    ORY += 16;
                    for(int i=0; i<[orderReviewCollection[@"orderReviewData"][@"items"] count]; i++) {
                        NSDictionary *eachProductDict = [orderReviewCollection[@"orderReviewData"][@"items"] objectAtIndex:i];
                        NSDictionary *PNAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:20]};
                        CGSize PNStringSize = [eachProductDict[@"productName"] sizeWithAttributes:PNAttributes];
                        CGFloat PNWidth = PNStringSize.width;
                        UILabel *PNLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, PNWidth, 25)];
                        [PNLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [PNLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:20]];
                        [PNLabel setText:eachProductDict[@"productName"]];
                        [_ORInternalMainContainer addSubview:PNLabel];
                        ORY += 25;
                        NSDictionary *prePriceLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14]};
                        CGSize prePriceLabelStringSize = [@"Price : " sizeWithAttributes:prePriceLabelAttributes];
                        CGFloat prePriceLabelWidth = prePriceLabelStringSize.width;
                        UILabel *prePriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, ORY, prePriceLabelWidth, 15)];
                        [prePriceLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [prePriceLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                        [prePriceLabel setText:@"Price : "];
                        [_ORInternalMainContainer addSubview:prePriceLabel];
                        NSDictionary *priceLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14]};
                        CGSize priceLabelStringSize = [eachProductDict[@"price"] sizeWithAttributes:priceLabelAttributes];
                        CGFloat priceLabelWidth = priceLabelStringSize.width;
                        UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(prePriceLabelWidth+10, ORY, priceLabelWidth, 15)];
                        [priceLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [priceLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                        [priceLabel setText:eachProductDict[@"price"]];
                        [_ORInternalMainContainer addSubview:priceLabel];
                        ORY += 15;
                        NSDictionary *preQtyLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14]};
                        CGSize preQtyLabelStringSize = [@"QTY : " sizeWithAttributes:preQtyLabelAttributes];
                        CGFloat preQtyLabelWidth = preQtyLabelStringSize.width;
                        UILabel *preQtyLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, ORY, preQtyLabelWidth, 15)];
                        [preQtyLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preQtyLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                        [preQtyLabel setText:@"QTY : "];
                        [_ORInternalMainContainer addSubview:preQtyLabel];
                        NSString* qty = [NSString stringWithFormat:@"%ld", [eachProductDict[@"qty"] integerValue]];
                        NSDictionary *qtyLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14]};
                        CGSize qtyLabelStringSize = [qty sizeWithAttributes:qtyLabelAttributes];
                        CGFloat qtyLabelWidth = qtyLabelStringSize.width;
                        UILabel *qtyLabel = [[UILabel alloc] initWithFrame:CGRectMake(preQtyLabelWidth+10, ORY, qtyLabelWidth, 15)];
                        [qtyLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [qtyLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                        [qtyLabel setText:qty];
                        [_ORInternalMainContainer addSubview:qtyLabel];
                        ORY += 15;
                        NSDictionary *preSubtotalLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14]};
                        CGSize preSubtotalLabelStringSize = [@"SUBTOTAL : " sizeWithAttributes:preSubtotalLabelAttributes];
                        CGFloat preSubtotalLabelWidth = preSubtotalLabelStringSize.width;
                        UILabel *preSubtotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, ORY, preSubtotalLabelWidth, 15)];
                        [preSubtotalLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preSubtotalLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                        [preSubtotalLabel setText:@"SUBTOTAL : "];
                        [_ORInternalMainContainer addSubview:preSubtotalLabel];
                        NSDictionary *subtotalLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14]};
                        CGSize subtotalLabelStringSize = [eachProductDict[@"subTotal"] sizeWithAttributes:subtotalLabelAttributes];
                        CGFloat subtotalLabelWidth = subtotalLabelStringSize.width;
                        UILabel *subtotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(preSubtotalLabelWidth+10, ORY, subtotalLabelWidth, 15)];
                        [subtotalLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [subtotalLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                        [subtotalLabel setText:eachProductDict[@"subTotal"]];
                        [_ORInternalMainContainer addSubview:subtotalLabel];
                        ORY += 30;
                    }
                    UIView *hr5 = [[UIView alloc]initWithFrame:CGRectMake(5, ORY, SCREEN_WIDTH-30, 1)];
                    [hr5 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_ORInternalMainContainer addSubview:hr5];
                    ORY += 11;
                    int thisScreenWidth = SCREEN_WIDTH-30;
                    if([orderReviewCollection[@"orderReviewData"] objectForKey:@"subtotal"]){
                        UILabel *preSubTotal = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, ((thisScreenWidth/3)*2), 25)];
                        [preSubTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preSubTotal setBackgroundColor:[UIColor clearColor]];
                        [preSubTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preSubTotal setText:orderReviewCollection[@"orderReviewData"][@"subtotal"][@"title"]];
                        [_ORInternalMainContainer addSubview:preSubTotal];
                        UILabel *subTotal = [[UILabel alloc] initWithFrame:CGRectMake(((thisScreenWidth/3)*2)+5, ORY, thisScreenWidth/3, 25)];
                        [subTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [subTotal setBackgroundColor:[UIColor clearColor]];
                        [subTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [subTotal setText:orderReviewCollection[@"orderReviewData"][@"subtotal"][@"value"]];
                        subTotal.textAlignment = NSTextAlignmentRight;
                        [_ORInternalMainContainer addSubview:subTotal];
                        ORY += 30;
                    }
                    if([orderReviewCollection[@"orderReviewData"] objectForKey:@"discount"]){
                        UILabel *preDiscount = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, ((thisScreenWidth/3)*2), 25)];
                        [preDiscount setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preDiscount setBackgroundColor:[UIColor clearColor]];
                        [preDiscount setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preDiscount setText:orderReviewCollection[@"orderReviewData"][@"discount"][@"title"]];
                        [_ORInternalMainContainer addSubview:preDiscount];
                        UILabel *discount = [[UILabel alloc] initWithFrame:CGRectMake(((thisScreenWidth/3)*2)+5, ORY, thisScreenWidth/3, 25)];
                        [discount setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [discount setBackgroundColor:[UIColor clearColor]];
                        [discount setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [discount setText:orderReviewCollection[@"orderReviewData"][@"discount"][@"value"]];
                        discount.textAlignment = NSTextAlignmentRight;
                        [_ORInternalMainContainer addSubview:discount];
                        ORY += 30;
                    }
                    if([orderReviewCollection[@"orderReviewData"] objectForKey:@"tax"]){
                        UILabel *preTax = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, ((thisScreenWidth/3)*2), 25)];
                        [preTax setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preTax setBackgroundColor:[UIColor clearColor]];
                        [preTax setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preTax setText:orderReviewCollection[@"orderReviewData"][@"tax"][@"title"]];
                        [_ORInternalMainContainer addSubview:preTax];
                        UILabel *tax = [[UILabel alloc] initWithFrame:CGRectMake(((thisScreenWidth/3)*2)+5, ORY, thisScreenWidth/3, 25)];
                        [tax setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [tax setBackgroundColor:[UIColor clearColor]];
                        [tax setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [tax setText:orderReviewCollection[@"orderReviewData"][@"tax"][@"value"]];
                        tax.textAlignment = NSTextAlignmentRight;
                        [_ORInternalMainContainer addSubview:tax];
                        ORY += 30;
                    }
                    if([orderReviewCollection[@"orderReviewData"] objectForKey:@"shipping"]){
                        UILabel *preShipping = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, ((thisScreenWidth/3)*2), 25)];
                        [preShipping setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preShipping setBackgroundColor:[UIColor clearColor]];
                        [preShipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preShipping setText:orderReviewCollection[@"orderReviewData"][@"shipping"][@"title"]];
                        [_ORInternalMainContainer addSubview:preShipping];
                        UILabel *shipping = [[UILabel alloc] initWithFrame:CGRectMake(((thisScreenWidth/3)*2)+5, ORY, thisScreenWidth/3, 25)];
                        [shipping setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [shipping setBackgroundColor:[UIColor clearColor]];
                        [shipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [shipping setText:orderReviewCollection[@"orderReviewData"][@"shipping"][@"value"]];
                        shipping.textAlignment = NSTextAlignmentRight;
                        [_ORInternalMainContainer addSubview:shipping];
                        ORY += 30;
                    }
                    if([orderReviewCollection[@"orderReviewData"] objectForKey:@"grandtotal"]){
                        UILabel *preShipping = [[UILabel alloc] initWithFrame:CGRectMake(5, ORY, ((thisScreenWidth/3)*2), 30)];
                        [preShipping setTextColor:[GlobalData colorWithHexString:@"000000"]];
                        [preShipping setBackgroundColor:[UIColor clearColor]];
                        [preShipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:23.0f]];
                        [preShipping setText:orderReviewCollection[@"orderReviewData"][@"grandtotal"][@"title"]];
                        [_ORInternalMainContainer addSubview:preShipping];
                        UILabel *shipping = [[UILabel alloc] initWithFrame:CGRectMake(((thisScreenWidth/3)*2)+5, ORY, thisScreenWidth/3, 30)];
                        [shipping setTextColor:[GlobalData colorWithHexString:@"000000"]];
                        [shipping setBackgroundColor:[UIColor clearColor]];
                        [shipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:23.0f]];
                        [shipping setText:orderReviewCollection[@"orderReviewData"][@"grandtotal"][@"value"]];
                        shipping.textAlignment = NSTextAlignmentRight;
                        [_ORInternalMainContainer addSubview:shipping];
                        ORY += 35;
                    }
                    UIView *hr6 = [[UIView alloc]initWithFrame:CGRectMake(5, ORY, SCREEN_WIDTH-30, 1)];
                    [hr6 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [_ORInternalMainContainer addSubview:hr6];
                    ORY += 11;
                    UIButton *saveOrderButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    saveOrderButton.tag = 807;
                    [saveOrderButton addTarget:self action:@selector(actionButtonEventHandler:) forControlEvents:UIControlEventTouchUpInside];
                    saveOrderButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
                    [saveOrderButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [saveOrderButton setTitle:@"SAVE ORDER" forState:UIControlStateNormal];
                    saveOrderButton.frame = CGRectMake(5, ORY, SCREEN_WIDTH-30, 40);
                    [_ORInternalMainContainer addSubview:saveOrderButton];
                    ORY += 45;
                    _ORContainerHeightConstraint.constant = 35 + ORY + 5;
                    _ORInternalMainContainerHeightConstraint.constant = ORY;
                }
                else
                    if([whichApiDataToprocess isEqual:@"saveOrder"]){
                        saveOrderCollection = collection;
                        if(saveOrderCollection != NULL){
                            NSString *error = [NSString stringWithFormat:@"%ld", [saveOrderCollection[@"error"] integerValue]];
                            if([error isEqualToString:@"0"]){
                                [self performSelector:@selector(openSuccessPage) withObject:nil afterDelay:1];
                            }
                            else
                                [ToastView showToastInParentView:self.view withText:@"Sorry!! Something went wrong please try again later." withStatus:@"error" withDuaration:5.0];
                        }
                    }
                    else
                        if([whichApiDataToprocess isEqual: @"login"]){
                            loginCollection = collection;
                            if([loginCollection[@"status"] isEqual:@"true"]){
                                UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
                                [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", loginCollection[@"cartCount"]]];
                                [preferences setObject:loginCollection[@"customerName"] forKey:@"customerName"];
                                [preferences setObject:loginCollection[@"customerId"] forKey:@"customerId"];
                                [preferences setObject:loginCollection[@"customerEmail"] forKey:@"customerEmail"];
                                [preferences setObject:@"1" forKey:@"isLoggedIn"];
                                [preferences setObject:nil forKey:@"quoteId"];
                                [preferences synchronize];
                                GlobalData *obj = [GlobalData getInstance];
                                [obj.menuListData replaceObjectAtIndex:0 withObject:@"Log Out"];
                                [obj.menuListData addObject:[globalObjectCheckOut.languageBundle localizedStringForKey:@"Bolgs" value:@"" table:nil]];
                                [self viewDidLoad];
                            }
                            else{
                                UIAlertController * loginErrorNotification = [UIAlertController alertControllerWithTitle:@"Login Error" message:loginCollection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                [loginErrorNotification addAction:noBtn];
                                [self.parentViewController presentViewController:loginErrorNotification animated:YES completion:nil];
                            }
                        }
                        else
                            if([whichApiDataToprocess isEqual:@"forgotpassword"]){
                                forgotPasswordCollection = collection;
                                UIAlertController *forgotpasswordNotification = [UIAlertController alertControllerWithTitle:@"" message:forgotPasswordCollection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                [forgotpasswordNotification addAction:noBtn];
                                [self.parentViewController presentViewController:forgotpasswordNotification animated:YES completion:nil];
                            }
                            else
                                if([whichApiDataToprocess isEqual:@"getbraintreetoken"])
                                {
                                    [self setBrainTreePayment];
                                }
                                else
                                    if([whichApiDataToprocess isEqual:@"getbraintreepayment"])
                                    {
                                        whichApiDataToprocess = @"saveOrder";
                                        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                        
                                        if(savedSessionId == nil)
                                            [self loginRequest];
                                        else
                                            [self callingHttppApi];
                                    }
    }
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(void)openSuccessPage{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self performSegueWithIdentifier:@"orderSuccessSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"orderSuccessSegue"]) {
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        OrderSuccess *destViewController = [[navigationController viewControllers] lastObject];
        destViewController.orderId = saveOrderCollection[@"orderId"];
        destViewController.incrementId = saveOrderCollection[@"incrementId"];
        destViewController.canReorder = saveOrderCollection[@"canReorder"];
    }
}

-(void)useBillingAddressFlip:(id) sender {
    UISwitch *onoff = (UISwitch *) sender;
    if(onoff.on)
        useBillingAddress = @"1";
    else
        useBillingAddress = @"0";
}

-(void)saveInAddressBookFlip:(id) sender {
    UISwitch *onoff = (UISwitch *) sender;
    if([sender tag] == 711){
        if(onoff.on)
            savenewBillingAddress = @"1";
        else
            savenewBillingAddress = @"0";
    }
    else{
        if(onoff.on)
            savenewShippingAddress = @"1";
        else
            savenewShippingAddress = @"0";
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    if(pickerView.tag == 751 || pickerView.tag == 651){
        return [addressPickerData count];
    }
    else
        if(pickerView.tag == 752 || pickerView.tag == 652){
            return [countryPickerData count];
        }
        else{
            return [statePickerData count];
        }
}

-(NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 751 || pickerView.tag == 651){
        return [addressPickerData objectAtIndex:row];
    }
    else
        if(pickerView.tag == 752 || pickerView.tag == 652){
            statePickerState = @"0";
            return [countryPickerData objectAtIndex:row];
        }
        else{
            statePickerState = @"1";
            return [statePickerData objectAtIndex:row];
        }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;{
    if(pickerView.tag == 751){
        if(row == [mainCollection[@"address"] count]){
            isBillingFormOpen = @"1";
            
            if([heightModificationDoneOfBillingInfo isEqualToString:@"0"]){
                heightModificationDoneOfBillingInfo = @"1";
                selectedbillingId = @"0";
                choosingNewBillingAddress = @"1";
                [_BIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
                _BIAddressFormHeightConstraint.constant = NBAY;
                _BIInternalMainContainerHeightConstraint.constant = 118 + NBAY + 8 + lowerActionsY + 5;
                _BIActionContainerHeightConstraint.constant = lowerActionsY;
                _BIContainerHeightConstraint.constant = 45 + 118 + NBAY + lowerActionsY + 10;
            }
        }
        else{
            isBillingFormOpen = @"0";
            NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:row];
            selectedbillingId = addressDict[@"id"];
            if([heightModificationDoneOfBillingInfo isEqualToString:@"1"]){
                heightModificationDoneOfBillingInfo = @"0";
                [_BIAddressForm setValue:@"YES" forKeyPath:@"hidden"];
                _BIAddressFormHeightConstraint.constant = 0;
                _BIInternalMainContainerHeightConstraint.constant = 118 + 8 + lowerActionsY + 5;
                _BIActionContainerHeightConstraint.constant = lowerActionsY;
                _BIContainerHeightConstraint.constant = 45 + 118 + lowerActionsY + 10;
            }
        }
    }
    else
        if(pickerView.tag == 651){
            if(row == [mainCollection[@"address"] count]){
                isShippingFormOpen = @"1";
                if([heightModificationDoneOfShippingInfo isEqualToString:@"0"]){
                    heightModificationDoneOfShippingInfo = @"1";
                    selectedshippingId = @"0";
                    choosingNewShippingAddress = @"1";
                    [_SIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
                    _SIAddressFormHeightConstraint.constant = NSAY;
                    _SIInternalMainContainerHeightConstraint.constant = 118 + NSAY + SIlowerActionsY + 8;
                    _SIContainerHeightConstraint.constant = 45 + 118 + NSAY + SIlowerActionsY + 5;
                    _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                }
            }
            else{
                isShippingFormOpen = @"0";
                NSDictionary *addressDict = [mainCollection[@"address"] objectAtIndex:row];
                selectedshippingId = addressDict[@"id"];
                if([heightModificationDoneOfShippingInfo isEqualToString:@"1"]){
                    heightModificationDoneOfShippingInfo = @"0";
                    [_SIAddressForm setValue:@"YES" forKeyPath:@"hidden"];
                    _SIAddressFormHeightConstraint.constant = 0;
                    _SIInternalMainContainerHeightConstraint.constant = 118 + SIlowerActionsY + 8;
                    _SIContainerHeightConstraint.constant = 45 + 118 + SIlowerActionsY + 5;
                    _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                }
            }
            if(selectedshippingId != selectedbillingId){
                useBillingAddress = @"0";
                shipToThisOrDifferentAddress = @"2";
                UISwitch *onoff = [_SIActionContainer viewWithTag:612];
                [onoff setOn:NO animated:NO];
            }
        }
        else
            if(pickerView.tag == 752){
                NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:row];
                billingCountryId = countryDict[@"country_id"];
                selectedCountryIndex = row;
                if(countryDict[@"states"] != nil) {
                    isBillingStateId = @"1";
                    UITextField *stateField = [_BIAddressForm viewWithTag:710];
                    [stateField setValue:@"YES" forKeyPath:@"hidden"];
                    statePickerData = [[NSMutableArray alloc] init];
                    for(int i=0; i<[countryDict[@"states"] count]; i++) {
                        NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:i];
                        if(i == 0)
                            billingStateId = stateDict[@"region_id"];
                        [statePickerData addObject:stateDict[@"name"]];
                    }
                    UIPickerView *statePicker = [_BIAddressForm viewWithTag:753];
                    [statePicker reloadAllComponents];
                    [statePicker setValue:@"NO" forKeyPath:@"hidden"];
                }
                else{
                    statePickerState = @"0";
                    isBillingStateId = @"0";
                    statePickerData = [[NSMutableArray alloc] init];
                    UIPickerView *statePicker = [_BIAddressForm viewWithTag:753];
                    [statePicker reloadAllComponents];
                    [statePicker setValue:@"YES" forKeyPath:@"hidden"];
                    UITextField *stateField = [_BIAddressForm viewWithTag:710];
                    [stateField setValue:@"NO" forKeyPath:@"hidden"];
                }
            }
            else
                if(pickerView.tag == 753){
                    statePickerState = @"1";
                    NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:selectedCountryIndex];
                    NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:row];
                    billingStateId = stateDict[@"region_id"];
                    isBillingStateId = @"1";
                }
                else
                    if(pickerView.tag == 652){
                        NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:row];
                        shippingCountryId = countryDict[@"country_id"];
                        SIselectedCountryIndex = row;
                        if(countryDict[@"states"] != nil) {
                            isShippingStateId = @"1";
                            UITextField *stateField = [_SIAddressForm viewWithTag:610];
                            [stateField setValue:@"YES" forKeyPath:@"hidden"];
                            statePickerData = [[NSMutableArray alloc] init];
                            for(int i=0; i<[countryDict[@"states"] count]; i++) {
                                NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:i];
                                if(i == 0)
                                    shippingStateId = stateDict[@"region_id"];
                                [statePickerData addObject:stateDict[@"name"]];
                            }
                            UIPickerView *statePicker = [_SIAddressForm viewWithTag:653];
                            [statePicker reloadAllComponents];
                            [statePicker setValue:@"NO" forKeyPath:@"hidden"];
                        }
                        else{
                            statePickerState = @"0";
                            isShippingStateId = @"0";
                            statePickerData = [[NSMutableArray alloc] init];
                            UIPickerView *statePicker = [_SIAddressForm viewWithTag:653];
                            [statePicker reloadAllComponents];
                            [statePicker setValue:@"YES" forKeyPath:@"hidden"];
                            UITextField *stateField = [_SIAddressForm viewWithTag:610];
                            [stateField setValue:@"NO" forKeyPath:@"hidden"];
                        }
                    }
                    else
                        if(pickerView.tag == 653){
                            statePickerState = @"1";
                            NSDictionary *countryDict = [mainCollection[@"countryData"] objectAtIndex:SIselectedCountryIndex];
                            NSDictionary *stateDict = [countryDict[@"states"] objectAtIndex:row];
                            shippingStateId = stateDict[@"region_id"];
                            isShippingStateId = @"1";
                        }
}

-(void)openStep:(UITapGestureRecognizer *)recognizer{
    int thisTag = (int)recognizer.view.tag;
    int tappedStepNumber = thisTag-690;
    if(tappedStepNumber < currentStep){
        if(thisTag == 690){
            
        }
        else
            if(thisTag == 691){
                currentStep = 1;
                NSString *customerId = [preferences objectForKey:@"customerId"];
                if(customerId == nil){
                    _BIAddressFormHeightConstraint.constant = (NBAY - 40);
                    _BIInternalMainContainerHeightConstraint.constant = 8 + (NBAY - 40) + lowerActionsY + 23;
                    _BIActionContainerHeightConstraint.constant = lowerActionsY;
                    _BIContainerHeightConstraint.constant = 45 + (NBAY - 40) + lowerActionsY + 26;
                }
                else{
                    if([isBillingFormOpen isEqualToString:@"1"]){
                        if ([mainCollection[@"address"] count] > 0) {
                            _BIAddressFormHeightConstraint.constant = NBAY;
                            _BIInternalMainContainerHeightConstraint.constant = 118 + 8 + NBAY + lowerActionsY + 5;
                            _BIActionContainerHeightConstraint.constant = lowerActionsY;
                            _BIContainerHeightConstraint.constant = 45 + 118 + NBAY + lowerActionsY + 10;
                        }
                        else{
                            _BIAddressFormHeightConstraint.constant = NBAY;
                            _BIInternalMainContainerHeightConstraint.constant = 18 + 8 + NBAY + lowerActionsY + 5;
                            _BIActionContainerHeightConstraint.constant = lowerActionsY;
                            _BIContainerHeightConstraint.constant = 45 + 18 + NBAY + lowerActionsY + 10;
                        }
                    }
                    else{
                        _BIAddressFormHeightConstraint.constant = 0;
                        _BIInternalMainContainerHeightConstraint.constant = 118 + 8 + lowerActionsY + 5;
                        _BIContainerHeightConstraint.constant = 45 + 118 + lowerActionsY + 10;
                        _BIActionContainerHeightConstraint.constant = lowerActionsY;
                    }
                }
                _SIContainerHeightConstraint.constant = 35;
                _SIInternalMainContainerHeightConstraint.constant = 0;
                _SIAddressFormHeightConstraint.constant = 0;
                _SIActionContainerHeightConstraint.constant = 0;
                _SMContainerHeightConstraint.constant = 35;
                _SMInternalMainContainerHeightConstraint.constant = 0;
                _PIContainerHeightConstraint.constant = 35;
                _PIInternalMainContainerHeightConstraint.constant = 0;
                _ORContainerHeightConstraint.constant = 35;
                _ORInternalMainContainerHeightConstraint.constant = 0;
            }
            else
                if(thisTag == 692){
                    currentStep = 2;
                    NSString *customerId = [preferences objectForKey:@"customerId"];
                    if(customerId == nil){
                        shipToThisOrDifferentAddress = @"2";
                        useBillingAddress = @"0";
                        [_SIInternalHeadingLabel setValue:@"YES" forKeyPath:@"hidden"];
                        [_SIAddressPicker setValue:@"YES" forKeyPath:@"hidden"];
                        _SIAddressPickerHeightConstraint.constant = 0;
                        _SIInternalHeadingHeightConstraint.constant = 0;
                        [_SIAddressForm setValue:@"NO" forKeyPath:@"hidden"];
                        _SIAddressFormHeightConstraint.constant = (NSAY - 40);
                        _SIInternalMainContainerHeightConstraint.constant = 8 + (NSAY - 40) + SIlowerActionsY + 23;
                        _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                        _SIContainerHeightConstraint.constant = 45 + (NSAY - 40) + SIlowerActionsY + 26;
                    }
                    else{
                        if([isShippingFormOpen isEqualToString:@"1"]){
                            _SIAddressFormHeightConstraint.constant = NSAY;
                            if ([mainCollection[@"address"] count] > 0) {
                                _SIInternalMainContainerHeightConstraint.constant = 118 + NSAY + SIlowerActionsY + 8;
                                _SIContainerHeightConstraint.constant = 45 + 118 + NSAY + SIlowerActionsY + 5;
                                _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                            }
                            else{
                                _SIInternalMainContainerHeightConstraint.constant = 18 + NSAY + SIlowerActionsY + 8;
                                _SIContainerHeightConstraint.constant = 45 + 18 + NSAY + SIlowerActionsY + 5;
                                _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                                
                            }
                        }
                        else{
                            _SIAddressFormHeightConstraint.constant = 0;
                            _SIInternalMainContainerHeightConstraint.constant = 118 + SIlowerActionsY + 8;
                            _SIContainerHeightConstraint.constant = 45 + 118 + SIlowerActionsY + 5;
                            _SIActionContainerHeightConstraint.constant = SIlowerActionsY;
                        }
                    }
                    _SMContainerHeightConstraint.constant = 35;
                    _SMInternalMainContainerHeightConstraint.constant = 0;
                    _PIContainerHeightConstraint.constant = 35;
                    _PIInternalMainContainerHeightConstraint.constant = 0;
                    _ORContainerHeightConstraint.constant = 35;
                    _ORInternalMainContainerHeightConstraint.constant = 0;
                }
                else
                    if(thisTag == 693){
                        currentStep = 3;
                        _SMContainerHeightConstraint.constant = 35 + SMY + 5;
                        _SMInternalMainContainerHeightConstraint.constant = SMY;
                        _PIContainerHeightConstraint.constant = 35;
                        _PIInternalMainContainerHeightConstraint.constant = 0;
                        _ORContainerHeightConstraint.constant = 35;
                        _ORInternalMainContainerHeightConstraint.constant = 0;
                    }
                    else
                        if(thisTag == 694){
                            currentStep = 4;
                            _PIContainerHeightConstraint.constant = 35 + PIY + 5;
                            _PIInternalMainContainerHeightConstraint.constant = PIY;
                            _ORContainerHeightConstraint.constant = 35;
                            _ORInternalMainContainerHeightConstraint.constant = 0;
                        }
    }
}

-(void)billingInfoShippingInfoSelection:(UITapGestureRecognizer *)recognizer{
    for(UIView *subViews in _BIActionContainer.subviews){
        if(subViews.tag == recognizer.view.superview.tag){
            for(UIView *subViews1 in subViews.subviews)
                [subViews1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    if(recognizer.view.tag == 712){
        shipToThisOrDifferentAddress = @"1";
        useBillingAddress = @"1";
    }
    else{
        shipToThisOrDifferentAddress = @"2";
        useBillingAddress = @"0";
    }
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    if([sizeModified isEqualToString:@"0"]){
        sizeModified = @"1";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        _mainViewBottomConstraint.constant += keyboardSize.height;
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    if([sizeModified isEqualToString:@"1"]){
        sizeModified = @"0";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        _mainViewBottomConstraint.constant -= keyboardSize.height;
    }
}

-(void)setBrainTreePayment
{
    NSDictionary *dictTemp = (NSDictionary *)collection[@"orderReviewData"];
    
    braintreeClient = [[BTAPIClient alloc] initWithAuthorization:strToken];
    
    dropInViewController = [[BTDropInViewController alloc] initWithAPIClient:braintreeClient];
    dropInViewController.delegate = self;
    
    BTPaymentRequest *paymentRequest = [[BTPaymentRequest alloc] init];
    
    paymentRequest.summaryTitle = [[[dictTemp valueForKey:@"items"] objectAtIndex:0] valueForKey:@"productName"];
    paymentRequest.summaryDescription = [NSString stringWithFormat:@"%@ - %@",[[dictTemp valueForKey:@"grandtotal"] valueForKey:@"title"],[[dictTemp valueForKey:@"grandtotal"] valueForKey:@"value"]];//"\(dictMemberShip.valueForKey("duration")!) Month Subscription"
    paymentRequest.displayAmount = [[dictTemp valueForKey:@"grandtotal"] valueForKey:@"value"];
    paymentRequest.callToActionText = [NSString stringWithFormat:@"%@ - PURCHASE",[[dictTemp valueForKey:@"grandtotal"] valueForKey:@"value"]];//"$\(dictMemberShip.valueForKey("price")!) - PURCHASE"
    
    dropInViewController.paymentRequest = paymentRequest;
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                             target:self
                             action:@selector(userDidCancelPayment)];
    dropInViewController.navigationItem.leftBarButtonItem = item;
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:dropInViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)userDidCancelPayment
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewController:(BTDropInViewController *)viewController
  didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce
{
    // Send payment method nonce to your server for processing
    [self postNonceToServer:paymentMethodNonce.nonce];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)postNonceToServer:(NSString*)strTemp
{
    NSDictionary *dictTemp = (NSDictionary *)collection[@"orderReviewData"];
    
    whichApiDataToprocess = @"getbraintreepayment";
    
//    if(savedSessionId == nil)
//        [self loginRequest];
//    else
//        [self callingHttppApi];
    
    [GlobalData alertController:currentWindow msg:[globalObjectCheckOut.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *strSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", strSessionId];
    [post appendFormat:@"payment_method_nonce=%@&", strTemp];
    [post appendFormat:@"amount=%@&", [[dictTemp valueForKey:@"grandtotal"] valueForKey:@"unformatedValue"]];
    
    [globalObjectCheckOut callHTTPPostMethod:post api:@"mobikulhttp/customer/getbraintreepayment" signal:@"HttpPostMetod"];
    globalObjectCheckOut.delegate = self;
}

@end

