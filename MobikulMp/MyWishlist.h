//
//  MyWishList.h
//  Mobikul
//
//  Created by Ratnesh on 5/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWishList : UIViewController <NSXMLParserDelegate, UITextFieldDelegate, UITextViewDelegate,UIScrollViewDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger  isAlertVisible;
    id collection;
    NSCache *imageCache;
    NSMutableArray *massUpdateWhishList;
    NSString *whichApiToCall;
    NSString *qtyValue ,*produtId,*itemId;
    UIWindow *currentWindow;
    NSArray *arrayMainCollection;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
    NSString *reloadPageData;

}

@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UIView *wishListContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wishListContainerHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroller;

@end
