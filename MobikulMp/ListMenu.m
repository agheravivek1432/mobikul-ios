//
//  ListMenu.m
//  Mobikul
//
//  Created by Ratnesh on 12/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "ListMenu.h"
#import "GlobalData.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#import "Home.h"
#import "MakeReviewData.h"
#import "ToastView.h"

GlobalData *globalObjectListMenu;

@implementation ListMenu
GlobalData *obj;

- (void)viewDidLoad {
    [super viewDidLoad];
    preferences = [NSUserDefaults standardUserDefaults];
    obj = [GlobalData getInstance];
    globalObjectListMenu = [[GlobalData alloc] init];
    globalObjectListMenu.delegate = self;
    [self registerForKeyboardNotifications];
    keyboardSetFlag = 1;
    menuListData = obj.menuListData;
      NSInteger isSeller = [[preferences objectForKey:@"isSeller"] intValue];
    if(isSeller == 1){
    headerTitle = obj.listMenuTitleData;
        marketPlaceData = obj.marketPlaceData;
        allData =obj.completeListMenuData;
    }
    else{
        headerTitle = obj.listMenuTitleData;
        allData = obj.completeListMenuData;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    languageCode =[NSUserDefaults standardUserDefaults];
    allData = obj.completeListMenuData;
    headerTitle = obj.listMenuTitleData;
    [_menuListTable reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [allData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return [menuListData count];
   return [[allData objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *sectionContents = [allData objectAtIndex:[indexPath section]];
    NSString *contentForThisRow = [sectionContents objectAtIndex:[indexPath row]];
    cell.textLabel.text = contentForThisRow;
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //return @"   My Account";
 return [headerTitle objectAtIndex:section];
    
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
    tableViewHeaderFooterView.textLabel.textColor = [GlobalData colorWithHexString:@"FFFFFF"];
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        tableViewHeaderFooterView.textLabel.textAlignment = NSTextAlignmentRight;
    tableViewHeaderFooterView.contentView.backgroundColor = [GlobalData colorWithHexString:@"00CBDF"];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == 0)
    {
        switch ([indexPath row])
        {
            case 0:
            {
                if([preferences objectForKey:@"customerId"] == nil)
                {
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                }
                else
                {
                    NSDictionary * dict = [preferences dictionaryRepresentation];
                    
                    for(id key in dict)
                    {
                        if([key isEqual:@"sessionId"] || [key isEqual:@"storeId"] || [key isEqual:@"language"] || [key isEqual:@"isDeviceToken"] || [key isEqual:@"websiteId"]|| [key isEqual:@"customerEmail"] || [key isEqual:@"customerPassword"] || [key isEqual:@"customerTouchUpIDFlag"])
                            
                            continue;
                        [preferences removeObjectForKey:key];
                    }
                    
                    [preferences synchronize];
                    [menuListData replaceObjectAtIndex:0 withObject:@"LogIn"];
                    obj.listMenuTitleData = [[NSMutableArray alloc] initWithObjects:@" My Account",nil];
                    [obj.menuListData removeLastObject];
                    obj.completeListMenuData =[[NSMutableArray alloc] initWithObjects:obj.menuListData,nil];
                    headerTitle = obj.listMenuTitleData;
                    allData = obj.completeListMenuData;
                    [_menuListTable reloadData];
                    
                    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
                    
                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                        [[tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:nil];
                    else
                        [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:nil];
                }
                break;
            }
            case 1:
            {
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"listMenuToDashboardSeague" sender:self];
                break;
            }
            case 2:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"accountInformationSegue" sender:self];
                break;
            }
            case 3:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"addressBookSeague" sender:self];
                break;
            }
            case 4:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"myOrdersSegue" sender:self];
                break;
            }
            case 5:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"productReviewSegue" sender:self];
                break;
            }
            case 6:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"myWishListSegue" sender:self];
                break;
            }
            case 7:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"newsLetterSegue" sender:self];
                break;
            }
            case 8:{
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"myDownloadableProductSegue" sender:self];
                break;
            }
            case 9:
            {
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"blogListViewSegue" sender:self];
                break;
            }
            default:
                break;
        }}
    if([indexPath section] == 1){
        switch ([indexPath row]) {
            case 0:
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"sellerDashBoardDataSegue" sender:self];
                break;
            case 1:
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"sellerSoldOrderDeatilsSegue" sender:self];
                break;
            case 2:
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self questionToAdmin];
                break;
            case 3:
                if([preferences objectForKey:@"customerId"] == nil)
                    [self performSegueWithIdentifier:@"customerLogin" sender:self];
                else
                    [self performSegueWithIdentifier:@"listMenuToReviewDetailsSegue" sender:self];
                break;
            default:
                
                break;
        }
    }
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectListMenu callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    [alert dismissViewControllerAnimated:YES completion:nil];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:@"Warning" message:@"ERROR with the Conenction" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
   [GlobalData alertController:currentWindow msg:[globalObjectListMenu.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    [post appendFormat:@"customerId=%@&", [preferences objectForKey:@"customerId"]];
    [post appendFormat:@"subject=%@&", [questionAskToAdminData objectForKey:@"subject"]];
    [post appendFormat:@"query=%@", [questionAskToAdminData objectForKey:@"query"]];
    [globalObjectListMenu callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/askQuestion" signal:@"HttpPostMetod"];
    globalObjectListMenu.delegate = self;

}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
   if([collection[@"success"] boolValue] == true )
        [ToastView showToastInParentView:currentWindow withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
    else
        [ToastView showToastInParentView:currentWindow withText:collection[@"message"] withStatus:@"error" withDuaration:5.0];
    
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)keyboardWasShown:(NSNotification*)aNotification{
    if(keyboardSetFlag == 1){
        keyboardSetFlag = 0;
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGRect newFrame = requestContainerView.frame;
            newFrame.origin.y = requestContainerView.frame.origin.y - kbSize.height + containerYPosition ;
          requestContainerView.frame = newFrame;
        
        
    }
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    if(keyboardSetFlag == 0){
        keyboardSetFlag = 1;
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGRect newFrame = requestContainerView.frame;
        newFrame.origin.y = requestContainerView.frame.origin.y + kbSize.height - containerYPosition;
        requestContainerView.frame = newFrame;
    }
}


    //question ask to admin

-(void)questionToAdmin{
    UIBlurEffect  *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    currentWindow = [UIApplication sharedApplication].keyWindow;
    blurEffectView.frame = currentWindow.bounds;
    
    
    requestContainerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5)];
    requestContainerView.backgroundColor = [UIColor whiteColor];
    [requestContainerView setUserInteractionEnabled:YES];
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [requestContainerView  addGestureRecognizer:tapGestureRecognizer];

    
  
    float y = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 32)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [sortTitle setText : @"ASK QUESTION TO ADMIN"];
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [requestContainerView addSubview : sortTitle];
    
    y += 42;
    UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 1)];
    [hr setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [requestContainerView addSubview : hr];
    
    y +=10;
    
    UITextField *subjectField = [[UITextField alloc] initWithFrame:CGRectMake(10, y,requestContainerView.frame.size.width -20, 40)];
    subjectField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    subjectField.textColor = [UIColor blackColor];
    [subjectField setPlaceholder:@"Enter Subject"];
    subjectField.layer.borderWidth = 1.0f;
    subjectField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    subjectField.tag = 3;
    [subjectField setKeyboardType:UIKeyboardTypePhonePad];
    subjectField.textAlignment = NSTextAlignmentLeft;
    subjectField.borderStyle = UITextBorderStyleRoundedRect;
    [requestContainerView addSubview:subjectField];
    
    y += 50;
    
    UITextView *textArea = [[UITextView alloc] initWithFrame:CGRectMake(10, y,requestContainerView.frame.size.width -20, 100)];
    textArea.layer.borderWidth = 1.0f;
    textArea.textColor = [UIColor blackColor];
    textArea.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textArea.font = [UIFont fontWithName: @"Trebuchet MS" size: 16.0f];
    textArea.tag = 4;
    [requestContainerView addSubview:textArea ];
    
    y += 120;
    
    UIView *requestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , requestContainerView.frame.size.width, 30)];
    requestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    requestContainer.layer.borderWidth = 0.0f;
    
    [requestContainerView addSubview:requestContainer];
    
    NSDictionary *reqAttributesforCancelButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforCancelButtonText = [@"Cancel" sizeWithAttributes:reqAttributesforCancelButtonText];
    CGFloat reqStringWidthVCancelButtonText = reqStringSizeforCancelButtonText.width;
    UIButton  *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(requestCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0,0,reqStringWidthVCancelButtonText+20, 40);  //requestContainerView.frame.size.width/4 , y
    [cancelButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [cancelButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [cancelButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    
    [requestContainer addSubview:cancelButton];
    
    NSDictionary *reqAttributesforSubmitButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforSubmitButtonText = [@"Submit" sizeWithAttributes:reqAttributesforSubmitButtonText];
    CGFloat reqStringWidthSubmitButtonText = reqStringSizeforSubmitButtonText.width;
    UIButton  *SubmitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [SubmitButton addTarget:self action:@selector(requestSubmitButton :) forControlEvents:UIControlEventTouchUpInside];
    [SubmitButton setTitle:@"Submit" forState:UIControlStateNormal];
    SubmitButton.frame = CGRectMake(reqStringWidthVCancelButtonText +20 +10,0,reqStringWidthSubmitButtonText+20, 40);
    [SubmitButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [SubmitButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [SubmitButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [requestContainer addSubview:SubmitButton];
    
    CGRect  newFrame = requestContainer.frame;
    newFrame.size.width = reqStringWidthVCancelButtonText +20 +10 + reqStringWidthSubmitButtonText+20;
    requestContainer.frame = newFrame;

    UIView *finalrequestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , requestContainerView.frame.size.width, 30)];
    finalrequestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    finalrequestContainer.layer.borderWidth = 0.0f;
    requestContainer.center = CGPointMake(finalrequestContainer.frame.size.width  / 2,
                                          finalrequestContainer.frame.size.height / 2);
    [finalrequestContainer addSubview:requestContainer];
    
    [requestContainerView addSubview:finalrequestContainer];

    
    
    CGRect  contactNewFrame = requestContainerView.frame;
    contactNewFrame.size.height = y +50;
    requestContainerView.frame = contactNewFrame;
    
    
    requestContainerView.center = currentWindow.center;
    containerYPosition = requestContainerView.frame.origin.y;
    [blurEffectView addSubview:requestContainerView];
    [currentWindow addSubview:blurEffectView];
    
}

-(void)requestCancelButton :(UIButton*)button{
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    [parent2.superview removeFromSuperview];
}

-(void)requestSubmitButton :(UIButton*)button{
    questionAskToAdminData = [[NSMutableDictionary alloc]init];
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    NSString *errorMessage = @"";
    UITextField *nameField = [parent2 viewWithTag:1];
    UITextField *emailField = [parent2 viewWithTag:2];
    UITextField *subjectlField = [parent2 viewWithTag:3];
    UITextView *queryField = [parent2 viewWithTag:4];
    
    
    isValid = 1;
    if ([queryField.text isEqualToString:@""]){
        isValid =0;
        errorMessage = @"Please enter the Query";
    }
    if ([subjectlField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Subject";
    }
    if ([emailField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Email";
    }
    if ([nameField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Name";
    }
    
    
    if(isValid == 1){
        [questionAskToAdminData setObject:nameField.text forKey:@"name"];
        [questionAskToAdminData setObject:emailField.text forKey:@"email"];
        [questionAskToAdminData setObject:subjectlField.text forKey:@"subject"];
        [questionAskToAdminData setObject:queryField.text forKey:@"query"];
        isValid=2;
    }
    
    if(isValid == 0){
        
        [requestContainerView endEditing:YES];
        [ToastView showToastInParentView:currentWindow withText:errorMessage  withStatus:@"error" withDuaration:3.0];
    }
    if(isValid == 2) {
        [parent2.superview removeFromSuperview];
        whichApiDataToprocess = @"contactAdmin";
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}
-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer{
    [requestContainerView endEditing:YES];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
     if([segue.identifier isEqualToString:@"listMenuToReviewDetailsSegue"]) {
        
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        MakeReviewData *destViewController = [[navigationController viewControllers] lastObject];
        [preferences setObject:[preferences objectForKey:@"customerId"] forKey:@"sellerId"];   // here we are setting seller id because every seller has id of its customerid.
        destViewController.profileUrl = [preferences objectForKey:@"profileUrl"];
        destViewController.popUpView = @"1";
    }
}


@end
