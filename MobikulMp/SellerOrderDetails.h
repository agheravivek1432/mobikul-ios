//
//  SellerOrderDetails.h
//  MobikulMp
//
//  Created by kunal prasad on 27/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerOrderDetails : UIViewController
{
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSString *sessionId, *message, *dataFromApi;
    id mainCollection,collection;
    NSString *whichApiDataToprocess, *incementIdIdForApiCall;
    UIButton *buttonSendInvoice;
    UITapGestureRecognizer *invoiceTitleTap;
    UIWindow *currentWindow;
}
@property (nonatomic) NSString *incrementId;
@property (nonatomic) NSString *customerId;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;
@end
