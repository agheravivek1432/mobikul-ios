//
//  Newsletter.h
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Newsletter : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *subscriberEmailId;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id collection;
    UIWindow *currentWindow;
}

@property (weak, nonatomic) IBOutlet UITextField *subscriberEmailId;
- (IBAction)subscribeClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *subscribeButton;
@property (weak, nonatomic) IBOutlet UIButton *subscribe;

@end