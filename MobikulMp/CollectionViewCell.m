//
//  CollectionViewCell.m
//  Mobikul
//
//  Created by Ratnesh on 15/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "CollectionViewCell.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

@implementation CollectionViewCell

@end