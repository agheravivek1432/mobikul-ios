//
//  CategoryListCell.m
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "CategoryListCell.h"

@implementation CategoryListCell

@synthesize dictCategory;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reloadCategoryListCell
{
    lblCategoryName.text = [dictCategory valueForKey:@"category"];
    lblDate.text = [dictCategory valueForKey:@"created_at"];
}

@end
