//
//  ViewController.m
//  Mobikul
//
//  Created by Ratnesh on 02/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import "CustomerLogin.h"
#include "GlobalData.h"
#include "Home.h"
#import <LocalAuthentication/LocalAuthentication.h>

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectCustomerLogin;

@implementation CustomerLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    flag  = 0;
    languageCode = [NSUserDefaults standardUserDefaults];
    preferences = [NSUserDefaults standardUserDefaults];
    globalObjectCustomerLogin=[[GlobalData alloc] init];
    globalObjectCustomerLogin.delegate=self;
    [globalObjectCustomerLogin language];
    languageCode = [NSUserDefaults standardUserDefaults];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:@"00CBDF"];
    _login.backgroundColor = [GlobalData colorWithHexString:@"268ED76"];
    self.navigationItem.title = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"customerLogin" value:@"" table:nil];
    _alreadyRegistered.text = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"alreadyRegistered" value:@"" table:nil];
    _subHeading.text = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"pleaseLogin" value:@"" table:nil];
    [_forgotPassword setTitle: [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"forgotPassword" value:@"" table:nil] forState: UIControlStateNormal];
    [_login setTitle: [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"logIn" value:@"" table:nil] forState: UIControlStateNormal];
    _emailAddress.text = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"EmailAddress" value:@"" table:nil];
    _password.text = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"password" value:@"" table:nil];
    _requiredField.text = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"requireFields" value:@"" table:nil];
    [_createAccount setTitle: [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"createAccount" value:@"" table:nil] forState: UIControlStateNormal];
    currentWindow = [UIApplication sharedApplication].keyWindow;

}
- (void)viewDidAppear:(BOOL)animated {
    LAContext *context = [[LAContext alloc] init];
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]){
        if([preferences objectForKey:@"customerEmail"] && [preferences objectForKey:@"customerPassword"]){
            //NSLog(@"YES  %@",[preferences objectForKey:@"customerTouchUpIDFlag"]);
            if([[preferences objectForKey:@"customerTouchUpIDFlag"] isEqualToString:@"1"]){
                UIAlertController *userAlert = [UIAlertController alertControllerWithTitle:nil message:@"Would You Like To Login APP With TouchID" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action){
                                                                  [ self authenticaticateOnLoad];
                                                              }];
                
                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    flag = 1;
                }];
                [userAlert addAction:okBtn];
                [userAlert addAction:noBtn];
                [self.parentViewController presentViewController:userAlert animated:YES completion:nil];
            }
        }
    }
    
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}
-(void)loginRequestCall{
    [self loginRequest];
}


- (IBAction)forgotPasswordButton:(id)sender {
    UIAlertController *forgotpasswordAlert = [UIAlertController alertControllerWithTitle:nil message:@"Please Enter your Email ID" preferredStyle:UIAlertControllerStyleAlert];
    [forgotpasswordAlert addTextFieldWithConfigurationHandler:^(UITextField *enteredEmailFromPromt){
        enteredEmailFromPromt.placeholder = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"enterEmail" value:@"" table:nil];
    }];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      @try{
                                                          int isValid = 1;
                                                          NSString *errorMessage;
                                                          userEmail = ((UITextField *)[forgotpasswordAlert.textFields objectAtIndex:0]).text;
                                                          globalObjectCustomerLogin.employEmailId=((UITextField *)[forgotpasswordAlert.textFields objectAtIndex:0]).text;
                                                          NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                                                          NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                                                          if([userEmail isEqual:@""]){
                                                              isValid = 0;
                                                              errorMessage = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"emailAddress" value:@"" table:nil];
                                                          }
                                                          else
                                                              if(![emailTest evaluateWithObject:userEmail]){
                                                                  isValid = 0;
                                                                  errorMessage = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"validEmail" value:@"" table:nil];
                                                              }
                                                          if(isValid == 0){
                                                              UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                                                              UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                                                              [errorAlert addAction:noBtn];
                                                              [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                                                          }
                                                          else{
                                                              whichApiToCall = @"forgotpassword";
                                                              globalObjectCustomerLogin.whichApitoCallForCustomerLogin = @"forgotpassword";
                                                              NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                              if(savedSessionId == nil)
                                                                  [self loginRequest];
                                                              else
                                                              {
                                                                  [self callingHttppApi];
                                                              }
                                                          }
                                                      }
                                                      @catch (NSException *e) {
                                                          //NSLog(@"catching %@ reason %@", [e name], [e reason]);
                                                      }
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [forgotpasswordAlert addAction:okBtn];
    [forgotpasswordAlert addAction:noBtn];
    [self.parentViewController presentViewController:forgotpasswordAlert animated:YES completion:nil];
}


- (IBAction)submitButton:(id)sender {
    enteredEmail = _emailAddressField.text;
    enteredPassword = _passwordField.text;
    @try{
        int isValid = 1;
        NSString *errorMessage;
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if([enteredEmail isEqual: @""]){
            isValid = 0;
            errorMessage = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"emailAddress" value:@"" table:nil];
        }
        else
            if(![emailTest evaluateWithObject:enteredEmail]){
                isValid = 0;
                errorMessage = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"validEmail" value:@"" table:nil];
            }
            else
                if([enteredPassword isEqual: @""]){
                    isValid = 0;
                    errorMessage = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"passwordBlank" value:@"" table:nil];
                }
                else
                    if([enteredPassword length] < 7){
                        isValid = 0;
                        errorMessage = [globalObjectCustomerLogin.languageBundle localizedStringForKey:@"passwordCharacter" value:@"" table:nil];
                    }
        if(isValid == 0){
            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
            [errorAlert addAction:noBtn];
            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
        }
        else{
            whichApiToCall = @"login";
            globalObjectCustomerLogin.whichApitoCallForCustomerLogin = @"login";
            preferences = [NSUserDefaults standardUserDefaults];
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
    @catch (NSException *e) {
        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
    }
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    
    [globalObjectCustomerLogin callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}
-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    if(websiteId == nil){
        [post appendFormat:@"websiteId=%@&", DEFAULT_WEBSITE_ID];
        [preferences setObject:DEFAULT_WEBSITE_ID forKey:@"websiteId"];
        [preferences synchronize];
    }
    else
        [post appendFormat:@"websiteId=%@&", websiteId];
    if([whichApiToCall isEqualToString:@"forgotpassword"]){
        [post appendFormat:@"emailAddress=%@",userEmail];
        [globalObjectCustomerLogin callHTTPPostMethod:post api:@"mobikulhttp/customer/forgotpasswordPost" signal:@"HttpPostMetod"];
        globalObjectCustomerLogin.delegate = self;
    }
    else{
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId != nil)
            [post appendFormat:@"quoteId=%@&", quoteId];
        [post appendFormat:@"username=%@&", enteredEmail];
        [post appendFormat:@"password=%@", enteredPassword];
        [globalObjectCustomerLogin callHTTPPostMethod:post api:@"mobikulhttp/customer/logIn" signal:@"HttpPostMetod"];
        globalObjectCustomerLogin.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult{
    
    whichApiToCall = globalObjectCustomerLogin.whichApitoCallForCustomerLogin;
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
            [preferences setObject:[collection objectForKey:@"customerBannerImage"] forKey:@"profileBanner"];
            [preferences setObject:[collection objectForKey:@"customerProfileImage"] forKey:@"profilePicture"];
            [preferences synchronize];
            if([whichApiToCall isEqual: @"login"]){
                if([collection[@"status"] isEqual:@"true"]){
                    
                    NSString *Str_isSeller = [NSString stringWithFormat:@"%@",collection[@"isSeller"]];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:Str_isSeller forKey:@"isSellerKey"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"]){
                        if([collection[@"cartCount"] integerValue] > 0)
                            [[tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", collection[@"cartCount"]]];
                        else
                            [[tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:nil];
                    }
                    else{
                        if([collection[@"cartCount"] integerValue] > 0)
                            [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", collection[@"cartCount"]]];
                        else
                            [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:nil];
                    }
                    [preferences setObject:collection[@"customerName"] forKey:@"customerName"];
                    [preferences setObject:collection[@"customerId"] forKey:@"customerId"];
                    [preferences setObject:collection[@"customerEmail"] forKey:@"customerEmail"];
                    [preferences setObject:enteredPassword forKey:@"customerPassword"];
                    [preferences setObject:@"1" forKey:@"isLoggedIn"];
                    [preferences setObject:nil forKey:@"quoteId"];
                    [preferences setObject:collection[@"isSeller"] forKey:@"isSeller"];
                    [preferences synchronize];
                    GlobalData *obj = [GlobalData getInstance];
                    [obj.menuListData replaceObjectAtIndex:0 withObject:@"Log Out"];
                    [obj.menuListData addObject:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"Bolgs" value:@"" table:nil]];
                    NSCache *catCache = [[NSCache alloc] init];
                    if(collection[@"customerProfileImage"]){
                        [preferences setObject:collection[@"customerProfileImage"] forKey:@"profilePicture" ];
                        NSString *profileUrl = [preferences objectForKey:@"profilePicture"];
                        UIImage *profile=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:profileUrl]]];
                        if([profileUrl isEqualToString:@""] == false){
                            [catCache removeObjectForKey:profileUrl];
                            [catCache setObject:profile forKey:profileUrl];
                        }
                    }
                    if(collection[@"customerBannerImage"]){
                        [preferences setObject:collection[@"customerBannerImage"] forKey:@"profileBanner" ];
                        NSString *bannerUrl = [preferences objectForKey:@"profileBanner"];
                        UIImage *banner=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:bannerUrl]]];
                        if([bannerUrl isEqualToString:@""] == false){
                            [catCache removeObjectForKey:bannerUrl];
                            [catCache setObject:banner forKey:bannerUrl];
                        }
                    }
//                    
//                    NSString *profileUrl = [preferences objectForKey:@"profilePicture"];
//                    UIImage *profile=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:profileUrl]]];
//                    [catCache removeObjectForKey:profileUrl];
//                    [catCache setObject:profile forKey:profileUrl];
                    
//                    NSString *bannerUrl = [preferences objectForKey:@"profileBanner"];
//                    UIImage *banner=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:bannerUrl]]];
//                    [catCache removeObjectForKey:bannerUrl];
//                    [catCache setObject:banner forKey:bannerUrl];
                    
                    obj.profileCache = catCache;
                    
                    NSInteger isSeller = [[preferences objectForKey:@"isSeller"] intValue];
                    if(isSeller == 1){
                        [preferences setObject:collection[@"profileUrl"] forKey:@"profileUrl"];
                        obj.listMenuTitleData = [[NSMutableArray alloc] initWithObjects:@"  My Account", @"  MarketPlace",nil];
                        obj.marketPlaceData =  [[NSMutableArray alloc] initWithObjects:@"Seller Dash Board", @"Seller Order",@"Ask Question to Admin",@"Seller Review List",nil];
                        obj.completeListMenuData =[[NSMutableArray alloc] initWithObjects:obj.menuListData,obj.marketPlaceData,nil];
                    }
                    else{
                        obj.listMenuTitleData = [[NSMutableArray alloc] initWithObjects:@"  My Account",nil];
                        obj.completeListMenuData =[[NSMutableArray alloc] initWithObjects:obj.menuListData,nil];
                    }
                    
                    LAContext *context = [[LAContext alloc] init];
                    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]  && (flag!=1)){
                        UIAlertController *userAlert = [UIAlertController alertControllerWithTitle:nil message:@"Would You Like To Connect APP With TouchID" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action){
                                                                          [ self authenticateWithTouchIdAfterLogin];
                                                                      }];
                        
                        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                            
                            [self performSegueWithIdentifier:@"customerLoginToDashboardSeague" sender:self];
                            
                        }];
                        [userAlert addAction:okBtn];
                        [userAlert addAction:noBtn];
                        [self.parentViewController presentViewController:userAlert animated:YES completion:nil];
                        
                    }
                    
                    
                    else{
                         [self performSegueWithIdentifier:@"customerLoginToDashboardSeague" sender:self];
                       }

                }
                else{
                    UIAlertController * loginErrorNotification = [UIAlertController alertControllerWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"loginError" value:@"" table:nil] message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                    [loginErrorNotification addAction:noBtn];
                    [self.parentViewController presentViewController:loginErrorNotification animated:YES completion:nil];
                }
            }
            if([whichApiToCall isEqual:@"forgotpassword"]){
                UIAlertController *forgotpasswordNotification = [UIAlertController alertControllerWithTitle:@"" message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                [forgotpasswordNotification addAction:noBtn];
                [self.parentViewController presentViewController:forgotpasswordNotification animated:YES completion:nil];
            }
        //}];
    }
}
-(void)authenticateWithTouchIdAfterLogin{
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]){
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthentication
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  switch (error.code) {
                                      case LAErrorUserCancel:
                                          // NSLog(@"user pressed cancel");
                                          break;
                                  }
                                  
                                  UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:nil message:@"You are not the device owner." preferredStyle:UIAlertControllerStyleAlert];
                                  UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action){
                                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                                        [self performSegueWithIdentifier:@"customerLoginToDashboardSeague" sender:self];
                                                                                    });
                                                                                }];
                                  [errorAlert addAction:okBtn];
                                  [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                                  return ;
                                  
                              }
                              
                              if (success) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self success];
                                      [preferences setObject:@"1" forKey:@"customerTouchUpIDFlag"];
                                      
                                  });
                                  return;
                                  
                              } else {
                                  UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:nil message:@"You are not the device owner." preferredStyle:UIAlertControllerStyleAlert];
                                  UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction * action){
                                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                                        [self performSegueWithIdentifier:@"customerLoginToDashboardSeague" sender:self];
                                                                                    });
                                                                                }];
                                  [errorAlert addAction:okBtn];
                                  [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
                              }
                              
                          }];
        
    }
    else {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:nil message:@"Your device are not authenticate using TouchID." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCustomerLogin.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          [self performSegueWithIdentifier:@"customerLoginToDashboardSeague" sender:self];
                                                      }];
        [errorAlert addAction:okBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
}

-(void)success{
    [self performSegueWithIdentifier:@"customerLoginToDashboardSeague" sender:self];
    //return;
    
    // moradiyaakash
}

-(void)authenticaticateOnLoad{
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]){
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthentication
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  //dispatch_async(dispatch_get_main_queue(), ^{
                                  switch (error.code) {
                                      case LAErrorUserCancel:
                                          //NSLog(@"User pressed Cancel button");
                                          
                                          break;
                                          
                                  }
                                  return ;
                              }
                              if (success) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self goToDashBoardModuleOnViewLoad];
                                      [preferences setObject:@"1" forKey:@"CustomerTouchUpIDFlag"];
                                      
                                  });
                                  return;
                                  
                              } else {
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                  message:@"You are not the device owner."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                                  return;
                              }
                              
                          }];
        
    }else{
        
        
        
    }
    
    
    
    
}

-(void)goToDashBoardModuleOnViewLoad{
    whichApiToCall = @"login";
    globalObjectCustomerLogin.whichApitoCallForCustomerLogin = @"login";
    enteredEmail = [preferences objectForKey:@"customerEmail"];
    enteredPassword = [preferences objectForKey:@"customerPassword"];
    flag = 1;
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else{
        [self callingHttppApi];
    }
    
}


- (IBAction)openCreateAccount:(id)sender {
    [self performSegueWithIdentifier:@"openCreateAccount" sender:self];
}

@end
