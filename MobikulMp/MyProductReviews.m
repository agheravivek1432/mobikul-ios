//
//  MyProductReviews.m
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "MyProductReviews.h"
#import "GlobalData.h"
#import "ReviewDetails.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectMyProductReview;

@implementation MyProductReviews

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectMyProductReview=[[GlobalData alloc] init];
    globalObjectMyProductReview.delegate = self;
    _myProductReviewScrollView.delegate = self;
    [globalObjectMyProductReview language];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectMyProductReview.languageBundle localizedStringForKey:@"productReviews" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    isAlertVisible = 0;
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    pageNumber = 1;
    loadPageRequestFlag = 1;
    contentHeight = -64;
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectMyProductReview callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMyProductReview.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectMyProductReview.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectMyProductReview.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMyProductReview.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    if(pageNumber == 1)
        [GlobalData alertController:currentWindow msg:[globalObjectMyProductReview.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    else{
        [GlobalData loadingController:currentWindow];
    }
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    [post appendFormat:@"websiteId=%@&", websiteId];
    NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
    [post appendFormat:@"customerEmail=%@&",prefCustomerEmail];
    NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    [post appendFormat:@"width=%@&",screenWidth];
    [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
    [globalObjectMyProductReview callHTTPPostMethod:post api:@"mobikulhttp/customer/getproReviews" signal:@"HttpPostMetod"];
    globalObjectMyProductReview.delegate = self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger currentCellCount = [arrayMainCollection count];
    //NSLog(@" called   %ld   %ld",totalCount,currentCellCount);
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height) {
        if( (totalCount > currentCellCount) && loadPageRequestFlag ){
            reloadPageData = @"true";
            pageNumber +=1;
            loadPageRequestFlag = 0;
            contentHeight = _myProductReviewScrollView.contentOffset.y;
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
    
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    loadPageRequestFlag = 1;
    NSArray *newData = collection[@"allReviews"];
    arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
    
    totalCount = [collection[@"totalCount"] integerValue];
    
    
    for(int i=0; i<[arrayMainCollection count]; i++){
        NSDictionary *dict = [arrayMainCollection objectAtIndex:i];
        UIView *paintView = [[UIView alloc]initWithFrame:CGRectMake(2, (i*(SCREEN_WIDTH/3+10))+(2*(i+1)), (_productReviewContainer.frame.size.width)-4, (SCREEN_WIDTH/3+10))];
        [paintView setBackgroundColor:[UIColor whiteColor]];
        paintView.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        
        paintView.layer.borderWidth = 2.0f;
        [_productReviewContainer addSubview:paintView];
        
        UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,SCREEN_WIDTH/3,SCREEN_WIDTH/3)];
        productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
        UIImage *image = [imageCache objectForKey:dict[@"thumbNail"]];
        if(image)
            productImage.image = image;
        else{
            [_queue addOperationWithBlock:^{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:dict[@"thumbNail"]]];
                UIImage *image = [UIImage imageWithData: imageData];
                if(image){
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        productImage.image = image;
                    }];
                    [imageCache setObject:image forKey:dict[@"thumbNail"]];
                }
            }];
        }
        [paintView addSubview:productImage];
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+5), 5, (_productReviewContainer.frame.size.width)-4, 20)];
        [date setTextColor:[UIColor blackColor]];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *dateText = [NSString stringWithFormat:@"Date : %@",dict[@"date"]];
        [date setText:dateText];
        [paintView addSubview:date];
        
        UILabel *proName = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+5), 30, (_productReviewContainer.frame.size.width)-(SCREEN_WIDTH/3+5), 20)];
        [proName setTextColor:[UIColor blackColor]];
        [proName setBackgroundColor:[UIColor clearColor]];
        [proName setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *proNameText = [NSString stringWithFormat:@"Product Name : %@",dict[@"proName"]];
        [proName setText:proNameText];
        [paintView addSubview:proName];
        
        UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+5), 55, (_productReviewContainer.frame.size.width)-4, 20)];
        [details setTextColor:[UIColor blackColor]];
        [details setBackgroundColor:[UIColor clearColor]];
        [details setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
        NSString *detailsText = [NSString stringWithFormat:@"Details : %@",dict[@"details"]];
        [details setText:detailsText];
        [paintView addSubview:details];
        
        UILabel *editLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+5), 90, (_productReviewContainer.frame.size.width), 20)];
        [editLabel setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
        [editLabel setText:@"VIEW DETAILS"];
        editLabel.userInteractionEnabled = YES;
        [editLabel sizeToFit];
        editLabel.tag = [dict[@"id"] integerValue];
        [paintView addSubview:editLabel];
        
        UITapGestureRecognizer *tapGesture1 =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ViewDetails:)];
        tapGesture1.numberOfTapsRequired = 1;
        [editLabel addGestureRecognizer:tapGesture1];
    }
    if([arrayMainCollection count] == totalCount){
        if([arrayMainCollection count] > 0)
            _heightConstraint.constant = ((12+SCREEN_WIDTH/3)*[arrayMainCollection count]) + 50;
        else
            _heightConstraint.constant = 0;
    }
    else{
        if([arrayMainCollection count] > 0)
            _heightConstraint.constant = ((12+SCREEN_WIDTH/3)*[arrayMainCollection count]) + 50 +50;  // to give space for display a loader alert
        else
            _heightConstraint.constant = 0;
    }
    [_productReviewContainer layoutIfNeeded];
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"reviewDetail"]) {
        ReviewDetails *destViewController = segue.destinationViewController;
        destViewController.reviewId = reviewId;
    }
}

- (void)ViewDetails:(UITapGestureRecognizer *)recognizer{
    //    NSDictionary *temp = [arrayMainCollection objectAtIndex:(int)recognizer.view.tag];
    //    reviewId = (int)temp[@"id"];
    reviewId = (int)recognizer.view.tag;
    [self performSegueWithIdentifier:@"reviewDetail" sender:self];
}

- (IBAction)unwindToProductReview:(UIStoryboardSegue *)unwindSegue{}

@end