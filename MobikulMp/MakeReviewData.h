//
//  MakeReviewData.h
//  MobikulMp
//
//  Created by kunal prasad on 27/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MakeReviewData : UIViewController{
    
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSString *sessionId, *message, *dataFromApi,*name,*entityId ,*typeId, *profileurl ;
    id mainCollection,collection;
    NSCache *imageCache;
    UIView *ratingContainerBlock;
    NSArray *ReviewData;
    NSMutableDictionary *ratingsDict;
    NSMutableDictionary *feedBackData;
    NSInteger isValid;
    NSString *whichApiDataToprocess;
    NSInteger keyboardSetFlag;
    UIScrollView *ratingScrollView;
    UIBlurEffect *blurEffect;
    UIVisualEffectView *blurEffectView;
    UIWindow *currentWindow;
    
}
@property (nonatomic) NSString *profileUrl;
@property (nonatomic) NSString *popUpView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backPressedButton;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;
@end
