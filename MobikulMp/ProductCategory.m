//
//  Category.m
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "ProductCategory.h"
#import "GlobalData.h"
#import "CatalogProduct.h"
#import "CollectionViewCell.h"
#import "ToastView.h"
#import "CustomerLogin.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectProductCat;
CollectionViewCell *cell;

@implementation ProductCategory

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    languageCode = [NSUserDefaults standardUserDefaults];
    globalObjectProductCat=[[GlobalData alloc] init];
    globalObjectProductCat.delegate = self;
    [globalObjectProductCat language];
    whichApiDataToprocess = @"";
    sortSignal = 0;
    _sortByView.hidden = YES;
    pageNumber = 1;
    loadPageRequestFlag = 1;
    reloadPageData = @"false";
    _sortByView.backgroundColor = [GlobalData colorWithHexString:@"00CBDF"];
    sortItem = @"relevance";
    sortDir = @"0";
    finalData = [[NSMutableString alloc] initWithString: @""];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProductShowOptions:)];
    [_categoryProductCollectionView addGestureRecognizer:tap];
    sortDirection = [[NSMutableArray alloc] init];
    
    UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-2, 0,4, _sortByView.frame.size.height)];
    lineView.backgroundColor = [GlobalData colorWithHexString:@"ffffff"];
    [_sortByView addSubview:lineView];
    
    UILabel *filterByTapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 3,SCREEN_WIDTH/2-2 ,30)];
    [filterByTapLabel setTextColor: [UIColor whiteColor]];
    [filterByTapLabel setText:[globalObjectProductCat.languageBundle localizedStringForKey:@"filterby" value:@"" table:nil]];
    [filterByTapLabel setFont:[UIFont fontWithName: @"Trebuchet MS" size: 25.0f]];
    filterByTapLabel.textAlignment = NSTextAlignmentCenter;
    [_sortByView addSubview:filterByTapLabel];
    
    UITapGestureRecognizer *filterByTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterByTapped:)];
    filterByTap.numberOfTapsRequired = 1;
    filterByTapLabel.userInteractionEnabled = YES;
    [filterByTapLabel addGestureRecognizer:filterByTap];
    
    UIView *requestContainer = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2+2,0 , SCREEN_WIDTH/2, 30)];
    requestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    requestContainer.layer.borderWidth = 0.0f;
    [_sortByView addSubview:requestContainer];
    
    
    NSDictionary *reqAttributesforSortByText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]};
    CGSize reqStringSizeforSortByText = [[globalObjectProductCat.languageBundle localizedStringForKey:@"sortBy" value:@"" table:nil] sizeWithAttributes:reqAttributesforSortByText];
    UILabel *sortByTapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,reqStringSizeforSortByText.width,30)];
    [sortByTapLabel setTextColor: [UIColor whiteColor]];
    //[sortByTapLabel setBackgroundColor:[UIColor blueColor]];
    [sortByTapLabel setText:[globalObjectProductCat.languageBundle localizedStringForKey:@"sortBy" value:@"" table:nil]];
    [sortByTapLabel setFont:[UIFont fontWithName: @"Trebuchet MS" size: 25.0f]];
    sortByTapLabel.textAlignment = NSTextAlignmentCenter;
    [requestContainer addSubview:sortByTapLabel];
    
    UIImageView *imageTap = [[UIImageView alloc] initWithFrame:CGRectMake(reqStringSizeforSortByText.width, 0, 32, 32)];
    [imageTap setImage:[UIImage imageNamed:@"ic_sort.png"]];
    [requestContainer addSubview:imageTap];
    
    CGRect  newFrame = requestContainer.frame;
    newFrame.size.width = reqStringSizeforSortByText.width + 42;
    requestContainer.frame = newFrame;
    
    UIView *finalrequestSortByContainer = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2+2,3 , SCREEN_WIDTH/2, 30)];
    finalrequestSortByContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    finalrequestSortByContainer.layer.borderWidth = 0.0f;
    requestContainer.center = CGPointMake(finalrequestSortByContainer.frame.size.width  / 2,
                                          finalrequestSortByContainer.frame.size.height / 2);
    [finalrequestSortByContainer addSubview:requestContainer];
    [_sortByView addSubview:finalrequestSortByContainer];
    UITapGestureRecognizer *sortByTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sortByTapped:)];
    sortByTap.numberOfTapsRequired = 1;
    [finalrequestSortByContainer addGestureRecognizer:sortByTap];
    finalrequestSortByContainer.backgroundColor = [GlobalData colorWithHexString:@"00CBDF"];
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    preferences = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.title = _categoryName;
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    flag = 0;
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil){
        [self loginRequest];
    }
    else{
        [self callingHttppApi];
    }
}

-(void) callingHttppApi{
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiDataToprocess isEqual:@"addToWishlist"]){
        [GlobalData alertController:currentWindow msg:[globalObjectProductCat.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId != nil)
            [post appendFormat:@"customerId=%@&", customerId];
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectProductCat callHTTPPostMethod:post api:@"mobikulhttp/catalog/addtoWishlist" signal:@"HttpPostMetod"];
    }
    else if([whichApiDataToprocess isEqual: @"addToCart"]){
        [GlobalData alertController:currentWindow msg:[globalObjectProductCat.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId != nil)
            [post appendFormat:@"customerId=%@&", customerId];
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId != nil)
            [post appendFormat:@"quoteId=%@&", quoteId];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"productId=%@", productIdForApiCall];
        [globalObjectProductCat callHTTPPostMethod:post api:@"mobikulhttp/checkout/addtoCart" signal:@"HttpPostMetod"];
        globalObjectProductCat.delegate = self;
    }
    else{
        if(pageNumber == 1)
            [GlobalData alertController:currentWindow msg:[globalObjectProductCat.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        else{
            [GlobalData loadingController:currentWindow];
        }
        [post appendFormat:@"categoryId=%@&", _categoryId];
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@&", screenWidth];
        NSArray *sortData = @[sortItem, sortDir];
        NSError *error = nil;
        NSData *jsonSortData = [NSJSONSerialization dataWithJSONObject:sortData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonSortString = [[NSString alloc] initWithData:jsonSortData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"sortData=%@&",jsonSortString];
        NSArray *filterData = [[NSArray alloc] initWithObjects:filterIdValue,filterCodeValue, nil];
        NSData *jsonFilterData = [NSJSONSerialization dataWithJSONObject:filterData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonFilterString = [[NSString alloc] initWithData:jsonFilterData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"filterData=%@&", jsonFilterString];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
        [globalObjectProductCat callHTTPPostMethod:post api:@"mobikulhttp/catalog/getcategoryproductList" signal:@"HttpPostMetod"];
        globalObjectProductCat.delegate = self;
    }
}
#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    //NSLog(@" %@",collectionData);
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}


-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectProductCat callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectProductCat.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectProductCat.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectProductCat.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectProductCat.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    _sortByView.hidden = NO;
    
    if([whichApiDataToprocess isEqual: @"addToWishlist"]){
        addToWishListCollection = collection;
        if([addToWishListCollection[@"status"] boolValue])
            [ToastView showToastInParentView:self.view withText:@"Product Added To Wishlist Successfully." withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:@"Sorry!! Something went wrong please try again later." withStatus:@"error" withDuaration:5.0];
    }
    else
        if([whichApiDataToprocess isEqual: @"addToCart"]){
            addToCartCollection = collection;
            if([addToCartCollection objectForKey:@"quoteId"]){
                [preferences setObject:addToCartCollection[@"quoteId"] forKey:@"quoteId"];
                [preferences synchronize];
            }
            if([addToCartCollection[@"error"] boolValue])
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"error" withDuaration:5.0];
            else
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"success" withDuaration:5.0];
            UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
            if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                [[tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
            else
                [[tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
        }
        else if([reloadPageData isEqualToString:@"true"]){
            [[currentWindow viewWithTag:313131] removeFromSuperview];
            loadPageRequestFlag = 1;
            [self.view setUserInteractionEnabled:YES];
            NSArray *newData = collection[@"categoryData"];
            arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
            mainCollection = collection;
            for(int i=0; i<[arrayMainCollection count]; i++)
                productCurtainOpenSignal[i] = @"0";
            if(sortSignal == 0) {
                for(int i=0; i<[collection[@"sortingData"] count]; i++) {
                    if(i == 0)
                        [sortDirection addObject:@"1"];
                    else
                        [sortDirection addObject:@"0"];
                }
                sortSignal++;
            }
            [_categoryProductCollectionView reloadData];
            [self scrollAutomatically];
            
            
        }
        else{
            NSArray *newData = collection[@"categoryData"];
            arrayMainCollection = newData;
            mainCollection = collection;
            totalCount = [collection[@"totalCount"] integerValue];       // count total cell from server.
            if(flag == 0){
                filterCodeValue = [[NSMutableArray alloc] initWithCapacity:[mainCollection[@"layeredData"] count]];
                filterIdValue = [[NSMutableArray alloc] initWithCapacity:[mainCollection[@"layeredData"] count]];
                filterCodeHeader = [[NSMutableArray alloc] initWithCapacity:[mainCollection[@"layeredData"] count]];
                filterItemValue = [[NSMutableArray alloc] initWithCapacity:[mainCollection[@"layeredData"] count]];
                flag = 1;
            }
            productCurtainOpenSignal = [[NSMutableArray alloc] initWithCapacity:[mainCollection[@"categoryData"] count]];
            for(int i=0; i<[mainCollection[@"categoryData"] count]; i++)
                productCurtainOpenSignal[i] = @"0";
            if(sortSignal == 0) {
                for(int i=0; i<[mainCollection[@"sortingData"] count]; i++) {
                    if(i == 0)
                        [sortDirection addObject:@"1"];
                    else
                        [sortDirection addObject:@"0"];
                }
                sortSignal++;
            }
            [_categoryProductCollectionView reloadData];
        }
}

-(void)scrollAutomatically{
    
    //    float width = CGRectGetWidth(_categoryProductCollectionView.frame);
    //    float height = CGRectGetHeight(_categoryProductCollectionView.frame);
    //    float newPosition = contentHeight;
    //    CGRect toVisible = CGRectMake(0, newPosition, width, height);
    //   //[_categoryProductCollectionView scrollRectToVisible:toVisible animated:YES];
    ////    CGRect newFrame = _mainView.frame;
    ////    newFrame.size.height = 2000000 ;
    ////   _mainView.frame = newFrame;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if([arrayMainCollection count] == totalCount)
        return CGSizeMake(0,0);
    else
        return CGSizeMake(0,45);
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [arrayMainCollection count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // NSDictionary *productCollectionDictionary = [mainCollection[@"categoryData"] objectAtIndex:[indexPath row]];
    NSDictionary *productCollectionDictionary = [arrayMainCollection objectAtIndex:[indexPath row]];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.layer.cornerRadius = 3.0f;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    cell.layer.shadowRadius = 3.0f;
    cell.layer.shadowOpacity = 0.5;
    cell.layer.masksToBounds = NO;
    cell.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds cornerRadius:cell.contentView.layer.cornerRadius].CGPath;
    
    for(UIView *lbl in cell.contentView.subviews)
        [lbl removeFromSuperview];
    float x = (((SCREEN_WIDTH/2)-10) - SCREEN_WIDTH/2.5)/2;
    float y = 10;
    float productBlockHeight;
    UIView *productImageBlock = [[UIView alloc]initWithFrame:CGRectMake(x, y, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
    productImageBlock.clipsToBounds = YES;
    UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
    productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
    productImage.userInteractionEnabled = YES;
    productImage.tag = [indexPath row];
    [productImageBlock addSubview:productImage];
    UIImage *image = [imageCache objectForKey:productCollectionDictionary[@"thumbNail"]];
    if (image)
        productImage.image = image;
    else{
        [_queue addOperationWithBlock:^{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: productCollectionDictionary[@"thumbNail"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            if(image){
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    productImage.image = image;
                }];
                [imageCache setObject:image forKey:productCollectionDictionary[@"thumbNail"]];
            }
        }];
    }
    UITapGestureRecognizer *openProductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewProduct:)];
    openProductGesture.numberOfTapsRequired = 1;
    [productImage addGestureRecognizer:openProductGesture];
    
    UIView *productImageCurtain;
    if([productCurtainOpenSignal[[indexPath row]] isEqual: @"1"])
        productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
    else
        productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
    [productImageCurtain setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [productImageBlock addSubview:productImageCurtain];
    
    float subWish = 64;
    float subAdd = 32;
    
    if(SCREEN_WIDTH < 500){
        subWish = 54;
        subAdd = 22;
    }
    
    
    
    UIImageView *wishlistButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)-subWish, (SCREEN_WIDTH/2.5)-40, 32, 32)];
    [wishlistButton setImage:[UIImage imageNamed:@"ic_addtowishlist.png"]];
    wishlistButton.tag = [indexPath row];
    wishlistButton.userInteractionEnabled = YES;
    UITapGestureRecognizer *addtowishlistGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addtoWishlist:)];
    addtowishlistGesture.numberOfTapsRequired = 1;
    [wishlistButton addGestureRecognizer:addtowishlistGesture];
    [productImageCurtain addSubview:wishlistButton];
    if([productCollectionDictionary[@"hasOptions"] isEqual: @"1"] || [productCollectionDictionary[@"typeId"] isEqual: @"configurable"] || [productCollectionDictionary[@"typeId"] isEqual: @"bundle"] || [productCollectionDictionary[@"typeId"] isEqual: @"grouped"]){
        UIImageView *viewproductButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
        [viewproductButton setImage:[UIImage imageNamed:@"ic_viewproduct.png"]];
        viewproductButton.tag = [indexPath row];
        viewproductButton.userInteractionEnabled = YES;
        UITapGestureRecognizer *viewproductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewProduct:)];
        viewproductGesture.numberOfTapsRequired = 1;
        [viewproductButton addGestureRecognizer:viewproductGesture];
        [productImageCurtain addSubview:viewproductButton];
    }
    else{
        UIImageView *addtocartButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
        [addtocartButton setImage:[UIImage imageNamed:@"ic_addtocart.png"]];
        addtocartButton.tag = [indexPath row];
        addtocartButton.userInteractionEnabled = YES;
        UITapGestureRecognizer *addtocartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addtoCart:)];
        addtocartGesture.numberOfTapsRequired = 1;
        [addtocartButton addGestureRecognizer:addtocartGesture];
        [productImageCurtain addSubview:addtocartButton];
    }
    [cell.contentView addSubview:productImageBlock];
    y += SCREEN_WIDTH/2.5+5;
    
    UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(10, y, ((SCREEN_WIDTH/2)-50), 25)];
    [productName setTextColor:[GlobalData colorWithHexString:@"555555"]];
    productName.tag = [indexPath row];
    [productName setBackgroundColor:[UIColor clearColor]];
    [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [productName setText:productCollectionDictionary[@"name"]];
    [cell.contentView addSubview:productName];
    
    int t = y;
    if(SCREEN_WIDTH >500){
        t = y+60;
    }
    
    UIImageView *productOption = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2)-42, t, 32, 32)];
    [productOption setImage:[UIImage imageNamed:@"ic_pro_option.png"]];
    productOption.userInteractionEnabled = YES;
    productOption.tag = [indexPath row];
    [cell.contentView addSubview:productOption];
    
    if(SCREEN_WIDTH >500){
        y += 30;
        if([productCollectionDictionary[@"typeId"] isEqual: @"grouped"]){
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 90, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [preString setText:[globalObjectProductCat.languageBundle localizedStringForKey:@"starting" value:@"" table:nil]];
            [cell.contentView addSubview:preString];
            
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(95, y, 70, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [price setText:productCollectionDictionary[@"groupedPrice"]];
            [cell.contentView addSubview:price];
        }
        else
            if([productCollectionDictionary[@"typeId"] isEqual: @"bundle"]){
                UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 50, 25)];
                [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [from setBackgroundColor:[UIColor clearColor]];
                [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [from setText:[globalObjectProductCat.languageBundle localizedStringForKey:@"from" value:@"" table:nil]];
                [cell.contentView addSubview:from];
                
                UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(55, y, 60, 25)];
                [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [minPrice setBackgroundColor:[UIColor clearColor]];
                [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [minPrice setText:productCollectionDictionary[@"formatedMinPrice"]];
                [cell.contentView addSubview:minPrice];
                
                UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(125, y, 25, 25)];
                [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [to setBackgroundColor:[UIColor clearColor]];
                [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [to setText:[globalObjectProductCat.languageBundle localizedStringForKey:@"to" value:@"" table:nil]];
                
                
                UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(147, y, 60, 25)];
                [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [maxPrice setBackgroundColor:[UIColor clearColor]];
                [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [maxPrice setText:productCollectionDictionary[@"formatedMaxPrice"]];
                
                float widthX = cell.frame.size.width - 125;
                
                if(widthX > 90){
                    [cell.contentView addSubview:to];
                    [cell.contentView addSubview:maxPrice];
                }else{
                    UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(125, y, widthX, 25)];
                    [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [dot setBackgroundColor:[UIColor clearColor]];
                    [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [dot setText:[[globalObjectProductCat.languageBundle localizedStringForKey:@"to" value:@"" table:nil] stringByAppendingString:@" ..."]];
                    [cell.contentView addSubview:dot];
                }
                
            }
            else{
                if(![productCollectionDictionary[@"specialPrice"] isEqual:[NSNull null]] && [productCollectionDictionary[@"isInRange"] boolValue]){
                    UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 70, 25)];
                    [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [regularprice setBackgroundColor:[UIColor clearColor]];
                    [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:productCollectionDictionary[@"formatedPrice"]];
                    [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                    [regularprice setAttributedText:attributeString];
                    [cell.contentView addSubview:regularprice];
                    
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 85, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [price setText:productCollectionDictionary[@"formatedSpecialPrice"]];
                    [cell.contentView addSubview:price];
                }
                else{
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 60, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [price setText:productCollectionDictionary[@"formatedPrice"]];
                    [cell.contentView addSubview:price];
                }
            }
        if([productCollectionDictionary[@"hasTierPrice"] isEqual:@"true"]){
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 90, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"cf5050"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [preString setText:[globalObjectProductCat.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil]];
            
            
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(165, y, 60, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [price setText:productCollectionDictionary[@"tierPrice"]];
            
            float widthX = cell.frame.size.width - 80;
            
            if(widthX > 150){
                [cell.contentView addSubview:preString];
                [cell.contentView addSubview:price];
            }
            else{
                UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(80, y, widthX, 25)];
                [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [dot setBackgroundColor:[UIColor clearColor]];
                [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [dot setText:[[globalObjectProductCat.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil] stringByAppendingString:@" ..."]];
                [cell.contentView addSubview:dot];
            }
            //[cell.contentView addSubview:price];
        }
    }
    y += 30;
    
    UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(10,y,120,24)];
    UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
    [ratingContainer addSubview:grayContainer];
    UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
    gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI1];
    UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
    gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI2];
    UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
    gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI3];
    UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
    gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI4];
    UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
    gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI5];
    
    double percent = 24 * [productCollectionDictionary[@"rating"] doubleValue];
    UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
    blueContainer.clipsToBounds = YES;
    [ratingContainer addSubview:blueContainer];
    UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
    bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI1];
    UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
    bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI2];
    UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
    bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI3];
    UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
    bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI4];
    UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
    bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI5];
    [cell.contentView addSubview:ratingContainer];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float productBlockHeight;
    
    if(SCREEN_WIDTH > 500){
        productBlockHeight = (SCREEN_WIDTH/2.5)+117;
    }
    else{
        productBlockHeight = (SCREEN_WIDTH/2.5)+75;
    }
    return CGSizeMake((SCREEN_WIDTH/2)-10, productBlockHeight);
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    // NSLog(@" %ld",[self.categoryProductCollectionView numberOfItemsInSection:0]-1);
    NSInteger currentCellCount = [self.categoryProductCollectionView numberOfItemsInSection:0];
    //NSLog(@"current cell count %ld   %ld",currentCellCount,totalCount);
    for (UICollectionViewCell *cell in [self.categoryProductCollectionView visibleCells]) {
        indexPathValue = [self.categoryProductCollectionView indexPathForCell:cell];
        if([indexPathValue row] == [self.categoryProductCollectionView numberOfItemsInSection:0]-1){
            if((totalCount > currentCellCount) && loadPageRequestFlag ){
                whichApiDataToprocess = @"";
                reloadPageData = @"true";
                pageNumber +=1;
                loadPageRequestFlag = 0;
                contentHeight = _categoryProductCollectionView.contentOffset.y;
                NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                if(savedSessionId == nil)
                    [self loginRequest];
                else
                    [self callingHttppApi];
            }
        }
    }
}


-(void)applySort:(UITapGestureRecognizer *)recognizer{
    int viewIndex = (int)(2*recognizer.view.tag)+2;
    int index = (int)recognizer.view.tag;
    UIView *currentView = [recognizer.view.superview.subviews objectAtIndex:viewIndex];
    UIImageView *dir = [currentView.subviews objectAtIndex:1];
    if([[sortDirection objectAtIndex:index] isEqual: @"0"]){
        [sortDirection replaceObjectAtIndex:index withObject:@"1"];
        dir.image = [UIImage imageNamed:@"ic-down.png"];
        sortDir = @"1";
    }
    else{
        [sortDirection replaceObjectAtIndex:index withObject:@"0"];
        dir.image = [UIImage imageNamed:@"ic-up.png"];
        sortDir = @"0";
    }
    NSDictionary *sortingDict = [mainCollection[@"sortingData"] objectAtIndex:recognizer.view.tag];
    sortItem = sortingDict[@"code"];
    UIVisualEffectView *toRemove = (UIVisualEffectView *)[self.view viewWithTag:999];
    [toRemove removeFromSuperview];
    finalData = [[NSMutableString alloc] initWithString: @""];
    NSError *error = nil;
    NSData *objectData = [finalData dataUsingEncoding:NSUTF8StringEncoding];
    mainCollection = [NSJSONSerialization JSONObjectWithData:objectData options:0 error:&error];
    [_categoryProductCollectionView reloadData];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    whichApiDataToprocess = @"";
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    pageNumber = 1;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)sortByTapped:(UITapGestureRecognizer *)recognizer{
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 999;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UIView *sortBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
    [sortBlock setBackgroundColor : [UIColor whiteColor]];
    sortBlock.layer.cornerRadius = 4;
    
    float y = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 32)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [sortTitle setText : [globalObjectProductCat.languageBundle localizedStringForKey:@"sortBy" value:@"" table:nil]];
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [sortBlock addSubview : sortTitle];
    
    y += 42;
    UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 1)];
    [hr setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [sortBlock addSubview : hr];
    
    
    for(int i=0; i<[mainCollection[@"sortingData"] count]; i++) {
        NSDictionary *sortingDict = [mainCollection[@"sortingData"] objectAtIndex:i];
        y += 6;
        UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 32)];
        [containerView setBackgroundColor:[UIColor whiteColor]];
        containerView.tag = i;
        containerView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(applySort:)];
        tap.numberOfTapsRequired = 1;
        [containerView addGestureRecognizer:tap];
        
        UILabel *sortTerm = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, (SCREEN_WIDTH/2.5)-47, 32)];
        [sortTerm setTextColor:[GlobalData colorWithHexString : @"555555"]];
        [sortTerm setBackgroundColor:[UIColor clearColor]];
        [sortTerm setFont:[UIFont fontWithName:@"Trebuchet MS" size:22.0f]];
        [sortTerm setText: sortingDict[@"label"]];
        [containerView addSubview:sortTerm];
        
        UIImageView *directionIcon = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)-47, 0, 32,32)];
        if([[sortDirection objectAtIndex:i] isEqual: @"0"]){
            directionIcon.image = [UIImage imageNamed:@"ic-down.png"];
            sortDir = @"1";
        }
        else{
            directionIcon.image = [UIImage imageNamed:@"ic-up.png"];
            sortDir = @"0";
        }
        [containerView addSubview:directionIcon];
        [sortBlock addSubview:containerView];
        
        y += 37;
        if(i < [mainCollection[@"sortingData"] count]-1){
            UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2.5, 1)];
            [hr setBackgroundColor:[GlobalData colorWithHexString : @"555555"]];
            [sortBlock addSubview : hr];
        }
    }
    CGRect newFrame = sortBlock.frame;
    newFrame.size.height = y+5;
    sortBlock.frame = newFrame;
    sortBlock.center = blurEffectView.center;
    
    UIImageView *closeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-52, 82, 32,32)];
    closeIcon.image = [UIImage imageNamed:@"ic_close.png"];
    closeIcon.userInteractionEnabled = YES;
    [blurEffectView addSubview:closeIcon];
    UITapGestureRecognizer *tapClose = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeSortView:)];
    tapClose.numberOfTapsRequired = 1;
    [closeIcon addGestureRecognizer:tapClose];
    [blurEffectView addSubview:sortBlock];
    [self.view addSubview:blurEffectView];
}

-(void)closeSortView:(UITapGestureRecognizer *)recognizer{
    [recognizer.view.superview removeFromSuperview];
}

-(void)showProductShowOptions:(UITapGestureRecognizer *)recognizer{
    CGPoint tapLocation = [recognizer locationInView:_categoryProductCollectionView];
    NSIndexPath *indexPath = [_categoryProductCollectionView indexPathForItemAtPoint:tapLocation];
    CollectionViewCell *cell = (CollectionViewCell *)[_categoryProductCollectionView cellForItemAtIndexPath:indexPath];
    UIImageView *productOption = [cell.contentView.subviews objectAtIndex:2];
    UIView *containerView = [cell.contentView.subviews objectAtIndex:0];
    UIView *curtainView = [containerView.subviews objectAtIndex:1];
    CGRect productOptionCordinate = [_categoryProductCollectionView convertRect:productOption.frame fromView:cell];
    if (CGRectContainsPoint(productOptionCordinate, tapLocation)){
        if([productCurtainOpenSignal[productOption.tag] isEqual: @"0"]) {
            [UIView animateWithDuration:0.5 delay:0.1 options: UIViewAnimationOptionCurveEaseOut animations:^{
                curtainView.frame = CGRectMake(0, 0, curtainView.frame.size.width, curtainView.frame.size.height);
            }
                             completion:^(BOOL finished){
                                 if (finished)
                                     productCurtainOpenSignal[productOption.tag] = @"1";
                             }];
        }
        else{
            [UIView animateWithDuration:0.5 delay:0.1 options: UIViewAnimationOptionCurveEaseOut animations:^{
                curtainView.frame = CGRectMake(0, curtainView.frame.size.height, curtainView.frame.size.width, curtainView.frame.size.height);
            }
                             completion:^(BOOL finished){
                                 if (finished)
                                     productCurtainOpenSignal[productOption.tag] = @"0";
                             }];
        }
    }
}

-(void)addtoWishlist:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId == nil){
        UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectProductCat.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectProductCat.languageBundle localizedStringForKey:@"logInWishList" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectProductCat.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          [self performSegueWithIdentifier:@"addtoWishlistLoginSegue" sender:self];
                                                      }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectProductCat.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [AC addAction:okBtn];
        [AC addAction:noBtn];
        [self.parentViewController presentViewController:AC animated:YES completion:nil];
    }
    else{
        NSDictionary *tempDictionary = [[NSDictionary alloc] init];
        // tempDictionary = [mainCollection[@"categoryData"] objectAtIndex:recognizer.view.tag];
        tempDictionary = [arrayMainCollection objectAtIndex:recognizer.view.tag];
        whichApiDataToprocess = @"addToWishlist";
        productIdForApiCall = tempDictionary[@"entityId"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)viewProduct:(UITapGestureRecognizer *)recognizer{
    productDictionary = [[NSDictionary alloc] init];
    productDictionary = [arrayMainCollection objectAtIndex:recognizer.view.tag];
    [self performSegueWithIdentifier:@"openProductFromCategoryPageSegue" sender:self];
}

-(void)addtoCart:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary *tempDictionary = [[NSDictionary alloc] init];
    //tempDictionary = [mainCollection[@"categoryData"] objectAtIndex:recognizer.view.tag];
    tempDictionary = [arrayMainCollection objectAtIndex:recognizer.view.tag];
    whichApiDataToprocess = @"addToCart";
    productIdForApiCall = tempDictionary[@"entityId"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
}

-(void)filterByTapped:(UITapGestureRecognizer *)recognizer{
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 888;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    float Y = 0;
    
    
    float height= [[UIScreen mainScreen] bounds].size.height;
    
    ratingContainerBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5, 900)];
    [ratingContainerBlock setBackgroundColor : [UIColor whiteColor]];
    ratingContainerBlock.layer.cornerRadius = 4;
    
    ratingContainerBlock.tag = 999;
    
    
    UIScrollView *ratingScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0  ,SCREEN_WIDTH/2+SCREEN_WIDTH/2.5,height-100)];
    [ratingScrollView setBackgroundColor: [UIColor whiteColor]];
    ratingScrollView.userInteractionEnabled = YES;
    [ratingScrollView  addSubview:ratingContainerBlock];
    
    UIButton  *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(filterRatingCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"  <  Filter By" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0,0,ratingContainerBlock.frame.size.width, 40);
    [cancelButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cancelButton setBackgroundColor:[GlobalData colorWithHexString:@"E80C22"] ];
    [cancelButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [ratingContainerBlock addSubview:cancelButton];
    
    Y +=60;
    
    UILabel *headerName = [[UILabel alloc] initWithFrame:CGRectMake(5, Y ,ratingContainerBlock.frame.size.width -50 , 35)];
    [headerName setTextColor:[GlobalData colorWithHexString:@"B91F2E"]];
    [headerName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [headerName setText:@"Shop By"];
    [ratingContainerBlock addSubview:headerName];
    
    Y +=40;
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, Y, ratingContainerBlock.frame.size.width, 1)];           // line2
    [line setBackgroundColor:[GlobalData colorWithHexString : @"D12738"]];
    [ratingContainerBlock addSubview : line];
    
    Y +=4;
    
    for(int k=0;k<[filterCodeHeader count];k++){
        
        NSString *labelName = [[filterCodeHeader objectAtIndex:k] stringByAppendingString:@" : "];
        
        
        NSString *labelValue =  [filterItemValue objectAtIndex:k];
        UILabel *ratingHead = [[UILabel alloc] initWithFrame:CGRectMake(5, Y ,ratingContainerBlock.frame.size.width -50 , 25)];
        [ratingHead setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [ratingHead setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
        [ratingHead setText:[labelName stringByAppendingString:labelValue]];
        [ratingHead setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [ratingContainerBlock addSubview:ratingHead];
        
        UIButton  *cancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [cancel addTarget:self action:@selector(filterRemoveItemFromArray:) forControlEvents:UIControlEventTouchUpInside];
        [cancel setTitle:@"X" forState:UIControlStateNormal];
        cancel.frame = CGRectMake(ratingContainerBlock.frame.size.width -50,Y,40, 25);
        [cancel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [cancel setBackgroundColor:[GlobalData colorWithHexString:@"E80C22"]];
        cancel.tag = k;
        [cancel setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [ratingContainerBlock addSubview:cancel];
        
        
        Y +=30;
    }
    if([filterCodeHeader count]>0){
        
        Y +=10;
        UILabel *clearAll = [[UILabel alloc] initWithFrame:CGRectMake(ratingContainerBlock.frame.size.width -100 , Y ,100 , 25)];
        [clearAll setTextColor:[GlobalData colorWithHexString:@"E80C22"]];
        [clearAll setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
        [clearAll setText:@"Clear All"];
        [ratingContainerBlock addSubview:clearAll];
        
        Y +=30;
        
        UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, Y, ratingContainerBlock.frame.size.width, 1)];           // line2
        [line1 setBackgroundColor:[GlobalData colorWithHexString : @"D12738"]];
        [ratingContainerBlock addSubview : line1];
        
        Y +=4;
        
        UITapGestureRecognizer *clearAllTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clearAllFilterData:)];
        clearAllTap.numberOfTapsRequired = 1;
        clearAll.userInteractionEnabled = YES;
        [clearAll addGestureRecognizer:clearAllTap];
        
        
    }
    
    
    Y +=20;
    
    for(int i=0 ;i<[mainCollection[@"layeredData"] count]; i++){
        NSDictionary *filterOption = [mainCollection[@"layeredData"] objectAtIndex:i];
        UILabel *ratingHead = [[UILabel alloc] initWithFrame:CGRectMake(15, Y ,ratingContainerBlock.frame.size.width-30, 25)];
        [ratingHead setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [ratingHead setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [ratingHead setText:filterOption[@"label"]];
        [ratingHead setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [ratingContainerBlock addSubview:ratingHead];
        
        Y +=40;
        
        
        for(int j= 0 ;j<[filterOption[@"options"] count]; j++){
            
            NSDictionary *filterValue = [filterOption[@"options"] objectAtIndex:j];
            
            
            //NSString *filterName = filterValue[@"label"];
            NSString *labelName = [filterValue[@"label"] stringByAppendingString:@" ("];
            NSString *labelNameWithCount = [labelName stringByAppendingString:filterValue[@"count"]];
            NSString *finalLabelNameWithCount = [labelNameWithCount stringByAppendingString:@")"];
            
            
            UILabel *ratingValue = [[UILabel alloc] initWithFrame:CGRectMake(50, Y ,ratingContainerBlock.frame.size.width, 25)];
            [ratingValue setTextColor:[GlobalData colorWithHexString:@"218E1D"]];
            [ratingValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
            [ratingValue setText:finalLabelNameWithCount];
            [ratingContainerBlock addSubview:ratingValue];
            
            UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake( 15 , Y, 25, 25)];
            radioBtnContainer.tag = 100+i;
            radioBtnContainer.layer.cornerRadius = 13;
            radioBtnContainer.layer.masksToBounds = YES;
            radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
            radioBtnContainer.layer.borderWidth = 2.0f;
            
            
            UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
            [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
            radioBtn.tag = 1+j;
            radioBtn.layer.cornerRadius = 13;
            radioBtn.layer.masksToBounds = YES;
            radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
            radioBtn.layer.borderWidth = 5.0f;
            UITapGestureRecognizer *addReviewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectThisOption:)];
            addReviewGesture.numberOfTapsRequired = 1;
            [radioBtn addGestureRecognizer:addReviewGesture];
            [radioBtnContainer addSubview:radioBtn];
            [ratingContainerBlock addSubview:radioBtnContainer];
            
            
            Y +=30;
            
        }
        Y +=10;
        
    }
    
    
    
    CGRect  ratingContainerNewFrame = ratingContainerBlock.frame;
    ratingContainerNewFrame.size.height = Y;
    ratingContainerBlock.frame = ratingContainerNewFrame;
    
    
    ratingScrollView.contentSize = CGSizeMake(50,Y);
    ratingScrollView.center = blurEffectView.center;
    [blurEffectView addSubview:ratingScrollView];
    [self.view addSubview:blurEffectView];
    
}

-(void)filterRatingCancelButton :(UIButton*)button{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    [parent1.superview removeFromSuperview];
    
}
-(void)filterRemoveItemFromArray :(UIButton*)button{
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [filterCodeHeader removeObjectAtIndex:button.tag];
    [filterItemValue removeObjectAtIndex:button.tag];
    
    [filterCodeValue removeObjectAtIndex:button.tag];
    [filterIdValue removeObjectAtIndex:button.tag];
    
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    [parent1.superview removeFromSuperview];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    whichApiDataToprocess = @"";
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    pageNumber = 1;
    if(savedSessionId == nil){
        [self loginRequest];
        
    }
    else{
        [self callingHttppApi];
    }
    
    
}

-(void)selectThisOption:(UITapGestureRecognizer *)recognizer{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    for(UIView *subViews in ratingContainerBlock.subviews){
        if(subViews.tag == recognizer.view.superview.tag){
            for(UIView *subViews1 in subViews.subviews)
                [subViews1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    NSDictionary *filterOption = [mainCollection[@"layeredData"] objectAtIndex:recognizer.view.superview.tag-100];
    NSDictionary *filterValue = [filterOption[@"options"] objectAtIndex:recognizer.view.tag-1];
    [filterCodeValue addObject:filterOption[@"code"]];  // to send
    [filterIdValue addObject:filterValue[@"id"]];      // to send
    [filterCodeHeader addObject:filterOption[@"label"]];  // to display
    [filterItemValue addObject:filterValue[@"label"]];   // to display
    UIView *parent = recognizer.view;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    UIView *parent3 = parent2.superview;
    [parent3.superview removeFromSuperview];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    whichApiDataToprocess = @"";
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    pageNumber = 1;
    if(savedSessionId == nil){
        [self loginRequest];
        
    }
    else{
        [self callingHttppApi];
    }
    
    
}

-(void)clearAllFilterData:(UITapGestureRecognizer *)recognizer{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [filterCodeValue removeAllObjects];  // to send
    [filterIdValue removeAllObjects];      // to send
    [filterCodeHeader removeAllObjects];  // to display
    [filterItemValue removeAllObjects];   // todisplay
    UIView *parent = recognizer.view;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    [parent2.superview removeFromSuperview];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    pageNumber = 1;
    whichApiDataToprocess = @"";
    if(savedSessionId == nil){
        [self loginRequest];
    }
    else{
        [self callingHttppApi];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openProductFromCategoryPageSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = productDictionary[@"entityId"];
        destViewController.productName = productDictionary[@"name"];
        destViewController.productType = productDictionary[@"typeId"];
        if([self.tabBarController isViewLoaded] ){
            destViewController.parentClass = @"notification";
        }else{
            destViewController.parentClass = @"ProductCategory"; 
        }
    }
    if([segue.identifier isEqualToString:@"addtoWishlistLoginSegue"]) {
        CustomerLogin *destViewController = segue.destinationViewController;
        destViewController.catalogProductFlag = @"1";
    }
}


@end
