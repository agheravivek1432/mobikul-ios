//
//  MyProductReviews.h
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProductReviews : UIViewController <NSXMLParserDelegate,UIScrollViewDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id collection;
    NSCache *imageCache;
    int reviewId;
    UIWindow *currentWindow;
    NSArray *arrayMainCollection;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
    NSString *reloadPageData;
}

@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UIView *productReviewContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *myProductReviewScrollView;

@end
