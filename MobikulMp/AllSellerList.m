//
//  AllSellerList.m
//  MobikulMp
//
//  Created by kunal prasad on 29/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AllSellerList.h"
#import "GlobalData.h"
#import "SellerProfileData.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

@interface AllSellerList ()

@end

GlobalData *globalObjectSllerLIst;

@implementation AllSellerList

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectSllerLIst = [[GlobalData alloc] init];
    globalObjectSllerLIst.delegate = self;
    [globalObjectSllerLIst language];
    isAlertVisible = 0;
    whichApiDataToProcess = @"";
    shopTitle = [[NSMutableDictionary alloc]init];
    imageCache = [[NSCache alloc] init];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];

}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectSllerLIst callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectSllerLIst.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectSllerLIst.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectSllerLIst.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectSllerLIst.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectSllerLIst.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *storeId = [preferences objectForKey:@"storeId"];
     NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiDataToProcess isEqual:@"searchSellerItem"]){
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"width=%@&", screenWidth];
        [post appendFormat:@"shopTitle=%@", [shopTitle objectForKey:@"shoptitle"]];
    }
    else {
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"width=%@", screenWidth];
    }
    [globalObjectSllerLIst callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getsellerList" signal:@"HttpPostMetod"];
    globalObjectSllerLIst.delegate = self;
    
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    float mainContainerY = 0;
    float internalY = 0;
    
    for(UIView *subViews in _mainView.subviews){
        [subViews removeFromSuperview];
    }
    
    // banner immage;
    
    UIView *bannerContainer = [[UIView alloc] initWithFrame:CGRectMake(5, 0,  _mainView.frame.size.width-10, SCREEN_WIDTH/2)];
    [_mainView addSubview:bannerContainer];
    
    UIImageView *banner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 , bannerContainer.frame.size.width, bannerContainer.frame.size.height)];
    banner.image = [UIImage imageNamed:@"placeholder_marketplace_home_banner.png"];
    banner.userInteractionEnabled = YES;
    UIImage *image = [imageCache objectForKey:mainCollection[@"bannerImage"]];
    if(mainCollection[@"bannerImage"]){
    if(image)
        banner.image = image;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"bannerImage"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        banner.image = image;
        if(image != nil)
            [imageCache setObject:image forKey:mainCollection[@"bannerImage"]];
    }
    }
    [bannerContainer addSubview:banner];

    
    //banner image head text
    
    NSString *bannerHeadText = [mainCollection[@"banner"] objectAtIndex:0];
    UILabel *bannerHead = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, bannerContainer.frame.size.width, 25)];
    [bannerHead setTextColor:[GlobalData colorWithHexString:@"ffffff"]];
    [bannerHead setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [bannerHead setText:bannerHeadText];
    bannerHead.textAlignment = NSTextAlignmentCenter;
    [bannerContainer addSubview:bannerHead];
    
    
    //banner image description
    
    NSString *bannerDescText = [mainCollection[@"banner"] objectAtIndex:1];
    NSDictionary *reqAttributesforDesc = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};
    CGSize reqStringSizeDesc = [bannerDescText sizeWithAttributes:reqAttributesforDesc];
    CGFloat reqStringHeightDesc = reqStringSizeDesc.height;
    UILabel *bannerDesc = [[UILabel alloc] initWithFrame:CGRectMake(0,(SCREEN_WIDTH-4)/16 + 26, SCREEN_WIDTH, reqStringHeightDesc)];
    [bannerDesc setTextColor:[GlobalData colorWithHexString:@"ffffff"]];
    [bannerDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    [bannerDesc setText:bannerDescText];
    bannerDesc.lineBreakMode = NSLineBreakByWordWrapping;
    bannerDesc.numberOfLines = 0;
    bannerDesc.textAlignment = NSTextAlignmentCenter;
    [bannerContainer addSubview:bannerDesc];
    
    mainContainerY+=(SCREEN_WIDTH)/2;
    internalY +=mainContainerY;
    internalY +=10;
    
    //banner title
    
    UILabel *requiredLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, internalY,SCREEN_WIDTH, 30)];
    [requiredLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
    [requiredLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [requiredLabel setText:mainCollection[@"topLabel"]];
    requiredLabel.textAlignment = NSTextAlignmentCenter;
    [_mainView addSubview:requiredLabel];
    
    internalY +=35;
    
    // search box
    
    UITextField *nameField = [[UITextField alloc] initWithFrame:CGRectMake(5, internalY, SCREEN_WIDTH /2+SCREEN_WIDTH/4 , 40)];
    nameField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    nameField.textColor = [UIColor blackColor];	
    [nameField setPlaceholder:@"Search sellers by shop name from here....."];
    nameField.layer.borderWidth = 1.0f;
    nameField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    nameField.tag = 999;
    [nameField setKeyboardType:UIKeyboardTypePhonePad];
    nameField.textAlignment = NSTextAlignmentLeft;
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    [_mainView addSubview:nameField];
    
    // Search button
    
    UIButton *Searchbutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [Searchbutton addTarget:self action:@selector(buttonHandlerForSearchItem :) forControlEvents:UIControlEventTouchUpInside];
    [Searchbutton setTitle:@"SEARCH" forState:UIControlStateNormal];
    Searchbutton.frame = CGRectMake(SCREEN_WIDTH /2+SCREEN_WIDTH/4 +5,internalY,_mainView.frame.size.width/4 -5, 40.0);
    [Searchbutton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
    [Searchbutton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"]];
    Searchbutton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [Searchbutton setTitleColor:[GlobalData colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [_mainView addSubview:Searchbutton];
    
    internalY += 50;
    
    if(SCREEN_WIDTH >600){
    if([mainCollection[@"sellers"] count]>0){
    for(int i =0 ;i <[mainCollection[@"sellers"] count];i++){
        NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:i];

        UIView *sellerGroupContainer = [[UIView alloc]initWithFrame:CGRectMake(5, internalY, _mainView.frame.size.width-10, 200)];
        sellerGroupContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
        sellerGroupContainer.layer.borderWidth = 2.0f;
        sellerGroupContainer.tag = i;
        [_mainView addSubview:sellerGroupContainer];
        
        UIImageView *sellerIcon = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10,SCREEN_WIDTH/6,SCREEN_WIDTH/6)];
        sellerIcon.image = [UIImage imageNamed:@"ic_placeholder.png"];
        NSData *imageDataProd = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"sellerIcon"]]];
        UIImage *imagePro = [UIImage imageWithData: imageDataProd];
        sellerIcon.image = imagePro;
        sellerIcon.tag = i;
        //sellerImageTouch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callFourthSellerIcon:)];
        sellerIcon.userInteractionEnabled = "YES";
       // sellerImage4Touch.numberOfTapsRequired = 1;
       // [sellerIcon4 addGestureRecognizer:sellerImage4Touch];
        [sellerGroupContainer addSubview:sellerIcon];

        NSDictionary *reqAttributesforShopTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:24.0f]};
        CGSize reqStringSizeShopTitle = [sellerOption[@"shopTitle"] sizeWithAttributes:reqAttributesforShopTitle];
        CGFloat reqStringWidthShopTitle = reqStringSizeShopTitle.width;
        UILabel *requiredShopTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/6 + 40, 10, SCREEN_WIDTH-SCREEN_WIDTH/6-50 , SCREEN_WIDTH/18)];
        [requiredShopTitleLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
        [requiredShopTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [requiredShopTitleLabel setText:sellerOption[@"shopTitle"]];
        requiredShopTitleLabel.textAlignment = NSTextAlignmentCenter;
        requiredShopTitleLabel.tag = i;
         sellorTitleTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callSellerTitleTap:)];
        requiredShopTitleLabel.userInteractionEnabled = "YES";
         sellorTitleTap.numberOfTapsRequired = 1;
        [requiredShopTitleLabel addGestureRecognizer:sellorTitleTap];
        [sellerGroupContainer addSubview:requiredShopTitleLabel];

        
        NSString *inStr = [NSString stringWithFormat: @"%d", [sellerOption[@"sellerProductCount"] intValue]];
        NSString *sellerCount = [inStr stringByAppendingString:@" PRODUCTS"];
        NSDictionary *reqAttributesforSellerCounnt = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:24.0f]};
        CGSize reqStringSellerSize = [sellerCount sizeWithAttributes:reqAttributesforSellerCounnt];
        CGFloat reqStringWidthSellerCount = reqStringSellerSize.width;
        UILabel *requiredSellerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/6 + 40, SCREEN_WIDTH/18 +10 , SCREEN_WIDTH-SCREEN_WIDTH/6-50, SCREEN_WIDTH/18)];
        [requiredSellerCountLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [requiredSellerCountLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [requiredSellerCountLabel setText:sellerCount];
        requiredSellerCountLabel.textAlignment = NSTextAlignmentCenter;
        [sellerGroupContainer addSubview:requiredSellerCountLabel];
        
        
        
        NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforViewAllButtonText = [@"View All" sizeWithAttributes:reqAttributesforViewAllButtonText];
        CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
        UIButton  *buttonViewAll = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [buttonViewAll addTarget:self action:@selector(buttonHandlerForViewAll :) forControlEvents:UIControlEventTouchUpInside];
        [buttonViewAll setTitle:@"View All" forState:UIControlStateNormal];
        buttonViewAll.frame = CGRectMake(SCREEN_WIDTH/6 + 40,SCREEN_WIDTH/18 +10+SCREEN_WIDTH/18,SCREEN_WIDTH-SCREEN_WIDTH/6-50-10 ,SCREEN_WIDTH/18);
        [buttonViewAll setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [buttonViewAll setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
        [buttonViewAll setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [sellerGroupContainer addSubview:buttonViewAll];
        
        
        
        CGRect newFrame = sellerGroupContainer.frame;
        newFrame.size.height = SCREEN_WIDTH/6 +20;
        sellerGroupContainer.frame = newFrame;
        internalY +=SCREEN_WIDTH/6 +30;
        mainContainerY =internalY ;
    }}
    
    else{
        
        mainContainerY = internalY;
        UIView *emptyContainer = [[UIView alloc]initWithFrame:CGRectMake(5, internalY, _mainView.frame.size.width-10, 40)];
        emptyContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
        emptyContainer.layer.borderWidth = 2.0f;
        emptyContainer.backgroundColor =[GlobalData colorWithHexString:@"7F7F7F"];
        [_mainView addSubview:emptyContainer];
        
        UILabel *defaultTItleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0 , _mainView.frame.size.width-10 ,40)];
        [defaultTItleLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [defaultTItleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [defaultTItleLabel setText:@"No Items Found For Requested Value... Please Search with different Value."];
        defaultTItleLabel.textAlignment = NSTextAlignmentCenter;
        [emptyContainer addSubview:defaultTItleLabel];
        internalY += 100;
        mainContainerY = internalY ;
    }
  }
    else{  // screen less than 600;
        if([mainCollection[@"sellers"] count]>0){
            for(int i =0 ;i <[mainCollection[@"sellers"] count];i++){
                NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:i];
                
                UIView *sellerGroupContainer = [[UIView alloc]initWithFrame:CGRectMake(5, internalY, _mainView.frame.size.width-10, 200)];
                sellerGroupContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
                sellerGroupContainer.layer.borderWidth = 2.0f;
                sellerGroupContainer.tag = i;
                [_mainView addSubview:sellerGroupContainer];
                
                UIImageView *sellerIcon = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10,SCREEN_WIDTH/6,SCREEN_WIDTH/6 +20)];
                sellerIcon.image = [UIImage imageNamed:@"ic_placeholder.png"];
                NSData *imageDataProd = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"sellerIcon"]]];
                UIImage *imagePro = [UIImage imageWithData: imageDataProd];
                sellerIcon.image = imagePro;
                sellerIcon.tag = i;
                //sellerImageTouch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callFourthSellerIcon:)];
                sellerIcon.userInteractionEnabled = "YES";
                // sellerImage4Touch.numberOfTapsRequired = 1;
                // [sellerIcon4 addGestureRecognizer:sellerImage4Touch];
                [sellerGroupContainer addSubview:sellerIcon];
                
            
                UILabel *requiredShopTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/6 + 40, 10, SCREEN_WIDTH-SCREEN_WIDTH/6-50 , SCREEN_WIDTH/18)];
                [requiredShopTitleLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
                [requiredShopTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
                [requiredShopTitleLabel setText:sellerOption[@"shopTitle"]];
                requiredShopTitleLabel.textAlignment = NSTextAlignmentCenter;
                requiredShopTitleLabel.tag = i;
                sellorTitleTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callSellerTitleTap:)];
                requiredShopTitleLabel.userInteractionEnabled = "YES";
                sellorTitleTap.numberOfTapsRequired = 1;
                [requiredShopTitleLabel addGestureRecognizer:sellorTitleTap];
                [sellerGroupContainer addSubview:requiredShopTitleLabel];
                
                
                NSString *inStr = [NSString stringWithFormat: @"%d", [sellerOption[@"sellerProductCount"] intValue]];
                NSString *sellerCount = [inStr stringByAppendingString:@" PRODUCTS"];
                UILabel *requiredSellerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/6 + 40, SCREEN_WIDTH/18 +10 +5 , SCREEN_WIDTH-SCREEN_WIDTH/6-50, SCREEN_WIDTH/18)];
                [requiredSellerCountLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
                [requiredSellerCountLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
                [requiredSellerCountLabel setText:sellerCount];
                requiredSellerCountLabel.textAlignment = NSTextAlignmentCenter;
                [sellerGroupContainer addSubview:requiredSellerCountLabel];
                
                
                
                UIButton  *buttonViewAll = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [buttonViewAll addTarget:self action:@selector(buttonHandlerForViewAll :) forControlEvents:UIControlEventTouchUpInside];
                [buttonViewAll setTitle:@"View All" forState:UIControlStateNormal];
                buttonViewAll.frame = CGRectMake(SCREEN_WIDTH/6 + 40,SCREEN_WIDTH/18 +10+SCREEN_WIDTH/18+10,SCREEN_WIDTH-SCREEN_WIDTH/6-50-10 ,SCREEN_WIDTH/18+15);
                [buttonViewAll setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
                [buttonViewAll setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
                [buttonViewAll setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
                [sellerGroupContainer addSubview:buttonViewAll];
                
                
                
                CGRect newFrame = sellerGroupContainer.frame;
                newFrame.size.height = SCREEN_WIDTH/6 +20 +20;
                sellerGroupContainer.frame = newFrame;
                internalY +=SCREEN_WIDTH/6 +30 +20;
                mainContainerY =internalY ;
            }}
        
        else{
            
            mainContainerY = internalY;
            UIView *emptyContainer = [[UIView alloc]initWithFrame:CGRectMake(5, internalY, _mainView.frame.size.width-10, 40)];
            emptyContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
            emptyContainer.layer.borderWidth = 2.0f;
            emptyContainer.backgroundColor =[GlobalData colorWithHexString:@"7F7F7F"];
            [_mainView addSubview:emptyContainer];
            
            UILabel *defaultTItleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0 , _mainView.frame.size.width-10 ,40)];
            [defaultTItleLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
            [defaultTItleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [defaultTItleLabel setText:@"No Items Found For Requested Value... Please Search with different Value."];
            defaultTItleLabel.textAlignment = NSTextAlignmentCenter;
            [emptyContainer addSubview:defaultTItleLabel];
            internalY += 100;
            mainContainerY = internalY ;
        }
        
        
        
    }
    _mainViewHeightConstraints.constant = mainContainerY;
    
}

-(void)buttonHandlerForSearchItem :(UIButton*)button{
    shopTitle = [[NSMutableDictionary alloc]init];
    UIView *parent = button.superview;
    UITextField *searchField = [parent viewWithTag:999];
    if ([searchField.text isEqualToString:@""]){
        [shopTitle setObject:@"" forKey:@"shoptitle"];
        return ;
    }
    else{
        [shopTitle setObject:searchField.text forKey:@"shoptitle"];
    }
    [_mainBaseView setNeedsUpdateConstraints];
    [_mainView setNeedsUpdateConstraints];
    whichApiDataToProcess = @"searchSellerItem";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
    
}
-(void)buttonHandlerForViewAll :(UIButton*)button{
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:button.superview.tag];
    profileUrl = sellerOption[@"profileurl"];
    [self performSegueWithIdentifier:@"sellerListToSellerProductsSegue" sender:self];
}


-(void) callSellerTitleTap:(UITapGestureRecognizer *)recognize {
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:recognize.view.superview.tag];
    profileUrl = sellerOption[@"profileurl"];
    [self performSegueWithIdentifier:@"sellerListToSellerProfieDataSegue" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"sellerListToSellerProfieDataSegue"]){
        SellerProfileData *destViewController = segue.destinationViewController;
        destViewController.profileUrl = profileUrl;
    }
    else if([segue.identifier isEqualToString:@"sellerListToSellerProductsSegue"]){
        SellerProfileData *destViewController = segue.destinationViewController;
        destViewController.profileUrl = profileUrl;
    }
}


@end
