//
//  InvoiceDetails.h
//  MobikulMp
//
//  Created by kunal prasad on 30/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoiceDetails : UIViewController{
    
    NSUserDefaults *preferences;
    NSUInteger isAlertVisible;
    UIAlertController *alert;
    NSString  *dataFromApi;
    id mainCollection,collection;
    NSString *whichApiDataToProcess;
    UIWindow *currentWindow;
    
}
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic) NSString *incrementId;
@property (nonatomic) NSString *customerId;
@property (nonatomic) NSString *invoiceId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;
@end
