//
//  addReview.m
//  Mobikul
//
//  Created by Ratnesh on 26/04/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AddReview.h"
#import "GlobalData.h"
#import "ToastView.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectAddReview;

@implementation AddReview

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectAddReview=[[GlobalData alloc] init];
    storeSubviewsTag =[[NSMutableArray alloc]init];
    globalObjectAddReview.delegate=self;
    [globalObjectAddReview language];
    [self registerForKeyboardNotifications];
    whichApiDataToprocess = @"";sizeModified = @"0";
    ratingsDict = [NSMutableDictionary dictionary];
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.title = _productName;
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationItem.title = [globalObjectAddReview.languageBundle localizedStringForKey:@"reviews" value:@"" table:nil];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil){
        [self loginRequest];
    }
    else{
        [self callingHttppApi];
    }
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectAddReview.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    if([whichApiDataToprocess isEqual:@"saveReview"])
    {
        UITextView *nickNameField = [_mainView viewWithTag:3000];
        UITextField *summaryField = [_mainView viewWithTag:2000];
        UITextView *textArea = [_mainView viewWithTag:1000];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"id=%@&", _productId];
        [post appendFormat:@"title=%@&", summaryField.text];
        [post appendFormat:@"detail=%@&",textArea.text];
        [post appendFormat:@"nickname=%@&",nickNameField.text];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&",customerId];
        
        NSData *jsonSortData = [NSJSONSerialization dataWithJSONObject:ratingsDict options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonSortString = [[NSString alloc] initWithData:jsonSortData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"ratings=%@&",jsonSortString];
        
        //For Image
        [post appendFormat:@"reviewimage=%@",Str_ReviewImageBase64];
        
        [globalObjectAddReview callHTTPPostMethod:post api:@"mobikulhttp/customer/saveReview" signal:@"HttpPostMetod"];
        globalObjectAddReview.delegate = self;
        
    }
    else{
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"productId=%@", _productId];
        [globalObjectAddReview callHTTPPostMethod:post api:@"mobikulhttp/catalog/getproductratingDetails" signal:@"HttpPostMetod"];
        globalObjectAddReview.delegate = self;
    }
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectAddReview callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}


-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectAddReview.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectAddReview.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectAddReview.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAddReview.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

//-(void)callingApi{
//    @try {
//        NSString *prefSessionId = [preferences objectForKey:@"sessionId"];
//        NSString *apiName = @"";
//        NSString *nameSpace = @"urn:Magento";
//        NSError *error;
//        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//        NSString *storeId = [preferences objectForKey:@"storeId"];
//        [dictionary setObject:storeId forKey:@"storeId"];
//        if([whichApiDataToprocess isEqual:@"saveReview"]){
//            apiName = @"mobikulCustomerSaveReview";
//            UITextView *nickNameField = [_mainView viewWithTag:3000];
//            UITextField *summaryField = [_mainView viewWithTag:2000];
//            UITextView *textArea = [_mainView viewWithTag:1000];
//            [dictionary setObject:nickNameField.text forKey:@"nickname"];
//            [dictionary setObject:summaryField.text forKey:@"title"];
//            [dictionary setObject:textArea.text forKey:@"detail"];
//            [dictionary setObject:_productId forKey:@"id"];
//            NSString *customerId = [preferences objectForKey:@"customerId"];
//            if(customerId != nil)
//                [dictionary setObject:customerId forKey:@"customerId"];
//            else
//                [dictionary setObject:@"" forKey:@"customerId"];
//            [dictionary setObject:ratingsDict forKey:@"ratings"];
//        }
//        else{
//            apiName = @"mobikulCatalogGetproductratingDetails";
//            NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
//            [dictionary setObject:screenWidth forKey:@"width"];
//            [dictionary setObject:_productId forKey:@"productId"];
//        }
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
//        NSString *jsonString;
//        if(!jsonData){
//            //NSLog(@"Got an error: %@", error);
//            }
//        else
//            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSString *parameters = [NSString stringWithFormat:@"<sessionId xsi:type=\"xsd:string\">%@</sessionId><attributes xsi:type=\"xsd:string\">%@</attributes>", prefSessionId, jsonString];
//        alert = [UIAlertController alertControllerWithTitle: @"Please Wait..." message: nil preferredStyle: UIAlertControllerStyleAlert];
//        NSString *envelope = [GlobalData createEnvelope:apiName  forNamespace:nameSpace forParameters:parameters currentView:self isAlertVisiblef:0 alertf:alert];
//        isAlertVisible=1;
//        [globalObjectAddReview sendingApiData:envelope];
//        globalObjectAddReview.delegate = self;
//    }
//    @catch (NSException *e) {
//        //NSLog(@"catching %@ reason 2 %@", [e name], [e reason]);
//    }
//}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([whichApiDataToprocess isEqual:@"saveReview"]){
        addReviewCollection = collection;
        [ToastView showToastInParentView:self.view withText:addReviewCollection[@"message"] withStatus:@"success" withDuaration:5.0];
    }
    else{
        float mainContainerY = 0;
        mainCollection = collection;
        float y = 0;
        UIImage *thumbNail = [imageCache objectForKey:mainCollection[@"thumbNail"]];
        if(!thumbNail){
            NSData *thumbNailData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"thumbNail"]]];
            thumbNail = [UIImage imageWithData:thumbNailData];
        }
        float actualHeight = thumbNail.size.height;
        float actualWidth = thumbNail.size.width;
        float ratio = 0, heightToApply = 0, widthToApply = 0;
        if(actualWidth <= actualHeight){
            ratio = actualHeight/actualWidth;
            heightToApply = actualWidth;
            widthToApply = actualWidth/ratio;
        }
        else{
            heightToApply = actualHeight;
            widthToApply = actualWidth;
        }
        
        UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2)-(widthToApply/2)), y, widthToApply, heightToApply)];
        productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
        [_mainView addSubview:productImage];
        y += heightToApply;
        mainContainerY += y;
        if(thumbNail)
            productImage.image = thumbNail;
        else{
            [_queue addOperationWithBlock:^{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"thumbNail"]]];
                UIImage *thumbNail = [UIImage imageWithData: imageData];
                if(thumbNail){
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        productImage.image = thumbNail;
                    }];
                    [imageCache setObject:thumbNail forKey:mainCollection[@"thumbNail"]];
                }
            }];
        }
        
        UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 30)];
        [productName setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
        [productName setBackgroundColor:[UIColor clearColor]];
        [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:26.0f]];
        [productName setText:[mainCollection[@"name"] uppercaseString]];
        [_mainView addSubview:productName];
        
        mainContainerY += 35;
        UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
        [hr setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
        [_mainView addSubview:hr];
        
        mainContainerY += 6;
        if([mainCollection[@"typeId"] isEqual:@"grouped"]){
            NSDictionary *preStringAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
            CGSize preStringSize = [@"Starting at:" sizeWithAttributes:preStringAttributes];
            CGFloat preStringWidth = preStringSize.width;
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, preStringWidth, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
            [preString setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"starting" value:@"" table:nil]];
            [_mainView addSubview:preString];
            
            NSDictionary *priceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
            CGSize priceStringSize = [mainCollection[@"groupedPrice"] sizeWithAttributes:priceAttributes];
            CGFloat priceStringWidth = priceStringSize.width;
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(preStringWidth+5, mainContainerY, priceStringWidth, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
            [price setText:mainCollection[@"groupedPrice"]];
            [_mainView addSubview:price];
        }
        else
            if([mainCollection[@"typeId"] isEqual:@"bundle"]){
                NSDictionary *fromAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                CGSize fromStringSize = [@"From:" sizeWithAttributes:fromAttributes];
                CGFloat fromStringWidth = fromStringSize.width;
                UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, fromStringWidth, 25)];
                [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [from setBackgroundColor:[UIColor clearColor]];
                [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                [from setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"from" value:@"" table:nil]];
                [_mainView addSubview:from];
                
                NSDictionary *minPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                CGSize minPriceStringSize = [mainCollection[@"formatedMinPrice"] sizeWithAttributes:minPriceAttributes];
                CGFloat minPriceStringWidth = minPriceStringSize.width;
                UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(fromStringWidth+5, mainContainerY, minPriceStringWidth, 25)];
                [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [minPrice setBackgroundColor:[UIColor clearColor]];
                [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                [minPrice setText:mainCollection[@"formatedMinPrice"]];
                [_mainView addSubview:minPrice];
                
                NSDictionary *toAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                CGSize toStringSize = [@"To:" sizeWithAttributes:toAttributes];
                CGFloat toStringWidth = toStringSize.width;
                UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(fromStringWidth+minPriceStringWidth+5, mainContainerY, toStringWidth, 25)];
                [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [to setBackgroundColor:[UIColor clearColor]];
                [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                [to setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"to" value:@"" table:nil]];
                [_mainView addSubview:to];
                
                NSDictionary *maxPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                CGSize maxPriceStringSize = [mainCollection[@"formatedMaxPrice"] sizeWithAttributes:maxPriceAttributes];
                CGFloat maxPriceStringWidth = maxPriceStringSize.width;
                UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(fromStringWidth+minPriceStringWidth+toStringWidth+5, mainContainerY, maxPriceStringWidth, 25)];
                [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [maxPrice setBackgroundColor:[UIColor clearColor]];
                [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                [maxPrice setText:mainCollection[@"formatedMaxPrice"]];
                [_mainView addSubview:maxPrice];
            }
            else{
                if(![mainCollection[@"specialPrice"] isEqual:[NSNull null]] && [mainCollection[@"isInRange"] boolValue]){
                    NSDictionary *regularPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize regularPriceStringSize = [mainCollection[@"formatedPrice"] sizeWithAttributes:regularPriceAttributes];
                    CGFloat regularPriceStringWidth = regularPriceStringSize.width;
                    UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, regularPriceStringWidth, 25)];
                    [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [regularprice setBackgroundColor:[UIColor clearColor]];
                    [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:mainCollection[@"formatedPrice"]];
                    [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                    [regularprice setAttributedText:attributeString];
                    [_mainView addSubview:regularprice];
                    
                    NSDictionary *specialPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize specialPriceStringSize = [mainCollection[@"formatedSpecialPrice"] sizeWithAttributes:specialPriceAttributes];
                    CGFloat specialPriceStringWidth = specialPriceStringSize.width;
                    UILabel *specialPrice = [[UILabel alloc] initWithFrame:CGRectMake(regularPriceStringWidth+5, mainContainerY, specialPriceStringWidth, 25)];
                    [specialPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [specialPrice setBackgroundColor:[UIColor clearColor]];
                    [specialPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [specialPrice setText:mainCollection[@"formatedSpecialPrice"]];
                    [_mainView addSubview:specialPrice];
                }
                else{
                    NSDictionary *priceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize priceStringSize = [mainCollection[@"formatedPrice"] sizeWithAttributes:priceAttributes];
                    CGFloat priceStringWidth = priceStringSize.width;
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, priceStringWidth, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [price setText:mainCollection[@"formatedPrice"]];
                    [_mainView addSubview:price];
                }
            }
        mainContainerY += 30;
        if([mainCollection[@"tierPrices"] count] > 0){
            for(int i=0; i<[mainCollection[@"tierPrices"] count]; i++) {
                NSDictionary *tierPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                CGSize tierPriceStringSize = [mainCollection[@"tierPrices"][i] sizeWithAttributes:tierPriceAttributes];
                CGFloat tierPriceStringWidth = tierPriceStringSize.width;
                UILabel *tierPrice = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, tierPriceStringWidth, 25)];
                [tierPrice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [tierPrice setBackgroundColor:[GlobalData colorWithHexString:@"FBF4DE"]];
                [tierPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                [tierPrice setText:mainCollection[@"tierPrices"][i]];
                [_mainView addSubview:tierPrice];
                mainContainerY += 25;
            }
            mainContainerY -= 25;
        }
        else
            mainContainerY -= 30;
        mainContainerY += 30;
        UIView *hr2 = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
        [hr2 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
        [_mainView addSubview:hr2];
        
        mainContainerY += 6;
        if([mainCollection[@"ratingData"] count] > 0){
            float maxRatingCodeWidth = 0;
            for(int i=0; i<[mainCollection[@"ratingData"] count]; i++) {
                NSDictionary *ratingDict = [mainCollection[@"ratingData"] objectAtIndex:i];
                NSDictionary *ratingCodeAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                CGSize ratingCodeStringSize = [ratingDict[@"ratingCode"] sizeWithAttributes:ratingCodeAttributes];
                CGFloat ratingCodeStringWidth = ratingCodeStringSize.width;
                if(ratingCodeStringWidth > maxRatingCodeWidth)
                    maxRatingCodeWidth = ratingCodeStringWidth;
            }
            for(int i=0; i<[mainCollection[@"ratingData"] count]; i++) {
                NSDictionary *ratingDict = [mainCollection[@"ratingData"] objectAtIndex:i];
                UILabel *ratingCode = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, maxRatingCodeWidth, 25)];
                [ratingCode setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [ratingCode setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                [ratingCode setText:ratingDict[@"ratingCode"]];
                [_mainView addSubview:ratingCode];
                
                UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(maxRatingCodeWidth+10,mainContainerY,120,24)];
                UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
                [ratingContainer addSubview:grayContainer];
                UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI1];
                UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI2];
                UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI3];
                UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI4];
                UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI5];
                double percent = 24 * [ratingDict[@"ratingValue"] doubleValue];
                UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
                blueContainer.clipsToBounds = YES;
                [ratingContainer addSubview:blueContainer];
                UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI1];
                UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI2];
                UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI3];
                UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI4];
                UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI5];
                [_mainView addSubview:ratingContainer];
                mainContainerY += 30;
            }
        }
        if([mainCollection[@"reviewList"] count] > 0){
            NSDictionary *reviewCountAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18]};
            CGSize reviewCountStringSize = [[NSString stringWithFormat:@"Total %d Review(s)", (int)[mainCollection[@"reviewList"] count]] sizeWithAttributes:reviewCountAttributes];
            CGFloat reviewCountWidth = reviewCountStringSize.width;
            UILabel *reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, reviewCountWidth, 25)];
            [reviewCountLabel setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
            [reviewCountLabel setText:[NSString stringWithFormat:@"Total %d Review(s)", (int)[mainCollection[@"reviewList"] count]]];
            [reviewCountLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
            [_mainView addSubview:reviewCountLabel];
            mainContainerY += 30;
            
            float reviewInternalY = 5;
            UIView *reviewContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 35)];
            reviewContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            reviewContainer.layer.borderWidth = 2.0f;
            [_mainView addSubview:reviewContainer];
            mainContainerY += 5;
            UIView *hr1 = [[UIView alloc]initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 1)];
            [hr1 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
            [reviewContainer addSubview:hr1];
            
            reviewInternalY += 6;
            NSDictionary *reviewSummaryAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
            CGSize reviewSummaryStringSize = [[NSString stringWithFormat:@"CUSTOMER REVIEWS %d ITEM(S)", (int)[mainCollection[@"reviewList"] count]] sizeWithAttributes:reviewSummaryAttributes];
            CGFloat reviewSummaryStringWidth = reviewSummaryStringSize.width;
            UILabel *reviewSummary = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewSummaryStringWidth, 25)];
            [reviewSummary setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [reviewSummary setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [reviewSummary setText:[NSString stringWithFormat:@"CUSTOMER REVIEWS %d ITEM(S)", (int)[mainCollection[@"reviewList"] count]]];
            [reviewContainer addSubview:reviewSummary];
            
            reviewInternalY += 30;
            UIView *hr2 = [[UIView alloc]initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 1)];
            [hr2 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
            [reviewContainer addSubview:hr2];
            
            reviewInternalY += 6;
            if([mainCollection[@"reviewList"] count] > 0){
                for(int i=0; i<[mainCollection[@"reviewList"] count]; i++) {
                    NSDictionary *reviewDict = [mainCollection[@"reviewList"] objectAtIndex:i];
                    NSDictionary *reviewTitleAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                    CGSize reviewTitleStringSize = [reviewDict[@"title"] sizeWithAttributes:reviewTitleAttributes];
                    CGFloat reviewTitleStringWidth = reviewTitleStringSize.width;
                    UILabel *reviewTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewTitleStringWidth, 25)];
                    [reviewTitle setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [reviewTitle setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewTitle setText:reviewDict[@"title"]];
                    [reviewContainer addSubview:reviewTitle];
                    
                    reviewInternalY += 30;
                    UILabel *reviewDetails = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 25)];
                    [reviewDetails setText:reviewDict[@"details"]];
                    [reviewDetails setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewDetails setLineBreakMode:NSLineBreakByWordWrapping];
                    reviewDetails.numberOfLines = 0;
                    reviewDetails.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
                    reviewDetails.textAlignment = NSTextAlignmentLeft;
                    reviewDetails.preferredMaxLayoutWidth = reviewContainer.frame.size.width-10;
                    [reviewDetails sizeToFit];
                    [reviewContainer addSubview:reviewDetails];
                    
                    reviewInternalY += reviewDetails.frame.size.height+5;
                    float ratingLabelMaxWidth = 0;
                    for(int k=0; k<[reviewDict[@"ratings"] count]; k++) {
                        NSDictionary *ratingLabelDict = [reviewDict[@"ratings"] objectAtIndex:k];
                        NSDictionary *ratingLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                        CGSize ratingLabelStringSize = [ratingLabelDict[@"label"] sizeWithAttributes:ratingLabelAttributes];
                        CGFloat ratingLabelStringWidth = ratingLabelStringSize.width;
                        if(ratingLabelStringWidth > ratingLabelMaxWidth)
                            ratingLabelMaxWidth = ratingLabelStringWidth;
                    }
                    for(int j=0; j<[reviewDict[@"ratings"] count]; j++) {
                        NSDictionary *ratingDict = [reviewDict[@"ratings"] objectAtIndex:j];
                        UILabel *ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, ratingLabelMaxWidth, 25)];
                        [ratingLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [ratingLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                        [ratingLabel setText:ratingDict[@"label"]];
                        [reviewContainer addSubview:ratingLabel];
                        UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(ratingLabelMaxWidth+10,reviewInternalY,120,24)];
                        UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
                        [ratingContainer addSubview:grayContainer];
                        UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                        gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI1];
                        UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                        gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI2];
                        UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                        gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI3];
                        UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                        gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI4];
                        UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                        gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI5];
                        double percent = 24 * [ratingDict[@"value"] doubleValue];
                        UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
                        blueContainer.clipsToBounds = YES;
                        [ratingContainer addSubview:blueContainer];
                        UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                        bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI1];
                        UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                        bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI2];
                        UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                        bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI3];
                        UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                        bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI4];
                        UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                        bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI5];
                        [reviewContainer addSubview:ratingContainer];
                        reviewInternalY += 29;
                    }
                    
                    NSDictionary *reviewByAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                    CGSize reviewByStringSize = [reviewDict[@"reviewBy"] sizeWithAttributes:reviewByAttributes];
                    CGFloat reviewByStringWidth = reviewByStringSize.width;
                    UILabel *reviewBy = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewByStringWidth, 25)];
                    [reviewBy setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [reviewBy setBackgroundColor:[UIColor clearColor]];
                    [reviewBy setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewBy setText:reviewDict[@"reviewBy"]];
                    [reviewContainer addSubview:reviewBy];
                    
                    reviewInternalY += 30;
                    NSDictionary *reviewOnAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                    CGSize reviewOnStringSize = [reviewDict[@"reviewOn"] sizeWithAttributes:reviewOnAttributes];
                    CGFloat reviewOnStringWidth = reviewOnStringSize.width;
                    UILabel *reviewOn = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewOnStringWidth, 25)];
                    [reviewOn setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [reviewOn setBackgroundColor:[UIColor clearColor]];
                    [reviewOn setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewOn setText:reviewDict[@"reviewOn"]];
                    [reviewContainer addSubview:reviewOn];
                    
                    reviewInternalY += 30;
                    UIView *hr3 = [[UIView alloc]initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 1)];
                    [hr3 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [reviewContainer addSubview:hr3];
                    reviewInternalY += 11;
                }
                reviewInternalY += 7;
                CGRect newFrame = reviewContainer.frame;
                newFrame.size.height = reviewInternalY;
                reviewContainer.frame = newFrame;
                reviewInternalY += 5;
                mainContainerY += reviewInternalY;
            }
        }
        
        mainContainerY += 20;
        NSDictionary *addReviewHeadingAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21]};
        CGSize addReviewHeadingStringSize = [@"HOW DO YOU RATE THIS PRODUCT?" sizeWithAttributes:addReviewHeadingAttributes];
        CGFloat addReviewHeadingWidth = addReviewHeadingStringSize.width;
        UILabel *addReviewHeading = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, addReviewHeadingWidth, 25)];
        [addReviewHeading setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
        [addReviewHeading setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
        [addReviewHeading setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"rateProduct" value:@"" table:nil]];
        [addReviewHeading setBackgroundColor:[UIColor clearColor]];
        [_mainView addSubview:addReviewHeading];
        
        mainContainerY += 30;
        UIView *hr3 = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
        [hr3 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
        [_mainView addSubview:hr3];
        
        mainContainerY += 6;
        if([mainCollection[@"ratingFormData"] count] > 0){
            float maxRatingCodeWidth = 0;
            for(int i=0; i<[mainCollection[@"ratingFormData"] count]; i++) {
                NSDictionary *ratingDict = [mainCollection[@"ratingFormData"] objectAtIndex:i];
                NSDictionary *ratingCodeAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]};
                CGSize ratingCodeStringSize = [ratingDict[@"name"] sizeWithAttributes:ratingCodeAttributes];
                CGFloat ratingCodeStringWidth = ratingCodeStringSize.width;
                if(ratingCodeStringWidth > maxRatingCodeWidth)
                    maxRatingCodeWidth = ratingCodeStringWidth;
            }
            for(int i=0; i<[mainCollection[@"ratingFormData"] count]; i++) {
                NSDictionary *ratingDict = [mainCollection[@"ratingFormData"] objectAtIndex:i];
                UILabel *ratingCode = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, maxRatingCodeWidth, 25)];
                [ratingCode setTextColor:[GlobalData colorWithHexString:@"555555"]];
                ratingCode.tag = i + 50;
                [ratingCode setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                [ratingCode setText:ratingDict[@"name"]];
                [_mainView addSubview:ratingCode];
                for(int j=0; j<[ratingDict[@"values"] count]; j++) {
                    UIView *radioBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(maxRatingCodeWidth+10, mainContainerY, 25, 25)];
                    radioBtnContainer.tag = i;
                    radioBtnContainer.layer.cornerRadius = 13;
                    radioBtnContainer.layer.masksToBounds = YES;
                    radioBtnContainer.layer.borderColor = [GlobalData colorWithHexString:@"3E464C"].CGColor;
                    radioBtnContainer.layer.borderWidth = 2.0f;
                    
                    UIView *radioBtn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
                    [radioBtn setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    radioBtn.tag = [ratingDict[@"values"][j] integerValue];
                    radioBtn.layer.cornerRadius = 13;
                    radioBtn.layer.masksToBounds = YES;
                    radioBtn.layer.borderColor = [GlobalData colorWithHexString:@"FFFFFF"].CGColor;
                    radioBtn.layer.borderWidth = 5.0f;
                    UITapGestureRecognizer *addReviewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectThisOption:)];
                    addReviewGesture.numberOfTapsRequired = 1;
                    [radioBtn addGestureRecognizer:addReviewGesture];
                    [radioBtnContainer addSubview:radioBtn];
                    [_mainView addSubview:radioBtnContainer];
                    
                    UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(maxRatingCodeWidth+40, mainContainerY, 120, 24)];
                    UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
                    [ratingContainer addSubview:grayContainer];
                    UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                    gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
                    [grayContainer addSubview:gI1];
                    UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                    gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
                    [grayContainer addSubview:gI2];
                    UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                    gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
                    [grayContainer addSubview:gI3];
                    UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                    gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
                    [grayContainer addSubview:gI4];
                    UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                    gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
                    [grayContainer addSubview:gI5];
                    double percent = 24 * (j+1);
                    UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
                    blueContainer.clipsToBounds = YES;
                    [ratingContainer addSubview:blueContainer];
                    UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                    bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
                    [blueContainer addSubview:bI1];
                    UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                    bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
                    [blueContainer addSubview:bI2];
                    UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                    bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
                    [blueContainer addSubview:bI3];
                    UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                    bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
                    [blueContainer addSubview:bI4];
                    UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                    bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
                    [blueContainer addSubview:bI5];
                    [_mainView addSubview:ratingContainer];
                    mainContainerY += 30;
                }
                mainContainerY += 10;
            }
        }
        UIView *hr4 = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
        [hr4 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
        [_mainView addSubview:hr4];
        
        mainContainerY += 6;
        UILabel *requiredStar;
        NSDictionary *reviewDetailAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
        CGSize reviewDetailStringSize = [@"Let us know your thoughts" sizeWithAttributes:reviewDetailAttributes];
        CGFloat reviewDetailWidth = reviewDetailStringSize.width;
        UILabel *reviewDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, reviewDetailWidth, 25)];
        [reviewDetailLabel setTextColor: [GlobalData colorWithHexString:@"555555"]];
        [reviewDetailLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
        [reviewDetailLabel setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"knowThoughts" value:@"" table:nil]];
        [reviewDetailLabel setBackgroundColor:[UIColor clearColor]];
        [_mainView addSubview:reviewDetailLabel];
        requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(reviewDetailWidth+8, mainContainerY, 10, 25)];
        [requiredStar setText:@"*"];
        [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
        [_mainView addSubview:requiredStar];
        
        mainContainerY += 30;
        UITextView *textArea = [[UITextView alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 200)];
        UIEdgeInsets textViewInsets = UIEdgeInsetsMake(3, 3, 3, 3);
        textArea.contentInset = textViewInsets;
        textArea.tag = 1000;
        textArea.layer.cornerRadius = 4;
        textArea.scrollIndicatorInsets = textViewInsets;
        textArea.font = [UIFont systemFontOfSize:19.0];
        textArea.scrollEnabled = YES;
        textArea.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        textArea.layer.borderWidth = 1.0f;
        textArea.editable = YES;
        [_mainView addSubview:textArea];
        
        mainContainerY += 205;
        NSDictionary *reviewSummaryAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
        CGSize reviewSummaryStringSize = [@"Summary of your Review" sizeWithAttributes:reviewSummaryAttributes];
        CGFloat reviewSummaryWidth = reviewSummaryStringSize.width;
        UILabel *reviewSummaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, reviewSummaryWidth, 25)];
        [reviewSummaryLabel setTextColor: [GlobalData colorWithHexString:@"555555"]];
        [reviewSummaryLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
        [reviewSummaryLabel setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"summaryReview" value:@"" table:nil]];
        [reviewSummaryLabel setBackgroundColor:[UIColor clearColor]];
        [_mainView addSubview:reviewSummaryLabel];
        requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(reviewSummaryWidth+8, mainContainerY, 10, 25)];
        [requiredStar setText:@"*"];
        [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
        [_mainView addSubview:requiredStar];
        
        mainContainerY += 30;
        UITextField *summaryField = [[UITextField alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 40)];
        summaryField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
        summaryField.tag = 2000;
        summaryField.textColor = [UIColor blackColor];
        summaryField.textAlignment = NSTextAlignmentLeft;
        summaryField.borderStyle = UITextBorderStyleRoundedRect;
        [_mainView addSubview:summaryField];
        
        mainContainerY += 45;
        NSDictionary *nickNameAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19]};
        CGSize nickNameStringSize = [@"What's your Nickname" sizeWithAttributes:nickNameAttributes];
        CGFloat nickNameWidth = nickNameStringSize.width;
        UILabel *nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, nickNameWidth, 25)];
        [nickNameLabel setTextColor: [GlobalData colorWithHexString:@"555555"]];
        [nickNameLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
        [nickNameLabel setText:[globalObjectAddReview.languageBundle localizedStringForKey:@"nickname" value:@"" table:nil]];
        [nickNameLabel setBackgroundColor:[UIColor clearColor]];
        [_mainView addSubview:nickNameLabel];
        requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(nickNameWidth+8, mainContainerY, 10, 25)];
        [requiredStar setText:@"*"];
        [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
        [_mainView addSubview:requiredStar];
        
        mainContainerY += 30;
        UITextField *nickNameField = [[UITextField alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 40)];
        nickNameField.font = [UIFont fontWithName:@"Trebuchet MS" size:19.0f];
        nickNameField.tag = 3000;
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId != nil)
            nickNameField.text = [preferences objectForKey:@"customerName"];
        nickNameField.textColor = [UIColor blackColor];
        nickNameField.textAlignment = NSTextAlignmentLeft;
        nickNameField.borderStyle = UITextBorderStyleRoundedRect;
        [_mainView addSubview:nickNameField];
        
        
        //New -
        mainContainerY += 65;
        
        UIButton *BTN_SelectImg =[[UIButton alloc]initWithFrame:CGRectMake(10, mainContainerY+20, 100, 30)];
        [BTN_SelectImg setTitle:@"Select Image" forState:UIControlStateNormal];
        BTN_SelectImg.titleLabel.font = [UIFont systemFontOfSize:12];
        [BTN_SelectImg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        BTN_SelectImg.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
        [BTN_SelectImg addTarget:self action:@selector(CLK_SelectImage:) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:BTN_SelectImg];
        
        IV_SelectImg=[[UIImageView alloc]initWithFrame:CGRectMake(150, mainContainerY-5, 120, 90)];
        IV_SelectImg.backgroundColor = [UIColor clearColor];
        [_mainView addSubview:IV_SelectImg];
        // -
        
        mainContainerY += 100;

        
        //mainContainerY += 65;
        
        UILabel *submitReviewButton = [[UILabel alloc] initWithFrame: CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 40)];
        [submitReviewButton setBackgroundColor: [GlobalData colorWithHexString:@"268ED7"]];
        [submitReviewButton setTextColor: [UIColor whiteColor]];
        [submitReviewButton setText:@"SUBMIT REVIEW"];
        submitReviewButton.userInteractionEnabled = YES;
        submitReviewButton.textAlignment = NSTextAlignmentCenter;
        [_mainView addSubview:submitReviewButton];
        UITapGestureRecognizer *addToCartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitReview:)];
        addToCartGesture.numberOfTapsRequired = 1;
        [submitReviewButton addGestureRecognizer:addToCartGesture];
        mainContainerY += 45;
        _mainViewHeightConstraint.constant = mainContainerY;
    }
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}


#pragma mark - IBAction

-(void)CLK_SelectImage:(UIButton*)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Image"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Remove Selected Image", @"Choose Existing", @"Take photo", nil];
    actionSheet.tag=55555;
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==55555) {
        
        if (buttonIndex==0) {
            //NSLog(@"Remove Image");
            [self RemoveSelectedImage];
        }
        else if (buttonIndex==1)
        {
            //NSLog(@"Choose Existing");
            [self FromGallery];
        }
        else if (buttonIndex==2)
        {
            //NSLog(@"Take photo");
            [self TakePhoto];
        }
    }
}

#pragma mark - Image Delegate

-(void)RemoveSelectedImage
{
    IV_SelectImg.image = nil;
    SelectImageBool = NO;
    Str_ReviewImageBase64=@"";
}

-(void)FromGallery
{
    IMG_PickerController= [[UIImagePickerController alloc] init];
    IMG_PickerController.delegate = self;
    IMG_PickerController.allowsEditing=YES;
    IMG_PickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:IMG_PickerController animated:YES completion:nil];
}

-(void)TakePhoto
{
    IMG_PickerController = [[UIImagePickerController alloc] init];
    IMG_PickerController.delegate=self;
    IMG_PickerController.allowsEditing=YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        IMG_PickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:IMG_PickerController animated:YES completion:NULL];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"No Camera Available." message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    SelectImageBool = YES;
    UIImage *ReSizeImage= [self imageWithImage:[info objectForKey:UIImagePickerControllerEditedImage] convertToSize:CGSizeMake(300, 300)];
    Str_ReviewImageBase64 = [self encodeToBase64String:ReSizeImage];
    //Str_ReviewImageBase64 = [Str_ReviewImageBase64 stringByReplacingOccurrencesOfString:@"=" withString:@""];
    //Str_ReviewImageBase64 = [Str_ReviewImageBase64 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //Str_ReviewImageBase64 = [Str_ReviewImageBase64 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    Str_ReviewImageBase64 = [Str_ReviewImageBase64 stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    IV_SelectImg.image = ReSizeImage;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (NSString *)encodeToBase64String:(UIImage *)image
{
    NSData *data = UIImagePNGRepresentation(image);
    
    return [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    //return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

#pragma mark -

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    if([sizeModified isEqualToString:@"0"]){
        sizeModified = @"1";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        _mainViewHeightConstraint.constant += keyboardSize.height;
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    if([sizeModified isEqualToString:@"1"]){
        sizeModified = @"0";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        _mainViewHeightConstraint.constant -= keyboardSize.height;
    }
}

-(void)submitReview:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
    int isValid = 1;
    int select = 0;
    UILabel *tempLabel;
    NSString *errorMessage;
    
    for(int i = 0; i<[mainCollection[@"ratingFormData"] count]; i++) {
        for(int j = 0; j < [storeSubviewsTag count]; j++){
            if(i == [[storeSubviewsTag objectAtIndex:j] intValue]){
                select = 1;
                break;
            }
        }
        if(select == 0){
            tempLabel= [_mainView viewWithTag:i + 50];
            isValid = 0;
            errorMessage = [NSString stringWithFormat:@"%@ is a required Field.",tempLabel.text];
        }
        select = 0;
    }
    
    UITextView *textArea = [_mainView viewWithTag:1000];
    UITextField *summaryField = [_mainView viewWithTag:2000];
    UITextView *nickNameField = [_mainView viewWithTag:3000];
    
    if([textArea.text isEqual: @""]){
        isValid = 0;
        errorMessage = [globalObjectAddReview.languageBundle localizedStringForKey:@"reviewRequired" value:@"" table:nil];
    }
    else
        if([summaryField.text isEqual: @""]){
            isValid = 0;
            errorMessage = [globalObjectAddReview.languageBundle localizedStringForKey:@"reviewSummaryRequired" value:@"" table:nil];
        }
        else
            if([nickNameField.text isEqual: @""]){
                isValid = 0;
                errorMessage = [globalObjectAddReview.languageBundle localizedStringForKey:@"nicknameRequired" value:@"" table:nil];
            }
    
    
    if(isValid == 0){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectAddReview.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAddReview.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    else{
        preferences = [NSUserDefaults standardUserDefaults];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        whichApiDataToprocess = @"saveReview";
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}


-(void)selectThisOption:(UITapGestureRecognizer *)recognizer{
    [storeSubviewsTag addObject:[NSString stringWithFormat:@"%lu",recognizer.view.superview.tag]];
    for(UIView *subViews in _mainView.subviews){
        if(subViews.tag == recognizer.view.superview.tag){
            for(UIView *subViews1 in subViews.subviews)
                [subViews1 setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
        }
    }
    [recognizer.view setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
    [ratingsDict setObject:[NSString stringWithFormat:@"%d", (int)recognizer.view.tag] forKey:[NSString stringWithFormat:@"%d", (int)recognizer.view.superview.tag]];
}

@end
