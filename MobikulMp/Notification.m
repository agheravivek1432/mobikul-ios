//
//  Notification.m
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "Notification.h"
#import "GlobalData.h"
#import "ProductCategory.h"
#import "CatalogProduct.h"

GlobalData *globalObjectNotification;

@implementation Notification

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectNotification=[[GlobalData alloc] init];
    globalObjectNotification.delegate=self;
    [globalObjectNotification language];
    isAlertVisible = 0;
    notificationList = [[NSMutableArray alloc] init];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectNotification.languageBundle localizedStringForKey:@"notification" value:@"" table:nil];
    self.navigationController.title = [globalObjectNotification.languageBundle localizedStringForKey:@"notification" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
}
#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if(collection != NULL)
        [self doFurtherProcessingWithResult];
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectNotification callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectNotification.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectNotification.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectNotification.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectNotification.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectNotification.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@", storeId];
    [globalObjectNotification callHTTPPostMethod:post api:@"mobikulhttp/extra/getnotificationList" signal:@"HttpPostMetod"];
    globalObjectNotification.delegate = self;
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        [self.view setUserInteractionEnabled:YES];
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    for(int i = 0; i < [collection count]; i++) {
        NSDictionary *notificationDict = [collection objectAtIndex:i];
        notificationList[i] = notificationDict[@"title"];
    }
    [_notificationTable reloadData];
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [notificationList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [notificationList objectAtIndex:[indexPath row]];
    cell.imageView.image = [UIImage imageNamed:@"ic_bell.png"];
    return cell;
}
- (IBAction)unwindToHome:(UIStoryboardSegue *)unwindSegue{}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    NSDictionary *notificationDict = [collection objectAtIndex:[indexPath row]];
    if([notificationDict[@"notificationType"] integerValue] == 2){
        categoryId = notificationDict[@"categoryId"];
        categoryName = notificationDict[@"categoryName"];
        [self performSegueWithIdentifier:@"notificationtoProductCategory" sender:self];
    }
    else if([notificationDict[@"notificationType"] integerValue] == 1){
        productId = notificationDict[@"productId"];
        productName = notificationDict[@"productName"];
        productType = notificationDict[@"productType"];
        [self performSegueWithIdentifier:@"notificationtoCatalogueProductSegue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"notificationtoProductCategory"]) {
        ProductCategory *destViewController = segue.destinationViewController;
        destViewController.categoryId = categoryId ;
        destViewController.categoryName = categoryName;
    }
    if([segue.identifier isEqualToString:@"notificationtoCatalogueProductSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = productId;
        destViewController.productName = productName;
        destViewController.productType = productType;
        destViewController.parentClass = @"notification";
    }
}



@end