//
//  CatalogSearch.h
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogSearch : UIViewController <NSXMLParserDelegate, UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *dataFromApi;
    UIAlertController *alert;
    NSMutableString *finalData;
    NSString *searchQuery;
    NSInteger isAlertVisible, sortSignal;
    id mainCollection, addToWishListCollection, addToCartCollection,collection;
    NSMutableArray *productCurtainOpenSignal, *sortDirection;
    NSCache *imageCache;
    NSString *sortItem, *sortDir;
    NSDictionary *productDictionary;
    NSString *whichApiDataToprocess, *productIdForApiCall,*reloadPageData;
    UIWindow *currentWindow;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSIndexPath *indexPathValue;
    NSArray *arrayMainCollection;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
}

@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UICollectionView *searchCollectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *sortByView;
@property (nonatomic) NSString *searchTermQuery;
@property (nonatomic) NSString *seachTermSignal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMargin;
@property (weak, nonatomic) IBOutlet UILabel *sortBy;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@end