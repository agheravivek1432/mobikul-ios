//
//  BlogListViewController.m
//  MobikulMp
//
//  Created by Developer on 10/11/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "BlogListViewController.h"
#import "GlobalData.h"
#import "BlogListCell.h"
#import "BlogDetailViewController.h"

@interface BlogListViewController ()

@end

@implementation BlogListViewController

GlobalData *globalObjectBlogList;

@synthesize isComingFromSellerProfile;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    btnAddBlog.layer.cornerRadius = btnAddBlog.frame.size.width/2;
    btnAddBlog.clipsToBounds = YES;
    
    globalObjectBlogList = [[GlobalData alloc] init];
    globalObjectBlogList.delegate = self;
    [globalObjectBlogList language];
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    wantChangePassword = 0;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectBlogList.languageBundle localizedStringForKey:@"Blogs" value:@"" table:nil];
    
    
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    
    if(savedSessionId == nil)
        [self loginRequest];
    else{
        whichApiDataToProcess = @"getallblog";
        [self callingHttppApi];
    }
    
    tblViewBlogList.estimatedRowHeight = 142;
    tblViewBlogList.rowHeight = UITableViewAutomaticDimension;
    
    if (isComingFromSellerProfile == YES)
    {
        btnAddBlog.hidden = YES;
        [btnAddBlog removeFromSuperview];
        [btnAddCategory setEnabled:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData
{
    isAlertVisible = 1;
    collection = collectionData ;
    
    if([collection[@"success"] integerValue] == 5)
    {
        [self loginRequest];
    }
    else
    {
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted
{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}


-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectBlogList callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectBlogList.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectBlogList.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectBlogList.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogList.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi
{
    // http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/getallblog
    // customerId
    
    [GlobalData alertController:currentWindow msg:[globalObjectBlogList.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToProcess isEqual: @"getallblog"])
    {
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", customerId];
        
        [globalObjectBlogList callHTTPPostMethodForBlog:post api:@"getallblog" signal:@"HttpPostMetod"];
        globalObjectBlogList.delegate = self;
    }
    else
    {
        
    }
}

-(void)doFurtherProcessingWithResult
{
    if(isAlertVisible == 1)
    {
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    if([whichApiDataToProcess isEqual: @"getallblog"])
    {
        
        arrBlogList = (NSArray *)collection[@"blogs"];
        
        if (arrBlogList.count > 0)
        {
            [tblViewBlogList reloadData];
        }
    }
    else
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectBlogList.languageBundle localizedStringForKey:@"message" value:@"" table:nil] message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogList.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    
  //  _mainContainerHeightConstraint.constant = SCREEN_HEIGHT-64;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrBlogList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 142.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"BlogListCell";
    
    BlogListCell *cell = (BlogListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BlogListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
    cell.dictBlog = [arrBlogList objectAtIndex:indexPath.row];
    
    [cell reloadBlogListCell];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([preferences objectForKey:@"customerId"] == nil)
        [self performSegueWithIdentifier:@"customerLogin" sender:self];
    else
        dictBlog = [arrBlogList objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"blogDetailViewSegue" sender:self];
}

-(void)btnAddCategoryPressed:(id)sender
{
    [self performSegueWithIdentifier:@"blogAddCategoryViewSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"blogDetailViewSegue"])
    {
        BlogDetailViewController *controller = (BlogDetailViewController *)segue.destinationViewController;
        controller.dictBlog = dictBlog;
    }
}

-(IBAction)btnAddBlogPressed:(id)sender
{
    [self performSegueWithIdentifier:@"addBlogViewSegue" sender:self];
}

- (IBAction)unwindToBlog:(UIStoryboardSegue *)unwindSegue{}
@end
