//
//  MarketPlace.h
//  MobikulMp
//
//  Created by kunal prasad on 14/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketPlace : UIViewController
{
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSString *sessionId, *message, *dataFromApi,*name,*entityId ,*typeId, *profileurl ;
    id mainCollection,collection;
    NSCache *imageCache;
    UIView *sellerGroupContainer,*viewAllContainer , *viewAllDescription , *groupIconImageContainer , *bannerImageView;
    UITapGestureRecognizer *sellerImage1Touch,*sellerImage2Touch,*sellerImage3Touch,*sellerImage4Touch,*sellorTitleTap ;
    UIWindow *currentWindow;
}

@property (strong, nonatomic) IBOutlet UIView *mainBaseView;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;

@end
