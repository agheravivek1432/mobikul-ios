//
//  CreditMemoList.m
//  MobikulMp
//
//  Created by kunal prasad on 30/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "CreditMemoList.h"
#import "GlobalData.h"
#include "CreditMemosDetails.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

@interface CreditMemoList ()

@end

GlobalData *globalObjectCreditMemoList;

@implementation CreditMemoList

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectCreditMemoList = [[GlobalData alloc] init];
    globalObjectCreditMemoList.delegate = self;
    [globalObjectCreditMemoList language];
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectCreditMemoList callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectCreditMemoList.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectCreditMemoList.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCreditMemoList.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCreditMemoList.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectCreditMemoList.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];

    [post appendFormat:@"incrementId=%@&", _incrementId];
    [post appendFormat:@"customerId=%@", _customerId];
    [globalObjectCreditMemoList callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/creditmemoList" signal:@"HttpPostMetod"];
    globalObjectCreditMemoList.delegate = self;
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    float mainContainerY = 0;
    float internalY = 0;
    float Y = 0;
    
    
    UILabel *mainHeading = [[UILabel alloc] initWithFrame:CGRectMake(5,5,_mainView.frame.size.width-10, 25)];    //  viewall generate label
    [mainHeading setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [mainHeading setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [mainHeading setText:mainCollection[@"mainHeading"]];
    mainHeading.textAlignment = NSTextAlignmentCenter;
    [mainHeading setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [_mainView addSubview:mainHeading];
    
    internalY +=35;
    
    for(int i=0;i<[mainCollection[@"allCreditMemos"] count];i++){
        NSDictionary *creditMemos = [mainCollection[@"allCreditMemos"] objectAtIndex:i];
        
        UIView *memoListContainer = [[UIView alloc] initWithFrame:CGRectMake(5,internalY,_mainView.frame.size.width-10,180 )];
        memoListContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        memoListContainer.layer.borderWidth = 2.0f;
        memoListContainer.tag = i;
        [_mainView addSubview:memoListContainer];
        
        Y = 5;
        NSDictionary *reqAttributesforLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizefoLabel = [[mainCollection[@"labels"][@"incrementId"] stringByAppendingString:@" :"]  sizeWithAttributes:reqAttributesforLabel];
        UILabel *memoLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,Y,reqStringSizefoLabel.width , 25)];
        [memoLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [memoLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [memoLabel setText:[mainCollection[@"labels"][@"incrementId"] stringByAppendingString:@":"]];
       // memoLabel.textAlignment = NSTextAlignmentCenter;
       //[memoLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [memoListContainer addSubview:memoLabel];
        
        NSDictionary *reqAttributeLabelValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
        CGSize reqStringSizeLabelValue = [creditMemos[@"incrementId"] sizeWithAttributes:reqAttributeLabelValue];
        UILabel *incrementLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizefoLabel.width+5,Y, reqStringSizeLabelValue.width, 25)];
        [incrementLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [incrementLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [incrementLabelValue setText:creditMemos[@"incrementId"]];
        [memoListContainer addSubview:incrementLabelValue];
        
       
        Y+=30;
        
        NSDictionary *reqAttributesfoBillLabelName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizefoBillLabelName = [[mainCollection[@"labels"][@"billToName"] stringByAppendingString:@" :"]  sizeWithAttributes:reqAttributesfoBillLabelName];
        UILabel *billLabelName = [[UILabel alloc] initWithFrame:CGRectMake(5,Y,reqStringSizefoBillLabelName.width , 25)];
        [billLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [billLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [billLabelName setText:[mainCollection[@"labels"][@"billToName"] stringByAppendingString:@":"]];
        // memoLabel.textAlignment = NSTextAlignmentCenter;
        //[memoLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [memoListContainer addSubview:billLabelName];
        
        NSDictionary *reqAttributeBillNameLabelValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
        CGSize reqStringSizeBillNameLabelValue = [creditMemos[@"billToName"] sizeWithAttributes:reqAttributeBillNameLabelValue];
        UILabel *BillNameLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizefoBillLabelName.width+5,Y, reqStringSizeBillNameLabelValue.width, 25)];
        [BillNameLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [BillNameLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [BillNameLabelValue setText:creditMemos[@"billToName"]];
        [memoListContainer addSubview:BillNameLabelValue];
        
        Y +=30;
        
        NSDictionary *reqAttributesfoDateLabelName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforDateLabelName = [[mainCollection[@"labels"][@"date"] stringByAppendingString:@" :"]  sizeWithAttributes:reqAttributesfoDateLabelName];
        UILabel *dateLabelName = [[UILabel alloc] initWithFrame:CGRectMake(5,Y,reqStringSizeforDateLabelName.width , 25)];
        [dateLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [dateLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [dateLabelName setText:[mainCollection[@"labels"][@"date"] stringByAppendingString:@":"]];
        // memoLabel.textAlignment = NSTextAlignmentCenter;
        //[memoLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [memoListContainer addSubview:dateLabelName];
        
        NSDictionary *reqAttributeDateLabelValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
        CGSize reqStringSizeDateLabelValue = [creditMemos[@"date"] sizeWithAttributes:reqAttributeDateLabelValue];
        UILabel *dateLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeforDateLabelName.width+5,Y, reqStringSizeDateLabelValue.width, 25)];
        [dateLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [dateLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [dateLabelValue setText:creditMemos[@"date"]];
        [memoListContainer addSubview:dateLabelValue];
        
        Y += 30;
        
        NSDictionary *reqAttributesfoStatusLabelName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforStatusLabelName = [[mainCollection[@"labels"][@"status"] stringByAppendingString:@" :"]  sizeWithAttributes:reqAttributesfoStatusLabelName];
        UILabel *statusLabelName = [[UILabel alloc] initWithFrame:CGRectMake(5,Y,reqStringSizeforStatusLabelName.width , 25)];
        [statusLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [statusLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [statusLabelName setText:[mainCollection[@"labels"][@"status"] stringByAppendingString:@":"]];
        // memoLabel.textAlignment = NSTextAlignmentCenter;
        //[memoLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [memoListContainer addSubview:statusLabelName];
        
        NSDictionary *reqAttributeStstusLabelValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
        CGSize reqStringSizeStstusLabelValue = [creditMemos[@"status"] sizeWithAttributes:reqAttributeStstusLabelValue];
        UILabel *statusLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeforStatusLabelName.width+5,Y, reqStringSizeStstusLabelValue.width, 25)];
        [statusLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [statusLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [statusLabelValue setText:creditMemos[@"status"]];
        [memoListContainer addSubview:statusLabelValue];

        Y +=30;
        
        
        NSDictionary *reqAttributesfoAmountLabelName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforAmountLabelName = [[mainCollection[@"labels"][@"amount"] stringByAppendingString:@" :"]  sizeWithAttributes:reqAttributesfoAmountLabelName];
        UILabel *amountLabelName = [[UILabel alloc] initWithFrame:CGRectMake(5,Y,reqStringSizeforAmountLabelName.width , 25)];
        [amountLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
        [amountLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [amountLabelName setText:[mainCollection[@"labels"][@"amount"] stringByAppendingString:@":"]];
        // memoLabel.textAlignment = NSTextAlignmentCenter;
        //[memoLabel setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
        [memoListContainer addSubview:amountLabelName];
        
        NSDictionary *reqAttributeAmountLabelValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
        CGSize reqStringSizeAmountLabelValue = [creditMemos[@"amount"] sizeWithAttributes:reqAttributeAmountLabelValue];
        UILabel *amountLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeforAmountLabelName.width+5,Y, reqStringSizeAmountLabelValue.width, 25)];
        [amountLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [amountLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [amountLabelValue setText:creditMemos[@"amount"]];
        [memoListContainer addSubview:amountLabelValue];

        Y +=30;
        
        
        UILabel *viewAllLink = [[UILabel alloc] initWithFrame:CGRectMake(10,Y,200, 20)];    //  viewall generate label
        [viewAllLink setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
        [viewAllLink setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [viewAllLink setText:@"View"];
        [memoListContainer addSubview:viewAllLink];
        viewAll =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callViewTitleTap:)];
        viewAllLink.userInteractionEnabled = "YES";
        viewAll.numberOfTapsRequired = 1;
        [viewAllLink addGestureRecognizer:viewAll];
        internalY +=190;
        
    }
    mainContainerY += internalY ;
    
    _mainViewHeightConstraints.constant = mainContainerY+10;
    
}

-(void) callViewTitleTap:(UITapGestureRecognizer *)recognize {
    NSDictionary *creditMemos = [mainCollection[@"allCreditMemos"] objectAtIndex:recognize.view.tag];
    creditMemoId = creditMemos[@"id"];
    [self performSegueWithIdentifier:@"creditMemoListToCreditMemoDetails" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"creditMemoListToCreditMemoDetails"]) {
        CreditMemosDetails *destViewController = segue.destinationViewController;
        destViewController.incrementId = _incrementId;
        destViewController.customerId = _customerId;
        destViewController.creditMemoId = creditMemoId;
    }
  
}



@end
