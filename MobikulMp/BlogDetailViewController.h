//
//  BlogDetailViewController.h
//  MobikulMp
//
//  Created by Apple on 15/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogDetailViewController : UIViewController <NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate>

{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *firstName, *lastName, *emailAddress, *currentPassword, *password, *confirmPassword, *wantChangePassword, *whichApiDataToProcess;
    NSInteger isAlertVisible;
    UIAlertController *alert;
    id collection;
    UIWindow *currentWindow;
    
    IBOutlet UITableView *tblView;
    
    NSArray *arrCommentList;
    
    NSString *strAddCommentText;
    NSString *strAddWebsitText;
    __weak IBOutlet NSLayoutConstraint *tblBottomConstant;
}

@property (nonatomic, retain) NSDictionary *dictBlog;

@end
