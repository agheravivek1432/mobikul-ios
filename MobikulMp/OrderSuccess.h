//
//  OrderSuccess.h
//  Mobikul
//
//  Created by Ratnesh on 04/05/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSuccess : UIViewController

{
    NSUserDefaults *languageCode;
}

@property (nonatomic) NSString *orderId;
@property (nonatomic) NSString *incrementId;
@property (nonatomic) NSString *canReorder;
@property (weak, nonatomic) IBOutlet UIView *mainContainer;
@property (weak, nonatomic) IBOutlet UILabel *mainHeading;
@property (weak, nonatomic) IBOutlet UILabel *subHeading;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UILabel *noticeLabel;
- (IBAction)continueShopping:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *continueShopping;

@end
