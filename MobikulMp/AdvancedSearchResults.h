//
//  AdvancedSearchResults.h
//  Mobikul
//
//  Created by Ratnesh on 30/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvancedSearchResults : UIViewController<NSXMLParserDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id mainCollection, addToWishListCollection, addToCartCollection,collection;
    NSMutableString *finalData;
    NSMutableArray *productCurtainOpenSignal, *sortDirection;
    NSCache *imageCache;
    NSString *sortItem, *sortDir,*reloadPageData;
    NSInteger sortSignal;
    NSDictionary *productDictionary;
    NSString *whichApiDataToprocess, *productIdForApiCall;
    UIWindow *currentWindow;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSIndexPath *indexPathValue;
    NSArray *arrayMainCollection;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
}

@property (nonatomic) NSMutableArray *queryString;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UICollectionView *productCollectionView;
@property (weak, nonatomic) IBOutlet UIView *sortByView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@end
