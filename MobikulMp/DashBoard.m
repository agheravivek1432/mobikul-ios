//
//  DashBoard.m
//  Mobikul
//
//  Created by Ratnesh on 02/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import "DashBoard.h"
#import "GlobalData.h"
#import "AddEditAddress.h"
#import "CustomerOrderDetails.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectDashBoard;

@implementation DashBoard

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectDashBoard=[[GlobalData alloc] init];
    globalObjectDashBoard.delegate=self;
    [globalObjectDashBoard language];
    UITapGestureRecognizer *viewAllRecentProductsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadAllRecentOrders:)];
    _viewAllOrderButton.userInteractionEnabled = YES;
    viewAllRecentProductsTap.numberOfTapsRequired = 1;
    [_viewAllOrderButton addGestureRecognizer:viewAllRecentProductsTap];
    
    UITapGestureRecognizer *editAccountInformationTap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAccountInformationUser:)];
    _editAccountInformation.userInteractionEnabled = YES;
    editAccountInformationTap.numberOfTapsRequired = 1;
    [_editAccountInformation addGestureRecognizer:editAccountInformationTap];
    
    UITapGestureRecognizer *changePasswordTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAccountInformationUser:)];
    _changePassword.userInteractionEnabled = YES;
    changePasswordTap.numberOfTapsRequired = 1;
    [_changePassword addGestureRecognizer:changePasswordTap];
    
    UITapGestureRecognizer *newsLetterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newsLetterSubscription:)];
    _newsLetterSubscription.userInteractionEnabled = YES;
    newsLetterTap.numberOfTapsRequired = 1;
    [_newsLetterSubscription addGestureRecognizer:newsLetterTap];
    
    UITapGestureRecognizer *manageAdressTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(manageAddressBook:)];
    _manageAddress.userInteractionEnabled = YES;
    manageAdressTap.numberOfTapsRequired = 1;
    [_manageAddress addGestureRecognizer:manageAdressTap];
    
    UITapGestureRecognizer *editBillingTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAddress:)];
    _editDefaultBillingAddress.userInteractionEnabled = YES;
    editBillingTap.numberOfTapsRequired = 1;
    [_editDefaultBillingAddress addGestureRecognizer:editBillingTap];
    
    UITapGestureRecognizer *editShippingTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAddress:)];
    _editDefaultShippingAddress.userInteractionEnabled = YES;
    editShippingTap.numberOfTapsRequired = 1;
    [_editDefaultShippingAddress addGestureRecognizer:editShippingTap];
    
    UITapGestureRecognizer *viewAllReviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewAllReview:)];
    _viewAllRecentReview.userInteractionEnabled = YES;
    viewAllReviewTap.numberOfTapsRequired = 1;
    [_viewAllRecentReview addGestureRecognizer:viewAllReviewTap];
    isAlertVisible = 0;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    //    self.navigationItem.title = [globalObjectDashBoard.languageBundle localizedStringForKey:@"dashboard" value:@"" table:nil];
    _dashboardMessage.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"dashboardMessage" value:@"" table:nil];
    _accountInformation.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"accountInformation" value:@"" table:nil];
    _contactInformation.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"contactInformation" value:@"" table:nil];
    _customerName.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"customerName" value:@"" table:nil];
    _customerEmail.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"customerEmail" value:@"" table:nil];
    _changePassword.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"changePassword" value:@"" table:nil];
    _newsletter.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"newsletterTitle" value:@"" table:nil];
    _newsletterNotification.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"newsletterNotification" value:@"" table:nil];
    _addressBook.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"addressBook" value:@"" table:nil];
    _manageAddress.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"manageAddress" value:@"" table:nil];
    _defaultBilling.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"defaultBilling" value:@"" table:nil];
    _editDefaultBillingAddress.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"editAddress" value:@"" table:nil];
    _defaultBillingAddress.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"defaultBillingAddress" value:@"" table:nil];
    _defaultShipping.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"defaultShipping" value:@"" table:nil];
    _editDefaultShippingAddress.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"editAddress" value:@"" table:nil];
    _defaultShippingAddress.text = [globalObjectDashBoard.languageBundle localizedStringForKey:@"defaultShippingAddress" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}


#pragma mark - Sample protocol delegate

-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}


-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectDashBoard callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectDashBoard.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectDashBoard.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectDashBoard.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectDashBoard.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
- (void)loadAllRecentOrders:(UITapGestureRecognizer *)recognize {
    [self performSegueWithIdentifier:@"loadAllRecenctProductSegue" sender:self];
}

- (void)editAccountInformationUser:(UITapGestureRecognizer *)recognize {
    [self performSegueWithIdentifier:@"editAccountInformationSegue" sender:self];
}

- (void)newsLetterSubscription:(UITapGestureRecognizer *)recognize {
    [self performSegueWithIdentifier:@"newsLetterSubscribe" sender:self];
}

- (void)manageAddressBook:(UITapGestureRecognizer *)recognize {
    [self performSegueWithIdentifier:@"manageAddressBookSegue" sender:self];
}

- (void)editAddress:(UITapGestureRecognizer *)recognize {
    address = [NSString stringWithFormat:@"%d", (int)_defaultBillingAddress.tag];
    [self performSegueWithIdentifier:@"editAddressSegueFromDash" sender:self];
}

- (void)viewAllReview:(UITapGestureRecognizer *)recognize {
    [self performSegueWithIdentifier:@"viewAllReviewSegueFromDash" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"editAddressSegueFromDash"]) {
        AddEditAddress *destViewController = segue.destinationViewController;
        destViewController.addressId = address;
        destViewController.addOrEdit = @"0";
    }
    if([segue.identifier isEqualToString:@"dashBoardToOrderDetailsSegue"]) {
        CustomerOrderDetails *destViewController = segue.destinationViewController;
        destViewController.incrementId = incrementId;
    }
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectDashBoard.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    [post appendFormat:@"websiteId=%@&", websiteId];
    NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
    [post appendFormat:@"customerEmail=%@", prefCustomerEmail];
    [globalObjectDashBoard callHTTPPostMethod:post api:@"mobikulhttp/customer/getdashboardData" signal:@"HttpPostMetod"];
    globalObjectDashBoard.delegate = self;
    
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    _welcomeMessage.text = collection[@"welcomeMsg"];
    _customerName.text = collection[@"customerName"];
    _customerEmail.text = collection[@"customerEmail"];
    _newsletterNotification.text = collection[@"subscriptionMsg"];
    _defaultBillingAddress.text = collection[@"billingAddress"];
    _defaultShippingAddress.text = collection[@"shippingAddress"];
    _defaultBillingAddress.tag= [collection[@"billingId"] integerValue];
    _defaultShippingAddress.tag=[collection[@"shippingId"] integerValue];
    
    for(int i = 0; i < [collection[@"recentOrders"] count]; i++){
        NSDictionary *dict = [collection[@"recentOrders"] objectAtIndex:i];
        UIView *paintView =[[UIView alloc]initWithFrame:CGRectMake(2, (i*155)+(2*(i+1)), (_recentOrderContainer.frame.size.width)-4, 155)];
        [paintView setBackgroundColor:[UIColor whiteColor]];
        [_recentOrderContainer addSubview:paintView];
        UILabel *orderId = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (_recentOrderContainer.frame.size.width)-4, 25)];
        [orderId setTextColor:[UIColor blackColor]];
        [orderId setBackgroundColor:[UIColor clearColor]];
        [orderId setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        NSString *orderIdText = [NSString stringWithFormat:@"Order ID : %@",dict[@"order_id"]];
        [orderId setText:orderIdText];
        [paintView addSubview:orderId];
        
        UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(5, 35, (_recentOrderContainer.frame.size.width)-4, 25)];
        [date setTextColor:[UIColor blackColor]];
        [date setBackgroundColor:[UIColor clearColor]];
        [date setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        NSString *dateText = [NSString stringWithFormat:@"Date : %@",dict[@"date"]];
        [date setText:dateText];
        [paintView addSubview:date];
        
        UILabel *shipTo = [[UILabel alloc] initWithFrame:CGRectMake(5, 65, (_recentOrderContainer.frame.size.width)-4, 25)];
        [shipTo setTextColor:[UIColor blackColor]];
        [shipTo setBackgroundColor:[UIColor clearColor]];
        [shipTo setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        NSString *shipToText = [NSString stringWithFormat:@"Ship To : %@",dict[@"ship_to"]];
        [shipTo setText:shipToText];
        [paintView addSubview:shipTo];
        
        UILabel *orderTotal = [[UILabel alloc] initWithFrame:CGRectMake(5, 95, (_recentOrderContainer.frame.size.width)-4, 25)];
        [orderTotal setTextColor:[UIColor blackColor]];
        [orderTotal setBackgroundColor:[UIColor clearColor]];
        [orderTotal setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        NSString *orderTotalText = [NSString stringWithFormat:@"Order Total : %@",dict[@"order_total"]];
        [orderTotal setText:orderTotalText];
        [paintView addSubview:orderTotal];
        
        UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(5, 125, (_recentOrderContainer.frame.size.width)-4, 25)];
        [status setTextColor:[UIColor blackColor]];
        [status setBackgroundColor:[UIColor clearColor]];
        [status setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        NSString *statusText = [NSString stringWithFormat:@"Status : %@",dict[@"status"]];
        [status setText:statusText];
        [paintView addSubview:status];
        
        NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforViewAllButtonText = [@"View Order" sizeWithAttributes:reqAttributesforViewAllButtonText];
        CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
        UIButton *buttonViewAllDesc = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buttonViewAllDesc.tag = i;
        [buttonViewAllDesc addTarget:self action:@selector(buttonHandlerForViewAllOrderDetails :) forControlEvents:UIControlEventTouchUpInside];
        [buttonViewAllDesc setTitle:@"View Order" forState:UIControlStateNormal];
        buttonViewAllDesc.frame = CGRectMake(paintView.frame.size.width-reqStringWidthViewAllButtonText-30,95 ,reqStringWidthViewAllButtonText + 20, 50);
        [buttonViewAllDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [buttonViewAllDesc setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
        [buttonViewAllDesc setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [paintView addSubview:buttonViewAllDesc ];
        
        
    }
    _heightConstraint.constant = (157*[collection[@"recentOrders"] count]) + 2;
    [_recentOrderContainer layoutIfNeeded];
    
    
    if(SCREEN_WIDTH<600){
        _recentReviewWidth.constant = 200;
        _recentReviewLabel.adjustsFontSizeToFitWidth = YES;
    }
    for(int i=0; i<[collection[@"recentReview"] count]; i++){
        NSDictionary *dict = [collection[@"recentReview"] objectAtIndex:i];
        UIView *paintView =[[UIView alloc]initWithFrame:CGRectMake(2, (i*65)+(2*(i+1)), (_recentReviewsContainer.frame.size.width)-4, 65)];
        [paintView setBackgroundColor:[UIColor whiteColor]];
        [_recentReviewsContainer addSubview:paintView];
        
        UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (_recentReviewsContainer.frame.size.width)-4, 25)];
        [productName setTextColor:[UIColor blackColor]];
        [productName setBackgroundColor:[UIColor clearColor]];
        [productName setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        NSString *productNameText = [NSString stringWithFormat:@"%d.  %@",i+1,dict[@"name"]];
        [productName setText:productNameText];
        [paintView addSubview:productName];
        
        UILabel *rating = [[UILabel alloc] initWithFrame:CGRectMake(5, 35, (_recentReviewsContainer.frame.size.width)-4, 25)];
        [rating setTextColor:[UIColor blackColor]];
        [rating setBackgroundColor:[UIColor clearColor]];
        [rating setFont:[UIFont fontWithName: @"Trebuchet MS" size: 19.0f]];
        [rating setText:[globalObjectDashBoard.languageBundle localizedStringForKey:@"rating" value:@"" table:nil]];
        [paintView addSubview:rating];
        
        UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(110,35,120,24)];
        UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
        [ratingContainer addSubview:grayContainer];
        UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI1];
        UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI2];
        UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI3];
        UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI4];
        UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI5];
        
        double percent = 24 * [dict[@"rating"] doubleValue];
        UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
        blueContainer.clipsToBounds = YES;
        [ratingContainer addSubview:blueContainer];
        UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI1];
        UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI2];
        UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI3];
        UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI4];
        UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI5];
        
        [paintView addSubview:ratingContainer];
    }
    _reviewContainerHeightConstraint.constant = (67*[collection[@"recentReview"] count]) + 2;
    [_recentReviewsContainer layoutIfNeeded];
    
    float totalHeight = 515;
    if([collection[@"recentReview"] count] > 0)
        totalHeight += ((67*[collection[@"recentReview"] count])+2);
    else{
        totalHeight -= 33;
        _recentReviewHeading.hidden = YES;
        _viewAllRecentReview.hidden = YES;
        _recentReviewsContainer.hidden = YES;
    }
    if([collection[@"recentOrders"] count] > 0)
        totalHeight += ((157*[collection[@"recentOrders"] count])+2);
    else{
        totalHeight -= 33;
        _recentOrderHeading.hidden = YES;
        _viewAllOrderButton.hidden = YES;
        _recentOrderContainer.hidden = YES;
    }
    totalHeight += _dashboardMessage.frame.size.height;
    if(![collection[@"billingAddress"] isEqual: @""])
        totalHeight += (23*5);
    if(![collection[@"shippingAddress"] isEqual: @""])
        totalHeight += (23*5);
    _internalViewHeightConstraint.constant = totalHeight+50;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}


-(void)buttonHandlerForViewAllOrderDetails:(UIButton*)button{
    NSDictionary *dict = [collection[@"recentOrders"] objectAtIndex:button.tag];
    incrementId = dict[@"order_id"];
    [self performSegueWithIdentifier:@"dashBoardToOrderDetailsSegue" sender:self];
}




@end
