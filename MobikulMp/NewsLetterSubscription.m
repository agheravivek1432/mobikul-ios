//
//  NewsLetterSubscription.m
//  Mobikul
//
//  Created by Ratnesh on 08/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "NewsLetterSubscription.h"
#import "GlobalData.h"
#import "ToastView.h"

#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

GlobalData *globalObjectNewsLetterSub;

@implementation NewsLetterSubscription

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectNewsLetterSub=[[GlobalData alloc] init];
    globalObjectNewsLetterSub.delegate = self;
    [globalObjectNewsLetterSub language];
    whichApiToCall = @"isSuscribed";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectNewsLetterSub callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectNewsLetterSub.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectNewsLetterSub.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectNewsLetterSub.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectNewsLetterSub.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectNewsLetterSub.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiToCall isEqualToString:@"save"]){
        NSString *prefCustomerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", prefCustomerId];
        [post appendFormat:@"isSubscribed=%@&", isSubscribed];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectNewsLetterSub callHTTPPostMethod:post api:@"mobikulhttp/customer/subscribetoNewsletter" signal:@"HttpPostMetod"];
        globalObjectNewsLetterSub.delegate = self;
    }else{
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        [post appendFormat:@"websiteId=%@&", websiteId];
        NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
        [post appendFormat:@"customerEmail=%@", prefCustomerEmail];
        [globalObjectNewsLetterSub callHTTPPostMethod:post api:@"mobikulhttp/customer/isSubscribed" signal:@"HttpPostMetod"];
        globalObjectNewsLetterSub.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([whichApiToCall isEqual: @"save"]){
        [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
    }
    else{
        if([[NSString stringWithFormat:@"%@",collection[@"isSubscribed"]] isEqual: @"0"]){
            [_subscriptionSwitch setOn:NO animated:NO];
            isSubscribed = @"false";
        }
        else{
            [_subscriptionSwitch setOn:YES animated:NO];
            isSubscribed = @"true";
        }
    }
    _mainContainerHeightConstraint.constant = SCREEN_HEIGHT;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

- (IBAction)saveSubscription:(id)sender {
    if(_subscriptionSwitch.isOn)
        isSubscribed = @"true";
    else
        isSubscribed = @"false";
    whichApiToCall = @"save";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    isAlertVisible=1;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
}

- (IBAction)subscriptionSwitchValueChanged:(id)sender {
    if(_subscriptionSwitch.isOn)
        isSubscribed = @"true";
    else
        isSubscribed = @"false";
}

@end