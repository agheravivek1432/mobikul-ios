//
//  AdvancedSearch.m
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AdvancedSearch.h"
#import "AdvancedSearchResults.h"
#import "GlobalData.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

GlobalData *globalObjectAdvanceSearch;

@implementation AdvancedSearch

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectAdvanceSearch = [[GlobalData alloc] init];
    globalObjectAdvanceSearch.delegate = self;
    [globalObjectAdvanceSearch language];
    selectedDate = [[NSDate alloc] init];
    isAlertVisible = 0;
    queryString = [[NSMutableArray alloc] init];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"advanceSearch" value:@"" table:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil){
        [self loginRequest];
        
    }
    else{
        [self callingHttppApi];
    }
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}


-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@", storeId];
    [globalObjectAdvanceSearch callHTTPPostMethod:post api:@"mobikulhttp/catalog/getadvancedsearchFields" signal:@"HttpPostMetod"];
    globalObjectAdvanceSearch.delegate = self;
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    // if([collection[@"success"] integerValue] == 5){
    //   [self loginRequest];
    //}
    //else{
    
    [self doFurtherProcessingWithResult];
    //}
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectAdvanceSearch callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}
-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        [self.view setUserInteractionEnabled:YES];
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    y = 8;
    float x = 8;
    for(int i=0; i<[collection count]; i++){
        NSDictionary *formFieldDict = [collection objectAtIndex:i];
        if([formFieldDict[@"inputType"] isEqual: @"string"] || [formFieldDict[@"inputType"] isEqual: @"number"]){
            UILabel *fieldLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, SCREEN_WIDTH-16, 32)];
            [fieldLabel setTextColor:[GlobalData colorWithHexString : @"636363"]];
            [fieldLabel setBackgroundColor:[UIColor clearColor]];
            [fieldLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size : 19.0f]];
            [fieldLabel setText:formFieldDict[@"label"]];
            [_advancedSearchForm addSubview:fieldLabel];
            y += 37;
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, SCREEN_WIDTH-16, 32)];
            NSString *pleaceholderText = [NSString stringWithFormat:@"Enter  %@",formFieldDict[@"label"]];
            textField.placeholder = pleaceholderText;
            textField.tag = i;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.font = [UIFont systemFontOfSize:19];
            [_advancedSearchForm addSubview:textField];
            y += 40;
            queryStringDict = [[NSMutableDictionary alloc] init];
            [queryStringDict setObject:formFieldDict[@"attributeCode"] forKey:@"code"];
            [queryStringDict setObject:formFieldDict[@"inputType"] forKey:@"inputType"];
            [queryStringDict setObject:@"" forKey:@"value"];
            [queryString addObject:queryStringDict];
        }
        else
            if([formFieldDict[@"inputType"] isEqual: @"price"]){
                UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, SCREEN_WIDTH-16, 32)];
                [priceLabel setTextColor:[GlobalData colorWithHexString : @"636363"]];
                [priceLabel setBackgroundColor:[UIColor clearColor]];
                [priceLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size : 19.0f]];
                [priceLabel setText:formFieldDict[@"label"]];
                [_advancedSearchForm addSubview:priceLabel];
                y += 37;
                UITextField *fromField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, (SCREEN_WIDTH-16)/3, 32)];
                fromField.tag = i;
                fromField.borderStyle = UITextBorderStyleRoundedRect;
                fromField.font = [UIFont systemFontOfSize:19];
                [_advancedSearchForm addSubview:fromField];
                
                UILabel *hifen = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-16)/3)+x+5, y, 8, 32)];
                [hifen setTextColor:[UIColor blackColor]];
                [hifen setBackgroundColor:[UIColor clearColor]];
                [hifen setFont:[UIFont fontWithName:@"Trebuchet MS" size : 21.0f]];
                [hifen setText:@"-"];
                [_advancedSearchForm addSubview:hifen];
                
                UITextField *toField = [[UITextField alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-16)/3)+x+18, y, (SCREEN_WIDTH-16)/3, 32)];
                toField.tag = i;
                toField.borderStyle = UITextBorderStyleRoundedRect;
                toField.font = [UIFont systemFontOfSize:19];
                [_advancedSearchForm addSubview:toField];
                y += 40;
                
                queryStringDict = [[NSMutableDictionary alloc] init];
                [queryStringDict setObject:formFieldDict[@"attributeCode"] forKey:@"code"];
                [queryStringDict setObject:formFieldDict[@"inputType"] forKey:@"inputType"];
                NSMutableDictionary *priceFromToDict = [[NSMutableDictionary alloc] init];
                [priceFromToDict setObject:@"" forKey:@"from"];
                [priceFromToDict setObject:@"" forKey:@"to"];
                [queryStringDict setObject:priceFromToDict forKey:@"value"];
                [queryString addObject:queryStringDict];
            }
            else
                if([formFieldDict[@"inputType"] isEqual: @"select"]){
                    UILabel *selectLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, SCREEN_WIDTH-16, 32)];
                    [selectLabel setTextColor:[GlobalData colorWithHexString : @"636363"]];
                    [selectLabel setBackgroundColor:[UIColor clearColor]];
                    [selectLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size : 19.0f]];
                    [selectLabel setText:formFieldDict[@"label"]];
                    [_advancedSearchForm addSubview:selectLabel];
                    y += 37;
                    queryStringDict = [[NSMutableDictionary alloc] init];
                    [queryStringDict setObject:formFieldDict[@"attributeCode"] forKey:@"code"];
                    [queryStringDict setObject:formFieldDict[@"inputType"] forKey:@"inputType"];
                    NSMutableDictionary *selectArrayDict = [[NSMutableDictionary alloc] init];
                    for(int j=0; j<[formFieldDict[@"options"] count]; j++){
                        NSDictionary *dict = [formFieldDict[@"options"] objectAtIndex:j];
                        [selectArrayDict setObject:@"false" forKey:dict[@"value"]];
                        UISwitch *checkBox = [[UISwitch alloc] initWithFrame:CGRectMake(x, y, 55, 32)];
                        [checkBox setOn:NO];
                        NSString *thisTagString = [NSString stringWithFormat:@"%@.%d", dict[@"value"], i];
                        long thisTag = (long)thisTagString;
                        checkBox.tag = thisTag;
                        [_advancedSearchForm addSubview:checkBox];
                        
                        UILabel *checkboxLabel = [[UILabel alloc] initWithFrame:CGRectMake(x+60, y, SCREEN_WIDTH-121, 32)];
                        [checkboxLabel setTextColor:[GlobalData colorWithHexString : @"636363"]];
                        [checkboxLabel setBackgroundColor:[UIColor clearColor]];
                        [checkboxLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size : 19.0f]];
                        [checkboxLabel setText:dict[@"label"]];
                        [_advancedSearchForm addSubview:checkboxLabel];
                        y += 37;
                    }
                    y += 3;
                    
                    [queryStringDict setObject:selectArrayDict forKey:@"value"];
                    [queryString addObject:queryStringDict];
                }
                else
                    if([formFieldDict[@"inputType"] isEqual: @"date"]){
                        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, SCREEN_WIDTH-16, 32)];
                        [dateLabel setTextColor:[GlobalData colorWithHexString : @"636363"]];
                        [dateLabel setBackgroundColor:[UIColor clearColor]];
                        [dateLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size : 19.0f]];
                        [dateLabel setText:formFieldDict[@"label"]];
                        [_advancedSearchForm addSubview:dateLabel];
                        y += 37;
                        UITextField *fromField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, (SCREEN_WIDTH-16)/3, 32)];
                        fromField.tag = i;
                        fromField.borderStyle = UITextBorderStyleRoundedRect;
                        fromField.font = [UIFont systemFontOfSize:19];
                        fromField.enabled = NO;
                        [_advancedSearchForm addSubview:fromField];
                        
                        UIImageView *calenderFromButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-16)/3)+x, y, 32, 32)];
                        calenderFromButton.tag = i;
                        [calenderFromButton setImage:[UIImage imageNamed:@"ic_calender.png"]];
                        calenderFromButton.userInteractionEnabled = YES;
                        UITapGestureRecognizer *fromTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateTap:)];
                        fromTap.numberOfTapsRequired = 1;
                        [calenderFromButton addGestureRecognizer:fromTap];
                        [_advancedSearchForm addSubview:calenderFromButton];
                        
                        UILabel *hifen = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-16)/3)+x+37, y, 8, 32)];
                        [hifen setTextColor:[UIColor blackColor]];
                        [hifen setBackgroundColor:[UIColor clearColor]];
                        [hifen setFont:[UIFont fontWithName:@"Trebuchet MS" size : 21.0f]];
                        [hifen setText:@"-"];
                        [_advancedSearchForm addSubview:hifen];
                        
                        UITextField *toField = [[UITextField alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-16)/3)+x+55, y, (SCREEN_WIDTH-16)/3, 32)];
                        toField.tag = i*100000;
                        toField.borderStyle = UITextBorderStyleRoundedRect;
                        toField.font = [UIFont systemFontOfSize:19];
                        toField.enabled = NO;
                        [_advancedSearchForm addSubview:toField];
                        
                        UIImageView *calenderToButton = [[UIImageView alloc] initWithFrame:CGRectMake((((SCREEN_WIDTH-16)/3)*2)+x+55, y, 32, 32)];
                        calenderToButton.tag = i*100000;
                        [calenderToButton setImage:[UIImage imageNamed:@"ic_calender.png"]];
                        calenderToButton.userInteractionEnabled = YES;
                        UITapGestureRecognizer *toTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateTap:)];
                        toTap.numberOfTapsRequired = 1;
                        [calenderToButton addGestureRecognizer:toTap];
                        [_advancedSearchForm addSubview:calenderToButton];
                        y += 40;
                        
                        queryStringDict = [[NSMutableDictionary alloc] init];
                        [queryStringDict setObject:formFieldDict[@"attributeCode"] forKey:@"code"];
                        [queryStringDict setObject:formFieldDict[@"inputType"] forKey:@"inputType"];
                        NSMutableDictionary *dateFromToDict = [[NSMutableDictionary alloc] init];
                        [dateFromToDict setObject:@"" forKey:@"from"];
                        [dateFromToDict setObject:@"" forKey:@"to"];
                        [queryStringDict setObject:dateFromToDict forKey:@"value"];
                        [queryString addObject:queryStringDict];
                    }
    }
    
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton addTarget:self action:@selector(startSearch:) forControlEvents:UIControlEventTouchUpInside];
    searchButton.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
    [searchButton.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
    [searchButton setTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"search" value:@"" table:nil] forState:UIControlStateNormal];
    searchButton.frame = CGRectMake(x, y, _advancedSearchForm.frame.size.width-16, 50);
    [_advancedSearchForm addSubview:searchButton];
    
    _advancedSearchFormHeightConstraint.constant = y+120;
    [_advancedSearchForm layoutIfNeeded];
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(void)dateTap:(UITapGestureRecognizer *)recognizer{
    _mainScrollView.contentSize = CGSizeMake(_mainScrollView.contentSize.width,0);
    textFieldTag = recognizer.view.tag;
    dateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    dateView.backgroundColor = [UIColor blackColor];
    dateView.alpha = 0.5;
    
    UIView *detePickerContainer = [[UIView alloc]initWithFrame:CGRectMake(((SCREEN_WIDTH/2)-((SCREEN_WIDTH/2)/2)), ((SCREEN_HEIGHT/2)-(((SCREEN_WIDTH/4)+100)/2)), SCREEN_WIDTH/2, (SCREEN_WIDTH/4)+45)];
    detePickerContainer.backgroundColor = [GlobalData colorWithHexString:@"00CBDF"];
    
    
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [okButton addTarget:self action:@selector(setDate:) forControlEvents:UIControlEventTouchUpInside];
    [okButton setTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"done" value:@"" table:nil] forState:UIControlStateNormal];
    okButton.frame = CGRectMake(0, (SCREEN_WIDTH/4)+5, (SCREEN_WIDTH/2)/2, 35);
    [detePickerContainer addSubview:okButton];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(dismissPicker:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"cancel" value:@"" table:nil] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake((SCREEN_WIDTH/2)/2, (SCREEN_WIDTH/4)+5, (SCREEN_WIDTH/2)/2, 35);
    [detePickerContainer addSubview:cancelButton];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, SCREEN_WIDTH/4)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.layer.cornerRadius = 2;
    datePicker.hidden = NO;
    if(selectedDate != [NSDate date])
        datePicker.date = selectedDate;
    else
        datePicker.date = [NSDate date];
    datePicker.alpha = 1;
    datePicker.backgroundColor = [UIColor whiteColor];
    
    [detePickerContainer addSubview:datePicker];
    [dateView addSubview:detePickerContainer];
    [_advancedSearchForm addSubview:dateView];
}

-(void)setDate:(id)sender{
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    df.dateStyle = NSDateFormatterMediumStyle;
    UITextField *tempField = (UITextField*)[_advancedSearchForm viewWithTag:textFieldTag];
    selectedDate = datePicker.date;
    tempField.text = [NSString stringWithFormat:@"%@", [df stringFromDate:datePicker.date]];
    [dateView removeFromSuperview];
    _mainScrollView.contentSize = CGSizeMake(_mainScrollView.contentSize.width, y-26);
}

-(void)dismissPicker:(id)sender{
    [dateView removeFromSuperview];
    _mainScrollView.contentSize = CGSizeMake(_mainScrollView.contentSize.width, y-26);
}

-(void)startSearch:(id)sender{
    // NSLog(@"called   %@",queryString);
    NSMutableDictionary *tempDict1;
    NSMutableDictionary *internalTempDict1;
    [internalTempDict1 removeAllObjects];
    [tempDict1 removeAllObjects];
    int isValid = 1;
    long lastTag = -1;
    for (UIView *subview in _advancedSearchForm.subviews){
        long thisTag = subview.tag;
        if(thisTag > 99999)
            thisTag = thisTag/100000;
        if([subview isKindOfClass:[UITextField class]]){
            UITextField *tempField = (UITextField*)subview;
            if(isValid){
                if(![tempField.text isEqualToString:@""])
                    isValid = 0;
            }
            
            NSMutableDictionary *tempDict = (NSMutableDictionary *)[queryString objectAtIndex:thisTag];
            
            if([tempDict[@"inputType"] isEqual:@"string"] || [tempDict[@"inputType"] isEqual:@"number"]){
                [tempDict setObject:tempField.text forKey:@"value"];
                [queryString replaceObjectAtIndex:thisTag withObject:tempDict];
            }
            else
                if([tempDict[@"inputType"] isEqual:@"date"] || [tempDict[@"inputType"] isEqual:@"price"]){
                    NSMutableDictionary *internalTempDict = (NSMutableDictionary *)tempDict[@"value"];
                    if(thisTag == lastTag)
                        [internalTempDict setObject:tempField.text forKey:@"to"];
                    else
                        [internalTempDict setObject:tempField.text forKey:@"from"];
                    [tempDict setObject:internalTempDict forKey:@"value"];
                    [queryString replaceObjectAtIndex:thisTag withObject:tempDict];
                    lastTag = thisTag;
                }
            
        }
        if([subview isKindOfClass:[UISwitch class]]){
            UISwitch *tempField = (UISwitch*)subview;
            NSString *tagString = [NSString stringWithFormat:@"%@", tempField.tag];
            NSArray *stringArray = [tagString componentsSeparatedByString:@"."];
            tempDict1 = (NSMutableDictionary *)[queryString objectAtIndex:[[stringArray objectAtIndex:1] integerValue]];
            internalTempDict1 = (NSMutableDictionary *)tempDict1[@"value"];
            if([tempField isOn])
                [internalTempDict1 setObject:@"true" forKey:stringArray[0]];
            if(![tempField isOn])
                [internalTempDict1 setObject:@"false" forKey:stringArray[0]];
            if(isValid){
                if([tempField isOn])
                    isValid = 0;
            }
        }
    }
    if(isValid == 1){
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:@"Please Select at least One Option" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAdvanceSearch.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    else{
        [self performSegueWithIdentifier:@"advancedSearchResult" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"advancedSearchResult"]) {
        AdvancedSearchResults *destViewController = segue.destinationViewController;
        destViewController.queryString = queryString;
    }
}


@end
