//
//  MyDownloads.m
//  Mobikul
//
//  Created by Ratnesh on 08/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "MyDownloads.h"
#import "AFNetworking.h"
#import "GlobalData.h"
#import "ToastView.h"

GlobalData *globalObjectMyDownload;

@implementation MyDownloads

- (void)viewDidLoad {
    [super viewDidLoad];
    whichApiResponseToHandle = @"";
    globalObjectMyDownload=[[GlobalData alloc] init];
    globalObjectMyDownload.delegate = self;
    _myDownloadScrollView.delegate = self;
    [globalObjectMyDownload language];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationController.title = [globalObjectMyDownload.languageBundle localizedStringForKey:@"myDownloadable" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    pageNumber = 1;
    loadPageRequestFlag = 1;
    contentHeight = -64;
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectMyDownload callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMyDownload.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectMyDownload.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectMyDownload.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMyDownload.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger currentCellCount = [arrayMainCollection count];
    // NSLog(@" called   %ld   %ld",totalCount,currentCellCount);
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height-50) {
        if( (totalCount > currentCellCount) && loadPageRequestFlag ){
            reloadPageData = @"true";
            pageNumber +=1;
            loadPageRequestFlag = 0;
            contentHeight = _myDownloadScrollView.contentOffset.y;
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
    
}


-(void) callingHttppApi{
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiResponseToHandle isEqualToString:@"getDownloadUrl"]){
        [GlobalData alertController:currentWindow msg:[globalObjectMyDownload.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        NSString *prefCustomerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", prefCustomerId];
        [post appendFormat:@"hash=%@", hash];
        [globalObjectMyDownload callHTTPPostMethod:post api:@"mobikulhttp/customer/downloadProduct" signal:@"HttpPostMetod"];
        globalObjectMyDownload.delegate = self;
    }
    else{
        if(pageNumber == 1)
            [GlobalData alertController:currentWindow msg:[globalObjectMyDownload.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        else{
            [GlobalData loadingController:currentWindow];
        }
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        [post appendFormat:@"websiteId=%@&", websiteId];
        NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
        [post appendFormat:@"customerEmail=%@&", prefCustomerEmail];
        [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
        [globalObjectMyDownload callHTTPPostMethod:post api:@"mobikulhttp/customer/getmyDownloads" signal:@"HttpPostMetod"];
        globalObjectMyDownload.delegate = self;
    }
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([whichApiResponseToHandle isEqual: @"getDownloadUrl"]){
        downloadUrlCollection = collection;
        NSString *status = [NSString stringWithFormat:@"%@", downloadUrlCollection[@"success"]];
        if([status isEqual: @"1"]){
            fileName = downloadUrlCollection[@"fileName"];
            NSURLSessionConfiguration *backgroundConfigurationObject = [NSURLSessionConfiguration backgroundSessionConfiguration:@"myBackgroundSessionIdentifier"];
            self.backgroundSession = [NSURLSession sessionWithConfiguration:backgroundConfigurationObject delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            //            [self.progressView setProgress:0 animated:NO];
            NSURL *url = [NSURL URLWithString:downloadUrlCollection[@"url"]];
            download = [self.backgroundSession downloadTaskWithURL:url];
            [download resume];
        }
        else{
            [ToastView showToastInParentView:self.view withText:downloadUrlCollection[@"message"] withStatus:@"error" withDuaration:5.0];
        }
    }
    else{
        if([currentWindow viewWithTag:313131])
            [[currentWindow viewWithTag:313131] removeFromSuperview];
        loadPageRequestFlag = 1;
        NSArray *newData = collection[@"allDownloads"];
        arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
        
        totalCount = [collection[@"totalCount"] integerValue];
        for(int i = 0; i < [arrayMainCollection count]; i++){
            NSDictionary *dict = [arrayMainCollection objectAtIndex:i];
            UIView *paintView =[[UIView alloc]initWithFrame:CGRectMake(5, (i*130)+(2*(i+1)), (_downloadContainer.frame.size.width)-10, 130)];
            paintView.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            [paintView setBackgroundColor:[UIColor whiteColor]];
            paintView.layer.borderWidth = 2.0f;
            [_downloadContainer addSubview:paintView];
            
            UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (_downloadContainer.frame.size.width)-14, 20)];
            [date setTextColor:[UIColor blackColor]];
            [date setBackgroundColor:[UIColor clearColor]];
            [date setFont:[UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
            NSString *dateText = [NSString stringWithFormat:@"DATE : %@",dict[@"date"]];
            [date setText:dateText];
            [paintView addSubview:date];
            
            UILabel *orderId = [[UILabel alloc] initWithFrame:CGRectMake(5, 30, (_downloadContainer.frame.size.width)-14, 20)];
            [orderId setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
            [orderId setBackgroundColor: [UIColor clearColor]];
            [orderId setFont: [UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
            NSString *orderIdText = [NSString stringWithFormat:@"ORDER# : %@",dict[@"incrementId"]];
            [orderId setText: orderIdText];
            [paintView addSubview:orderId];
            
            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, 55, (_downloadContainer.frame.size.width)-86, 20)];
            [title setTextColor:[UIColor blackColor]];
            [title setBackgroundColor:[UIColor clearColor]];
            [title setFont: [UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
            NSString *titleText = [NSString stringWithFormat:@"TITLE : %@",dict[@"proName"]];
            [title setText: titleText];
            [paintView addSubview:title];
            
            UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake(5, 80, (_downloadContainer.frame.size.width)-14, 20)];
            [status setTextColor: [UIColor blackColor]];
            [status setBackgroundColor: [UIColor clearColor]];
            [status setFont: [UIFont fontWithName: @"Helvetica-Oblique" size: 16.0f]];
            NSString *capitalizeText = dict[@"status"];
            capitalizeText = [NSString stringWithFormat:@"%@%@",[[capitalizeText substringToIndex:1] uppercaseString],[capitalizeText substringFromIndex:1] ];
            NSString *statusText = [NSString stringWithFormat:@"STATUS : %@", capitalizeText];
            [status setText: statusText];
            [paintView addSubview:status];
            
            UILabel *remainDownload = [[UILabel alloc] initWithFrame:CGRectMake(5, 105, (_downloadContainer.frame.size.width)-14, 20)];
            [remainDownload setTextColor: [UIColor blackColor]];
            [remainDownload setBackgroundColor: [UIColor clearColor]];
            [remainDownload setFont: [UIFont fontWithName: @"Trebuchet MS" size: 16.0f]];
            NSString *remainDownloadText = [NSString stringWithFormat:@"REMAINING DOWNLOADS : %@", dict[@"remainingDownloads"]];
            [remainDownload setText: remainDownloadText];
            [paintView addSubview:remainDownload];
            
            UIImageView *downloadIcon = [[UIImageView alloc] initWithFrame:CGRectMake((_downloadContainer.frame.size.width)-86,25,72,72)];
            downloadIcon.image = [UIImage imageNamed:@"ic_download.png"];
            downloadIcon.tag = i;
            downloadIcon.userInteractionEnabled = YES;
            [paintView addSubview:downloadIcon];
            
            UITapGestureRecognizer *tapGesture1 =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DownloadFile:)];
            tapGesture1.numberOfTapsRequired = 1;
            [downloadIcon addGestureRecognizer:tapGesture1];
        }
        if([arrayMainCollection count] == totalCount)
            _downloadContainerHeightConstraint.constant = (132*[arrayMainCollection count]) + 2;
        else
            _downloadContainerHeightConstraint.constant = (132*[arrayMainCollection count]) + 2 + 50;  // // to give space for display a loader alert
        [_downloadContainer layoutIfNeeded];
    }
    //[self scrollAutomatically];
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(void)scrollAutomatically{
    float width = CGRectGetWidth(_myDownloadScrollView.frame);
    float height = CGRectGetHeight(_myDownloadScrollView.frame);
    float newPosition = contentHeight;
    CGRect toVisible = CGRectMake(0,newPosition, width, height);
    [_myDownloadScrollView scrollRectToVisible:toVisible animated:YES];
}

- (void)DownloadFile:(UITapGestureRecognizer *)tapGesture{
    if (tapGesture.state == UIGestureRecognizerStateEnded)  {
        NSDictionary *tempDict =  [arrayMainCollection objectAtIndex:(long)[tapGesture.view tag]];
        hash = tempDict[@"hash"];
        hash = collection[@"allDownloads"][(long)[tapGesture.view tag]][@"hash"];
        whichApiResponseToHandle = @"getDownloadUrl";
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectoryPath = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Adding a new folder to the documents directory path
    NSString *appDir = [documentDirectoryPath stringByAppendingPathComponent:@"/MyDownLoadableProduct/"];
    // Checking for directory existence and creating if not already exists
    if(![fileManager fileExistsAtPath:appDir])
        [fileManager createDirectoryAtPath:appDir withIntermediateDirectories:NO attributes:nil error:&error];
    appDir = [appDir stringByAppendingFormat:@"/%@",[[downloadTask response] suggestedFilename]];
    
    //checking for file existence and deleting if already present.
    if([fileManager fileExistsAtPath:appDir]){
        //NSLog([fileManager removeItemAtPath:appDir error:&error]?@"deleted":@"not deleted");
    }
    NSURL *destinationURL = [NSURL fileURLWithPath:[documentDirectoryPath stringByAppendingPathComponent: fileName]];
    if([fileManager fileExistsAtPath:[destinationURL path]]){
        [fileManager replaceItemAtURL:destinationURL withItemAtURL:destinationURL backupItemName:nil options:NSFileManagerItemReplacementUsingNewMetadataOnly resultingItemURL:nil error:&error];
        [self showFile:[destinationURL path]];
    }
    else{
        if([fileManager moveItemAtURL:location toURL:destinationURL error:&error])
            [self showFile:[destinationURL path]];
        else{
            UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMyDownload.languageBundle localizedStringForKey:@"downloader" value:@"" table:nil] message:[NSString stringWithFormat:[globalObjectMyDownload.languageBundle localizedStringForKey:@"errorOccured" value:@"" table:nil],[error localizedDescription]] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMyDownload.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
            [AC addAction:noBtn];
            [self.parentViewController presentViewController:AC animated:YES completion:nil];
        }
    }
    //    BOOL fileCopied = [fileManager moveItemAtPath:[location path] toPath:appDir error:&error];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes{
    [ToastView showToastInParentView:self.view withText:[globalObjectMyDownload.languageBundle localizedStringForKey:@"downloadResume" value:@"" table:nil] withStatus:@"success" withDuaration:5.0];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    download = nil;
    if(error) {
        [ToastView showToastInParentView:self.view withText:[error localizedDescription] withStatus:@"error" withDuaration:5.0];
    }
}

- (void)showFile:(NSString*)path{
    BOOL isFound = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if(isFound) {
        UIDocumentInteractionController *viewer = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:path]];
        viewer.delegate = self;
        [viewer presentPreviewAnimated:YES];
    }
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller{
    return self;
}

@end