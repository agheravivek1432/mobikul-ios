//
//  Home.m
//  Mobikul
//
//  Created by Ratnesh on 10/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "Home.h"
#import "GlobalData.h"
#import "CatalogProduct.h"
#import "SWRevealViewController.h"
#import "ProductCategory.h"
#import "ToastView.h"
#import "CategoryMenu.h"
#import "AppDelegate.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectHome;

@implementation Home

- (void)viewDidLoad {
    [super viewDidLoad];
    imageCache = [[NSCache alloc] init];
    languageCode = [NSUserDefaults standardUserDefaults];
    preferences = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *flag =[NSUserDefaults standardUserDefaults];
    
    if([[flag stringForKey:@"loadAppDlegate"] isEqualToString:@"arabic"]){
        [flag removeObjectForKey:@"loadAppDlegate"];
        [appDelegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
    }
    else if([appDelegate.dataStore isEqualToString:@"otherLanguage"] && ![[languageCode stringForKey:@"language" ] isEqualToString:@"ar"]){
        appDelegate.dataStore = @"";
        [appDelegate application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:nil];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceivedProduct:) name:@"pushNotificationforProduct" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceivedCategory:) name:@"pushNotificationforCategory" object:nil];
    whichApiDataToprocess = @"";
    globalObjectHome=[[GlobalData alloc] init];
    globalObjectHome.delegate=self;
    isAlertVisible = 0;
    [globalObjectHome language];
    imageCache = [[NSCache alloc] init];
    catalogCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    _categotyMenuButton.target = self.revealViewController;
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        _categoryArabic.action = @selector(rightRevealToggle:);
    else
        _categotyMenuButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    _listMenuButton.target = self.revealViewController;
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        _listArabic.action = @selector(revealToggle:);
    else
        _listMenuButton.action = @selector(rightRevealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:@"00CBDF"];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    self.navigationItem.title = [globalObjectHome.languageBundle localizedStringForKey:@"home" value:@"" table:nil];
    
    
    _featureProductLabel.text =[globalObjectHome.languageBundle localizedStringForKey:@"featureProduct" value:@"" table:nil];
    _featuredCategoryTitle.text =[globalObjectHome.languageBundle localizedStringForKey:@"featureCategory" value:@"" table:nil];
    _productLabel.text =[globalObjectHome.languageBundle localizedStringForKey:@"newProduct" value:@"" table:nil];
    [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setTitle:[globalObjectHome.languageBundle localizedStringForKey:@"cart" value:@"" table:nil]];
    [[[[[self tabBarController] tabBar] items] objectAtIndex:3] setTitle:[globalObjectHome.languageBundle localizedStringForKey:@"notification" value:@"" table:nil]];
    [[[[[self tabBarController] tabBar] items] objectAtIndex:2] setTitle:[globalObjectHome.languageBundle localizedStringForKey:@"newsletterTitle" value:@"" table:nil]];
    [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setTitle:[globalObjectHome.languageBundle localizedStringForKey:@"search" value:@"" table:nil]];
    [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setTitle:[globalObjectHome.languageBundle localizedStringForKey:@"home" value:@"" table:nil]];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    NSLog(@" %@ ",collectionData);
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}
-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectHome callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}
-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:@"Warning" message:@"ERROR with the Conenction" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void) callingHttppApi
{
    [GlobalData alertController:currentWindow msg:[globalObjectHome.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiDataToprocess isEqual:@"addToWishlist"]){
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId != nil)
            [post appendFormat:@"customerId=%@&", customerId];
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectHome callHTTPPostMethod:post api:@"mobikulhttp/catalog/addtoWishlist" signal:@"HttpPostMetod"];
    }
    
    else if([whichApiDataToprocess isEqual: @"addToCart"]){
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId !=nil)
            [post appendFormat:@"customerId=%@&", customerId];
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId != nil)
            [post appendFormat:@"quoteId=%@&", quoteId];
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectHome callHTTPPostMethod:post api:@"mobikulhttp/checkout/addtoCart" signal:@"HttpPostMetod"];
        globalObjectHome.delegate = self;
        
    }
    
    else{
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        if(websiteId == nil){
            [post appendFormat:@"websiteId=%@&", DEFAULT_WEBSITE_ID];
            [preferences setObject:DEFAULT_WEBSITE_ID forKey:@"websiteId"];
            [preferences synchronize];
        }else{
            [post appendFormat:@"websiteId=%@&", websiteId];
        }
        
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId != nil)
            [post appendFormat:@"customerId=%@&", customerId];
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@", screenWidth];
        [globalObjectHome callHTTPPostMethod:post api:@"mobikulhttp/catalog/getcategoryList" signal:@"HttpPostMetod"];
    }
}


-(void)pushNotificationReceivedProduct:(NSNotification *)note{
    NSDictionary *root = [note valueForKeyPath:@"userInfo"];
    NSDictionary *value = [root valueForKeyPath:@"aps"];
    NSString *productId = value[@"productId"];
    NSString *productName = value[@"productName"];
    NSMutableDictionary *notificationProductDictionary = [[NSMutableDictionary alloc] init];
    [notificationProductDictionary setValue:productId forKey:@"entityId"];
    [notificationProductDictionary setValue:productName forKey:@"name"];
    productDictionary = notificationProductDictionary;
    [self performSegueWithIdentifier:@"openProductFromHomeSegue" sender:self];
}

-(void)pushNotificationReceivedCategory:(NSNotification *)note{
    NSDictionary *root = [note valueForKeyPath:@"userInfo"];
    NSDictionary *value = [root valueForKeyPath:@"aps"];
    NSString *categoryId = value[@"categoryId"];
    NSString *categoryName = value[@"categoryName"];
    NSMutableDictionary  *notificationCategoryDictionary = [[NSMutableDictionary alloc] init];
    [notificationCategoryDictionary setValue:categoryId forKey:@"categoryId"];
    [notificationCategoryDictionary setValue:categoryName forKey:@"categoryName"];
    featuredCategoryDictionary = notificationCategoryDictionary;
    [self performSegueWithIdentifier:@"featuredCategory" sender:self];
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([whichApiDataToprocess isEqual: @"addToWishlist"]){
        addToWishListCollection = collection;
        if([addToWishListCollection[@"status"] boolValue])
            [ToastView showToastInParentView:self.view withText:[globalObjectHome.languageBundle localizedStringForKey:@"productAdded" value:@"" table:nil] withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:[globalObjectHome.languageBundle localizedStringForKey:@"somethingWrong" value:@"" table:nil] withStatus:@"error" withDuaration:5.0];
    }
    else
        if([whichApiDataToprocess isEqual: @"addToCart"]){
            addToCartCollection = collection;
            if([addToCartCollection objectForKey:@"quoteId"]){
                [preferences setObject:addToCartCollection[@"quoteId"] forKey:@"quoteId"];
                [preferences synchronize];
            }
            if([addToCartCollection objectForKey:@"cartCount"]){
                if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                    [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
                else
                    [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
            }
            if([addToCartCollection[@"error"] boolValue])
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"error" withDuaration:5.0];
            else
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"success" withDuaration:5.0];
        }
        else{
            mainCollection = collection;
            [preferences setObject:collection[@"storeId"] forKey:@"storeId"];
            [preferences synchronize];
            if([mainCollection objectForKey:@"cartCount"])
                if([mainCollection[@"cartCount"] boolValue]){
                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", mainCollection[@"cartCount"]]];
                    else
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", mainCollection[@"cartCount"]]];
                }
            GlobalData *obj = [GlobalData getInstance];
            float totalHeight = 0;
            // Processing Categories
            categoryData = [[NSMutableArray alloc] init];
            categoryId = [[NSMutableArray alloc] init];
            for(int i = 0; i < [mainCollection[@"categories"][@"children"] count]; i++){
                NSDictionary *categoryDict = [mainCollection[@"categories"][@"children"] objectAtIndex:i];
                NSString *name = [NSString stringWithFormat:@"%@",categoryDict[@"name"]];
                [categoryData addObject: name];
                NSString *id = [NSString stringWithFormat:@"%@",categoryDict[@"category_id"]];
                [categoryId addObject: id];
            }
            obj.wholeCategoryData = mainCollection[@"categories"][@"children"];
            obj.categoryData = categoryData;
            obj.categoryId = categoryId;
            
            //processing categoryImages
            categoryImagesId =  [[NSMutableArray alloc] init];
            categoryImages =  [[NSMutableArray alloc] init];
            for(int i = 0; i < [mainCollection[@"categoryImages"] count]; i++){
                NSDictionary *categoryImagesDict = [mainCollection[@"categoryImages"] objectAtIndex:i];
                NSString *id = [NSString stringWithFormat:@"%@",categoryImagesDict[@"id"]];
                NSString *image = [NSString stringWithFormat:@"%@",categoryImagesDict[@"thumbnail"]];
                [categoryImages addObject: image];
                [categoryImagesId addObject: id];
            }
            if(collection[@"customerProfileImage"]){
                [preferences setObject:collection[@"customerProfileImage"] forKey:@"profilePicture" ];
            }
            if(collection[@"customerBannerImage"]){
                [preferences setObject:collection[@"customerBannerImage"] forKey:@"profileBanner" ];
            }
            
            NSString *profileUrl = [preferences objectForKey:@"profilePicture"];
            UIImage *profile=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:profileUrl]]];
            [catalogCache removeObjectForKey:profileUrl];
            if(profile != nil)
                [catalogCache setObject:profile forKey:profileUrl];
            
            NSString *bannerUrl = [preferences objectForKey:@"profileBanner"];
            UIImage *banner=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:bannerUrl]]];
            [catalogCache removeObjectForKey:bannerUrl];
            if(banner != nil)
                [catalogCache setObject:banner forKey:bannerUrl];
            _cache = [[NSCache alloc] init];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
            for (int i=0 ; i< [categoryImages count]; i++) {
                    UIImage *restri=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[categoryImages objectAtIndex:i]]]];
                    [_cache setObject:restri forKey:[categoryImages objectAtIndex:i]];
                }
            });
            obj.profileCache = catalogCache;
            obj.gloablCatalogIconCache = _cache;
            obj.categoryImagesId = categoryImagesId;
            obj.categoryImages = categoryImages;
            
            
            // Processing Stores
            storeData = [[NSMutableArray alloc] init];
            for(int i = 0; i < [mainCollection[@"storeData"] count]; i++){
                NSDictionary *storeDict = [mainCollection[@"storeData"] objectAtIndex:i];
                [storeData addObject: [NSString stringWithFormat:@"%@",storeDict[@"name"]]];
                for(int j = 0; j < [storeDict[@"stores"] count]; j++){
                    NSDictionary *viewDict = [storeDict[@"stores"] objectAtIndex:j];
                    NSString *name = [NSString stringWithFormat:@"      %@-_-%@",viewDict[@"name"],viewDict[@"id"]];
                    [storeData addObject: name];
                }
            }
            obj.wholeStoreData = mainCollection[@"storeData"];
            obj.storeData = storeData;
            
//            obj.extraData = [NSMutableArray arrayWithObjects:@"Search Terms", @"Advanced Search",@"Market place",@"ddd", nil];
            obj.extraData = [NSMutableArray arrayWithObjects:[globalObjectHome.languageBundle localizedStringForKey:@"searchTerms" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"advanceSearch" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"marketPlace" value:@"" table:nil], nil];
            
            if([preferences objectForKey:@"customerId"] == nil)
            {
                obj.menuListData = [NSMutableArray arrayWithObjects:[globalObjectHome.languageBundle localizedStringForKey:@"logIn" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"dashboard" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"accountInformation" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"addressBook" value:@"" table:nil],[globalObjectHome.languageBundle localizedStringForKey:@"myOrders" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"productReviews" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"wishlist" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"newsletter" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"downloadableProduct" value:@"" table:nil],  nil];
            }
            else{
                obj.menuListData = [NSMutableArray arrayWithObjects:[globalObjectHome.languageBundle localizedStringForKey:@"logOut" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"dashboard" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"accountInformation" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"addressBook" value:@"" table:nil],[globalObjectHome.languageBundle localizedStringForKey:@"myOrders" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"productReviews" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"wishlist" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"newsletter" value:@"" table:nil], [globalObjectHome.languageBundle localizedStringForKey:@"downloadableProduct" value:@"" table:nil],[globalObjectHome.languageBundle localizedStringForKey:@"Bolgs" value:@"" table:nil],  nil];
                
            }
            
            NSInteger isSeller = [[preferences objectForKey:@"isSeller"] intValue];
            if(isSeller == 1){
                obj.listMenuTitleData = [[NSMutableArray alloc] initWithObjects:@" My Account", @" MarketPlace",nil];
                obj.marketPlaceData = [[NSMutableArray alloc] initWithObjects:@"Seller Dash Board", @"Seller Order",@"Ask Question to Admin",@"Seller Review List",nil];
                obj.completeListMenuData =[[NSMutableArray alloc] initWithObjects:obj.menuListData,obj.marketPlaceData,nil];
            }
            else{
                obj.listMenuTitleData = [[NSMutableArray alloc] initWithObjects:@" My Account",nil];
                obj.completeListMenuData =[[NSMutableArray alloc] initWithObjects:obj.menuListData,nil];
            }
            
            // Processing Banner Images
            float x = 0;
            float subWish = 64;
            float subAdd = 32;
            _imageScrollerHeightConstraint.constant = (SCREEN_WIDTH-4) / 2;
            _imageScrollerWidthConstraint.constant = SCREEN_WIDTH-4;
            _imageScroller.contentSize = CGSizeMake(SCREEN_WIDTH-4, (SCREEN_WIDTH-4)/2);
            imageCount = [mainCollection[@"bannerImages"] count];
            if(imageCount > 0)
                totalHeight += (SCREEN_WIDTH-4)/2;
            else
                _imageScrollerHeightConstraint.constant = 0;
            for(int i=0; i<[mainCollection[@"bannerImages"] count]; i++) {
                NSDictionary *bannerDict = [mainCollection[@"bannerImages"] objectAtIndex:i];
                UIImageView *banner = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, (SCREEN_WIDTH-4), ((SCREEN_WIDTH-4)/2))];
                UIImage *image = [imageCache objectForKey:bannerDict[@"url"]];
                banner.image = [UIImage imageNamed:@"ic_placeholder.png"];
                if(image)
                    banner.image = image;
                else{
                    [_queue addOperationWithBlock:^{
                        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:bannerDict[@"url"]]];
                        UIImage *image = [UIImage imageWithData: imageData];
                        if(image){
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                banner.image = image;
                            }];
                            [imageCache setObject:image forKey:bannerDict[@"url"]];
                        }
                    }];
                }
                x += (SCREEN_WIDTH-4);
                banner.tag = i;
                banner.userInteractionEnabled = YES;
                UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBannerTap:)];
                singleFingerTap.numberOfTapsRequired = 1;
                [banner addGestureRecognizer:singleFingerTap];
                [_imageScroller addSubview:banner];
            }
            _imageScroller.contentSize = CGSizeMake(x, _imageScroller.frame.size.height);
            [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(scrollAutomatically:) userInfo:nil repeats:YES];
            
            // Processing Featured Products
            
            if([mainCollection[@"featuredProducts"] count] == 0){
                [_featureProductLabel setHidden:YES];
                _featuredProductHeightConstraint.constant=0;
                _featureProductHeightConstraints.constant=0;
            }
            else{
                if(SCREEN_WIDTH > 500){
                    _featuredProductHeightConstraint.constant = (SCREEN_WIDTH/2.5) + 110;
                }
                else{
                    _featuredProductHeightConstraint.constant = (SCREEN_WIDTH/2.5) + 80;
                }
                
            }
            //        _featuredProductHeightConstraint.constant = (SCREEN_WIDTH/2.5) + 110;
            x = 5;
            if([mainCollection[@"featuredProducts"] count] > 0){
                totalHeight += (SCREEN_WIDTH/2.5) + 100;
                totalHeight += 21;
            }
            featuredProductCurtainOpenSignal = [[NSMutableArray alloc] init];
            for(int i=0; i<[mainCollection[@"featuredProducts"] count]; i++) {
                NSDictionary *featuredProductDict = [mainCollection[@"featuredProducts"] objectAtIndex:i];
                float y = 5;
                float productBlockHeight;
                if(SCREEN_WIDTH > 500){
                    productBlockHeight = (SCREEN_WIDTH/2.5)+100;
                }
                else{
                    productBlockHeight = (SCREEN_WIDTH/2.5)+70;
                }
                UIView *productBlock = [[UIView alloc]initWithFrame:CGRectMake(x, y, (SCREEN_WIDTH/2.5)+10, productBlockHeight)];
                featuredProductCurtainOpenSignal[i] = @"0";
                x = x + ((SCREEN_WIDTH/2.5) + 20);
                [productBlock setBackgroundColor:[UIColor whiteColor]];
                productBlock.layer.cornerRadius = 2;
                productBlock.layer.shadowOffset = CGSizeMake(0, 0);
                productBlock.layer.shadowRadius = 3;
                productBlock.layer.shadowOpacity = 0.5;
                [_featuredProductContainer addSubview:productBlock];
                
                UIView *productImageBlock = [[UIView alloc]initWithFrame:CGRectMake(5, y, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
                productImageBlock.clipsToBounds = YES;
                UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
                productImage.userInteractionEnabled = YES;
                productImage.tag = i;
                productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
                UIImage *image = [imageCache objectForKey:featuredProductDict[@"thumbNail"]];
                if(image)
                    productImage.image = image;
                else{
                    [_queue addOperationWithBlock:^{
                        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:featuredProductDict[@"thumbNail"]]];
                        UIImage *image = [UIImage imageWithData: imageData];
                        if(image){
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                productImage.image = image;
                            }];
                            [imageCache setObject:image forKey:featuredProductDict[@"thumbNail"]];
                        }
                    }];
                }
                UITapGestureRecognizer *openFeaturedProductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredViewProduct:)];
                openFeaturedProductGesture.numberOfTapsRequired = 1;
                [productImage addGestureRecognizer:openFeaturedProductGesture];
                
                [productImageBlock addSubview:productImage];
                UIView *productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
                [productImageCurtain setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
                
                if(SCREEN_WIDTH < 500){
                    subWish = 54;
                    subAdd = 22;
                }
                
                UIImageView *wishlistButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)-subWish, (SCREEN_WIDTH/2.5)-40, 32, 32)];
                [wishlistButton setImage:[UIImage imageNamed:@"ic_addtowishlist.png"]];
                wishlistButton.tag = i;
                wishlistButton.userInteractionEnabled = YES;
                UITapGestureRecognizer *addtowishlistGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredAddtoWishlist:)];
                addtowishlistGesture.numberOfTapsRequired = 1;
                [wishlistButton addGestureRecognizer:addtowishlistGesture];
                [productImageCurtain addSubview:wishlistButton];
                if([featuredProductDict[@"hasOptions"] isEqual: @"1"] || [featuredProductDict[@"typeId"] isEqual: @"configurable"] || [featuredProductDict[@"typeId"] isEqual: @"bundle"] || [featuredProductDict[@"typeId"] isEqual: @"grouped"]){
                    UIImageView *viewproductButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
                    [viewproductButton setImage:[UIImage imageNamed:@"ic_viewproduct.png"]];
                    viewproductButton.tag = i;
                    viewproductButton.userInteractionEnabled = YES;
                    UITapGestureRecognizer *viewproductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredViewProduct:)];
                    viewproductGesture.numberOfTapsRequired = 1;
                    [viewproductButton addGestureRecognizer:viewproductGesture];
                    [productImageCurtain addSubview:viewproductButton];
                }
                else{
                    UIImageView *addtocartButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
                    [addtocartButton setImage:[UIImage imageNamed:@"ic_addtocart.png"]];
                    addtocartButton.tag = i;
                    addtocartButton.userInteractionEnabled = YES;
                    UITapGestureRecognizer *addtocartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredAddtoCart:)];
                    addtocartGesture.numberOfTapsRequired = 1;
                    [addtocartButton addGestureRecognizer:addtocartGesture];
                    [productImageCurtain addSubview:addtocartButton];
                }
                [productImageBlock addSubview:productImageCurtain];
                [productBlock addSubview:productImageBlock];
                
                y += (SCREEN_WIDTH/2.5)+5;
                UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(10, y, (SCREEN_WIDTH/2.5)-50, 25)];
                [productName setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [productName setBackgroundColor:[UIColor clearColor]];
                [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                [productName setText:featuredProductDict[@"name"]];
                [productBlock addSubview:productName];
                
                if(SCREEN_WIDTH > 500){
                    y += 30;
                    if([featuredProductDict[@"typeId"] isEqual:@"grouped"]){
                        UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 90, 25)];
                        [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preString setBackgroundColor:[UIColor clearColor]];
                        [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [preString setText:[globalObjectHome.languageBundle localizedStringForKey:@"starting" value:@"" table:nil]];
                        [productBlock addSubview:preString];
                        
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(95, y, 70, 25)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [price setText:featuredProductDict[@"groupedPrice"]];
                        [productBlock addSubview:price];
                    }
                    else
                        if([featuredProductDict[@"typeId"] isEqual:@"bundle"]){
                            UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 50, 25)];
                            [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [from setBackgroundColor:[UIColor clearColor]];
                            [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [from setText:[globalObjectHome.languageBundle localizedStringForKey:@"from" value:@"" table:nil]];
                            [productBlock addSubview:from];
                            
                            UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(55, y, 60, 25)];
                            [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                            [minPrice setBackgroundColor:[UIColor clearColor]];
                            [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [minPrice setText:featuredProductDict[@"formatedMinPrice"]];
                            [productBlock addSubview:minPrice];
                            
                            UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(125, y, 25, 25)];
                            [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [to setBackgroundColor:[UIColor clearColor]];
                            [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [to setText:[globalObjectHome.languageBundle localizedStringForKey:@"to" value:@"" table:nil]];
                            // [productBlock addSubview:to];
                            
                            UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(147, y, 60, 25)];
                            [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                            [maxPrice setBackgroundColor:[UIColor clearColor]];
                            [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [maxPrice setText:featuredProductDict[@"formatedMaxPrice"]];
                            
                            float widthX = productBlock.frame.size.width - 125;
                            
                            if(widthX > 90){
                                [productBlock addSubview:to];
                                [productBlock addSubview:maxPrice];
                            }else{
                                UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(125, y, widthX, 25)];
                                [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                [dot setBackgroundColor:[UIColor clearColor]];
                                [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                [dot setText:[[globalObjectHome.languageBundle localizedStringForKey:@"to" value:@"" table:nil] stringByAppendingString:@" ..."]];
                                [productBlock addSubview:dot];
                            }
                            
                            //[productBlock addSubview:maxPrice];
                        }
                        else{
                            if(![featuredProductDict[@"specialPrice"] isEqual:[NSNull null]] && [featuredProductDict[@"isInRange"] boolValue]){
                                UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 70, 25)];
                                [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                [regularprice setBackgroundColor:[UIColor clearColor]];
                                [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:featuredProductDict[@"formatedPrice"]];
                                [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                                [regularprice setAttributedText:attributeString];
                                [productBlock addSubview:regularprice];
                                
                                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 85, 25)];
                                [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                                [price setBackgroundColor:[UIColor clearColor]];
                                [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                [price setText:featuredProductDict[@"formatedSpecialPrice"]];
                                [productBlock addSubview:price];
                            }
                            else{
                                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 60, 25)];
                                [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                                [price setBackgroundColor:[UIColor clearColor]];
                                [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                [price setText:featuredProductDict[@"formatedPrice"]];
                                [productBlock addSubview:price];
                            }
                        }
                    if([featuredProductDict[@"hasTierPrice"] isEqual:@"true"] && [featuredProductDict[@"typeId"] isEqual: @"simple"]){
                        UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 90, 25)];
                        [preString setTextColor:[GlobalData colorWithHexString:@"cf5050"]];
                        [preString setBackgroundColor:[UIColor clearColor]];
                        [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [preString setText:[globalObjectHome.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil]];
                        
                        
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(165, y, 60, 25)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [price setText:featuredProductDict[@"tierPrice"]];
                        
                        float widthX = productBlock.frame.size.width - 80;
                        
                        if(widthX > 150){
                            [productBlock addSubview:preString];
                            [productBlock addSubview:price];
                        }else{
                            UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(80, y, widthX, 25)];
                            [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [dot setBackgroundColor:[UIColor clearColor]];
                            [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [dot setText:[[globalObjectHome.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil] stringByAppendingString:@" ..."]];
                            [productBlock addSubview:dot];
                        }
                    }
                }
                float yProductOption;
                if(SCREEN_WIDTH > 500)
                    yProductOption = (SCREEN_WIDTH/2.5)+65;
                else
                    yProductOption = (SCREEN_WIDTH/2.5)+10;
                UIImageView *productOption = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)-25, yProductOption, 32, 32)];
                [productOption setImage:[UIImage imageNamed:@"ic_pro_option.png"]];
                productOption.userInteractionEnabled = YES;
                productOption.tag = i;
                [productBlock addSubview:productOption];
                UITapGestureRecognizer *showOptionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredProductShowOptions:)];
                showOptionGesture.numberOfTapsRequired = 1;
                [productOption addGestureRecognizer:showOptionGesture];
                
                y += 30;
                UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(10,y,120,24)];
                UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
                [ratingContainer addSubview:grayContainer];
                UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI1];
                UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI2];
                UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI3];
                UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI4];
                UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI5];
                
                double percent = 24 * [featuredProductDict[@"rating"] doubleValue];
                UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
                blueContainer.clipsToBounds = YES;
                [ratingContainer addSubview:blueContainer];
                UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI1];
                UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI2];
                UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI3];
                UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI4];
                UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI5];
                [productBlock addSubview:ratingContainer];
            }
            _featuredProductContainer.contentSize = CGSizeMake(x, _featuredProductContainer.frame.size.height);
            
            // Processing New Products
            if([mainCollection[@"newProducts"] count] == 0){
                [_productLabel setHidden:YES];
                _nwProHeightConstraint.constant = 0;
                _productHeightLabelConstraint.constant = 0;
            }
            else{
                if(SCREEN_WIDTH > 500){
                    _nwProHeightConstraint.constant = (SCREEN_WIDTH/2.5) + 110;
                }
                else{
                    _nwProHeightConstraint.constant = (SCREEN_WIDTH/2.5) + 80;
                }
                
            }
            //        _nwProHeightConstraint.constant = (SCREEN_WIDTH/2.5) + 110;
            x = 5;
            if([mainCollection[@"newProducts"] count] > 0){
                totalHeight += (SCREEN_WIDTH/2.5) + 100;
                totalHeight += 21;
            }
            newProductCurtainOpenSignal = [[NSMutableArray alloc] init];
            for(int i = 0; i < [mainCollection[@"newProducts"] count]; i++) {
                NSDictionary *newProductDict = [mainCollection[@"newProducts"] objectAtIndex:i];
                float y = 5;
                float productBlockHeight;
                if(SCREEN_WIDTH > 500){
                    productBlockHeight = (SCREEN_WIDTH/2.5)+100;
                }
                else{
                    productBlockHeight = (SCREEN_WIDTH/2.5)+70;
                }
                UIView *productBlock =[[UIView alloc]initWithFrame:CGRectMake(x, 5, (SCREEN_WIDTH/2.5)+10, productBlockHeight)];
                newProductCurtainOpenSignal[i] = @"0";
                x = x + ((SCREEN_WIDTH/2.5) + 20);
                [productBlock setBackgroundColor:[UIColor whiteColor]];
                productBlock.layer.cornerRadius = 2;
                productBlock.layer.shadowOffset = CGSizeMake(0, 0);
                productBlock.layer.shadowRadius = 3;
                productBlock.layer.shadowOpacity = 0.5;
                [_nwProContainer addSubview:productBlock];
                
                UIView *productImageBlock = [[UIView alloc]initWithFrame:CGRectMake(5, y, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
                productImageBlock.clipsToBounds = YES;
                UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
                productImage.userInteractionEnabled = YES;
                productImage.tag = i;
                productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
                UIImage *image = [imageCache objectForKey:newProductDict[@"thumbNail"]];
                if(image)
                    productImage.image = image;
                else{
                    [_queue addOperationWithBlock:^{
                        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:newProductDict[@"thumbNail"]]];
                        UIImage *image = [UIImage imageWithData: imageData];
                        if(image){
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                productImage.image = image;
                            }];
                            [imageCache setObject:image forKey:newProductDict[@"thumbNail"]];
                        }
                    }];
                }
                UITapGestureRecognizer *openNewProductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newViewProduct:)];
                openNewProductGesture.numberOfTapsRequired = 1;
                [productImage addGestureRecognizer:openNewProductGesture];
                
                [productImageBlock addSubview:productImage];
                UIView *productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
                [productImageCurtain setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
                UIImageView *wishlistButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)-subWish, (SCREEN_WIDTH/2.5)-40, 32, 32)];
                [wishlistButton setImage:[UIImage imageNamed:@"ic_addtowishlist.png"]];
                wishlistButton.tag = i;
                wishlistButton.userInteractionEnabled = YES;
                UITapGestureRecognizer *addtowishlistGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newAddtoWishlist:)];
                addtowishlistGesture.numberOfTapsRequired = 1;
                [wishlistButton addGestureRecognizer:addtowishlistGesture];
                [productImageCurtain addSubview:wishlistButton];
                if([newProductDict[@"hasOptions"] isEqual: @"1"] || [newProductDict[@"typeId"] isEqual: @"configurable"] || [newProductDict[@"typeId"] isEqual: @"bundle"] || [newProductDict[@"typeId"] isEqual: @"grouped"]){
                    UIImageView *viewproductButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
                    [viewproductButton setImage:[UIImage imageNamed:@"ic_viewproduct.png"]];
                    viewproductButton.tag = i;
                    viewproductButton.userInteractionEnabled = YES;
                    UITapGestureRecognizer *viewproductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newViewProduct:)];
                    viewproductGesture.numberOfTapsRequired = 1;
                    [viewproductButton addGestureRecognizer:viewproductGesture];
                    [productImageCurtain addSubview:viewproductButton];
                }
                else{
                    UIImageView *addtocartButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
                    [addtocartButton setImage:[UIImage imageNamed:@"ic_addtocart.png"]];
                    addtocartButton.tag = i;
                    addtocartButton.userInteractionEnabled = YES;
                    UITapGestureRecognizer *addtocartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newAddtoCart:)];
                    addtocartGesture.numberOfTapsRequired = 1;
                    [addtocartButton addGestureRecognizer:addtocartGesture];
                    [productImageCurtain addSubview:addtocartButton];
                }
                [productImageBlock addSubview:productImageCurtain];
                [productBlock addSubview:productImageBlock];
                
                y += (SCREEN_WIDTH/2.5)+5;
                UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(10, y, (SCREEN_WIDTH/2.5)-50, 25)];
                [productName setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [productName setBackgroundColor:[UIColor clearColor]];
                [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                [productName setText:newProductDict[@"name"]];
                [productBlock addSubview:productName];
                if(SCREEN_WIDTH > 500){
                    y += 30;
                    if([newProductDict[@"typeId"] isEqual: @"grouped"]){
                        UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 90, 25)];
                        [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preString setBackgroundColor:[UIColor clearColor]];
                        [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [preString setText:[globalObjectHome.languageBundle localizedStringForKey:@"starting" value:@"" table:nil]];
                        [productBlock addSubview:preString];
                        
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(95, y, 70, 25)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [price setText:newProductDict[@"groupedPrice"]];
                        [productBlock addSubview:price];
                    }
                    else
                        if([newProductDict[@"typeId"] isEqual: @"bundle"]){
                            UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 50, 25)];
                            [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [from setBackgroundColor:[UIColor clearColor]];
                            [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [from setText:[globalObjectHome.languageBundle localizedStringForKey:@"from" value:@"" table:nil]];
                            [productBlock addSubview:from];
                            
                            UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(55, y, 60, 25)];
                            [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                            [minPrice setBackgroundColor:[UIColor clearColor]];
                            [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [minPrice setText:newProductDict[@"formatedMinPrice"]];
                            [productBlock addSubview:minPrice];
                            
                            UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(125, y, 25, 25)];
                            [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [to setBackgroundColor:[UIColor clearColor]];
                            [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [to setText:[globalObjectHome.languageBundle localizedStringForKey:@"to" value:@"" table:nil]];
                            // [productBlock addSubview:to];
                            
                            UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(147, y, 60, 25)];
                            [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                            [maxPrice setBackgroundColor:[UIColor clearColor]];
                            [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [maxPrice setText:newProductDict[@"formatedMaxPrice"]];
                            float widthX = productBlock.frame.size.width - 125;
                            
                            if(widthX > 90){
                                [productBlock addSubview:to];
                                [productBlock addSubview:maxPrice];
                            }else{
                                UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(125, y, widthX, 25)];
                                [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                [dot setBackgroundColor:[UIColor clearColor]];
                                [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                [dot setText:[[globalObjectHome.languageBundle localizedStringForKey:@"to" value:@"" table:nil] stringByAppendingString:@" ..."]];
                                [productBlock addSubview:dot];
                            }
                            //[productBlock addSubview:maxPrice];
                        }
                        else{
                            if(![newProductDict[@"specialPrice"] isEqual:[NSNull null]] && [newProductDict[@"isInRange"] boolValue]){
                                UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 70, 25)];
                                [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                [regularprice setBackgroundColor:[UIColor clearColor]];
                                [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:newProductDict[@"formatedPrice"]];
                                [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                                [regularprice setAttributedText:attributeString];
                                [productBlock addSubview:regularprice];
                                
                                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 85, 25)];
                                [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                                [price setBackgroundColor:[UIColor clearColor]];
                                [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                [price setText:newProductDict[@"formatedSpecialPrice"]];
                                [productBlock addSubview:price];
                            }
                            else{
                                UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 60, 25)];
                                [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                                [price setBackgroundColor:[UIColor clearColor]];
                                [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                                [price setText:newProductDict[@"formatedPrice"]];
                                [productBlock addSubview:price];
                            }
                        }
                    if([newProductDict[@"hasTierPrice"] isEqual:@"true"]){
                        UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 90, 25)];
                        [preString setTextColor:[GlobalData colorWithHexString:@"cf5050"]];
                        [preString setBackgroundColor:[UIColor clearColor]];
                        [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [preString setText:[globalObjectHome.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil]];
                        [productBlock addSubview:preString];
                        
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(165, y, 60, 25)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [price setText:newProductDict[@"tierPrice"]];
                        
                        
                        float widthX = productBlock.frame.size.width - 80;
                        
                        if(widthX > 150){
                            [productBlock addSubview:preString];
                            [productBlock addSubview:price];
                        } else{
                            UILabel *dot = [[UILabel alloc] initWithFrame:CGRectMake(80, y, widthX, 25)];
                            [dot setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [dot setBackgroundColor:[UIColor clearColor]];
                            [dot setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                            [dot setText:[[globalObjectHome.languageBundle localizedStringForKey:@"asLowAs" value:@"" table:nil] stringByAppendingString:@" ..."]];
                            [productBlock addSubview:dot];
                        }
                        
                    }
                }
                float yProductOption;
                if(SCREEN_WIDTH > 500)
                    yProductOption = (SCREEN_WIDTH/2.5)+65;
                else
                    yProductOption = (SCREEN_WIDTH/2.5)+10;
                
                UIImageView *productOption = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)-25, yProductOption, 32, 32)];
                [productOption setImage:[UIImage imageNamed:@"ic_pro_option.png"]];
                productOption.userInteractionEnabled = YES;
                productOption.tag = i;
                [productBlock addSubview:productOption];
                UITapGestureRecognizer *showOptionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newProductShowOptions:)];
                showOptionGesture.numberOfTapsRequired = 1;
                [productOption addGestureRecognizer:showOptionGesture];
                
                y += 30;
                UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(10,y,120,24)];
                UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
                [ratingContainer addSubview:grayContainer];
                UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI1];
                UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI2];
                UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI3];
                UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI4];
                UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
                [grayContainer addSubview:gI5];
                
                double percent = 24 * [newProductDict[@"rating"] doubleValue];
                UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
                blueContainer.clipsToBounds = YES;
                [ratingContainer addSubview:blueContainer];
                UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI1];
                UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI2];
                UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI3];
                UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI4];
                UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
                [blueContainer addSubview:bI5];
                [productBlock addSubview:ratingContainer];
            }
            _nwProContainer.contentSize = CGSizeMake(x, _nwProContainer.frame.size.height);
            
            // Processing Featured Categories
            
            float gap = (SCREEN_WIDTH - ((SCREEN_WIDTH/3.3)*3))/4;
            if([mainCollection[@"featuredCategories"] count] > 3){
                _feturedCategoryContainerHeightConstraint.constant = (((SCREEN_WIDTH * 2) / 3.3)*2) + (gap*3);
                totalHeight += (((SCREEN_WIDTH * 2) / 3.3)*2) + (gap*3);
                totalHeight += 21;
            }
            else
                if([mainCollection[@"featuredCategories"] count] == 0){
                    _feturedCategoryContainerHeightConstraint.constant = 0;
                    [_featuredCategoryTitle removeFromSuperview];
                }
                else{
                    _feturedCategoryContainerHeightConstraint.constant = ((SCREEN_WIDTH * 2) / 3.3) + (gap*2);
                    totalHeight += ((SCREEN_WIDTH * 2) / 3.3) + (gap*2);
                    totalHeight += 21;
                }
            x = gap;
            float y = gap;
            NSArray *colorArray = @[@"8AA0CC",@"E2958D", @"FCC58D", @"E69C86", @"7FABA4", @"A292C1"];
            for(int i = 0; i < [mainCollection[@"featuredCategories"] count]; i++) {
                NSDictionary *featuredCategoryDict = [mainCollection[@"featuredCategories"] objectAtIndex:i];
                UIView *categoryBlock = [[UIView alloc]initWithFrame:CGRectMake(x, y, SCREEN_WIDTH/3.3, (SCREEN_WIDTH*2)/3.3)];
                categoryBlock.tag = i;
                x = x + ((SCREEN_WIDTH/3.3) + gap);
                [categoryBlock setBackgroundColor:[UIColor whiteColor]];
                categoryBlock.layer.cornerRadius = 2;
                categoryBlock.layer.shadowOffset = CGSizeMake(0, 0);
                categoryBlock.layer.shadowRadius = 3;
                categoryBlock.layer.shadowOpacity = 0.5;
                [_featuredCategoryContainer addSubview:categoryBlock];
                
                UIImageView *categoryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/3.3,(SCREEN_WIDTH*2)/3.3)];
                categoryImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
                UIImage *image = [imageCache objectForKey:featuredCategoryDict[@"url"]];
                if (image)
                    categoryImage.image = image;
                else{
                    [_queue addOperationWithBlock:^{
                        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:featuredCategoryDict[@"url"]]];
                        UIImage *image = [UIImage imageWithData: imageData];
                        if(image){
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                categoryImage.image = image;
                            }];
                            [imageCache setObject:image forKey:featuredCategoryDict[@"url"]];
                        }
                    }];
                }
                [categoryBlock addSubview:categoryImage];
                
                UILabel *categoryName = [[UILabel alloc] initWithFrame:CGRectMake(0, SCREEN_WIDTH/2, (SCREEN_WIDTH/3.3), 40)];
                [categoryName setTextColor : [GlobalData colorWithHexString : @"FFFFFF"]];
                [categoryName setBackgroundColor : [GlobalData colorWithHexString: colorArray[i]]];
                [categoryName setFont : [UIFont fontWithName : @"Trebuchet MS" size : 22.0f]];
                NSString *categotyNameTitle = [NSString stringWithFormat:@"  %@",featuredCategoryDict[@"categoryName"]];
                [categoryName setText : categotyNameTitle];
                [categoryBlock addSubview : categoryName];
                if(i == 2){
                    x = gap;
                    y = ((SCREEN_WIDTH*2)/3.3) + (gap*2);
                }
                UITapGestureRecognizer *openCategoryGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCategory:)];
                openCategoryGesture.numberOfTapsRequired = 1;
                [categoryBlock addGestureRecognizer:openCategoryGesture];
            }
            if(SCREEN_WIDTH > 500)
                _containerHeightConstraint.constant = totalHeight;
            else
                _containerHeightConstraint.constant = totalHeight - 50;

        }
    [_mainContainer layoutIfNeeded];
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
    //    dataFromApi =@"";
}

-(void)featuredAddtoWishlist:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId == nil){
        UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectHome.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectHome.languageBundle localizedStringForKey:@"logInWishList" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectHome.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
              handler:^(UIAlertAction * action){
                  [self performSegueWithIdentifier:@"addtoWishlistLoginSegue" sender:self];
              }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectHome.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [AC addAction:okBtn];
        [AC addAction:noBtn];
        [self.parentViewController presentViewController:AC animated:YES completion:nil];
    }
    else{
        NSDictionary *tempDictionary = [[NSDictionary alloc] init];
        tempDictionary = [mainCollection[@"featuredProducts"] objectAtIndex:recognizer.view.tag];
        whichApiDataToprocess = @"addToWishlist";
        productIdForApiCall = tempDictionary[@"entityId"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)featuredViewProduct:(UITapGestureRecognizer *)recognizer{
    productDictionary = [[NSDictionary alloc] init];
    productDictionary = [mainCollection[@"featuredProducts"] objectAtIndex:recognizer.view.tag];
    [self performSegueWithIdentifier:@"openProductFromHomeSegue" sender:self];
}

-(void)featuredAddtoCart:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary *tempDictionary = [[NSDictionary alloc] init];
    tempDictionary = [mainCollection[@"featuredProducts"] objectAtIndex:recognizer.view.tag];
    whichApiDataToprocess = @"addToCart";
    productIdForApiCall = tempDictionary[@"entityId"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)newAddtoWishlist:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId == nil){
        UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectHome.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectHome.languageBundle localizedStringForKey:@"logInWishList" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectHome.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          [self performSegueWithIdentifier:@"addtoWishlistLoginSegue" sender:self];
                                                      }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectHome.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [AC addAction:okBtn];
        [AC addAction:noBtn];
        [self.parentViewController presentViewController:AC animated:YES completion:nil];
    }
    else{
        NSDictionary *tempDictionary = [[NSDictionary alloc] init];
        tempDictionary = [mainCollection[@"newProducts"] objectAtIndex:recognizer.view.tag];
        whichApiDataToprocess = @"addToWishlist";
        productIdForApiCall = tempDictionary[@"entityId"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)newViewProduct:(UITapGestureRecognizer *)recognizer{
    productDictionary = [[NSDictionary alloc] init];
    productDictionary = [mainCollection[@"newProducts"] objectAtIndex:recognizer.view.tag];
    [self performSegueWithIdentifier:@"openProductFromHomeSegue" sender:self];
}

-(void)newAddtoCart:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary *tempDictionary = [[NSDictionary alloc] init];
    tempDictionary = [mainCollection[@"newProducts"] objectAtIndex:recognizer.view.tag];
    whichApiDataToprocess = @"addToCart";
    productIdForApiCall = tempDictionary[@"entityId"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)featuredProductShowOptions:(UITapGestureRecognizer *)recognizer{
    UIView *containerView = [recognizer.view.superview.subviews objectAtIndex:0];
    UIView *curtainView = [containerView.subviews objectAtIndex:1];
    if([featuredProductCurtainOpenSignal[recognizer.view.tag] isEqual: @"0"]) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn
             animations:^{
                 curtainView.frame = CGRectMake(0, 0, curtainView.frame.size.width, curtainView.frame.size.height);
             }
             completion:^(BOOL finished){
                 if(finished)
                     featuredProductCurtainOpenSignal[recognizer.view.tag] = @"1";
             }
         ];
    }
    else{
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn
             animations:^{
                 curtainView.frame = CGRectMake(0, curtainView.frame.size.height, curtainView.frame.size.width, curtainView.frame.size.height);
             }
             completion:^(BOOL finished){
                 if(finished)
                     featuredProductCurtainOpenSignal[recognizer.view.tag] = @"0";
             }
         ];
    }
}

-(void)newProductShowOptions:(UITapGestureRecognizer *)recognizer{
    UIView *containerView = [recognizer.view.superview.subviews objectAtIndex:0];
    UIView *curtainView = [containerView.subviews objectAtIndex:1];
    if([newProductCurtainOpenSignal[recognizer.view.tag] isEqual: @"0"]) {
        [UIView animateWithDuration:0.5 delay:0.1 options: UIViewAnimationOptionCurveEaseOut
             animations:^{
                 curtainView.frame = CGRectMake(0, 0, curtainView.frame.size.width, curtainView.frame.size.height);
             }
             completion:^(BOOL finished){
                 if(finished)
                     newProductCurtainOpenSignal[recognizer.view.tag] = @"1";
             }
         ];
    }
    else{
        [UIView animateWithDuration:0.5 delay:0.1 options: UIViewAnimationOptionCurveEaseOut
             animations:^{
                 curtainView.frame = CGRectMake(0, curtainView.frame.size.height, curtainView.frame.size.width, curtainView.frame.size.height);
             }
             completion:^(BOOL finished){
                 if(finished)
                     newProductCurtainOpenSignal[recognizer.view.tag] = @"0";
             }
         ];
    }
}

-(void)openCategory:(UITapGestureRecognizer *)recognizer{
    featuredCategoryDictionary = [[NSDictionary alloc] init];
    featuredCategoryDictionary = [mainCollection[@"featuredCategories"] objectAtIndex:recognizer.view.tag];
    [self performSegueWithIdentifier:@"featuredCategory" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
    if([segue.identifier isEqualToString:@"featuredCategory"]) {
        ProductCategory *destViewController = [[navigationController viewControllers] lastObject];
        destViewController.categoryId = featuredCategoryDictionary[@"categoryId"];
        destViewController.categoryName = featuredCategoryDictionary[@"categoryName"];
    }
    if([segue.identifier isEqualToString:@"openProductFromHomeSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = productDictionary[@"entityId"];
        destViewController.productName = productDictionary[@"name"];
        destViewController.productType = productDictionary[@"typeId"];
        destViewController.parentClass = @"home";
    }
    if([segue.identifier isEqualToString:@"openBannerProductFromHomeSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = bannerProductDictionary[@"productId"];
        destViewController.productName = bannerProductDictionary[@"productName"];
        destViewController.productType = bannerProductDictionary[@"productId"];
        destViewController.parentClass = @"home";
    }
}

- (IBAction)unwindToHome:(UIStoryboardSegue *)unwindSegue{}

-(void)handleBannerTap:(UITapGestureRecognizer *)recognizer {
    NSDictionary *bannerDict = [mainCollection[@"bannerImages"] objectAtIndex:recognizer.view.tag];
    if(![bannerDict[@"error"] boolValue]){
        if([bannerDict[@"bannerType"] isEqual:@"category"]){
            featuredCategoryDictionary = [[NSDictionary alloc] init];
            featuredCategoryDictionary = [mainCollection[@"bannerImages"] objectAtIndex:recognizer.view.tag];
            [self performSegueWithIdentifier:@"featuredCategory" sender:self];
        }
        if([bannerDict[@"bannerType"] isEqual:@"product"]){
            bannerProductDictionary = [[NSDictionary alloc] init];
            bannerProductDictionary = [mainCollection[@"bannerImages"] objectAtIndex:recognizer.view.tag];
            [self performSegueWithIdentifier:@"openBannerProductFromHomeSegue" sender:self];
        }
    }
}

-(void)scrollAutomatically:(NSTimer *)timer{
    float width = CGRectGetWidth(_imageScroller.frame);
    float height = CGRectGetHeight(_imageScroller.frame);
    float newPosition = _imageScroller.contentOffset.x+width;
    if(newPosition == (SCREEN_WIDTH-4) * imageCount)
        newPosition = 0.0f;
    CGRect toVisible = CGRectMake(newPosition, 0, width, height);
    [_imageScroller scrollRectToVisible:toVisible animated:YES];
}


@end