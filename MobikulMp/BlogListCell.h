//
//  BlogListCell.h
//  MobikulMp
//
//  Created by Apple on 12/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogListCell : UITableViewCell

{
    IBOutlet UIView *viewBlog;
    IBOutlet UILabel *lblBlogName;
    IBOutlet UILabel *lblNameAndDate;
    IBOutlet UILabel *lblContent;
    IBOutlet UITextView *txtContent;
    IBOutlet UIWebView *webviewText;
    IBOutlet UILabel *lblTag;
}

@property (nonatomic, retain) NSDictionary *dictBlog;

-(void)reloadBlogListCell;

@end
