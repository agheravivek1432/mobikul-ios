//
//  AdvancedSearch.h
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvancedSearch : UIViewController<NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSDate *selectedDate;
    NSInteger isAlertVisible;
    id collection;
    UIDatePicker *datePicker;
    NSInteger textFieldTag;
    UIView *dateView;
    float y;
    NSMutableArray *queryString;
    NSMutableDictionary *queryStringDict;
    UIWindow *currentWindow;
}

@property (weak, nonatomic) IBOutlet UIView *advancedSearchForm;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *advancedSearchFormHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@end
