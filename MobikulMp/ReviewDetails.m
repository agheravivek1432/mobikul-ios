//
//  ReviewDetails.m
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "ReviewDetails.h"
#import "GlobalData.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectReviewDetails;

@implementation ReviewDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectReviewDetails=[[GlobalData alloc] init];
    globalObjectReviewDetails.delegate=self;
    [globalObjectReviewDetails language];
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectReviewDetails.languageBundle localizedStringForKey:@"reviewDetails" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}
-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectReviewDetails callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectReviewDetails.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectReviewDetails.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectReviewDetails.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectReviewDetails.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectReviewDetails.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *reviewId = [NSString stringWithFormat:@"%d", (int)_reviewId];
    [post appendFormat:@"reviewId=%@&", reviewId];
    NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    [post appendFormat:@"width=%@&", screenWidth];
    [globalObjectReviewDetails callHTTPPostMethod:post api:@"mobikulhttp/customer/getreviewDetail" signal:@"HttpPostMetod"];
    globalObjectReviewDetails.delegate = self;
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    float y = 5;
    float x = 5;
    UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2)-((SCREEN_WIDTH/2)/2), y, SCREEN_WIDTH/2, SCREEN_WIDTH/2)];
    productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
    UIImage *image = [imageCache objectForKey:collection[@"image"]];
    if (image)
        productImage.image = image;
    else{
        [_queue addOperationWithBlock:^{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:collection[@"image"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            if(image){
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    productImage.image = image;
                }];
                [imageCache setObject:image forKey:collection[@"image"]];
            }
        }];
    }
    [_mainContainer addSubview:productImage];
    
    y += (SCREEN_WIDTH/2)+5;
    UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_WIDTH-20, 25)];
    [productName setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
    [productName setBackgroundColor:[UIColor clearColor]];
    NSString *uppercaseName = [collection[@"name"] uppercaseString];
    [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [productName setText:uppercaseName];
    [_mainContainer addSubview:productName];
    
    y += 30;
    UILabel *reviewDate = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_WIDTH-20, 25)];
    [reviewDate setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [reviewDate setBackgroundColor:[UIColor clearColor]];
    NSString *uppercaseDate = [collection[@"reviewDate"] uppercaseString];
    [reviewDate setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [reviewDate setText:uppercaseDate];
    [_mainContainer addSubview:reviewDate];
    
    y += 30;
    UILabel *reviewDetails = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_WIDTH-20, 25)];
    [reviewDetails setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [reviewDetails setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [reviewDetails setText:collection[@"reviewDetail"]];
    [reviewDetails setBackgroundColor:[UIColor clearColor]];
    reviewDetails.lineBreakMode = NSLineBreakByWordWrapping;
    reviewDetails.numberOfLines = 0;
    reviewDetails.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    reviewDetails.textAlignment = NSTextAlignmentLeft;
    reviewDetails.preferredMaxLayoutWidth = SCREEN_WIDTH-20;
    [reviewDetails sizeToFit];
    [_mainContainer addSubview:reviewDetails];
    y += reviewDetails.frame.size.height;
    
    y += 5;
    UILabel *yourRating = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_WIDTH-20, 25)];
    [yourRating setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [yourRating setBackgroundColor:[UIColor clearColor]];
    NSString *uppercaseYourRating = [@"your rating : " uppercaseString];
    [yourRating setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:21.0f]];
    [yourRating setText:uppercaseYourRating];
    [_mainContainer addSubview:yourRating];
    
    y += 30;
    for(int i=0; i<[collection[@"ratingData"] count]; i++){
        NSDictionary *dict = [collection[@"ratingData"] objectAtIndex:i];
        UILabel *ratingCode = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 110, 25)];
        [ratingCode setTextColor:[UIColor blackColor]];
        [ratingCode setBackgroundColor:[UIColor clearColor]];
        [ratingCode setFont:[UIFont fontWithName: @"Trebuchet MS" size: 20.0f]];
        [ratingCode setText:dict[@"ratingCode"]];
        [_mainContainer addSubview:ratingCode];
        
        UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(110,y,120,24)];
        UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
        [ratingContainer addSubview:grayContainer];
        UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI1];
        UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI2];
        UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI3];
        UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI4];
        UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI5];
        
        double percent = 24 * ([dict[@"ratingValue"] doubleValue] / 20);
        UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
        blueContainer.clipsToBounds = YES;
        [ratingContainer addSubview:blueContainer];
        UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI1];
        UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI2];
        UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI3];
        UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI4];
        UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI5];
        [_mainContainer addSubview:ratingContainer];
        y += 30;
    }
    
    y += 5;
    UILabel *customerRating = [[UILabel alloc] initWithFrame:CGRectMake(10, y, SCREEN_WIDTH-20, 25)];
    [customerRating setTextColor:[GlobalData colorWithHexString:@"555555"]];
    [customerRating setBackgroundColor:[UIColor clearColor]];
    NSString *uppercaseCustomerRating = [@"average customer rating : " uppercaseString];
    [customerRating setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:21.0f]];
    [customerRating setText:uppercaseCustomerRating];
    [_mainContainer addSubview:customerRating];
    
    y += 30;
    UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(10,y,120,24)];
    UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
    [ratingContainer addSubview:grayContainer];
    UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
    gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI1];
    UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
    gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI2];
    UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
    gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI3];
    UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
    gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI4];
    UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
    gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
    [grayContainer addSubview:gI5];
    
    double percent = 24 * [collection[@"rating"] doubleValue];
    UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
    blueContainer.clipsToBounds = YES;
    [ratingContainer addSubview:blueContainer];
    UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
    bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI1];
    UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
    bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI2];
    UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
    bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI3];
    UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
    bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI4];
    UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
    bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
    [blueContainer addSubview:bI5];
    [_mainContainer addSubview:ratingContainer];
    _mainContainerHeightConstraint.constant = y+40;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

@end