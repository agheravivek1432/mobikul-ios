//
//  Notification.h
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Notification : UIViewController <NSXMLParserDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    id collection;
    NSInteger isAlertVisible;
    NSMutableArray *notificationList;
    UIWindow *currentWindow;
    NSString *categoryId,*categoryName,*productId,*productName,*productType;
}

@property (weak, nonatomic) IBOutlet UITableView *notificationTable;

@end