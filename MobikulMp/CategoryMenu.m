//
//  CategoryMenu.m
//  Mobikul
//
//  Created by Ratnesh on 12/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "CategoryMenu.h"
#import "GlobalData.h"
#import "ProductCategory.h"
#import "SubCategory.h"
#import "Home.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectCategoryMenu;

@implementation CategoryMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    pictureUpload =@"true";
    preferences = [NSUserDefaults standardUserDefaults];
    _language = [NSUserDefaults standardUserDefaults];
    NSString *languageCodeString = [[NSUserDefaults standardUserDefaults] stringForKey:@"language"];
    NSString *path = [[NSBundle mainBundle] pathForResource:languageCodeString ofType:@"lproj"];
    if (path == nil){
        path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    }
    _languageBundle = [NSBundle bundleWithPath:path];
    globalObjectCategoryMenu=[[GlobalData alloc] init];
    [globalObjectCategoryMenu language];
    _profilePicture.layer.cornerRadius = 35;
    _profilePicture.clipsToBounds = YES;
    [_profilePicture.layer setBorderColor: [[GlobalData colorWithHexString:@"444444"] CGColor]];
    [_profilePicture.layer setBorderWidth: 3.0];
    pictureCache = [[NSCache alloc] init];
    imageCache = [[NSCache alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    if([uploadingData isEqualToString:@"Uploading"]){
        [self uploadingImage];
    }
    
    GlobalData *obj = [GlobalData getInstance];
    categoryData = obj.categoryData;
    storeData = obj.storeData;
    extraData = obj.extraData;
    categoryId = obj.categoryId;
    imageCache = obj.gloablCatalogIconCache;
    categoryImagesId = obj.categoryImagesId;
    categoryImages = obj.categoryImages;
    profileCache = obj.profileCache;
    
    languageCode = [NSUserDefaults standardUserDefaults];
    allData = [[NSMutableArray alloc] initWithObjects:categoryData, storeData, extraData, nil];
    headerTitle = [[NSMutableArray alloc] initWithObjects:[_languageBundle localizedStringForKey:@"category" value:@"" table:nil], [_languageBundle localizedStringForKey:@"stores" value:@"" table:nil], [_languageBundle localizedStringForKey:@"extras" value:@"" table:nil], nil];
    if([obj.catalogMenuFlag isEqualToString:@"1"]){
        [_categoryMenuTable reloadData];
        obj.catalogMenuFlag = @"0";
    }
    customerId = [preferences objectForKey:@"customerId"];
    if([pictureUpload isEqualToString:@"true"])  if([pictureUpload isEqualToString:@"true"]){
        if([preferences objectForKey:@"customerId"] == nil){
            _profileBanner.userInteractionEnabled = NO;
            _profilePicture.userInteractionEnabled = NO;
            _profileBanner.image = [UIImage imageNamed:@"ic_profile_banner.png"];
            _profilePicture.image = [UIImage imageNamed:@"ic_profile_pic.png"];
        }
        else{
            
            if([preferences objectForKey:@"profilePicture"] != nil && ![[preferences objectForKey:@"profilePicture"] isEqualToString:@""]){
                NSString *profilePictureUrl = [preferences objectForKey:@"profilePicture"];
                UIImage *image = [pictureCache objectForKey:profilePictureUrl];
                if(image){
                    _profilePicture.image = image;}
            else if([obj.profileCache objectForKey:profilePictureUrl]){
                _profilePicture.image = [obj.profileCache objectForKey:profilePictureUrl];
            }
            }
        
            if([preferences objectForKey:@"profileBanner"] != nil && ![[preferences objectForKey:@"profileBanner"] isEqualToString:@""]){
                NSString *profileBannerUrl = [preferences objectForKey:@"profileBanner"];
                UIImage *image = [pictureCache objectForKey:profileBannerUrl];
                if(image)
                    _profileBanner.image = image;
                else if([obj.profileCache objectForKey:profileBannerUrl]){
                    _profileBanner.image = [obj.profileCache objectForKey:profileBannerUrl];
                }
            }
            
            _profileBanner.userInteractionEnabled = YES;
            _profilePicture.userInteractionEnabled = YES;
        }
        
    }
    UITapGestureRecognizer *profileBannerGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGestureProfileBanner:)];
    profileBannerGesture.numberOfTapsRequired = 1;
    [profileBannerGesture setDelegate:self];
    [_profileBanner addGestureRecognizer:profileBannerGesture];
    
    UITapGestureRecognizer *profilePicturerGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGestureProfilePicture:)];
    profilePicturerGesture.numberOfTapsRequired = 1;
    [profilePicturerGesture setDelegate:self];
    [_profilePicture addGestureRecognizer:profilePicturerGesture];
}

- (void) tapGestureProfileBanner: (id)sender{
    //handle Tap...
    imageForCategoryMenu = @"profileBanner";
    AC = [UIAlertController alertControllerWithTitle:@"Upload Profile Banner" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* clickBtn = [UIAlertAction actionWithTitle:@"Take Picture" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* cameraRollBtn = [UIAlertAction actionWithTitle:@"Upload Picture From Camera Roll" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary ;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:clickBtn];
    [AC addAction:cameraRollBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

- (void) tapGestureProfilePicture: (id)sender{
    imageForCategoryMenu = @"profilePicture";
    AC = [UIAlertController alertControllerWithTitle:@"UPLOAD Profile Picture" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* clickBtn = [UIAlertAction actionWithTitle:@"Take picture" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* cameraRollBtn = [UIAlertAction actionWithTitle:@"Upload Picture From Camera Roll" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary ;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:clickBtn];
    [AC addAction:cameraRollBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    pictureUpload =@"false";
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    imageData = UIImageJPEGRepresentation(chosenImage, 1.0);
    [picker dismissViewControllerAnimated:YES completion:NULL];
    uploadingData = @"Uploading";
}

-(void) uploadingImage{
    
    uploadingData =@"";
    customerId = [preferences objectForKey:@"customerId"];
    if(isAlertVisible == 0){
        [self.view setUserInteractionEnabled:NO];
        alert = [UIAlertController alertControllerWithTitle: [_languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]  message: nil preferredStyle: UIAlertControllerStyleAlert];
        UIViewController *tempVC = [[UIViewController alloc] init];
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [tempVC.view addSubview:spinner];
        [tempVC.view addConstraint:[NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:tempVC.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [tempVC.view addConstraint:[NSLayoutConstraint constraintWithItem:tempVC.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:30]];
        [alert setValue:tempVC forKey:@"contentViewController"];
        [self.parentViewController presentViewController:alert animated:YES completion:nil];
        isAlertVisible = 1;
    }
    
    NSString *imageSendUrl;
    if([imageForCategoryMenu isEqualToString:@"profileBanner"]){
        
        imageSendUrl = [NSString stringWithFormat:@"%@/mobikul/index/uploadBannerPic",BASE_DOMAIN];

    }
    else{
        imageSendUrl = [NSString stringWithFormat:@"%@/mobikul/index/uploadProfilePic",BASE_DOMAIN];
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:imageSendUrl]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"customerId"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", customerId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"width"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", screenWidth] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"imageFormKey"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode] == 200) {
            pictureUpload = @"true";
            if([imageForCategoryMenu isEqualToString:@"profileBanner"]){
                if(isAlertVisible == 1){
                    isAlertVisible = 0;
                    [self.view setUserInteractionEnabled:YES];
                    [alert dismissViewControllerAnimated:YES completion:^{
                        NSString *url = [NSString stringWithFormat:@"%@/media/customerpicture/%@/%fx%d/%@-banner.jpg", IMAGE_URL, customerId, SCREEN_WIDTH, (int)SCREEN_WIDTH/2, customerId];
                        [preferences setObject:url forKey:@"profileBanner"];
                        [preferences synchronize];
                        
                        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                        [pictureCache removeObjectForKey:url];
                        [pictureCache setObject:image forKey:url];
                        _profileBanner.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                    }];
                }
            }
            else{
                pictureUpload =@"true";
                if(isAlertVisible == 1){
                    isAlertVisible = 0;
                    [self.view setUserInteractionEnabled:YES];
                    [alert dismissViewControllerAnimated:YES completion:^{
                        NSString *url = [NSString stringWithFormat:@"%@/media/customerpicture/%@/100x100/%@-profile.jpg",IMAGE_URL, customerId, customerId];
                        [preferences setObject:url forKey:@"profilePicture"];
                        [preferences synchronize];
                        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                        [pictureCache removeObjectForKey:url];
                        [pictureCache setObject:image forKey:url];
                        _profilePicture.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                        isAlertVisible = 1;
                    }];
                }
            }
        }
        else{
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            if(isAlertVisible == 1){
                isAlertVisible = 0;
                [self.view setUserInteractionEnabled:YES];
                [alert dismissViewControllerAnimated:YES completion:^{
                    failedController = [UIAlertController alertControllerWithTitle:@"Retry Upload" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* clickBtn = [UIAlertAction actionWithTitle:@"Take Picture" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action){
                        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.allowsEditing = YES;
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        [self presentViewController:picker animated:YES completion:NULL];
                    }];
                    UIAlertAction* cameraRollBtn = [UIAlertAction actionWithTitle:@"Upload Picture From Camera Roll" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action){
                        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.allowsEditing = YES;
                        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary ;
                        [self presentViewController:picker animated:YES completion:NULL];
                    }];
                    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
                    [failedController addAction:clickBtn];
                    [failedController addAction:cameraRollBtn];
                    [failedController addAction:noBtn];
                    [self.parentViewController presentViewController:failedController animated:YES completion:nil];
                }];
            }
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [allData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[allData objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *sectionContents = [allData objectAtIndex:[indexPath section]];
    NSString *contentForThisRow = [sectionContents objectAtIndex:[indexPath row]];
    if(indexPath.section == 1){
        NSArray* firstSplittedTest = [contentForThisRow componentsSeparatedByString: @"-_-"];
        contentForThisRow = firstSplittedTest[0];
        NSArray* secondSplittedTest = [firstSplittedTest[0] componentsSeparatedByString: @"      "];
        if([secondSplittedTest count] == 1)
            cell.userInteractionEnabled = NO;
        else
            if([secondSplittedTest count] > 1){
                cell.tag = [firstSplittedTest[1] integerValue];
                NSString *tappedTag = [NSString stringWithFormat:@"%d", (int)[firstSplittedTest[1] integerValue]];
                
                NSString *storeId = [preferences objectForKey:@"storeId"];
                if([tappedTag isEqualToString:storeId]){
                    UIView *backgroundView = [[UIView alloc] init];
                    backgroundView.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
                    cell.textLabel.textColor = [GlobalData colorWithHexString:@"FFFFFF"];
                    cell.backgroundView = backgroundView;
                }
            }
    }
    if(indexPath.section == 0){
        cell.imageView.image = [UIImage imageNamed:@"ic_placeholder.png"];
        for(int i = 0; i < [categoryImagesId count]; i++){
            if([[categoryImagesId objectAtIndex:i] isEqualToString:[categoryId objectAtIndex:[indexPath row]]]){
                
                UIImage *imageIcon = [imageCache objectForKey:[categoryImages objectAtIndex:[indexPath row]]];
                if(imageIcon){
                    cell.imageView.image = imageIcon;
                }
                else{
                    GlobalData *obj = [GlobalData getInstance];
                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[categoryImages objectAtIndex:[indexPath row]]]]];
                    [obj.gloablCatalogIconCache setObject:image forKey:[categoryImages objectAtIndex:[indexPath row]]];
                    //cell.imageView.image = image;
                }
                break;
            }
            
        }
    }
    else
        cell.imageView.image = [UIImage imageNamed:@""];
    cell.textLabel.text = contentForThisRow;
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [headerTitle objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
    tableViewHeaderFooterView.textLabel.textColor = [GlobalData colorWithHexString:@"FFFFFF"];
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
        tableViewHeaderFooterView.textLabel.textAlignment =  NSTextAlignmentRight;
    tableViewHeaderFooterView.contentView.backgroundColor = [GlobalData colorWithHexString:@"00CBDF"];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    GlobalData *obj = [GlobalData getInstance];
    if([indexPath section] == 0){
        categoryDict = [obj.wholeCategoryData objectAtIndex:[indexPath row]];
        if([categoryDict[@"children"] count] > 0)
            [self performSegueWithIdentifier:@"subCategorySegue" sender:self];
        else
            [self performSegueWithIdentifier:@"categorySegue" sender:self];
    }
    else
        if([indexPath section] == 1){
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            for(int i=0; i<[obj.wholeStoreData count]; i++){
                NSDictionary *storeDict = [obj.wholeStoreData objectAtIndex:i];
                
                for(int j=0; j<[storeDict[@"stores"] count]; j++){
                    NSDictionary *viewDict = [storeDict[@"stores"] objectAtIndex:j];
                    int tag = (int)cell.tag;
                    NSString *tappedTag = [NSString stringWithFormat:@"%d", tag];
                    if([tappedTag isEqualToString:viewDict[@"id"]]){
                        if([viewDict[@"code"] isEqualToString:@"zh"]){
                            [_language setObject:@"zh-Hans" forKey:@"language"];
                        }
                        else{
                            [_language setObject:viewDict[@"code"] forKey:@"language"];
                        }
                        NSUserDefaults *flag =[NSUserDefaults standardUserDefaults];
                        if([viewDict[@"code"] isEqualToString:@"ar"]){
                            [flag setObject:@"arabic" forKey:@"loadAppDlegate"];
                            [flag synchronize];
                        }
                        [_language synchronize];
                        [preferences setObject:viewDict[@"id"] forKey:@"storeId"];
                        [preferences synchronize];
                        GlobalData *obj = [GlobalData getInstance];
                        obj.catalogMenuFlag = @"1";
                        [self performSegueWithIdentifier:@"reloadHomeSegue" sender:self];
                    }
                }
            }
        }
        else{
            if([indexPath row] == 0)
                [self performSegueWithIdentifier:@"searchTermSegue" sender:self];
            else
                if([indexPath row] == 1)
                    [self performSegueWithIdentifier:@"advancedSearchSegue" sender:self];
                else if ([indexPath row] == 2)
                    [self performSegueWithIdentifier:@"marketPlaceSegue" sender:self];
        }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"categorySegue"]) {
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        ProductCategory *destViewController = [[navigationController viewControllers] lastObject];
        destViewController.categoryId = categoryDict[@"category_id"];
        destViewController.categoryName = categoryDict[@"name"];
    }
    else
        if ([segue.identifier isEqualToString:@"subCategorySegue"]) {
            UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
            SubCategory *destViewController = [[navigationController viewControllers] lastObject];
            destViewController.subCategoryData = categoryDict;
            destViewController.categoryName = categoryDict[@"name"];
        }
    
}

@end
