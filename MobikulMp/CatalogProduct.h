//
//  Product.h
//  Mobikul
//
//  Created by Ratnesh on 11/04/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogProduct : UIViewController <NSXMLParserDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSUserDefaults *preferences;
    NSString *pickerValue;
    NSString *sessionId, *message, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id mainCollection, addToCartCollection, addToWishListCollection,collection,contactSellerCollection;
    NSCache *imageCache;
    UIView *addToCartBlock, *customOptionContainer, *groupedDataContainer, *downloadableContainer, *bundleContainer, *configurableContainer;
    NSString *whichApiDataToprocess, *addToCartMoved, *confi;
    NSMutableArray  *firstConfigIdData, *optionValues, *configStoreValue, *configtitleArray, *configIdData, *configDataForRow;
    NSMutableDictionary *selectedCustomOption, *selectedGroupedProduct, *selectedDownloadableProduct, *selectedBundleProduct, *selectedBundleProductQuantity, *configPickerDictionary, *configPickerInfo, *configRowValueDict, *selectedConfigProduct;
    int  bundlekeyCount, flag, configPickerCount, bundleValueRow;
    NSInteger configRowValue;
    NSUserDefaults *languageCode;
    UIWindow *currentWindow;
    NSMutableDictionary *contactUsData;
    NSInteger isValid;
}

@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic) NSString *productId;
@property (nonatomic) NSString *parentClass;
@property (nonatomic) NSString *productName;
@property (nonatomic) NSString *productType;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMargin;
@property (strong, nonatomic) IBOutlet UIView *mainBaseView;

@end