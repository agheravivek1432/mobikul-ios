//
//  AccountInformation.h
//  Mobikul
//
//  Created by Ratnesh on 03/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountInformation : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *firstName, *lastName, *emailAddress, *currentPassword, *password, *confirmPassword, *wantChangePassword, *whichApiDataToProcess;
    NSInteger isAlertVisible;
    UIAlertController *alert;
    id collection;
    UIWindow *currentWindow;
}
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *emailAddress;
@property (weak, nonatomic) IBOutlet UILabel *changePassword;
@property (weak, nonatomic) IBOutlet UIButton *save;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressField;
@property (weak, nonatomic) IBOutlet UITextField *currentPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordField;
@property (weak, nonatomic) IBOutlet UISwitch *changePasswordRef;
- (IBAction)changePasswordSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *changePasswordLayout;
- (IBAction)saveInformation:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *changePasswordLayoutHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainContainerHeightConstraint;

@end
