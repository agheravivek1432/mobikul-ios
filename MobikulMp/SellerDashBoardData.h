//
//  SellerDashBoardData.h
//  MobikulMp
//
//  Created by kunal prasad on 23/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBLineChartView.h"

@interface SellerDashBoardData : UIViewController <JBLineChartViewDelegate ,JBLineChartViewDataSource , JBChartViewDelegate,JBChartViewDataSource>
{
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    NSString *dataFromApi;
    UIAlertController *alert;
    id mainCollection,collection;
    float mainContainerY;
    JBLineChartView *lineChartView;
    UITapGestureRecognizer *sampleTouch;
    UIWindow *currentWindow;
    UILabel *tooltipData,*tooltipMonth;
    NSMutableArray *graphdata;
    UITapGestureRecognizer *sellerImage1Touch,*sellerImage2Touch,*sellerImage3Touch,*sellerImage4Touch,*sellorTitleTap ;
    
}
-(NSString*) monthName:(NSInteger) value;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;

@end
