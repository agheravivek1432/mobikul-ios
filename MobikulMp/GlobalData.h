
#import <UIKit/UIKit.h>


//////
//

//#define API_DOMAIN @"http://192.168.1.114/Mo3/index.php/api/v2_soap/"    // dummy api domain
//#define BASE_DOMAIN @"http://192.168.1.114/Mo3/index.php/"               // dummy base domain
#define API_USER_NAME @"akash"   // write username here like @"abc"
#define API_KEY @"admin123"         // write password here  like @"mmmm"admin akash123
//#define IMAGE_URL @"http://192.168.1.114/Mo3"
#define IMAGE_URL @"http://192.168.1.114/Mo3"
#define BASE_DOMAIN @"http://farmrichonline.com/demostore/index.php/"
#define BASE_DOMAIN_Blog @"http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/"


#define GLOBAL_COLOR  @"00CBDF"
//#define GLOBAL_COLOR  @"CCD34E"
#define BUTTON_COLOR  @"65BC6B"
#define LINK_COLOR    @"CCD34E"
#define STATUS_BAR    @""
#define DEFAULT_WEBSITE_ID @"1"
@protocol SampleProtocolDelegate <NSObject>
@required
- (void) finalDataprocessCompleted;
-(void)finalCallingApiCompleted;
-(void)connectionErorWindow;
-(void)loginRequestCall;
- (void) finalDataForRegisterDeviceCompleted:(id)collectionData;
-(void)finalHttpDataprocessCompleted:(id)collectionData;
@end

@interface GlobalData : NSObject <NSXMLParserDelegate,NSURLConnectionDelegate>  {
    id <SampleProtocolDelegate> _delegate;
   NSString *_finalResulttoSend;
   NSString *_whichApitoCallForCustomerLogin;
    NSString *employEmailId;
    NSString *signalName;
    id collection;
    id  collectionRegisterData;
    
}
-(void)startFinalDataProcess;
-(void)startCallingApi;
-(void) dragEvent;
-(void) language;


@property (nonatomic,strong) id delegate;


@property(nonatomic, strong) NSCache *testCache;

@property (strong, nonatomic,readwrite) NSString *catalogMenuFlag;
@property(nonatomic, strong) NSCache *gloablCatalogIconCache;
@property(nonatomic, strong) NSCache *profileCache;
@property(nonatomic, strong) NSMutableArray *categoryData;
@property(nonatomic, strong) NSMutableArray *wholeCategoryData;
@property(nonatomic, strong) NSMutableArray *storeData;
@property(nonatomic, strong) NSMutableArray *wholeStoreData;
@property(nonatomic, strong) NSMutableArray *extraData;
@property(nonatomic, strong) NSMutableArray *menuListData;
@property(nonatomic, strong) NSMutableArray *completeListMenuData;
@property(nonatomic, strong) NSMutableArray *listMenuTitleData;
@property(nonatomic, strong) UIView *mainHomeView;
@property(nonatomic, strong) NSMutableArray *marketPlaceData;
@property (strong, nonatomic,readwrite) NSString *finalResulttoSend;
@property (strong, nonatomic,readwrite) NSString *whichApitoCallForCustomerLogin;
@property (strong, nonatomic,readwrite) NSString *employEmailId;
@property (weak, nonatomic) NSString *isDragEvent;
@property (weak, nonatomic) NSBundle *languageBundle;
@property(nonatomic, strong) NSMutableArray *categoryId;
@property(nonatomic, strong) NSMutableArray *categoryImagesId;
@property(nonatomic, strong) NSMutableArray *categoryImages;
@property(nonatomic, strong) NSCache *imageCache1;

+(GlobalData*)getInstance;
+(NSString *)createEnvelope:(NSString *)method forNamespace:(NSString *)ns forParameters:(NSString *)params  currentView:(UIViewController *)views  isAlertVisiblef:(NSInteger)isAlertVisible alertf:(UIAlertController *)alert1;
+(UIColor*)colorWithHexString:(NSString*)hex;
-(void)sendingApiData: (NSString *)envelopValue;
-(void)sendingLoginData:(NSString *)envelopValue;
-(void)sendingErorReport;
-(void)sendingLoginRequest;
- (void)forgotpasswordApi;
-(void)callHTTPPostMethod:(NSString *)params api:(NSString*)apiName signal:(NSString*)signalName;
-(void)callHTTPPostMethodForBlog:(NSString *)params api:(NSString*)apiName signal:(NSString*)signalNameFunction;
+(void)alertController:(UIWindow *)alertnew msg:(NSString *)textMessage;
+(void)loadingController:(UIWindow *)alertnew;
@end