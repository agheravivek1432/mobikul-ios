//
//  CreateAccountVC.m
//  FirstForm
//
//  Created by Ratnesh on 25/11/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import "CreateAccount.h"
#import "GlobalData.h"

@interface CreateAccount ()
- (IBAction)createAccountBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *emailAddress;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UISwitch *newsLetterSubscribe;
@property (weak, nonatomic) IBOutlet UIButton *createAccountBtn;
@end

GlobalData *globalObjectCreateAccount;

@implementation CreateAccount

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectCreateAccount=[[GlobalData alloc] init];
    globalObjectCreateAccount.delegate=self;
    [globalObjectCreateAccount language];
    
    
    sizeModified = @"0";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    //    self.navigationController.title = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"createAccount" value:@"" table:nil];
    //    firstName.text = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"firstName" value:@"" table:nil];
    //    lastName.text = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"lastName" value:@"" table:nil];
    //    signUpNewsletter.text = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"signUpNewsletter" value:@"" table:nil];
    //    password.text = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"password" value:@"" table:nil];
    //    confirmPassword.text = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"ConfirmPassword" value:@"" table:nil];
    //    emailAddress.text = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"EmailAddress" value:@"" table:nil];
    //    [registerAccount setTitle: [globalObjectCreateAccount.languageBundle localizedStringForKey:@"register" value:@"" table:nil] forState: UIControlStateNormal];
    [scrollView setBackgroundColor:[UIColor whiteColor]];
    [self registerForKeyboardNotifications];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    currentWindow = [UIApplication sharedApplication].keyWindow;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

- (IBAction)createAccountBtn:(id)sender {
    @try{
        int isValid = 1;
        NSString *errorMessage;
        firstNameVal = self.firstName.text;
        lastNameVal = self.lastName.text;
        emailAddressVal = self.emailAddress.text;
        passwordVal = self.password.text;
        confirmPasswordVal = self.confirmPassword.text;
        if(self.newsLetterSubscribe.isOn)
            newsLetterSubscribeVal = @"1";
        else
            newsLetterSubscribeVal = @"0";
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if([firstNameVal isEqual: @""]){
            isValid = 0;
            errorMessage =[globalObjectCreateAccount.languageBundle localizedStringForKey:@"firstNameRequire" value:@"" table:nil];
        }
        else
            if([lastNameVal isEqual: @""]){
                isValid = 0;
                errorMessage = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"lastNameRequire" value:@"" table:nil];
            }
            else
                if([emailAddressVal isEqual: @""]){
                    isValid = 0;
                    errorMessage = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"emailAddress" value:@"" table:nil];
                }
                else
                    if(![emailTest evaluateWithObject:emailAddressVal]){
                        isValid = 0;
                        errorMessage = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"validEmail" value:@"" table:nil];
                    }
                    else
                        if([passwordVal isEqual: @""]){
                            errorMessage = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"passwordRequired" value:@"" table:nil];
                            isValid = 0;
                        }
                        else
                            if([confirmPasswordVal isEqual: @""]){
                                isValid = 0;
                                errorMessage = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"confirmPasswordRequired" value:@"" table:nil];
                            }
                            else
                                if(![passwordVal isEqual: confirmPasswordVal]){
                                    isValid = 0;
                                    errorMessage = [globalObjectCreateAccount.languageBundle localizedStringForKey:@"ConfirmPasswordMatch" value:@"" table:nil];
                                }
        if(isValid == 0){
            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectCreateAccount.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCreateAccount.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
            [errorAlert addAction:noBtn];
            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
        }
        else{
            preferences = [NSUserDefaults standardUserDefaults];
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
    @catch (NSException *e) {
        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
    }
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectCreateAccount callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}


-(void)showConnectionErrorDialogue{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:@"Warning" message:@"ERROR with theConenction" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectCreateAccount.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    if(websiteId == nil){
        [post appendFormat:@"websiteId=%@&", DEFAULT_WEBSITE_ID];
        [preferences setObject:DEFAULT_WEBSITE_ID forKey:@"websiteId"];
        [preferences synchronize];
    }
    else
        [post appendFormat:@"websiteId=%@&", websiteId];
    [post appendFormat:@"firstName=%@&", firstNameVal];
    [post appendFormat:@"lastName=%@&", lastNameVal];
    [post appendFormat:@"emailAddr=%@&", emailAddressVal];
    [post appendFormat:@"password=%@", passwordVal];
    [globalObjectCreateAccount callHTTPPostMethod:post api:@"mobikulhttp/customer/createPost" signal:@"HttpPostMetod"];
    globalObjectCreateAccount.delegate = self;
}

//-(void)callingApi{
//    @try {
//        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//        NSString *storeId = [preferences objectForKey:@"storeId"];
//        [dictionary setObject:storeId forKey:@"storeId"];
//        NSString *websiteId = [preferences objectForKey:@"websiteId"];
//        [dictionary setObject:websiteId forKey:@"websiteId"];
//        [dictionary setObject:firstNameVal forKey:@"firstName"];
//        [dictionary setObject:lastNameVal forKey:@"lastName"];
//        [dictionary setObject:emailAddressVal forKey:@"emailAddr"];
//        [dictionary setObject:passwordVal forKey:@"password"];
//        [dictionary setObject:newsLetterSubscribeVal forKey:@"isChecked"];
//        NSString *prefSessionId = [preferences objectForKey:@"sessionId"];
//        NSString *quoteId = [preferences objectForKey:@"quoteId"];
//        if(quoteId != nil)
//            [dictionary setObject:quoteId forKey:@"quoteId"];
//        NSString *apiName = @"mobikulCustomerCreatePost";
//        NSString *nameSpace = @"urn:Magento";
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
//        NSString *jsonString;
//        if(!jsonData){
//            //NSLog(@"Got an error: %@", error);
//        }
//        else
//            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//        NSString *parameters = [NSString stringWithFormat:@"<sessionId xsi:type=\"xsd:string\">%@</sessionId><attributes xsi:type=\"xsd:string\">%@</attributes>", prefSessionId, jsonString];
//        alert = [UIAlertController alertControllerWithTitle: @"Please Wait..." message: nil preferredStyle: UIAlertControllerStyleAlert];
//        NSString *envelope = [GlobalData createEnvelope:apiName  forNamespace:nameSpace forParameters:parameters currentView:self isAlertVisiblef:0 alertf:alert];
//        isAlertVisible = 1;
//        [globalObjectCreateAccount sendingApiData:envelope];
//        globalObjectCreateAccount.delegate = self;
//    }
//    @catch (NSException *e) {
//        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
//    }
//}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    if([collection[@"status"] isEqual: @"true"]){
        [preferences setObject:collection[@"customerName"] forKey:@"customerName"];
        [preferences setObject:collection[@"customerId"] forKey:@"customerId"];
        [preferences setObject:collection[@"customerEmail"] forKey:@"customerEmail"];
        [preferences synchronize];
        GlobalData *obj = [GlobalData getInstance];
        [obj.menuListData replaceObjectAtIndex:0 withObject:@"Log Out"];
        [self performSegueWithIdentifier:@"customerCreateAccountDashboardSeague" sender:self];
    }
    else{
        UIAlertController * loginErrorNotification = [UIAlertController alertControllerWithTitle:@"Error Creating Account" message:collection[@"customerMsg"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [loginErrorNotification addAction:noBtn];
        [self.parentViewController presentViewController:loginErrorNotification animated:YES completion:nil];
    }
    
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    if([sizeModified isEqualToString:@"0"]){
        sizeModified = @"1";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        _viewHeightConstraint.constant += keyboardSize.height;
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    if([sizeModified isEqualToString:@"1"]){
        sizeModified = @"0";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        _viewHeightConstraint.constant -= keyboardSize.height;
    }
}


@end