//
//  CategoryMenu.h
//  Mobikul
//
//  Created by Ratnesh on 12/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home.h"

@interface CategoryMenu : UIViewController <UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate
>{
    NSMutableArray *categoryData, *storeData, *extraData, *allData, *headerTitle, *categoryId, *categoryImagesId, *categoryImages;
    NSDictionary *categoryDict;
    NSUserDefaults *languageCode;
    Home *homeObject;
    NSUserDefaults *preferences;
    NSString *imageForCategoryMenu;
    NSString *customerId;
    UIAlertController *alert;
    UIAlertController *AC;
    UIAlertController *failedController;
    NSInteger isAlertVisible;
    NSString *pictureUpload;
    NSString *uploadingData;
    NSData *imageData;
    NSInteger sectionData;
    NSCache *imageCache;
    NSString *imagesLoad;
    NSCache *profileCache,*pictureCache;
}

@property (strong, nonatomic) IBOutlet UITableView *categoryMenuTable;
@property (weak, nonatomic) IBOutlet UIImageView *profileBanner;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic)  NSBundle *languageBundle;
@property (weak, nonatomic)  NSUserDefaults *language;

@end
