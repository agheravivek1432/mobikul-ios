//
//  BlogListViewController.h
//  MobikulMp
//
//  Created by Developer on 10/11/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogListViewController : UIViewController <NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource>
{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *firstName, *lastName, *emailAddress, *currentPassword, *password, *confirmPassword, *wantChangePassword, *whichApiDataToProcess;
    NSInteger isAlertVisible;
    UIAlertController *alert;
    id collection;
    UIWindow *currentWindow;
    
    IBOutlet UITableView *tblViewBlogList;
    IBOutlet UIButton *btnAddBlog;
    
    NSArray *arrBlogList;
    NSDictionary *dictBlog;
    
    IBOutlet UIBarButtonItem *btnAddCategory;
}

@property (nonatomic, assign) BOOL isComingFromSellerProfile;

-(IBAction)btnAddBlogPressed:(id)sender;
-(IBAction)btnAddCategoryPressed:(id)sender;


@end
