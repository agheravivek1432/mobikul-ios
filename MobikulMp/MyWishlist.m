//
//  MyWishlist.m
//  Mobikul
//
//  Created by Ratnesh on 5/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "MyWishList.h"
#import "GlobalData.h"
#import "ToastView.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectMyWishlist;

@implementation MyWishList

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectMyWishlist=[[GlobalData alloc] init];
    globalObjectMyWishlist.delegate = self;
    _mainScroller.delegate = self;
    [globalObjectMyWishlist language];
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationController.title = [globalObjectMyWishlist.languageBundle localizedStringForKey:@"myWishlist" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    whichApiToCall = @"";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    pageNumber = 1;
    loadPageRequestFlag = 1;
    contentHeight = -64;
    reloadPageData = @"false";
    arrayMainCollection = [[NSArray alloc] init];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    if([currentWindow viewWithTag:313131])
        [[currentWindow viewWithTag:313131] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectMyWishlist callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [[currentWindow viewWithTag:313131] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiToCall isEqualToString:@"massUpdateWishList" ]){
        [GlobalData alertController:currentWindow msg:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        NSError *error = nil;
        NSData *jsonItemData = [NSJSONSerialization dataWithJSONObject:massUpdateWhishList options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonSortString = [[NSString alloc] initWithData:jsonItemData encoding:NSUTF8StringEncoding];
        [post appendFormat:@"itemData=%@&", jsonSortString];
        [post appendFormat:@"customerId=%@", [preferences objectForKey:@"customerId"]];
        [globalObjectMyWishlist callHTTPPostMethod:post api:@"mobikulhttp/customer/updatewishList" signal:@"HttpPostMetod"];
        globalObjectMyWishlist.delegate = self;
    }
    else if([whichApiToCall isEqualToString:@"myWhishListAddToCart"]){
        [GlobalData alertController:currentWindow msg:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        [post appendFormat:@"customerId=%@&", [preferences objectForKey:@"customerId"]];
        [post appendFormat:@"qty=%@&", qtyValue];
        [post appendFormat:@"productId=%@&", produtId];
        [post appendFormat:@"itemId=%@&", itemId];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectMyWishlist callHTTPPostMethod:post api:@"mobikulhttp/customer/wishlistaddtoCart" signal:@"HttpPostMetod"];
        globalObjectMyWishlist.delegate = self;
    }
    else{
        if(pageNumber == 1)
            [GlobalData alertController:currentWindow msg:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
        else{
            [GlobalData loadingController:currentWindow];
        }
        [post appendFormat:@"storeId=%@&", storeId];
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        [post appendFormat:@"websiteId=%@&", websiteId];
        NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
        [post appendFormat:@"customerEmail=%@&",prefCustomerEmail];
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@&",screenWidth];
        [post appendFormat:@"pageNumber=%@", [NSString stringWithFormat: @"%ld", pageNumber]];
        [globalObjectMyWishlist callHTTPPostMethod:post api:@"mobikulhttp/customer/getwishlistData" signal:@"HttpPostMetod"];
        globalObjectMyWishlist.delegate = self;
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger currentCellCount = [arrayMainCollection count];
    //NSLog(@" called   %ld   %ld",totalCount,currentCellCount);
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height) {
        if( (totalCount > currentCellCount) && loadPageRequestFlag ){
            reloadPageData = @"true";
            pageNumber +=1;
            loadPageRequestFlag = 0;
            contentHeight = _mainScroller.contentOffset.y;
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    float Y = 3;
    float mainInternalY = 0;
    if([whichApiToCall isEqualToString:@"massUpdateWishList"]){
        if([collection[@"success"] boolValue] == true ){
            [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
            whichApiToCall = @"";
            pageNumber = 1;
            arrayMainCollection = [[NSArray alloc] init];
            [self viewDidLoad];
        }
        else
            [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"error" withDuaration:5.0];
        
    }
    else if([whichApiToCall isEqualToString:@"myWhishListAddToCart"]){
        if([collection[@"success"] boolValue] == true ){
            [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
            whichApiToCall = @"";
            pageNumber = 1;
            arrayMainCollection = [[NSArray alloc] init];
            [self viewDidLoad];
            
        }
        else
            [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"error" withDuaration:5.0];
        
    }
    
    else{
        if([currentWindow viewWithTag:313131])
            [[currentWindow viewWithTag:313131] removeFromSuperview];
        loadPageRequestFlag = 1;
        NSArray *newData = collection[@"wishlistData"];
        arrayMainCollection = [arrayMainCollection arrayByAddingObjectsFromArray:newData];
        totalCount = [collection[@"totalCount"] integerValue];
        for(UIView *subview in _wishListContainer.subviews)
            [subview removeFromSuperview];
        for(int i=0; i<[arrayMainCollection count]; i++){
            NSDictionary *dict = [arrayMainCollection objectAtIndex:i];
            UIView *paintView =[[UIView alloc]initWithFrame:CGRectMake(2, Y, (_wishListContainer.frame.size.width)-4, (SCREEN_WIDTH/3+10))];
            paintView.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            paintView.layer.borderWidth = 2.0f;
            paintView.backgroundColor = [UIColor whiteColor];
            paintView.tag = i;
            // [paintView setBackgroundColor:[UIColor whiteColor]];
            [_wishListContainer addSubview:paintView];
            UIImageView *productImage =[[UIImageView alloc] initWithFrame:CGRectMake(5,5,SCREEN_WIDTH/3,SCREEN_WIDTH/3)];
            productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
            UIImage *image = [imageCache objectForKey:dict[@"thumbNail"]];
            if(image)
                productImage.image = image;
            else{
                [_queue addOperationWithBlock:^{
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:dict[@"thumbNail"]]];
                    UIImage *image = [UIImage imageWithData: imageData];
                    if(image){
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            productImage.image = image;
                        }];
                        [imageCache setObject:image forKey:dict[@"thumbNail"]];
                    }
                }];
            }
            [paintView addSubview:productImage];
            mainInternalY = 5;
            UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, (((_mainScroller.frame.size.width)-14)-(SCREEN_WIDTH/3+5)), 25)];
            [productName setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
            //[productName setBackgroundColor:[UIColor clearColor]];
            [productName setFont:[UIFont fontWithName: @"Trebuchet MS" size: 21.0f]];
            [productName setText:dict[@"name"]];
            [paintView addSubview:productName];
            mainInternalY += 30;
            UILabel *sku = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, (((_mainScroller.frame.size.width)-14)-(SCREEN_WIDTH/3+5)), 20)];
            [sku setTextColor:[UIColor blackColor]];
            // [sku setBackgroundColor:[UIColor clearColor]];
            [sku setFont:[UIFont fontWithName: @"Trebuchet MS" size: 18.0f]];
            NSString *skuText = [NSString stringWithFormat:@"SKU : %@",dict[@"sku"]];
            [sku setText:skuText];
            [paintView addSubview:sku];
            mainInternalY += 25;
            
            UITextView* textArea = [[UITextView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, (((_mainScroller.frame.size.width)-14)-(SCREEN_WIDTH/3+5)), 70)];
            textArea.backgroundColor = [GlobalData colorWithHexString:@"F2F3F3"];
            textArea.font = [UIFont fontWithName: @"Trebuchet MS" size: 16.0f];
            if(dict[@"description"] == (id)[NSNull null]){
                textArea.textColor = [UIColor lightGrayColor];
                textArea.text = [globalObjectMyWishlist.languageBundle localizedStringForKey:@"enterComments" value:@"" table:nil];
            }
            else{
                textArea.textColor = [UIColor blackColor];
                textArea.text = dict[@"description"];
            }
            textArea.delegate = self;
            textArea.tag = 100;
            [paintView addSubview:textArea];
            mainInternalY += 75;
            
            UILabel *qtyText = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, 50, 20)];
            [qtyText setTextColor:[UIColor blackColor]];
            // [qtyText setBackgroundColor:[UIColor clearColor]];
            [qtyText setFont:[UIFont fontWithName: @"Trebuchet MS" size: 17.0f]];
            
            [qtyText setText:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"qty" value:@"" table:nil]];
            [paintView addSubview:qtyText];
            
            UITextField* qtyField = [[UITextField alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10)+50, 135, 50, 20)];
            qtyField.font = [UIFont fontWithName: @"Trebuchet MS" size: 17.0f];
            qtyField.textColor = [UIColor blackColor];
            qtyField.text = [NSString stringWithFormat:@"%@",dict[@"qty"]];
            qtyField.tag = 200;
            qtyField.textAlignment = NSTextAlignmentCenter;
            qtyField.borderStyle = UITextBorderStyleRoundedRect;
            [paintView addSubview:qtyField];
            mainInternalY +=25;
            
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, 120, 24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            
            double percent = 24 * [dict[@"rating"] doubleValue];
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [paintView addSubview:ratingContainer];
            mainInternalY +=30;
            
            UIView* hr = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, (((_mainScroller.frame.size.width)-14)-(SCREEN_WIDTH/3+5)), 1)];
            hr.backgroundColor = [GlobalData colorWithHexString:@"E5E4DF"];
            [paintView addSubview:hr];
            mainInternalY +=36;
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, (((_mainScroller.frame.size.width)-14)-(SCREEN_WIDTH/3+5)), 20)];
            [price setTextColor: [UIColor blackColor]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName: @"Trebuchet MS" size: 18.0f]];
            NSString *priceText = [NSString stringWithFormat:@"Price : %@",dict[@"price"]];
            [price setText:priceText];
            [paintView addSubview:price];
            mainInternalY +=30;
            UILabel *updateLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/3+10), mainInternalY, 170, 50)];
            [updateLabel setBackgroundColor: [GlobalData colorWithHexString:@"3399cc"]];
            [updateLabel setTextColor: [GlobalData colorWithHexString:@"ffffff"]];
            [updateLabel setText:@"Add To Cart"];
            updateLabel.userInteractionEnabled = YES;
            updateLabel.textAlignment = NSTextAlignmentCenter;
            updateLabel.tag = [dict[@"id"] integerValue];
            [paintView addSubview:updateLabel];
            UITapGestureRecognizer *updateWishListGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateWishList:)];
            updateWishListGesture.numberOfTapsRequired = 1;
            [updateLabel addGestureRecognizer:updateWishListGesture];
            
            mainInternalY +=70;
            CGRect newFrame = paintView.frame;
            newFrame.size.height = mainInternalY ;
            paintView.frame = newFrame;
            Y += mainInternalY ;
            //            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(5, Y ,_wishListContainer.frame.size.width-10 ,1)];           // line 2
            //            line.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
            //            line.layer.borderWidth = 2.0f;
            //            [_wishListContainer addSubview:line];
            
            Y +=5;
            
            
        }
        
        
        double wishListDataHeight = ((12+SCREEN_WIDTH/3)*[arrayMainCollection count]) + 2;
        if([arrayMainCollection count] > 0){
            UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(2, Y, (_wishListContainer.frame.size.width)-4, 60)];
            [buttonView setBackgroundColor:[UIColor whiteColor]];
            buttonView.tag = 999;
            [_wishListContainer addSubview:buttonView];
            
            UILabel *massUpdate = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (_wishListContainer.frame.size.width -10), 50)];
            [massUpdate setBackgroundColor: [GlobalData colorWithHexString:@"E5E4DF"]];
            [massUpdate setTextColor: [UIColor blackColor]];
            [massUpdate setText:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"updateWishlist" value:@"" table:nil]];
            massUpdate.userInteractionEnabled = YES;
            massUpdate.textAlignment = NSTextAlignmentCenter;
            [buttonView addSubview:massUpdate];
            UITapGestureRecognizer *massUpdateGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(massUpdate:)];
            massUpdateGesture.numberOfTapsRequired = 1;
            [massUpdate addGestureRecognizer:massUpdateGesture];
            
            //        UILabel *massAddToCart = [[UILabel alloc] initWithFrame:CGRectMake((((_wishListContainer.frame.size.width)-19)/2)+10, 5, ((_wishListContainer.frame.size.width)-19)/2, 50)];
            //        [massAddToCart setBackgroundColor: [GlobalData colorWithHexString:@"268ED7"]];
            //        [massAddToCart setTextColor: [UIColor whiteColor]];
            //        [massAddToCart setText:[globalObjectMyWishlist.languageBundle localizedStringForKey:@"allCart" value:@"" table:nil]];
            //        massAddToCart.userInteractionEnabled = YES;
            //        massAddToCart.textAlignment = NSTextAlignmentCenter;
            //        [buttonView addSubview:massAddToCart];
            //        UITapGestureRecognizer *massAddToCartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(massAddToCart:)];
            //        massAddToCartGesture.numberOfTapsRequired = 1;
            //        [massAddToCart addGestureRecognizer:massAddToCartGesture];
            if([arrayMainCollection count] == totalCount)
                _wishListContainerHeight.constant = Y + 62;
            else
                _wishListContainerHeight.constant = Y + 62 + 50;  // to give space for display a loader alert
        }
        else{
            UIView *emptyView = [[UIView alloc]initWithFrame:CGRectMake(2, wishListDataHeight, (_wishListContainer.frame.size.width)-4, 40)];
            [emptyView setBackgroundColor:[UIColor whiteColor]];
            [_wishListContainer addSubview:emptyView];
            
            UILabel *emptyText = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 20)];
            [emptyText setTextColor:[UIColor blackColor]];
            [emptyText setBackgroundColor:[UIColor clearColor]];
            [emptyText setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [emptyText setText:@"Your Wishlist is Empty"];
            [emptyView addSubview:emptyText];
        }
        [_wishListContainer layoutIfNeeded];
        [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
    }
}

-(void)updateWishList:(UITapGestureRecognizer *)recognizer{
    NSDictionary *dict = [arrayMainCollection objectAtIndex:recognizer.view.superview.tag];
    UIView *parent = [recognizer.view.superview viewWithTag:recognizer.view.superview.tag];
    UITextField *qty = [parent viewWithTag:200];
    qtyValue = qty.text ;
    itemId = dict[@"id"];
    produtId = dict[@"productId"];
    whichApiToCall = @"myWhishListAddToCart";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)massUpdate:(UITapGestureRecognizer *)recognizer{
    massUpdateWhishList = [[NSMutableArray alloc] initWithCapacity:[arrayMainCollection count]];
    UIView *child = recognizer.view.superview;
    UIView *parent = child.superview;
    for(int i=0; i<[arrayMainCollection count]; i++){
        UIView *viewData = [parent viewWithTag:i];
        UITextField *qty = [viewData viewWithTag:200];
        UITextView *desc = [viewData viewWithTag:100];
        NSDictionary *dict = [arrayMainCollection objectAtIndex:i];
        NSMutableDictionary *massUpdate = [[NSMutableDictionary alloc]init];
        [massUpdate setObject:qty.text forKey:@"qty"];
        if([desc.text isEqualToString:@""] == NO)
            [massUpdate setObject:desc.text forKey:@"description"];
        else
            [massUpdate setObject:@"" forKey:@"description"];
        [massUpdate setObject:dict[@"id"] forKey:@"id"];
        [massUpdateWhishList addObject:massUpdate];
    }
    whichApiToCall = @"massUpdateWishList";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
}

-(void)massAddToCart:(UITapGestureRecognizer *)recognizer{
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@"PLEASE ENTER YOUR COMMENTS.."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]) {
        textView.text = [globalObjectMyWishlist.languageBundle localizedStringForKey:@"enterComments" value:@"" table:nil];
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

@end