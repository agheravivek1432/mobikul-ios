//
//  DashBoard.h
//  Mobikul
//
//  Created by Ratnesh on 02/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashBoard : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSInteger isAlertVisible;
    NSString *sessionId, *message, *code, *dataFromApi, *address;
    UIAlertController *alert;
    id collection;
    UIWindow *currentWindow;
    NSString *incrementId;
}

@property (weak, nonatomic) IBOutlet UILabel *accountInformation;
@property (weak, nonatomic) IBOutlet UILabel *contactInformation;
@property (weak, nonatomic) IBOutlet UILabel *editDefaultBillingAddress;
@property (weak, nonatomic) IBOutlet UILabel *editAccountInformation;
@property (weak, nonatomic) IBOutlet UILabel *changePassword;
@property (weak, nonatomic) IBOutlet UILabel *newsLetterSubscription;
@property (weak, nonatomic) IBOutlet UILabel *manageAddress;
@property (weak, nonatomic) IBOutlet UILabel *editDefaultShippingAddress;
@property (weak, nonatomic) IBOutlet UILabel *newsletter;
@property (weak, nonatomic) IBOutlet UILabel *addressBook;
@property (weak, nonatomic) IBOutlet UILabel *defaultBilling;
@property (weak, nonatomic) IBOutlet UILabel *defaultShipping;
@property (weak, nonatomic) IBOutlet UILabel *welcomeMessage;
@property (weak, nonatomic) IBOutlet UILabel *customerName;
@property (weak, nonatomic) IBOutlet UILabel *customerEmail;
@property (weak, nonatomic) IBOutlet UILabel *newsletterNotification;
@property (weak, nonatomic) IBOutlet UILabel *defaultBillingAddress;
@property (weak, nonatomic) IBOutlet UILabel *defaultShippingAddress;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *recentOrderContainer;
@property (weak, nonatomic) IBOutlet UILabel *recentOrderHeading;
@property (weak, nonatomic) IBOutlet UILabel *viewAllOrderButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIView *recentReviewsContainer;
@property (weak, nonatomic) IBOutlet UILabel *recentReviewHeading;
@property (weak, nonatomic) IBOutlet UILabel *viewAllRecentReview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *recentReviewLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentReviewWidth;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *dashboardMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *internalViewHeightConstraint;

@end