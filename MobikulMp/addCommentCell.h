//
//  addCommentCell.h
//  MobikulMp
//
//  Created by Apple on 19/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface addCommentCell : UITableViewCell

{
    IBOutlet UIView *viewAddComment;
    IBOutlet UITextField *txtFUser;
    IBOutlet UITextField *txtFWebsite;
    IBOutlet UITextView *txtVComment;
    IBOutlet UIButton *btnComment;
}

@property (nonatomic, retain) IBOutlet UITextField *txtFUser;
@property (nonatomic, retain) IBOutlet UITextField *txtFWebsite;
@property (nonatomic, retain) IBOutlet UITextView *txtVComment;
@property (nonatomic, retain) IBOutlet UIButton *btnComment;

-(void)reloadAddCommentCell;

@end
