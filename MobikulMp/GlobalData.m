#import "GlobalData.h"
#import "Home.h"
#import "CatalogProduct.h"
#import "SWRevealViewController.h"
#import "ProductCategory.h"
#import "ToastView.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)



UIAlertController *alert;
NSMutableString *finalDataglobal;
 NSMutableData *webDataglobal;
NSString *gotSessionId, *gotMessage, *gotCode, *gotDataFromApiglobal;
NSUserDefaults *preferences;
NSString *sessionId, *message, *code, *dataFromApi;


@implementation GlobalData
@synthesize finalResulttoSend = _finalResulttoSend;
@synthesize whichApitoCallForCustomerLogin = _whichApitoCallForCustomerLogin;
@synthesize employEmailId = _employEmailId;
@synthesize catalogMenuFlag = _catalogMenuFlag;

    static GlobalData *instance = nil;

    +(GlobalData *)getInstance{
        @synchronized(self){
            if(instance == nil)
                instance = [GlobalData new];
        }
        return instance;
    }


-(void) dragEvent{
    _isDragEvent = @"moveLeftRight";
}

-(void) language{
    
    NSUserDefaults *languageCode = [NSUserDefaults standardUserDefaults];
    if ([languageCode stringForKey:@"language" ] != nil) {
        NSString *language = [languageCode stringForKey:@"language"];
        NSString *path= [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
        if (path == nil){
            path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
        }
        _languageBundle = [NSBundle bundleWithPath:path];
    }
    else{
        [languageCode setObject:@"en" forKey:@"language"];
        [languageCode synchronize];
        NSString *language = [languageCode stringForKey:@"language"];
        NSString *path= [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
        if (path == nil){
            path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
        }
        _languageBundle = [NSBundle bundleWithPath:path];
    }
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSError *error = nil;
    if([signalName isEqualToString:@"HttpPostMetod"]){
        collection = [NSJSONSerialization JSONObjectWithData:webDataglobal options:0 error:&error];
        signalName = @"";
        [self startFinalDataProcess];
    }
    else if([signalName isEqualToString:@"HttpPostRegisterDevice"]){
        collectionRegisterData = [NSJSONSerialization JSONObjectWithData:webDataglobal options:0 error:&error];
        signalName = @"";
        [self startFinalDataForRegisterDeviceProcess];
    }
    else if([signalName isEqualToString:@"HttpLoginPostMetod"]){
        signalName = @"";

        collection = [NSJSONSerialization JSONObjectWithData:webDataglobal options:0 error:&error];
        if([collection[@"error"] integerValue] == 0){
            preferences = [NSUserDefaults standardUserDefaults];
            NSString *newSessionId = collection[@"sessionId"];
            [preferences setObject:newSessionId forKey:@"sessionId"];
            [self startCallingApi];
        }
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [webDataglobal setLength: 0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [webDataglobal appendData:data];
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
//    NSLog(@"global data parse %@",error);
    [self  sendingErorReport];
    
}

-(void)startFinalDataProcess{
    [self.delegate finalHttpDataprocessCompleted:collection ];
}

-(void)startFinalDataForRegisterDeviceProcess{
    [self.delegate finalDataForRegisterDeviceCompleted:collectionRegisterData ];
}

//-(void)startFinalDataProcess{
//    _finalResulttoSend = dataFromApi;                                                         //delegation method of dofurtherprocesswithresult
//    [NSTimer scheduledTimerWithTimeInterval:0 target:self.delegate selector:@selector(finalDataprocessCompleted) userInfo:nil repeats:NO];
//}

-(void)startCallingApi{
    [NSTimer scheduledTimerWithTimeInterval:0 target:self.delegate selector:@selector(finalCallingApiCompleted) userInfo:nil repeats:NO];
    //delegate method of CallingApi
    
}
-(void)sendingErorReport{
    [NSTimer scheduledTimerWithTimeInterval:0 target:self.delegate selector:@selector(connectionErorWindow) userInfo:nil repeats:NO];
    //delegate method of error window

    
}
-(void)sendingLoginRequest{
    [NSTimer scheduledTimerWithTimeInterval:0 target:self.delegate selector:@selector(loginRequestCall) userInfo:nil repeats:NO];              // delegate method of loginRequest
}

-(void)callHTTPPostMethod:(NSString *)params api:(NSString*)apiName signal:(NSString*)signalNameFunction{
    NSString *BaseDomain = BASE_DOMAIN;
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseDomain, apiName];
    NSData *postData = [params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%ld",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    signalName = signalNameFunction;
//    NSLog(@"url string %@",urlString);
//     NSLog(@"url post %@",params);
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(connection){
        webDataglobal = [NSMutableData data];
    }
    else{
        // NSLog(@"The Connection is null");
    }
}

-(void)callHTTPPostMethodForBlog:(NSString *)params api:(NSString*)apiName signal:(NSString*)signalNameFunction
{
    NSString *BaseDomain = BASE_DOMAIN_Blog;
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BaseDomain, apiName];
    NSData *postData = [params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%ld",[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    signalName = signalNameFunction;
    //    NSLog(@"url string %@",urlString);
    //     NSLog(@"url post %@",params);
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(connection)
    {
        webDataglobal = [NSMutableData data];
    }
    else
    {
        // NSLog(@"The Connection is null");
    }
}

+(void)alertController:(UIWindow *)alertnew msg:(NSString *)textMessage {
    UIBlurEffect  *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    blurEffectView.tag = 121212;
    UIWindow *currentWindow = alertnew;
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.color = [UIColor blackColor];
    [spinner startAnimating];
    UIView *alertView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 256, 96)];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.layer.cornerRadius = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 256, 20)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"000000"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"Trebuchet MS" size : 18.0f]];
    [sortTitle setText : textMessage];
    sortTitle.adjustsFontSizeToFitWidth = YES;
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [alertView addSubview : sortTitle];
    UIView *sampleView = [[UIView alloc] initWithFrame:CGRectMake(0,45, 256, 40)];
    sampleView.backgroundColor = [UIColor whiteColor];
    [sampleView addSubview:spinner];
    [alertView addSubview:sampleView];
    spinner.center = CGPointMake(sampleView.frame.size.width  / 2,
                                 sampleView.frame.size.height / 2);
    alertView.center = currentWindow.center;
    blurEffectView.frame = currentWindow.bounds;
    [blurEffectView addSubview:alertView];
    [currentWindow addSubview:blurEffectView];
}

+(void)loadingController:(UIWindow *)alertnew {
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.color = [UIColor blackColor];
    
    [spinner startAnimating];
    UIView *alertView = [[UIView alloc] initWithFrame:CGRectMake(0,SCREEN_HEIGHT-50, SCREEN_WIDTH, 50)];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.layer.cornerRadius = 10;
    [alertView addSubview:spinner];
    spinner.center = CGPointMake(alertView.frame.size.width  / 2,
                                 alertView.frame.size.height / 2);
    alertView.tag = 313131;
    [alertnew addSubview:alertView];
}


+(UIColor*)colorWithHexString:(NSString*)hex    {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}







@end
