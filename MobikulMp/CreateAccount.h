//
//  CreateAccountVC.h
//  FirstForm
//
//  Created by Ratnesh on 25/11/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateAccount : UIViewController <NSXMLParserDelegate>{
    NSMutableData *webData;
    NSUserDefaults *preferences;
    NSString *gotSessionId, *gotMessage, *gotCode, *gotDataFromApi;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *firstNameVal, *lastNameVal, *emailAddressVal, *passwordVal, *confirmPasswordVal, *newsLetterSubscribeVal;
    UIAlertController *alert;
    NSMutableString *finalData;
    id collection;
    NSInteger isAlertVisible;
    NSString *sizeModified;
    UIWindow *currentWindow;
    
    __weak IBOutlet UIScrollView *scrollView;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraint;

@end

