//
//  BlogDetailViewController.m
//  MobikulMp
//
//  Created by Apple on 15/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "BlogDetailViewController.h"
#import "GlobalData.h"
#import "BlogListCell.h"
#import "commentListCell.h"
#import "addCommentCell.h"

@interface BlogDetailViewController ()

@end

@implementation BlogDetailViewController

GlobalData *globalObjectBlogDetail;

@synthesize dictBlog;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    strAddCommentText = @"";
    strAddWebsitText = @"";
    
    globalObjectBlogDetail = [[GlobalData alloc] init];
    globalObjectBlogDetail.delegate = self;
    [globalObjectBlogDetail language];
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    wantChangePassword = 0;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectBlogDetail.languageBundle localizedStringForKey:@"Blogs" value:@"" table:nil];
    
    
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    
    if(savedSessionId == nil)
        [self loginRequest];
    else{
        whichApiDataToProcess = @"getcomments";
        [self callingHttppApi];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData
{
    isAlertVisible = 1;
    collection = collectionData ;
    
    if([collection[@"success"] integerValue] == 5)
    {
        [self loginRequest];
    }
    else
    {
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted
{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}


-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectBlogDetail callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi
{
    // http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/getcomments
    // customerId
    
    [GlobalData alertController:currentWindow msg:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToProcess isEqual: @"getcomments"])
    {
//        NSString *customerId = [preferences objectForKey:@"customerId"];
//        [post appendFormat:@"customerId=%@&", customerId];
        [post appendFormat:@"postid=%@&", [dictBlog valueForKey:@"id"]];
        
        [globalObjectBlogDetail callHTTPPostMethodForBlog:post api:@"getcomments" signal:@"HttpPostMetod"];
        globalObjectBlogDetail.delegate = self;
    }
    else if ([whichApiDataToProcess isEqual: @"addcommnet"])
    {
        /*
         http://farmrichonline.com/demostore/index.php/mobikulhttp/blog/addcommnet
         userid
         postid
         cwebsite
         comment
         */
        
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"userid=%@&", customerId];
        [post appendFormat:@"postid=%@&", [dictBlog valueForKey:@"id"]];
        [post appendFormat:@"cwebsite=%@&", strAddWebsitText];
        [post appendFormat:@"comment=%@&", strAddCommentText];
        
        [globalObjectBlogDetail callHTTPPostMethodForBlog:post api:@"addcommnet" signal:@"HttpPostMetod"];
        globalObjectBlogDetail.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult
{
    if(isAlertVisible == 1)
    {
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    if([whichApiDataToProcess isEqual: @"getcomments"])
    {
        arrCommentList = (NSArray *)collection[@"comments"];
        
        if (arrCommentList.count > 0)
        {
            [tblView reloadData];
        }
    }
    else if ([whichApiDataToProcess isEqual: @"addcommnet"])
    {
        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        addCommentCell *cell = (addCommentCell *)[tblView cellForRowAtIndexPath:selectedIndexPath];
        
        cell.txtVComment.text = @"";
        cell.txtFWebsite.text = @"Add a commen";
        
        whichApiDataToProcess = @"getcomments";
        [self callingHttppApi];
    }
    else
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"message" value:@"" table:nil] message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectBlogDetail.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    
    //  _mainContainerHeightConstraint.constant = SCREEN_HEIGHT-64;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (section == 1)
//    {
//        if (arrCommentList.count > 0)
//        {
//            return @"COMMENTS";
//        }
//    }
//    return @"";
//}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader;
    
    if (section == 1)
    {
        if (arrCommentList.count > 0)
        {
            viewHeader = [[UIView alloc] init];
            viewHeader.frame = CGRectMake(0, 0, tblView.frame.size.width, 30);
            viewHeader.backgroundColor = [UIColor clearColor];
            
            UILabel  *lblComment = [[UILabel alloc] init];
            lblComment.frame = CGRectMake(10, 0, tblView.frame.size.width-20, 30);
            lblComment.backgroundColor = [UIColor clearColor];
            lblComment.text = @"COMMENTS";
            lblComment.font = [UIFont boldSystemFontOfSize:17.0];
            
            [viewHeader addSubview:lblComment];
        }
    }
    return viewHeader;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        if (arrCommentList.count > 0)
        {
            return 30.0;
        }
    }
    return 0.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int row = 0;
    
    if (section == 0)
    {
        return 1;
    }
    else if (section == 1)
    {
        return arrCommentList.count;
    }
    else if (section == 2)
    {
        return 1;
    }
        
    return row;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 142.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *simpleTableIdentifier = @"BlogListCell";
        
        BlogListCell *cell = (BlogListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BlogListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
        cell.dictBlog = self.dictBlog;
        
        [cell reloadBlogListCell];
        
        return cell;
    }
    else if (indexPath.section == 1)
    {
        static NSString *simpleTableIdentifier = @"commentListCell";
        
        commentListCell *cell = (commentListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"commentListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
        cell.dictComment = [arrCommentList objectAtIndex:indexPath.row];
        
        [cell reloadCommentListCell];
        
        return cell;
    }
    else if (indexPath.section == 2)
    {
        static NSString *simpleTableIdentifier = @"addCommentCell";
        
        addCommentCell *cell = (addCommentCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addCommentCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
        cell.txtFUser.delegate = self;
        cell.txtFWebsite.delegate = self;
        cell.txtVComment.delegate = self;
        cell.txtFUser.text = [preferences valueForKey:@"customerName"];
        cell.txtFUser.userInteractionEnabled = NO;
        
        [cell.btnComment addTarget:self action:@selector(btnAddCommentPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [cell reloadAddCommentCell];
        
        return cell;
    }
    
    UITableViewCell *cell;
    return cell;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    tblBottomConstant.constant = 200.0;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    tblBottomConstant.constant = 0.0;
    [textField resignFirstResponder];
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    tblBottomConstant.constant = 200.0;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        tblBottomConstant.constant = 0.0;
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)btnAddCommentPressed
{
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    addCommentCell *cell = (addCommentCell *)[tblView cellForRowAtIndexPath:selectedIndexPath];
    
    strAddCommentText = cell.txtVComment.text;
    strAddWebsitText = cell.txtFWebsite.text;
    
    if ([strAddWebsitText isEqualToString:@""] || [strAddCommentText isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please provide required detail." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }
    else
    {
        whichApiDataToProcess = @"addcommnet";
        [self callingHttppApi];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
@end
