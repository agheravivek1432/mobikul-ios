//
//  AccountInformation.m
//  Mobikul
//
//  Created by Ratnesh on 03/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import "AccountInformation.h"
#import "GlobalData.h"

#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

GlobalData *globalObjectAccountInformation;

@implementation AccountInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectAccountInformation = [[GlobalData alloc] init];
    globalObjectAccountInformation.delegate = self;
    [globalObjectAccountInformation language];
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    wantChangePassword = 0;
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"accountInformation" value:@"" table:nil];
    _lastName.text = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"lastName" value:@"" table:nil];
    _emailAddress.text = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"EmailAddress" value:@"" table:nil];
    _changePassword.text = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"changePassword" value:@"" table:nil];
    [_save setTitle: [globalObjectAccountInformation.languageBundle localizedStringForKey:@"save" value:@"" table:nil] forState: UIControlStateNormal];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else{
        whichApiDataToProcess = @"getAccountInformation";
        [self callingHttppApi];
    }
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

- (IBAction)changePasswordSwitch:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]){
        _changePasswordLayoutHeightConstraint.constant = 172;
        [_changePasswordLayout setValue:@"NO" forKeyPath:@"hidden"];
    }
    else{
        _changePasswordLayoutHeightConstraint.constant = 0;
        [_changePasswordLayout setValue:@"YES" forKeyPath:@"hidden"];
    }
}

- (IBAction)saveInformation:(id)sender {
    @try{
        int isValid = 1;
        NSString *errorMessage;
        firstName = _firstNameField.text;
        lastName = _lastNameField.text;
        emailAddress = _emailAddressField.text;
        currentPassword = _currentPasswordField.text;
        password = _passwordField.text;
        confirmPassword = _confirmPasswordField.text;
        if(_changePasswordRef.isOn)
            wantChangePassword = @"1";
        else
            wantChangePassword = @"0";
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if([firstName isEqual: @""]){
            isValid = 0;
            errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"firstNameRequire" value:@"" table:nil];
        }
        else
            if([lastName isEqual: @""]){
                isValid = 0;
                errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"lastNameRequire" value:@"" table:nil];
            }
            else
                if([emailAddress isEqual: @""]){
                    isValid = 0;
                    errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"emailAddress" value:@"" table:nil];
                }
                else
                    if(![emailTest evaluateWithObject:emailAddress]){
                        isValid = 0;
                        errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"validEmail" value:@"" table:nil];
                    }
        if([wantChangePassword  isEqual: @"1"]){
            if([currentPassword isEqual: @""]){
                errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"currentPassword" value:@"" table:nil];
                isValid = 0;
            }
            else
                if([password isEqual: @""]){
                    errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"newPassword" value:@"" table:nil];
                    isValid = 0;
                }
                else
                    if([confirmPassword isEqual: @""]){
                        isValid = 0;
                        errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"confirmPassword" value:@"" table:nil];
                    }
                    else
                        if(![password isEqual: confirmPassword]){
                            isValid = 0;
                            errorMessage = [globalObjectAccountInformation.languageBundle localizedStringForKey:@"passwordMatch" value:@"" table:nil];
                        }
        }
        if(isValid == 0){
            UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
            [errorAlert addAction:noBtn];
            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
        }
        else{
            whichApiDataToProcess = @"customerEditPost";
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
    @catch (NSException *e) {
        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
    }
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectAccountInformation callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToProcess isEqual: @"getAccountInformation"]){
        NSString *customerEmail = [preferences objectForKey:@"customerEmail"];
        [post appendFormat:@"customerEmail=%@&",customerEmail];
        NSString *websiteId = [preferences objectForKey:@"websiteId"];
        [post appendFormat:@"websiteId=%@",websiteId];
        [globalObjectAccountInformation callHTTPPostMethod:post api:@"mobikulhttp/customer/getaccountinfoData" signal:@"HttpPostMetod"];
        globalObjectAccountInformation.delegate = self;
    }else{
        NSString *customerId = [preferences objectForKey:@"customerId"];
        [post appendFormat:@"customerId=%@&", customerId];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"firstName=%@&", firstName];
        [post appendFormat:@"lastName=%@&", lastName];
        [post appendFormat:@"emailAddress=%@&", emailAddress];
        [post appendFormat:@"currentPassword=%@&", currentPassword];
        [post appendFormat:@"newPassword=%@&", password];
        [post appendFormat:@"confirmPassword=%@&", confirmPassword];
        [post appendFormat:@"doChangePassword=%@", wantChangePassword];
        [globalObjectAccountInformation callHTTPPostMethod:post api:@"mobikulhttp/customer/editPost" signal:@"HttpPostMetod"];
        globalObjectAccountInformation.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
        
    }
    if([whichApiDataToProcess isEqual: @"getAccountInformation"]){
        _firstNameField.text = collection[@"firstName"];
        _lastNameField.text = collection[@"lastName"];
        _emailAddressField.text = collection[@"email"];
    }
    else{
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"message" value:@"" table:nil] message:collection[@"message"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAccountInformation.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    
    _mainContainerHeightConstraint.constant = SCREEN_HEIGHT-64;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

@end