//
//  AddEditAddress.h
//  Mobikul
//
//  Created by Ratnesh on 12/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddEditAddress : UIViewController <NSXMLParserDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSUserDefaults *preferences;
    NSString  *customerName;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSString *firstNameValue, *lastNameValue, *companyValue, *telephoneValue, *faxValue, *street1Value, *street2Value, *cityValue, *regionValue, *regionIdValue, *zipValue, *countryValue, *default_billing, *default_shipping,*default_pickup, *whichApiResultDataToProcess;
    NSMutableArray *countryPickerData, *statePickerData;
    id collection;
    NSInteger isEdit, countryIndex;
    NSInteger isAlertVisible;
    NSMutableDictionary *addressDataDictionary;
    UIWindow *currentWindow;
    
    NSString *STR_SellerStatus;
}
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *city;
@property (weak, nonatomic) IBOutlet UILabel *streetAddress2;
@property (weak, nonatomic) IBOutlet UILabel *fax;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *telephone;
@property (weak, nonatomic) IBOutlet UILabel *streetAddress;

@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UITextField *stateField;
@property (weak, nonatomic) IBOutlet UIPickerView *statePicker;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *companyField;
@property (weak, nonatomic) IBOutlet UITextField *telephoneField;
@property (weak, nonatomic) IBOutlet UITextField *faxField;
@property (weak, nonatomic) IBOutlet UITextField *street1Field;
@property (weak, nonatomic) IBOutlet UITextField *street2Field;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UITextField *zipcodeField;
- (IBAction)saveAddressButton:(id)sender;
- (IBAction)defaultBillingSwitch:(id)sender;
- (IBAction)defaultShippingSwitch:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *defaultBillingSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *defaultShippingSwitch;

@property (weak, nonatomic) IBOutlet UILabel *LBL_defaultPickUpswitchTitle;
@property (weak, nonatomic) IBOutlet UISwitch *defaultPickUpSwitch;

@property(nonatomic) NSString *addOrEdit;
@property(nonatomic) NSString *addressId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stateFieldsContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainContainerHeightConstraint;

@end

