//
//  NewsLetterSubscription.h
//  Mobikul
//
//  Created by Ratnesh on 08/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsLetterSubscription : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger imageCount, isAlertVisible;
    id collection;
    NSString *isSubscribed, *whichApiToCall;
    UIWindow *currentWindow;
}

- (IBAction)saveSubscription:(id)sender;
- (IBAction)subscriptionSwitchValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *subscriptionSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainContainerHeightConstraint;

@end
