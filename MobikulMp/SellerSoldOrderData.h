//
//  SellerOrderDetails.h
//  MobikulMp
//
//  Created by kunal prasad on 25/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerSoldOrderData : UIViewController
{
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSString  *dataFromApi;
    id mainCollection,collection;
    UITapGestureRecognizer *productTitleTap;
    NSString *incrementId, *customerId;
    UIPickerView *dropDown;
    NSMutableArray *orderStatus,*orderStatusSend;
    NSMutableDictionary *orderListData;
    NSString *whichApiDataToprocess;
    UIDatePicker *datePickerFrom ,*datePickerTo;
    NSDateFormatter *dateFormat;
    NSString *statusToSend ,*productId,*type,*name;
    UIWindow *currentWindow;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSIndexPath *indexPathValue;
    NSArray *arrayMainCollection;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
    NSString *reloadPageData;
    
}
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;

@property (weak, nonatomic) IBOutlet UIScrollView *sellerSoldOrderDetailsScrollView;
@end
