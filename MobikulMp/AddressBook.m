//
//  AddressBook.m
//  Mobikul
//
//  Created by Ratnesh on 11/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AddressBook.h"
#import "GlobalData.h"
#import "ToastView.h"
#import "AddEditAddress.h"

GlobalData *globalObjectAddressBook;

@implementation AddressBook

- (void)viewDidLoad {
    [super viewDidLoad];
    isAlertVisible = 0;
    globalObjectAddressBook = [[GlobalData alloc] init];
    globalObjectAddressBook.delegate = self;
    [globalObjectAddressBook language];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectAddressBook.languageBundle localizedStringForKey:@"addressBook" value:@"" table:nil];
    _defaultAddresses.text = [globalObjectAddressBook.languageBundle localizedStringForKey:@"defaultAddresses" value:@"" table:nil];
    _defaultBilling.text = [globalObjectAddressBook.languageBundle localizedStringForKey:@"defaultBilling" value:@"" table:nil];
    _defaultBillingAddress.text = [globalObjectAddressBook.languageBundle localizedStringForKey:@"defaultAddresses" value:@"" table:nil];
    [_changeBilling setTitle: [globalObjectAddressBook.languageBundle localizedStringForKey:@"changeBilling" value:@"" table:nil] forState: UIControlStateNormal];
    _defaultShipping.text = [globalObjectAddressBook.languageBundle localizedStringForKey:@"defaultShipping" value:@"" table:nil];
    _defaultShippingAddress.text = [globalObjectAddressBook.languageBundle localizedStringForKey:@"defaultShippingAddress" value:@"" table:nil];
    _additionalAddress.text = [globalObjectAddressBook.languageBundle localizedStringForKey:@"additionalAddress" value:@"" table:nil];
    [_changeShipping setTitle: [globalObjectAddressBook.languageBundle localizedStringForKey:@"changeShipping" value:@"" table:nil] forState: UIControlStateNormal];
    whichApiResultToProcess = @"addressBook";
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
        [self LoginwithSeller];
    
}

-(void)LoginwithSeller
{
    STR_SellerStatus = [[NSUserDefaults standardUserDefaults]stringForKey:@"isSellerKey"];
    if (![STR_SellerStatus isEqualToString:@"1"]) {
        _defaultPickupAddress.hidden = YES;
        _changePickupButton.hidden=YES;
        _defaultPickUpAddress.hidden=YES;
    }
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectAddressBook callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:@"Warning" message:@"ERROR with the Conenction" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAddressBook.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi
{
    [GlobalData alertController:currentWindow msg:[globalObjectAddressBook.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    [post appendFormat:@"websiteId=%@&", websiteId];
    NSString *prefCustomerEmail = [preferences objectForKey:@"customerEmail"];
    [post appendFormat:@"customerEmail=%@", prefCustomerEmail];
    [globalObjectAddressBook callHTTPPostMethod:post api:@"mobikulhttp/customer/getaddrbookData" signal:@"HttpPostMetod"];
    globalObjectAddressBook.delegate = self;
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    double prevLableY = 0;
    if([whichApiResultToProcess isEqual: @"addressBook"]){
        _defaultBillingAddress.text = collection[@"billingAddress"][@"value"];
        _changeBillingButton.tag = [collection[@"billingAddress"][@"id"] integerValue];
        _defaultShippingAddress.text = collection[@"shippingAddress"][@"value"];
        _changeShippingButton.tag = [collection[@"shippingAddress"][@"id"] integerValue];
        
        if ([STR_SellerStatus isEqualToString:@"1"]) {
             _defaultPickupAddress.text = collection[@"pickupAddress"][@"value"];
            _changePickupButton.tag = [collection[@"pickupAddress"][@"id"] integerValue];
        }
        
        for (UIView *subUIView in _additionalAddressContainer.subviews) {
            [subUIView removeFromSuperview];
        }
        if(collection[@"additionalAddress"] != nil) {
            for(int i = 0; i < [collection[@"additionalAddress"] count]; i++){
                NSDictionary *dict = [collection[@"additionalAddress"] objectAtIndex:i];
                UILabel *address = [[UILabel alloc] initWithFrame:CGRectMake(8, prevLableY+8, (_additionalAddressContainer.frame.size.width)-16, 20)];
                NSString *addressText = [NSString stringWithFormat:@"%@",dict[@"value"]];
                [address setText:addressText];
                [address setLineBreakMode:NSLineBreakByWordWrapping];
                address.numberOfLines = 0;
                address.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
                address.textAlignment = NSTextAlignmentLeft;
                address.preferredMaxLayoutWidth = (_additionalAddressContainer.frame.size.width)-16;
                [address sizeToFit];
                [_additionalAddressContainer addSubview:address];
                
                prevLableY += address.frame.size.height;
                UILabel *editLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, prevLableY+10, (_additionalAddressContainer.frame.size.width), 20)];
                [editLabel setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
                [editLabel setText:@"Edit Address "];
                editLabel.userInteractionEnabled = YES;
                [editLabel sizeToFit];
                editLabel.tag = [dict[@"id"] integerValue];
                [_additionalAddressContainer addSubview:editLabel];
                
                UITapGestureRecognizer *tapGesture1 =
                [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAdditionalAddress:)];
                tapGesture1.numberOfTapsRequired = 1;
                [editLabel addGestureRecognizer:tapGesture1];
                
                UILabel *pipeLabel = [[UILabel alloc] initWithFrame:CGRectMake(editLabel.frame.size.width+5, prevLableY+10, (_additionalAddressContainer.frame.size.width), 20)];
                [pipeLabel setText:@" | "];
                [pipeLabel sizeToFit];
                [_additionalAddressContainer addSubview:pipeLabel];
                
                UILabel *deleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(editLabel.frame.size.width+5+ pipeLabel.frame.size.width, prevLableY+10, (_additionalAddressContainer.frame.size.width), 20)];
                [deleteLabel setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
                [deleteLabel setText:[globalObjectAddressBook.languageBundle localizedStringForKey:@"deleteAddress" value:@"" table:nil]];
                deleteLabel.userInteractionEnabled = YES;
                [deleteLabel sizeToFit];
                deleteLabel.tag = [dict[@"id"] integerValue];
                [_additionalAddressContainer addSubview:deleteLabel];
                
                UITapGestureRecognizer *tapGesture2 =
                [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAdditionalAddress:)];
                tapGesture2.numberOfTapsRequired = 1;
                [deleteLabel addGestureRecognizer:tapGesture2];
                prevLableY += editLabel.frame.size.height + 15;
            }
            
            if([collection[@"additionalAddress"] count] > 0)
                _additionalAddressContainerHeightConstraint.constant = prevLableY;
        }
    }
    else
        if([whichApiResultToProcess isEqual: @"deleteAddress"]){
            
            [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
            [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            whichApiResultToProcess = @"addressBook";
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    _mainContainerHeightConstraint.constant = 465+prevLableY+300;
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

- (void)deleteAdditionalAddress:(UITapGestureRecognizer *)tapGesture{
    if(tapGesture.state == UIGestureRecognizerStateEnded)  {
        UIAlertController *forgotpasswordAlert = [UIAlertController alertControllerWithTitle:[globalObjectAddressBook.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectAddressBook.languageBundle localizedStringForKey:@"sure" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          
                                                          NSMutableString *post = [NSMutableString string];
                                                          whichApiResultToProcess = @"deleteAddress";
                                                          preferences = [NSUserDefaults standardUserDefaults];
                                                          NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                          [post appendFormat:@"sessionId=%@&", savedSessionId];
                                                          [post appendFormat:@"addressId=%@", [NSString stringWithFormat:@"%d", (int)tapGesture.view.tag]];
                                                          [GlobalData alertController:currentWindow msg:[globalObjectAddressBook.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
                                                          [globalObjectAddressBook callHTTPPostMethod:post api:@"mobikulhttp/customer/addressDelete" signal:@"HttpPostMetod"];
                                                          globalObjectAddressBook.delegate = self;
                                                          //                                                          @try {
                                                          //                                                              NSString *prefSessionId = [preferences objectForKey:@"sessionId"];
                                                          //                                                              NSString *apiName = @"mobikulCustomerAddressDelete";
                                                          //                                                              whichApiResultToProcess = @"deleteAddress";
                                                          //                                                              NSString *nameSpace = @"urn:Magento";
                                                          //                                                              NSError *error;
                                                          //
                                                          //                                                              NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                                                          //                                                              [dictionary setObject:[NSString stringWithFormat:@"%d", (int)tapGesture.view.tag] forKey:@"addressId"];
                                                          //                                                              NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
                                                          //                                                              NSString *jsonString;
                                                          //                                                              if(!jsonData){
                                                          //                                                                  //NSLog(@"Got an error: %@", error);
                                                          //                                                              }
                                                          //                                                              else
                                                          //                                                                  jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                                          //
                                                          //                                                              NSString *parameters = [NSString stringWithFormat:@"<sessionId xsi:type=\"xsd:string\">%@</sessionId><attributes xsi:type=\"xsd:string\">%@</attributes>", prefSessionId, jsonString];
                                                          //                                                              alert = [UIAlertController alertControllerWithTitle: @"Please Wait..." message: nil preferredStyle: UIAlertControllerStyleAlert];
                                                          //                                                              NSString *envelope = [GlobalData createEnvelope:apiName  forNamespace:nameSpace forParameters:parameters currentView:self isAlertVisiblef:0 alertf:alert];
                                                          //                                                              isAlertVisible=1;
                                                          //                                                              [globalObjectAddressBook sendingApiData:envelope];
                                                          //                                                              globalObjectAddressBook.delegate = self;
                                                          //                                                          }
                                                          //                                                          @catch (NSException *e) {
                                                          //                                                              //NSLog(@"catching %@ reason %@", [e name], [e reason]);
                                                          //                                                          }
                                                      }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectAddressBook.languageBundle localizedStringForKey:@"no" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [forgotpasswordAlert addAction:okBtn];
        [forgotpasswordAlert addAction:noBtn];
        [self.parentViewController presentViewController:forgotpasswordAlert animated:YES completion:nil];
    }
}

- (void)editAdditionalAddress:(UITapGestureRecognizer *)tapGesture{
    if(tapGesture.state == UIGestureRecognizerStateEnded)  {
        addOrEdit = @"0";
        addressId = [NSString stringWithFormat:@"%d", (int)tapGesture.view.tag];
        [self performSegueWithIdentifier:@"addEditAddress" sender:self];
    }
}

- (IBAction)addNewAddress:(id)sender {
    addOrEdit = @"1";
    [self performSegueWithIdentifier:@"addEditAddress" sender:self];
}

- (IBAction)changeBillingAction:(id)sender {
    addOrEdit = @"0";
    addressId = [NSString stringWithFormat:@"%d", (int)_changeBillingButton.tag];
    [self performSegueWithIdentifier:@"addEditAddress" sender:self];
}

- (IBAction)changeShippingAction:(id)sender {
    addOrEdit = @"0";
    addressId = [NSString stringWithFormat:@"%d", (int)_changeShippingButton.tag];
    [self performSegueWithIdentifier:@"addEditAddress" sender:self];
}

- (IBAction)changePickupAction:(id)sender
{
    addOrEdit = @"0";
    addressId = [NSString stringWithFormat:@"%d", (int)_changePickupButton.tag];
    [self performSegueWithIdentifier:@"addEditAddress" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"addEditAddress"]) {
        AddEditAddress *destViewController = segue.destinationViewController;
        destViewController.addressId = addressId;
        destViewController.addOrEdit = addOrEdit;
    }
}

- (IBAction)unwindToAddressBook:(UIStoryboardSegue *)unwindSegue{}

@end
