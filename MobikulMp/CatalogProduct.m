//
//  Product.m
//  Mobikul
//
//  Created by Ratnesh on 11/04/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "CatalogProduct.h"
#import "GlobalData.h"
#import "ToastView.h"
#import "AddReview.h"
#import "CustomerLogin.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

@import AssetsLibrary;

GlobalData *globalObjectProduct;

@implementation CatalogProduct

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectProduct = [[GlobalData alloc] init];
    globalObjectProduct.delegate = self;
    [globalObjectProduct language];
    [self registerForKeyboardNotifications];
    configIdData = [[NSMutableArray alloc] init];
    configStoreValue = [[NSMutableArray alloc] init];
    firstConfigIdData = [[NSMutableArray alloc] init];
    configRowValueDict = [[NSMutableDictionary alloc] init];
    configPickerDictionary = [[NSMutableDictionary alloc] init];
    configPickerInfo = [[NSMutableDictionary alloc] init];
    isAlertVisible = 0;
    languageCode = [NSUserDefaults standardUserDefaults];
    whichApiDataToprocess = @"";addToCartMoved = @"0";
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.title = _productName;
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //     self.navigationItem.title = [globalObjectProduct.languageBundle localizedStringForKey:@"catalogProduct" value:@"" table:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(![_parentClass isEqualToString:@"home"] || [_parentClass isEqualToString:@"SearchProduct" ])
        _bottomMargin.constant = _bottomMargin.constant + 49;
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil){
        [self loginRequest];
        
    }
    else{
        [self callingHttppApi];
    }
}
-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectProduct.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    
    if([whichApiDataToprocess isEqual:@"addToWishlist"]){
        NSError *error = nil;
        if([mainCollection[@"configurableData"][@"attributes"] count] > 0){
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:selectedConfigProduct options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"super_attribute=%@&", jsonString];
        }
        if([mainCollection[@"customOptions"] count] > 0){
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:selectedCustomOption options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"options=%@&", jsonString];
        }
        if([mainCollection[@"groupedData"] count] > 0){
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:selectedGroupedProduct options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"super_group=%@&", jsonString];
        }
        if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"]){
            NSMutableArray *data = [[NSMutableArray alloc] init];
            for(id key in selectedDownloadableProduct) {
                id value = [selectedDownloadableProduct objectForKey:key];
                if(![value isEqualToString:@""]){
                    [data addObject:value];
                }}
            if([data count]>0){
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                [post appendFormat:@"links=%@&", jsonString];
            }
        }
        if([mainCollection[@"bundleOptions"] count] > 0){
            NSData *jsonBundleOptionData = [NSJSONSerialization dataWithJSONObject:selectedBundleProduct options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonBundleOptionString = [[NSString alloc] initWithData:jsonBundleOptionData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"bundle_option=%@&", jsonBundleOptionString];
            NSData *jsonBundleOptionQtData = [NSJSONSerialization dataWithJSONObject:selectedBundleProductQuantity options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonBundleOptionQtString = [[NSString alloc] initWithData:jsonBundleOptionQtData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"bundle_option_qty=%@&", jsonBundleOptionQtString];
        }
        UITextField *qtyField = [_mainBaseView viewWithTag:2000];
        [post appendFormat:@"qty=%@&", qtyField.text];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId != nil)
            [post appendFormat:@"customerId=%@&", customerId];
        [post appendFormat:@"productId=%@&", _productId];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectProduct callHTTPPostMethod:post api:@"mobikulhttp/catalog/addtoWishlist" signal:@"HttpPostMetod"];
    }
    else if([whichApiDataToprocess isEqual:@"addToCart"]){
        NSString *customerId = [preferences objectForKey:@"customerId"];
         if(customerId!=nil || customerId != NULL)
        [post appendFormat:@"customerId=%@&", customerId];
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId != nil)
            [post appendFormat:@"quoteId=%@&", quoteId];
        [post appendFormat:@"productId=%@&", _productId];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        
        UITextField *qtyField = [_mainBaseView viewWithTag:2000];
        NSError *error = nil;
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        if([mainCollection[@"configurableData"][@"attributes"] count] > 0){
            [params setObject:selectedConfigProduct forKey:@"super_attribute"];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"params=%@&", jsonString];
        }
        if([mainCollection[@"customOptions"] count] > 0){
            [params setObject:selectedCustomOption forKey:@"options"];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"params=%@&", jsonString];
        }
        if([mainCollection[@"groupedData"] count] > 0){
            [params setObject:selectedGroupedProduct forKey:@"super_group"];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"params=%@&", jsonString];
        }
        if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"]){
            [params setObject: selectedDownloadableProduct forKey:@"links"];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"params=%@&", jsonString];
        }
        if([mainCollection[@"bundleOptions"] count] > 0){
            [params setObject:selectedBundleProduct forKey:@"bundle_option"];
            [params setObject:selectedBundleProductQuantity forKey:@"bundle_option_qty"];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [post appendFormat:@"params=%@&", jsonString];
        }
        [post appendFormat:@"qty=%@", qtyField.text];
        [globalObjectProduct callHTTPPostMethod:post api:@"mobikulhttp/checkout/addtoCart" signal:@"HttpPostMetod"];
        globalObjectProduct.delegate = self;
        
    }
    else if([whichApiDataToprocess isEqual:@"contactSeller"]){
        NSString *sellerid = [preferences objectForKey:@"sellerId"];
         NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"subject=%@&", [contactUsData objectForKey:@"subject"]];
        [post appendFormat:@"query=%@&", [contactUsData objectForKey:@"query"]];
        [post appendFormat:@"sellerId=%@&", sellerid];
        [post appendFormat:@"customerEmail=%@&",[contactUsData objectForKey:@"email"]];
        [post appendFormat:@"customerName=%@", [contactUsData objectForKey:@"name"]];
        [globalObjectProduct callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/contactSeller" signal:@"HttpPostMetod"];
        globalObjectProduct.delegate = self;
    }
    else{
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"productId=%@&", _productId];
        NSString *customerId = [preferences objectForKey:@"customerId"];
        if(customerId !=NULL)
        [post appendFormat:@"customerId=%@&", customerId];
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId !=NULL)
            [post appendFormat:@"quoteId=%@&", quoteId];
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@", screenWidth];
        [globalObjectProduct callHTTPPostMethod:post api:@"mobikulhttp/catalog/getproductDetails" signal:@"HttpPostMetod"];
        globalObjectProduct.delegate = self;
    }
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Sample protocol delegate

-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}


-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    
    [globalObjectProduct callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectProduct.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void)callingApi{
    //    @try {
    //        NSString *prefSessionId = [preferences objectForKey:@"sessionId"];
    //        NSString *apiName = @"";
    //        NSString *nameSpace = @"urn:Magento";
    //        NSError *error;
    //        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    //        NSString *storeId = [preferences objectForKey:@"storeId"];
    //        [dictionary setObject:storeId forKey:@"storeId"];
    //        NSString *customerId = [preferences objectForKey:@"customerId"];
    //        if(customerId != nil)
    //            [dictionary setObject:customerId forKey:@"customerId"];
    //        NSString *quoteId = [preferences objectForKey:@"quoteId"];
    //        [dictionary setObject:_productId forKey:@"productId"];
    //        if(quoteId != nil)
    //            [dictionary setObject:quoteId forKey:@"quoteId"];
    //        if([whichApiDataToprocess isEqual:@"addToWishlist"]){
    //            apiName = @"mobikulCatalogAddtoWishlist";
    //        }
    //        else
    //            if([whichApiDataToprocess isEqual:@"addToCart"]){
    //                apiName = @"mobikulCheckoutAddtoCart";
    //                UITextField *qtyField = [_mainBaseView viewWithTag:2000];
    //                [dictionary setObject:qtyField.text forKey:@"qty"];
    //                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    //                if([mainCollection[@"configurableData"][@"attributes"] count] > 0){
    //                    [params setObject:selectedConfigProduct forKey:@"super_attribute"];
    //                    [dictionary setObject:params forKey:@"params"];
    //                }
    //                if([mainCollection[@"customOptions"] count] > 0){
    //                    [params setObject:selectedCustomOption forKey:@"options"];
    //                    [dictionary setObject:params forKey:@"params"];
    //                }
    //                if([mainCollection[@"groupedData"] count] > 0){
    //                    [params setObject:selectedGroupedProduct forKey:@"super_group"];
    //                    [dictionary setObject:params forKey:@"params"];
    //                }
    //                if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"]){
    //                    [params setObject: selectedDownloadableProduct forKey:@"links"];
    //                    [dictionary setObject:params forKey:@"params"];
    //                }
    //                if([mainCollection[@"bundleOptions"] count] > 0){
    //                    [params setObject:selectedBundleProduct forKey:@"bundle_option"];
    //                    [params setObject:selectedBundleProductQuantity forKey:@"bundle_option_qty"];
    //                    [dictionary setObject:params forKey:@"params"];
    //                }
    //            }
    //            else{
    //                apiName = @"mobikulCatalogGetproductDetails";
    //                NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    //                [dictionary setObject:screenWidth forKey:@"width"];
    //            }
    //        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    //        NSString *jsonString;
    //        if(!jsonData){
    //            NSLog(@"Got an error: %@", error);
    //        }
    //        else
    //            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //        NSString *parameters = [NSString stringWithFormat:@"<sessionId xsi:type=\"xsd:string\">%@</sessionId><attributes xsi:type=\"xsd:string\">%@</attributes>", prefSessionId, jsonString];
    //
    //        alert = [UIAlertController alertControllerWithTitle: [globalObjectProduct.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil] message: nil preferredStyle: UIAlertControllerStyleAlert];
    //        NSString *envelope = [GlobalData createEnvelope:apiName  forNamespace:nameSpace forParameters:parameters currentView:self isAlertVisiblef:0 alertf:alert];
    //        isAlertVisible = 1;
    //        [globalObjectProduct sendingApiData:envelope];
    //        globalObjectProduct.delegate = self;
    //    }
    //    @catch (NSException *e) {
    //        NSLog(@"catching %@ reason 2 %@", [e name], [e reason]);
    //    }
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([whichApiDataToprocess isEqual: @"addToWishlist"]){
        addToWishListCollection = collection;
        if([addToWishListCollection[@"status"] boolValue])
            [ToastView showToastInParentView:self.view withText:[globalObjectProduct.languageBundle localizedStringForKey:@"productAdded" value:@"" table:nil] withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:[globalObjectProduct.languageBundle localizedStringForKey:@"somethingWrong" value:@"" table:nil] withStatus:@"error" withDuaration:5.0];
    }
    else if([whichApiDataToprocess isEqual:@"contactSeller"]){
        contactSellerCollection = collection;
        if([contactSellerCollection objectForKey:@"message"]){
            
            [ToastView showToastInParentView:self.view withText:contactSellerCollection[@"message"] withStatus:@"success" withDuaration:5.0];
        }else{
            [ToastView showToastInParentView:self.view withText:contactSellerCollection[@"message"] withStatus:@"error" withDuaration:5.0];
        }
        
    }
    else
        if([whichApiDataToprocess isEqual: @"addToCart"]){
            addToCartCollection = collection;
            if([addToCartCollection objectForKey:@"quoteId"]){
                [preferences setObject:addToCartCollection[@"quoteId"] forKey:@"quoteId"];
                [preferences synchronize];
            }
            if([addToCartCollection objectForKey:@"cartCount"])
                if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                    [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
                else
                    [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
            if([addToCartCollection[@"error"] boolValue])
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"error" withDuaration:5.0];
            else
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"success" withDuaration:5.0];
        }
        else{
            float mainContainerY = 0;
            mainCollection = collection;
            NSDictionary *tempProductImageDict = [mainCollection[@"imageGallery"] objectAtIndex:0];
            float y = 0;
            UIImage *largeImage = [[UIImage alloc]init];
            largeImage = [imageCache objectForKey:tempProductImageDict[@"largeImage"]];
            if(!largeImage){
                NSData *largeImageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:tempProductImageDict[@"largeImage"]]];
                largeImage = [UIImage imageWithData:largeImageData];
            }
            float actualHeight = largeImage.size.height;
            float actualWidth = largeImage.size.width;
            float ratio = 0, heightToApply = 0, widthToApply = 0;
            if(actualWidth <= actualHeight){
                ratio = actualHeight/actualWidth;
                heightToApply = actualWidth;
                widthToApply = actualWidth/ratio;
            }
            else{
                heightToApply = actualHeight;
                widthToApply = actualWidth;
            }
            
            UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2)-(widthToApply/2)), y, widthToApply, heightToApply)];
            [_mainView addSubview:productImage];
            productImage.userInteractionEnabled = YES;
            productImage.tag = 1000;
            y += heightToApply;
            mainContainerY += y;
            productImage.image = [UIImage imageNamed:@"placeholder.png"];
            if(largeImage)
                productImage.image = largeImage;
            else{
                [_queue addOperationWithBlock:^{
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:tempProductImageDict[@"largeImage"]]];
                    UIImage *largeImage = [UIImage imageWithData: imageData];
                    if(largeImage){
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            productImage.image = largeImage;
                        }];
                        [imageCache setObject:largeImage forKey:tempProductImageDict[@"largeImage"]];
                    }
                }];
            }
            if([mainCollection[@"imageGallery"] count] > 1){
                UIScrollView *subImageScroller = [[UIScrollView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH, (SCREEN_WIDTH/4)+10)];
                [subImageScroller setBackgroundColor:[UIColor clearColor]];
                [subImageScroller setShowsHorizontalScrollIndicator:NO];
                [subImageScroller setShowsVerticalScrollIndicator:NO];
                subImageScroller.scrollEnabled = YES;
                subImageScroller.userInteractionEnabled = YES;
                subImageScroller.contentSize = CGSizeMake((((SCREEN_WIDTH/3)+5)*[mainCollection[@"imageGallery"] count])+5, (SCREEN_WIDTH/4)+10);
                [_mainView addSubview:subImageScroller];
                mainContainerY += (SCREEN_WIDTH/4)+10;
                float x = 5;
                y = 5;
                for(int i=0; i<[mainCollection[@"imageGallery"] count]; i++) {
                    NSDictionary *productImageDict = [mainCollection[@"imageGallery"] objectAtIndex:i];
                    UIImage *image = [[UIImage alloc]init];
                    image = [imageCache objectForKey:productImageDict[@"smallImage"]];
                    if(!image){
                        NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:productImageDict[@"smallImage"]]];
                        image = [UIImage imageWithData:imageData];
                    }
                    float actualHeight = image.size.height;
                    float actualWidth = image.size.width;
                    float expectedHeight = SCREEN_WIDTH/4;
                    float ratio = 0, heightToApply = 0, widthToApply = 0, innerY = 0;
                    if(actualHeight > expectedHeight){
                        ratio = actualHeight/expectedHeight;
                        heightToApply = expectedHeight;
                        widthToApply = actualWidth/ratio;
                    }
                    else{
                        heightToApply = actualHeight;
                        widthToApply = actualWidth;
                        innerY = ((expectedHeight/2)-(actualHeight/2));
                    }
                    UIView *thumbnailBlock = [[UIView alloc]initWithFrame:CGRectMake(x, y, SCREEN_WIDTH/3, expectedHeight)];
                    thumbnailBlock.userInteractionEnabled = YES;
                    thumbnailBlock.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
                    thumbnailBlock.layer.borderWidth = 2.0f;
                    thumbnailBlock.tag = i;
                    UITapGestureRecognizer *changeImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeImage:)];
                    changeImageGesture.numberOfTapsRequired = 1;
                    [thumbnailBlock addGestureRecognizer:changeImageGesture];
                    [thumbnailBlock setBackgroundColor:[UIColor whiteColor]];
                    float innerX = ((SCREEN_WIDTH/3)/2-(widthToApply/2));
                    UIImageView *thumbnailImage = [[UIImageView alloc] initWithFrame:CGRectMake(innerX, innerY, widthToApply, heightToApply)];
                    [thumbnailBlock addSubview:thumbnailImage];
                    if(image)
                        thumbnailImage.image = image;
                    else{
                        [_queue addOperationWithBlock:^{
                            NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:productImageDict[@"smallImage"]]];
                            UIImage *image = [UIImage imageWithData: imageData];
                            if(image){
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    thumbnailImage.image = image;
                                }];
                                [imageCache setObject:image forKey:productImageDict[@"smallImage"]];
                            }
                        }];
                    }
                    [subImageScroller addSubview:thumbnailBlock];
                    x += (SCREEN_WIDTH/3)+5;
                }
            }
            
            UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 30)];
            [productName setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
            [productName setBackgroundColor:[UIColor clearColor]];
            [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:26.0f]];
            [productName setText:[mainCollection[@"name"] uppercaseString]];
            [_mainView addSubview:productName];
            
            mainContainerY += 35;
            UILabel *stockStatus = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 25)];
            if([mainCollection[@"isAvailable"] boolValue])
                [stockStatus setTextColor:[GlobalData colorWithHexString:@"11B400"]];
            else
                [stockStatus setTextColor:[GlobalData colorWithHexString:@"DA4453"]];
            [stockStatus setBackgroundColor:[UIColor clearColor]];
            [stockStatus setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [stockStatus setText:[mainCollection[@"availability"] uppercaseString]];
            [_mainView addSubview:stockStatus];
            
            mainContainerY += 25;
            UILabel *shortDescription = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 20)];
            [shortDescription setText:mainCollection[@"shortDescription"]];
            [shortDescription setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [shortDescription setLineBreakMode:NSLineBreakByWordWrapping];
            shortDescription.numberOfLines = 0;
            shortDescription.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
            shortDescription.textAlignment = NSTextAlignmentLeft;
            shortDescription.preferredMaxLayoutWidth = _mainView.frame.size.width-10;
            [shortDescription sizeToFit];
            [_mainView addSubview:shortDescription];
            
            mainContainerY += shortDescription.frame.size.height+5;
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(5,mainContainerY,120,24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            float averageRating = 0;
            if([mainCollection[@"ratingData"] count] > 0){
                for(int i=0; i<[mainCollection[@"ratingData"] count]; i++) {
                    NSDictionary *ratingDict = [mainCollection[@"ratingData"] objectAtIndex:i];
                    averageRating += [ratingDict[@"ratingValue"] integerValue];
                }
                averageRating = averageRating/[mainCollection[@"ratingData"] count];
            }
            double percent = 24 * averageRating;
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [_mainView addSubview:ratingContainer];
            
            mainContainerY += 29;
            if([mainCollection[@"reviewList"] count] > 0){
                NSDictionary *reviewCountAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18]};
                CGSize reviewCountStringSize = [[NSString stringWithFormat:@"Total %d Review(s)", (int)[mainCollection[@"reviewList"] count]] sizeWithAttributes:reviewCountAttributes];
                CGFloat reviewCountWidth = reviewCountStringSize.width;
                UILabel *reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, reviewCountWidth, 25)];
                [reviewCountLabel setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
                [reviewCountLabel setText:[NSString stringWithFormat:@"Total %d Review(s)", (int)[mainCollection[@"reviewList"] count]]];
                [reviewCountLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
                [_mainView addSubview:reviewCountLabel];
                mainContainerY += 30;
            }
            
            NSDictionary *reviewAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18]};
            CGSize reviewStringSize = [@"Add Your Review" sizeWithAttributes:reviewAttributes];
            CGFloat addReviewWidth = reviewStringSize.width;
            UILabel *addReviewBtn = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, addReviewWidth, 20)];
            [addReviewBtn setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
            [addReviewBtn setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [addReviewBtn setText:[globalObjectProduct.languageBundle localizedStringForKey:@"yourReview" value:@"" table:nil]];
            [addReviewBtn setBackgroundColor:[UIColor clearColor]];
            addReviewBtn.userInteractionEnabled = YES;
            [_mainView addSubview:addReviewBtn];
            UITapGestureRecognizer *addReviewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addReview:)];
            addReviewGesture.numberOfTapsRequired = 1;
            [addReviewBtn addGestureRecognizer:addReviewGesture];
            
            NSDictionary *pipeAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18]};
            CGSize pipeStringSize = [@" | " sizeWithAttributes:pipeAttributes];
            CGFloat pipeWidth = pipeStringSize.width;
            UILabel *pipeLabel = [[UILabel alloc] initWithFrame:CGRectMake(addReviewWidth+5, mainContainerY, pipeWidth, 20)];
            [pipeLabel setText:@" | "];
            [pipeLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [pipeLabel setBackgroundColor:[UIColor clearColor]];
            [_mainView addSubview:pipeLabel];
            
            NSDictionary *addToWishlistAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18]};
            CGSize addToWishlistStringSize = [@"Add To Wishlist" sizeWithAttributes:addToWishlistAttributes];
            CGFloat addToWishlistWidth = addToWishlistStringSize.width;
            UILabel *addToWishlistBtn = [[UILabel alloc] initWithFrame:CGRectMake(addReviewWidth+pipeWidth+5, mainContainerY, addToWishlistWidth, 20)];
            [addToWishlistBtn setTextColor: [GlobalData colorWithHexString:@"268ED7"]];
            [addToWishlistBtn setText:[globalObjectProduct.languageBundle localizedStringForKey:@"addWishlist" value:@"" table:nil]];
            addToWishlistBtn.userInteractionEnabled = YES;
            [addToWishlistBtn setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [addToWishlistBtn setBackgroundColor:[UIColor clearColor]];
            addToWishlistBtn.tag = 3000;
            [_mainView addSubview:addToWishlistBtn];
            UITapGestureRecognizer *addToWishlistGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addToWishlist:)];
            addToWishlistGesture.numberOfTapsRequired = 1;
            [addToWishlistBtn addGestureRecognizer:addToWishlistGesture];
            
            mainContainerY += 25;
            UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
            [hr setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
            [_mainView addSubview:hr];
            mainContainerY += 6;
            
            if([mainCollection[@"typeId"] isEqual:@"grouped"]){
                //            NSDictionary *preStringAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                //            CGSize preStringSize = [@"Starting at:" sizeWithAttributes:preStringAttributes];
                //            CGFloat preStringWidth = preStringSize.width;
                //            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, preStringWidth, 25)];
                //            [preString setTextColor:[self colorWithHexString:@"555555"]];
                //            [preString setBackgroundColor:[UIColor clearColor]];
                //            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                //            [preString setText:@"Starting at:"];
                //            [_mainView addSubview:preString];
                //
                //            NSDictionary *priceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                //            CGSize priceStringSize = [mainCollection[@"groupedPrice"] sizeWithAttributes:priceAttributes];
                //            CGFloat priceStringWidth = priceStringSize.width;
                //            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(preStringWidth+5, mainContainerY, priceStringWidth, 25)];
                //            [price setTextColor:[self colorWithHexString:@"268ED7"]];
                //            [price setBackgroundColor:[UIColor clearColor]];
                //            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                //            [price setText:mainCollection[@"groupedPrice"]];
                //            [_mainView addSubview:price];
            }
            else
                if([mainCollection[@"typeId"] isEqual:@"bundle"]){
                    NSDictionary *fromAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize fromStringSize = [@"From:" sizeWithAttributes:fromAttributes];
                    CGFloat fromStringWidth = fromStringSize.width;
                    UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, fromStringWidth, 25)];
                    [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [from setBackgroundColor:[UIColor clearColor]];
                    [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [from setText:[globalObjectProduct.languageBundle localizedStringForKey:@"from" value:@"" table:nil]];
                    [_mainView addSubview:from];
                    
                    NSDictionary *minPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize minPriceStringSize = [mainCollection[@"formatedMinPrice"] sizeWithAttributes:minPriceAttributes];
                    CGFloat minPriceStringWidth = minPriceStringSize.width;
                    UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(fromStringWidth+5, mainContainerY, minPriceStringWidth, 25)];
                    [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [minPrice setBackgroundColor:[UIColor clearColor]];
                    [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [minPrice setText:mainCollection[@"formatedMinPrice"]];
                    [_mainView addSubview:minPrice];
                    
                    NSDictionary *toAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize toStringSize = [@"To:" sizeWithAttributes:toAttributes];
                    CGFloat toStringWidth = toStringSize.width;
                    UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(fromStringWidth+minPriceStringWidth+5, mainContainerY, toStringWidth, 25)];
                    [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [to setBackgroundColor:[UIColor clearColor]];
                    [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [to setText:[globalObjectProduct.languageBundle localizedStringForKey:@"to" value:@"" table:nil]];
                    [_mainView addSubview:to];
                    
                    NSDictionary *maxPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize maxPriceStringSize = [mainCollection[@"formatedMaxPrice"] sizeWithAttributes:maxPriceAttributes];
                    CGFloat maxPriceStringWidth = maxPriceStringSize.width;
                    UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(fromStringWidth+minPriceStringWidth+toStringWidth+5, mainContainerY, maxPriceStringWidth, 25)];
                    [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [maxPrice setBackgroundColor:[UIColor clearColor]];
                    [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [maxPrice setText:mainCollection[@"formatedMaxPrice"]];
                    [_mainView addSubview:maxPrice];
                }
                else{
                    if(![mainCollection[@"specialPrice"] isEqual:[NSNull null]] && [mainCollection[@"isInRange"] boolValue]){
                        NSDictionary *regularPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                        CGSize regularPriceStringSize = [mainCollection[@"formatedPrice"] sizeWithAttributes:regularPriceAttributes];
                        CGFloat regularPriceStringWidth = regularPriceStringSize.width;
                        UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, regularPriceStringWidth, 25)];
                        [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [regularprice setBackgroundColor:[UIColor clearColor]];
                        [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:mainCollection[@"formatedPrice"]];
                        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                        [regularprice setAttributedText:attributeString];
                        [_mainView addSubview:regularprice];
                        
                        NSDictionary *specialPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                        CGSize specialPriceStringSize = [mainCollection[@"formatedSpecialPrice"] sizeWithAttributes:specialPriceAttributes];
                        CGFloat specialPriceStringWidth = specialPriceStringSize.width;
                        UILabel *specialPrice = [[UILabel alloc] initWithFrame:CGRectMake(regularPriceStringWidth+5, mainContainerY, specialPriceStringWidth, 25)];
                        [specialPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [specialPrice setBackgroundColor:[UIColor clearColor]];
                        [specialPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                        [specialPrice setText:mainCollection[@"formatedSpecialPrice"]];
                        [_mainView addSubview:specialPrice];
                    }
                    else{
                        NSDictionary *priceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                        CGSize priceStringSize = [mainCollection[@"formatedPrice"] sizeWithAttributes:priceAttributes];
                        CGFloat priceStringWidth = priceStringSize.width;
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, priceStringWidth, 25)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                        [price setText:mainCollection[@"formatedPrice"]];
                        [_mainView addSubview:price];
                    }
                }
            mainContainerY += 30;
            if([mainCollection[@"tierPrices"] count] > 0){
                for(int i=0; i<[mainCollection[@"tierPrices"] count]; i++) {
                    NSDictionary *tierPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]};
                    CGSize tierPriceStringSize = [mainCollection[@"tierPrices"][i] sizeWithAttributes:tierPriceAttributes];
                    CGFloat tierPriceStringWidth = tierPriceStringSize.width;
                    UILabel *tierPrice = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, tierPriceStringWidth, 25)];
                    [tierPrice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [tierPrice setBackgroundColor:[GlobalData colorWithHexString:@"FBF4DE"]];
                    [tierPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:21.0f]];
                    [tierPrice setText:mainCollection[@"tierPrices"][i]];
                    [_mainView addSubview:tierPrice];
                    mainContainerY += 25;
                }
                mainContainerY -= 25;
            }
            else
                mainContainerY -= 30;
            if(![mainCollection[@"typeId"] isEqual:@"grouped"]){
                mainContainerY += 30;
                UIView *hr2 = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
                [hr2 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                [_mainView addSubview:hr2];
                mainContainerY += 21;
                
            }
            
            
            if(collection[@"seller"]){
                
            
            UIView *averageRatingContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY, SCREEN_WIDTH-10, 200)];
            averageRatingContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            averageRatingContainer.layer.borderWidth = 2.0f;
            [_mainView addSubview:averageRatingContainer];
            
                
            NSDictionary *reqAttributesforSoldBy = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};
            CGSize reqStringSizeSoldBy = [collection[@"seller"][@"label"] sizeWithAttributes:reqAttributesforSoldBy];
            UILabel *soldBy = [[UILabel alloc] initWithFrame:CGRectMake(5, 5,reqStringSizeSoldBy.width, 25)];
            [soldBy setTextColor:[UIColor blackColor]];
           // [soldBy setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
            [soldBy setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
            [soldBy setText:collection[@"seller"][@"label"]];
            [averageRatingContainer addSubview:soldBy];
             
            NSDictionary *reqAttributesforSoldByTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeSoldByTitle = [collection[@"seller"][@"title"] sizeWithAttributes:reqAttributesforSoldByTitle];
            UILabel *soldByTitle = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeSoldBy.width +10, 5,reqStringSizeSoldByTitle.width, 25)];
            [soldByTitle setTextColor:[GlobalData colorWithHexString:@"00FF00"]];
           // [soldByTitle setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
            [soldByTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [soldByTitle setText:collection[@"seller"][@"title"]];
            [averageRatingContainer addSubview:soldByTitle];
            
                
            NSDictionary *reqAttributesforRatingValue = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeRatingValue = [collection[@"seller"][@"rating"] sizeWithAttributes:reqAttributesforRatingValue];
            UILabel *ratingValue = [[UILabel alloc] initWithFrame:CGRectMake(10, 30,reqStringSizeRatingValue.width+20, 35)];
            [ratingValue setTextColor:[UIColor blackColor]];
            [ratingValue setBackgroundColor:[GlobalData colorWithHexString:@"00FF00"]];
            [ratingValue setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [ratingValue setText:collection[@"seller"][@"rating"]];
            [ratingValue setTextAlignment:NSTextAlignmentCenter];
            [averageRatingContainer addSubview:ratingValue];
                
                
             
            NSDictionary *reqAttributesforRatingDesc = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeRatingDesc = [collection[@"seller"][@"averageRatingTitle"] sizeWithAttributes:reqAttributesforRatingDesc];
            UILabel *ratingDesc = [[UILabel alloc] initWithFrame:CGRectMake(10, 70,reqStringSizeRatingDesc.width, 25)];
            [ratingDesc setTextColor:[UIColor blackColor]];
           // [ratingDesc setBackgroundColor:[GlobalData colorWithHexString:@"00FF00"]];
            [ratingDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [ratingDesc setText:collection[@"seller"][@"averageRatingTitle"]];
            [ratingDesc setTextAlignment:NSTextAlignmentCenter];
            [averageRatingContainer addSubview:ratingDesc];
             
            float max = 0;
                
            for(int i=0;i<[collection[@"seller"][@"averageRating"] count];i++){
                    NSDictionary *attributesvalues = [collection[@"seller"][@"averageRating"] objectAtIndex:i];
            NSDictionary *reqAttributesforRatingDesc = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeRatingDesc = [attributesvalues[@"label"] sizeWithAttributes:reqAttributesforRatingDesc];
            float t = reqStringSizeRatingDesc.width;
            if(t>max)
                max = t;
            }
            
            float Y = 100;
            for(int i=0;i<[collection[@"seller"][@"averageRating"] count];i++){
            NSDictionary *attributesvalues = [collection[@"seller"][@"averageRating"] objectAtIndex:i];
            UILabel *ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,max, 25)];
            [ratingLabel setTextColor:[UIColor blackColor]];
            [ratingLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [ratingLabel setText:attributesvalues[@"label"]];
            [averageRatingContainer addSubview:ratingLabel];
                
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake( max+15,Y,120,24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            
            double percent = 24 * [attributesvalues[@"value"] doubleValue];
            
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [averageRatingContainer addSubview:ratingContainer];
    
            Y +=30;
            }
                
            NSDictionary *reqAttributesForContactUs = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeContactUs = [@" Contact Us" sizeWithAttributes:reqAttributesForContactUs];
            CGFloat reqStringWidthContactUs = reqStringSizeContactUs.width;
            UILabel *contactUsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, Y + 5, reqStringWidthContactUs, 20)];
            [contactUsLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
            [contactUsLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
            [contactUsLabel setText:@" Contact Us"];
            [contactUsLabel setUserInteractionEnabled:YES];
            [averageRatingContainer addSubview:contactUsLabel];
        
            UITapGestureRecognizer *openContactUsWindow = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contactUsWindow:)];
            openContactUsWindow.numberOfTapsRequired = 1;
            [contactUsLabel addGestureRecognizer:openContactUsWindow];
                
            CGRect  averageContainerNewFrame = averageRatingContainer.frame;
            averageContainerNewFrame.size.height = Y +30;
            averageRatingContainer.frame = averageContainerNewFrame;
                
                
            NSString *sellerId = collection[@"seller"][@"id"];
            [preferences setObject:sellerId  forKey:@"sellerId"];
                
            mainContainerY +=Y+40;
            }
            
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////                         ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////// Creating Configurable   ////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////                         ////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            if([mainCollection[@"configurableData"][@"attributes"] count] > 0){
                configurableContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 100)];
                configurableContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
                configurableContainer.tag = 7000;
                configurableContainer.layer.borderWidth = 2.0f;
                [_mainView addSubview:configurableContainer];
                float configurableInternalY = 5;
                
                NSDictionary *attributesvalues1 = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:0];
                NSDictionary *configLabelDict=[[NSDictionary alloc] init];
                configLabelDict = [attributesvalues1[@"options"] objectAtIndex:0];
                for(int i=0; i<[mainCollection[@"configurableData"][@"attributes"] count]; i++) {
                    NSDictionary *attributesvalues = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:i];
                    NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                    CGSize titleStringSize = [attributesvalues[@"label"] sizeWithAttributes:titleAttributes];
                    CGFloat titleStringWidth = titleStringSize.width;
                    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, configurableInternalY, titleStringWidth+2, 23)];
                    [title setTextColor:[GlobalData colorWithHexString:@"000000"]];
                    [title setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                    [title setText:attributesvalues[@"label"]];
                    [configurableContainer addSubview:title];
                    UILabel *requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(titleStringWidth+5, configurableInternalY, 15, 23)];
                    [requiredStar setText:@"*"];
                    [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
                    [configurableContainer addSubview:requiredStar];
                    configurableInternalY += 35;
                    
                    if([attributesvalues[@"options"] count] > 0){
                        pickerValue = @"Configurable";
                        configPickerCount++;
                        UIPickerView *dataSelect1 = [[UIPickerView alloc] initWithFrame:CGRectMake(5, configurableInternalY,configurableContainer.frame.size.width-10, 50)];
                        dataSelect1.tag = i+1;
                        dataSelect1.showsSelectionIndicator = YES;
                        dataSelect1.hidden = NO;
                        dataSelect1.delegate = self;
                        [configurableContainer addSubview:dataSelect1];
                        configurableInternalY += 60;
                    }
                }
                
                CGRect newFrame1 = configurableContainer.frame;
                newFrame1.size.height = configurableInternalY;
                configurableContainer.frame = newFrame1;
                mainContainerY += (configurableInternalY + 21);
            }
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////                         ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////// Creating Custom Options ////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////                         ////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if([mainCollection[@"customOptions"] count] > 0){
                optionValues = [[NSMutableArray alloc]init];
                for(int i=0; i<[mainCollection[@"customOptions"] count]; i++) {
                    NSDictionary *customOptionDict = [mainCollection[@"customOptions"] objectAtIndex:i];
                    if([customOptionDict[@"optionValues"] count] > 0){
                        NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[customOptionDict[@"optionValues"] allKeys]];
                        [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
                        [optionValues addObject:sortedArray];
                    }
                    else
                        [optionValues addObject:@""];
                }
                pickerValue = @"custom";
                customOptionContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 100)];
                customOptionContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
                customOptionContainer.tag = 4000;
                customOptionContainer.layer.borderWidth = 2.0f;
                [_mainView addSubview:customOptionContainer];
                float customOptionInternalY = 5;
                NSDictionary *reqAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                CGSize reqStringSize = [@"* Required Fields" sizeWithAttributes:reqAttributes];
                CGFloat reqStringWidth = reqStringSize.width;
                UILabel *required = [[UILabel alloc] initWithFrame:CGRectMake((_mainView.frame.size.width-20)-reqStringWidth, customOptionInternalY, reqStringWidth, 25)];
                [required setTextColor:[GlobalData colorWithHexString:@"df280a"]];
                [required setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [required setText:[globalObjectProduct.languageBundle localizedStringForKey:@"requireFields" value:@"" table:nil]];
                [customOptionContainer addSubview:required];
                customOptionInternalY += 21;
                UILabel *requiredStar;
                for(int i=0; i<[mainCollection[@"customOptions"] count]; i++) {
                    NSDictionary *customOptionDict = [mainCollection[@"customOptions"] objectAtIndex:i];
                    NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                    CGSize titleStringSize = [customOptionDict[@"title"] sizeWithAttributes:titleAttributes];
                    CGFloat titleStringWidth = titleStringSize.width;
                    float fieldX = 5;
                    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(fieldX, customOptionInternalY, titleStringWidth, 23)];
                    [title setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [title setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                    [title setText:customOptionDict[@"title"]];
                    [customOptionContainer addSubview:title];
                    fieldX += titleStringWidth;
                    if([customOptionDict[@"is_require"] isEqualToString:@"1"]){
                        fieldX += 3;
                        requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(fieldX, customOptionInternalY, 10, 23)];
                        [requiredStar setText:@"*"];
                        [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
                        [customOptionContainer addSubview:requiredStar];
                    }
                    if([customOptionDict[@"unformated_price"] integerValue] > 0){
                        fieldX += 10;
                        NSString *additionalPriceString = @"";
                        if([customOptionDict[@"price_type"] isEqualToString:@"fixed"])
                            additionalPriceString = [NSString stringWithFormat:@"+%@", customOptionDict[@"formated_price"]];
                        else{
                            float price = [mainCollection[@"price"] floatValue];
                            price = (price/100)*[customOptionDict[@"unformated_price"] floatValue];
                            additionalPriceString = [NSString stringWithFormat:@"+%.02f", price];
                        }
                        NSDictionary *priceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18.0f]};
                        CGSize priceStringSize = [additionalPriceString sizeWithAttributes:priceAttributes];
                        CGFloat priceStringWidth = priceStringSize.width;
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(fieldX, customOptionInternalY, priceStringWidth, 23)];
                        [price setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [price setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:18.0f]];
                        [price setText:additionalPriceString];
                        [customOptionContainer addSubview:price];
                    }
                    customOptionInternalY += 25;
                    if([customOptionDict[@"type"] isEqualToString:@"field"]){
                        UITextField *fieldField = [[UITextField alloc] initWithFrame:CGRectMake(5, customOptionInternalY, customOptionContainer.frame.size.width-10, 35)];
                        fieldField.font = [UIFont fontWithName:@"Trebuchet MS" size:16.0f];
                        fieldField.textColor = [UIColor blackColor];
                        fieldField.tag = i+1;
                        fieldField.layer.borderWidth = 1.0f;
                        fieldField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                        fieldField.textAlignment = NSTextAlignmentLeft;
                        fieldField.borderStyle = UITextBorderStyleRoundedRect;
                        [customOptionContainer addSubview:fieldField];
                        customOptionInternalY += 40;
                        if([customOptionDict[@"max_characters"] integerValue] > 0){
                            customOptionInternalY -= 5;
                            NSDictionary *fieldHintAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]};
                            CGSize fieldHintStringSize = [[NSString stringWithFormat:@"Maximum number of characters: %@", customOptionDict[@"max_characters"]] sizeWithAttributes:fieldHintAttributes];
                            CGFloat fieldHintStringWidth = fieldHintStringSize.width;
                            UILabel *fieldHint = [[UILabel alloc] initWithFrame:CGRectMake(5, customOptionInternalY, fieldHintStringWidth, 20)];
                            [fieldHint setTextColor:[GlobalData colorWithHexString:@"555555"]];
                            [fieldHint setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                            [fieldHint setText:[NSString stringWithFormat:@"Maximum number of characters: %@", customOptionDict[@"max_characters"]]];
                            [customOptionContainer addSubview:fieldHint];
                            customOptionInternalY += 25;
                        }
                        customOptionInternalY += 10;
                    }
                    else
                        if([customOptionDict[@"type"] isEqualToString:@"area"]){
                            UITextView *textArea = [[UITextView alloc] initWithFrame:CGRectMake(5, customOptionInternalY, customOptionContainer.frame.size.width-10, 100)];
                            textArea.layer.borderWidth = 1.0f;
                            textArea.textColor = [UIColor blackColor];
                            textArea.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                            textArea.font = [UIFont fontWithName: @"Trebuchet MS" size: 16.0f];
                            textArea.tag = i+1;
                            textArea.delegate = self;
                            [customOptionContainer addSubview:textArea];
                            customOptionInternalY += 105;
                            if([customOptionDict[@"max_characters"] integerValue] > 0){
                                customOptionInternalY -= 5;
                                NSDictionary *fieldHintAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]};
                                CGSize fieldHintStringSize = [[NSString stringWithFormat:@"Maximum number of characters: %@", customOptionDict[@"max_characters"]] sizeWithAttributes:fieldHintAttributes];
                                CGFloat fieldHintStringWidth = fieldHintStringSize.width;
                                UILabel *fieldHint = [[UILabel alloc] initWithFrame:CGRectMake(5, customOptionInternalY, fieldHintStringWidth, 20)];
                                [fieldHint setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                [fieldHint setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                                [fieldHint setText:[NSString stringWithFormat:@"Maximum number of characters: %@", customOptionDict[@"max_characters"]]];
                                [customOptionContainer addSubview:fieldHint];
                                customOptionInternalY += 25;
                            }
                            customOptionInternalY += 10;
                        }
                        else
                            if([customOptionDict[@"type"] isEqualToString:@"file"]){
                                UIButton *filePickerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                                filePickerBtn.backgroundColor = [GlobalData colorWithHexString:@"268ED7"];
                                [filePickerBtn.titleLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                                [filePickerBtn setTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"chooseFile" value:@"" table:nil] forState:UIControlStateNormal];
                                filePickerBtn.frame = CGRectMake(5, customOptionInternalY, 150, 35);
                                [customOptionContainer addSubview:filePickerBtn];
                                customOptionInternalY += 35;
                                if(customOptionDict[@"file_extension"] != [NSNull null]){
                                    NSDictionary *fileExtensionAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]};
                                    CGSize fileExtensionStringSize = [[NSString stringWithFormat:@"Allowed file extensions to upload: %@", customOptionDict[@"file_extension"]] sizeWithAttributes:fileExtensionAttributes];
                                    CGFloat fileExtensionStringWidth = fileExtensionStringSize.width;
                                    UILabel *fileExtension = [[UILabel alloc] initWithFrame:CGRectMake(5, customOptionInternalY, fileExtensionStringWidth, 17)];
                                    [fileExtension setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                    [fileExtension setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                                    [fileExtension setText:[NSString stringWithFormat:@"Allowed file extensions to upload: %@", customOptionDict[@"file_extension"]]];
                                    [customOptionContainer addSubview:fileExtension];
                                    customOptionInternalY += 17;
                                }
                                if([customOptionDict[@"image_size_x"] integerValue] > 0){
                                    NSDictionary *fileWidthAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]};
                                    CGSize fileWidthStringSize = [[NSString stringWithFormat:@"Maximum image width: %@ px.", customOptionDict[@"image_size_x"]] sizeWithAttributes:fileWidthAttributes];
                                    CGFloat fileWidthStringWidth = fileWidthStringSize.width;
                                    UILabel *fileWidth = [[UILabel alloc] initWithFrame:CGRectMake(5, customOptionInternalY, fileWidthStringWidth, 17)];
                                    [fileWidth setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                    [fileWidth setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                                    [fileWidth setText:[NSString stringWithFormat:@"Maximum image width: %@ px.", customOptionDict[@"image_size_x"]]];
                                    [customOptionContainer addSubview:fileWidth];
                                    customOptionInternalY += 17;
                                }
                                if([customOptionDict[@"image_size_y"] integerValue] > 0){
                                    NSDictionary *fileHeightAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]};
                                    CGSize fileHeightStringSize = [[NSString stringWithFormat:@"Maximum image height: %@ px.", customOptionDict[@"image_size_y"]] sizeWithAttributes:fileHeightAttributes];
                                    CGFloat fileHeightStringWidth = fileHeightStringSize.width;
                                    UILabel *fileHeight = [[UILabel alloc] initWithFrame:CGRectMake(5, customOptionInternalY, fileHeightStringWidth, 17)];
                                    [fileHeight setTextColor:[GlobalData colorWithHexString:@"555555"]];
                                    [fileHeight setFont:[UIFont fontWithName:@"Trebuchet MS" size:14.0f]];
                                    [fileHeight setText:[NSString stringWithFormat:@"Maximum image height: %@ px.", customOptionDict[@"image_size_y"]]];
                                    [customOptionContainer addSubview:fileHeight];
                                    customOptionInternalY += 17;
                                }
                                customOptionInternalY += 10;
                            }
                            else
                                if([customOptionDict[@"type"] isEqualToString:@"drop_down"] || [customOptionDict[@"type"] isEqualToString:@"radio"]){
                                    UIPickerView *dropDown = [[UIPickerView alloc] initWithFrame:CGRectMake(5, customOptionInternalY, customOptionContainer.frame.size.width-10, 100)];
                                    dropDown.tag = i+1;
                                    dropDown.showsSelectionIndicator = YES;
                                    dropDown.hidden = NO;
                                    dropDown.delegate = self;
                                    dropDown.dataSource = self;
                                    [customOptionContainer addSubview:dropDown];
                                    customOptionInternalY += 105;
                                }
                                else
                                    if([customOptionDict[@"type"] isEqualToString:@"checkbox"] || [customOptionDict[@"type"] isEqualToString:@"multiple"]){
                                        float checkBoxContainerInternalY = 5;
                                        UIView *checkBoxContainer = [[UIView alloc] initWithFrame:CGRectMake(5, customOptionInternalY, customOptionContainer.frame.size.width-10, ([customOptionDict[@"optionValues"] count]*35)+5)];
                                        checkBoxContainer.tag = i+1;
                                        [customOptionContainer addSubview:checkBoxContainer];
                                        for(id key in customOptionDict[@"optionValues"]){
                                            UISwitch *checkBox = [[UISwitch alloc] initWithFrame:CGRectMake(5, checkBoxContainerInternalY, 55, 32)];
                                            [checkBox setOn:NO];
                                            NSInteger checkBocTag = [key integerValue];
                                            checkBox.tag = checkBocTag;
                                            [checkBoxContainer addSubview:checkBox];
                                            NSString *additionalPriceString = @"";
                                            if([customOptionDict[@"optionValues"][key][@"price_type"] isEqualToString:@"fixed"])
                                                additionalPriceString = [NSString stringWithFormat:@"+%@", customOptionDict[@"optionValues"][key][@"formated_price"]];
                                            else{
                                                float price = [mainCollection[@"price"] floatValue];
                                                price = (price/100)*[customOptionDict[@"optionValues"][key][@"price"] floatValue];
                                                additionalPriceString = [NSString stringWithFormat:@"+%.02f", price];
                                            }
                                            UILabel *checkboxLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, checkBoxContainerInternalY+5, checkBoxContainer.frame.size.width-65, 25)];
                                            [checkboxLabel setTextColor:[GlobalData colorWithHexString : @"636363"]];
                                            [checkboxLabel setBackgroundColor:[UIColor clearColor]];
                                            [checkboxLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size : 19.0f]];
                                            if([customOptionDict[@"optionValues"][key][@"price"] floatValue] > 0)
                                                [checkboxLabel setText:[NSString stringWithFormat:@"%@ %@", customOptionDict[@"optionValues"][key][@"default_title"], additionalPriceString]];
                                            else
                                                [checkboxLabel setText:customOptionDict[@"optionValues"][key][@"default_title"]];
                                            [checkBoxContainer addSubview:checkboxLabel];
                                            checkBoxContainerInternalY += 35;
                                        }
                                        customOptionInternalY += (checkBoxContainerInternalY + 10);
                                    }
                                    else
                                        if([customOptionDict[@"type"] isEqualToString:@"date"] || [customOptionDict[@"type"] isEqualToString:@"date_time"] || [customOptionDict[@"type"] isEqualToString:@"time"]){
                                            UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(5, customOptionInternalY, customOptionContainer.frame.size.width-10, 100)];
                                            datePicker.tag = i+1;
                                            if([customOptionDict[@"type"] isEqualToString:@"date"])
                                                [datePicker setDatePickerMode:UIDatePickerModeDate];
                                            if([customOptionDict[@"type"] isEqualToString:@"date_time"])
                                                [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
                                            if([customOptionDict[@"type"] isEqualToString:@"time"])
                                                [datePicker setDatePickerMode:UIDatePickerModeTime];
                                            [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
                                            [customOptionContainer addSubview:datePicker];
                                            customOptionInternalY += 105;
                                        }
                    if(i < [mainCollection[@"customOptions"] count]-1){
                        UIView *hr1 = [[UIView alloc]initWithFrame:CGRectMake(5, customOptionInternalY, customOptionContainer.frame.size.width-10, 1)];
                        [hr1 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                        [customOptionContainer addSubview:hr1];
                        customOptionInternalY += 11;
                    }
                }
                CGRect newFrame = customOptionContainer.frame;
                newFrame.size.height = customOptionInternalY;
                customOptionContainer.frame = newFrame;
                mainContainerY += (customOptionInternalY + 21);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////                                  ////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////// Creating Grouped Product Options ////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////                                  ////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            if([mainCollection[@"groupedData"] count] > 0){
                groupedDataContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 100)];
                groupedDataContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
                groupedDataContainer.tag = 5000;
                groupedDataContainer.layer.borderWidth = 2.0f;
                [_mainView addSubview:groupedDataContainer];
                float groupedInternalY = 5;
                for(int i=0; i<[mainCollection[@"groupedData"] count]; i++) {
                    NSDictionary *groupedDataDict = [mainCollection[@"groupedData"] objectAtIndex:i];
                    UIImageView *groupedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, groupedInternalY, SCREEN_WIDTH/5, SCREEN_WIDTH/5)];
                    UIImage *thumbNail = [imageCache objectForKey:groupedDataDict[@"thumbNail"]];
                    if(thumbNail)
                        groupedImageView.image = thumbNail;
                    else{
                        [_queue addOperationWithBlock:^{
                            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:groupedDataDict[@"thumbNail"]]];
                            UIImage *thumbNail = [UIImage imageWithData: imageData];
                            if(thumbNail){
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    groupedImageView.image = thumbNail;
                                }];
                                [imageCache setObject:thumbNail forKey:groupedDataDict[@"thumbNail"]];
                            }
                        }];
                    }
                    
                    [groupedDataContainer addSubview:groupedImageView];
                    NSDictionary *nameAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                    CGSize nameStringSize = [groupedDataDict[@"name"] sizeWithAttributes:nameAttributes];
                    CGFloat nameStringWidth = nameStringSize.width;
                    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/5)+10), groupedInternalY, nameStringWidth, 20)];
                    [name setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [name setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [name setText:groupedDataDict[@"name"]];
                    [groupedDataContainer addSubview:name];
                    if([[NSString stringWithFormat:@"%@",groupedDataDict[@"isAvailable"]] isEqualToString:@"1"]){
                        UITextField *qtyField = [[UITextField alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/5)+10), groupedInternalY+25, 30, 25)];
                        qtyField.font = [UIFont fontWithName:@"Trebuchet MS" size:20.0f];
                        qtyField.textColor = [UIColor blackColor];
                        qtyField.text = @"0";
                        qtyField.tag = i+1;
                        qtyField.textAlignment = NSTextAlignmentCenter;
                        [qtyField setKeyboardType:UIKeyboardTypePhonePad];
                        qtyField.borderStyle = UITextBorderStyleRoundedRect;
                        [groupedDataContainer addSubview:qtyField];
                        NSDictionary *qtylblAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                        CGSize qtylblStringSize = [@"Quantity" sizeWithAttributes:qtylblAttributes];
                        CGFloat qtylblStringWidth = qtylblStringSize.width;
                        UILabel *qtyLbl = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/5)+45), groupedInternalY+27, qtylblStringWidth, 20)];
                        [qtyLbl setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [qtyLbl setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [qtyLbl setText:[globalObjectProduct.languageBundle localizedStringForKey:@"quantity" value:@"" table:nil]];
                        [groupedDataContainer addSubview:qtyLbl];
                    }
                    else{
                        NSDictionary *OOSAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                        CGSize OOSStringSize = [@"OUT OF STOCK" sizeWithAttributes:OOSAttributes];
                        CGFloat OOSStringWidth = OOSStringSize.width;
                        UILabel *OOS = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/5)+10), groupedInternalY+27, OOSStringWidth, 20)];
                        [OOS setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [OOS setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [OOS setText:[globalObjectProduct.languageBundle localizedStringForKey:@"outStock" value:@"" table:nil]];
                        [groupedDataContainer addSubview:OOS];
                    }
                    
                    if([[NSString stringWithFormat:@"%@",groupedDataDict[@"isInRange"]] isEqualToString:@"1"]){
                        NSDictionary *regularPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                        CGSize regularPriceStringSize = [groupedDataDict[@"foramtedPrice"] sizeWithAttributes:regularPriceAttributes];
                        CGFloat regularPriceStringWidth = regularPriceStringSize.width;
                        UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/5)+10), groupedInternalY+55, regularPriceStringWidth, 20)];
                        [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [regularprice setBackgroundColor:[UIColor clearColor]];
                        [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:groupedDataDict[@"foramtedPrice"]];
                        [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                        [regularprice setAttributedText:attributeString];
                        [groupedDataContainer addSubview:regularprice];
                        NSDictionary *specialPriceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                        CGSize specialPriceStringSize = [groupedDataDict[@"specialPrice"] sizeWithAttributes:specialPriceAttributes];
                        CGFloat specialPriceStringWidth = specialPriceStringSize.width;
                        UILabel *specialPrice = [[UILabel alloc] initWithFrame:CGRectMake((regularPriceStringWidth+5)+((SCREEN_WIDTH/5)+10), groupedInternalY+55, specialPriceStringWidth, 20)];
                        [specialPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [specialPrice setBackgroundColor:[UIColor clearColor]];
                        [specialPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [specialPrice setText:groupedDataDict[@"specialPrice"]];
                        [groupedDataContainer addSubview:specialPrice];
                    }
                    else{
                        NSDictionary *priceAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                        CGSize priceStringSize = [groupedDataDict[@"foramtedPrice"] sizeWithAttributes:priceAttributes];
                        CGFloat priceStringWidth = priceStringSize.width;
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/5)+10), groupedInternalY+55, priceStringWidth, 20)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [price setText:groupedDataDict[@"foramtedPrice"]];
                        [groupedDataContainer addSubview:price];
                    }
                    
                    groupedInternalY += (SCREEN_WIDTH/5)+5;
                    if(i < [mainCollection[@"groupedData"] count]-1){
                        UIView *hr1 = [[UIView alloc]initWithFrame:CGRectMake(5, groupedInternalY, groupedDataContainer.frame.size.width-10, 1)];
                        [hr1 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                        [groupedDataContainer addSubview:hr1];
                        groupedInternalY += 6;
                    }
                }
                CGRect newFrame = groupedDataContainer.frame;
                newFrame.size.height = groupedInternalY;
                groupedDataContainer.frame = newFrame;
                mainContainerY += groupedInternalY + 21;
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////                             ///////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////// Creating Downloadable Links ///////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////                             ///////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            if([mainCollection[@"links"] count] > 0){
                float internalY = 10;
                downloadableContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 300)];
                downloadableContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
                downloadableContainer.tag = 6000;
                downloadableContainer.layer.borderWidth = 2.0f;
                [_mainView addSubview:downloadableContainer];
                NSDictionary *reqAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:22.0f]};
                CGSize reqStringSize = [mainCollection[@"links"][@"title"] sizeWithAttributes:reqAttributes];
                CGFloat reqStringWidth = reqStringSize.width;
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, internalY,reqStringWidth, 22)];
                [title setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [title setFont:[UIFont fontWithName:@"Helvetica-Bold" size:22.0f]];
                [title setText:mainCollection[@"links"][@"title"]];
                [downloadableContainer addSubview:title];
                if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"] ){
                    UILabel *star;
                    float fieldX = reqStringWidth+5;
                    star = [[UILabel alloc] initWithFrame:CGRectMake(fieldX, internalY, 10, 23)];
                    [star setText:@"*"];
                    [star setTextColor: [GlobalData colorWithHexString:@"df280a"]];
                    [downloadableContainer addSubview:star];
                    NSDictionary *reqAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                    CGSize reqStringSize = [@"* Required Fields" sizeWithAttributes:reqAttributes];
                    CGFloat reqStringWidth = reqStringSize.width;
                    UILabel *required = [[UILabel alloc] initWithFrame:CGRectMake((_mainView.frame.size.width-20)-reqStringWidth, internalY, reqStringWidth, 25)];
                    [required setTextColor:[GlobalData colorWithHexString:@"df280a"]];
                    [required setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [required setText:[globalObjectProduct.languageBundle localizedStringForKey:@"requireFields" value:@"" table:nil]];
                    [downloadableContainer addSubview:required];
                }
                internalY += 35;
                for(int i=0; i<[mainCollection[@"links"][@"linkData"] count]; i++){
                    NSDictionary *customOption = [mainCollection[@"links"][@"linkData"] objectAtIndex:i];
                    NSString *linktitle = customOption[@"linkTitle"];
                    float internalspaceX = 5;
                    if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"] ){
                        UISwitch *checkBox = [[UISwitch alloc] initWithFrame:CGRectMake(5, internalY, 50, 18)];
                        [checkBox setOn:NO];
                        checkBox.tag = i+1;
                        [downloadableContainer addSubview:checkBox];
                        internalspaceX = 60;
                        internalY += 5;
                    }
                    NSDictionary *reqAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:22.0f]};
                    CGSize reqStringSize = [linktitle sizeWithAttributes:reqAttributes];
                    CGFloat reqStringWidth = reqStringSize.width;
                    UILabel *required = [[UILabel alloc] initWithFrame:CGRectMake(internalspaceX,internalY,reqStringWidth, 22)];
                    [required setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [required setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [required setText:linktitle];
                    [downloadableContainer addSubview:required];
                    float pricespace = internalspaceX+reqStringWidth;
                    if(customOption[@"url"]){
                        NSDictionary *spacewidth = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]};
                        CGSize reqSampleSize = [customOption[@"linkSampleTitle"] sizeWithAttributes:spacewidth];
                        CGFloat reqSampleWidth = reqSampleSize.width;
                        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(linkSample:)];
                        tapGesture.numberOfTapsRequired = 1;
                        CGSize reqSampleleftSize = [@"(" sizeWithAttributes:spacewidth];
                        CGFloat reqSampleleftWidth = reqSampleleftSize.width;
                        UILabel *leftbrace = [[UILabel alloc] initWithFrame:CGRectMake(internalspaceX+reqStringWidth+5, internalY, reqSampleleftWidth, 20)];
                        [leftbrace setText:@"("];
                        [leftbrace setTextColor: [GlobalData colorWithHexString:@"555555"]];
                        [leftbrace setFont:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]];
                        [downloadableContainer addSubview:leftbrace];
                        UILabel *linkSample = [[UILabel alloc] initWithFrame:CGRectMake(internalspaceX+reqStringWidth+5+reqSampleleftWidth, internalY, reqSampleWidth, 20)];
                        linkSample.tag = i+1;
                        [linkSample setText:customOption[@"linkSampleTitle"]];
                        linkSample.userInteractionEnabled = "YES";
                        [linkSample setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [linkSample setFont:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]];
                        [linkSample addGestureRecognizer:tapGesture];
                        [downloadableContainer addSubview:linkSample];
                        CGSize reqSamplerightSize = [@")" sizeWithAttributes:spacewidth];
                        CGFloat reqSamplerightWidth = reqSamplerightSize.width;
                        UILabel *rightbrace = [[UILabel alloc] initWithFrame:CGRectMake(internalspaceX+reqStringWidth+reqSampleleftWidth +5+reqSampleWidth, internalY, reqSamplerightWidth, 20)];
                        [rightbrace setText:@")"];
                        [rightbrace setTextColor: [GlobalData colorWithHexString:@"555555"]];
                        [rightbrace setFont:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]];
                        [downloadableContainer addSubview:rightbrace];
                        pricespace += reqSampleWidth+reqSampleleftWidth+reqSamplerightWidth;
                    }
                    if([customOption[@"price"] intValue] > 0){
                        NSDictionary *reqAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]};
                        CGSize reqStringSize = [customOption[@"formatedPrice"] sizeWithAttributes:reqAttributes];
                        CGFloat reqStringWidth = reqStringSize.width;
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(pricespace+10, internalY, reqStringWidth, 23)];
                        [price setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [price setText:customOption[@"formatedPrice"]];
                        [downloadableContainer addSubview:price];
                    }
                    if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"])
                        internalY += 30;
                    else
                        internalY += 24;
                }
                if([mainCollection[@"samples"][@"linkSampleData"] count] > 0){
                    internalY += 14;
                    for(int i=0; i<[mainCollection[@"samples"][@"linkSampleData"] count]; i++){
                        NSDictionary *customOption = [mainCollection[@"samples"][@"linkSampleData"]  objectAtIndex:i];
                        NSDictionary *spacewidth = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]};
                        CGSize reqSampleSize = [customOption[@"sampleTitle"] sizeWithAttributes:spacewidth];
                        CGFloat reqSampleWidth = reqSampleSize.width;
                        UILabel *samplelink = [[UILabel alloc] initWithFrame:CGRectMake(5, internalY, reqSampleWidth, 20)];
                        [samplelink setText:customOption[@"sampleTitle"]];
                        [samplelink setFont:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]];
                        [samplelink setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [downloadableContainer addSubview:samplelink];
                        internalY += 22;
                    }
                }
                CGRect newFrame = downloadableContainer.frame;
                newFrame.size.height = internalY+10;
                downloadableContainer.frame = newFrame;
                mainContainerY+=newFrame.size.height+20 ;
            }
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////                         ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////// Creating Bundle Options ////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////                         ////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            if([mainCollection[@"bundleOptions"] count] > 0){
                bundleContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 100)];
                bundleContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
                bundleContainer.tag = 8000;
                bundleContainer.layer.borderWidth = 2.0f;
                [_mainView addSubview:bundleContainer];
                float bundleInternalY = 5;
                for(int i=0; i<[mainCollection[@"bundleOptions"] count]; i++) {
                    NSDictionary *bundleCollection = [mainCollection[@"bundleOptions"] objectAtIndex:i];
                    bundlekeyCount = 0;
                    for( id keys in bundleCollection[@"optionValues"]){
                        bundlekeyCount++;
                    }
                    NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                    CGSize titleStringSize = [bundleCollection[@"default_title"] sizeWithAttributes:titleAttributes];
                    CGFloat titleStringWidth = titleStringSize.width;
                    UILabel *defaulttitle = [[UILabel alloc] initWithFrame:CGRectMake(5, bundleInternalY, titleStringWidth, 23)];
                    [defaulttitle setTextColor:[GlobalData colorWithHexString:@"000000"]];
                    [defaulttitle setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                    [defaulttitle setText:[NSString stringWithFormat:@"%@", bundleCollection[@"default_title"]]];
                    [bundleContainer addSubview:defaulttitle];
                    
                    if([bundleCollection[@"required"] isEqualToString:@"1"] && bundlekeyCount == 1){
                        titleStringWidth += 4;
                        UILabel *requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(titleStringWidth, bundleInternalY, 10, 23)];
                        [requiredStar setText:@"*"];
                        [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
                        [bundleContainer addSubview:requiredStar];
                        NSDictionary *reqAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]};
                        CGSize reqStringSize = [@"* Required Fields" sizeWithAttributes:reqAttributes];
                        CGFloat reqStringWidth = reqStringSize.width;
                        UILabel *required = [[UILabel alloc] initWithFrame:CGRectMake((_mainView.frame.size.width-20)-reqStringWidth, bundleInternalY,reqStringWidth, 25)];
                        [required setTextColor:[GlobalData colorWithHexString:@"df280a"]];
                        [required setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                        [required setText:NSLocalizedString(@"Required_Fields",nil) ];
                        [bundleContainer addSubview:required];
                        bundleInternalY += 25;
                        
                        for(id key in bundleCollection[@"optionValues"]){
                            NSDictionary *titleAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                            CGSize titleStringSize = [bundleCollection[@"optionValues"][key][@"title"] sizeWithAttributes:titleAttributes];
                            CGFloat titleStringWidth = titleStringSize.width;
                            UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, bundleInternalY, titleStringWidth, 23)];
                            [title setTextColor:[GlobalData colorWithHexString:@"000000"]];
                            [title setBackgroundColor:[GlobalData colorWithHexString:@"ffffff"]];
                            [title setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                            [title setText:[NSString stringWithFormat:@"%@", bundleCollection[@"optionValues"][key][@"title"]]];
                            bundleInternalY += 35;
                            [bundleContainer addSubview:title];
                            
                            NSDictionary *qtyAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                            CGSize qtyStringSize = [@"QTY: " sizeWithAttributes:qtyAttributes];
                            CGFloat qtyStringWidth = qtyStringSize.width;
                            UILabel *qty = [[UILabel alloc] initWithFrame:CGRectMake(5, bundleInternalY, qtyStringWidth, 23)];
                            [qty setTextColor:[GlobalData colorWithHexString:@"000000"]];
                            [qty setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                            [qty setText:[globalObjectProduct.languageBundle localizedStringForKey:@"qty" value:@"" table:nil]];
                            [bundleContainer addSubview:qty];
                            
                            CGSize labelSize = [qty.text sizeWithFont:qty.font constrainedToSize:qty.frame.size lineBreakMode:NSLineBreakByWordWrapping];
                            CGFloat labelHeight = labelSize.height;
                            UITextField *qtyField = [[UITextField alloc] initWithFrame:CGRectMake(qtyStringWidth+15, bundleInternalY-labelHeight+23,  46, 40)];
                            qtyField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
                            qtyField.textColor = [UIColor blackColor];
                            qtyField.text = @"1";
                            qtyField.tag = i+4;
                            [qtyField setKeyboardType:UIKeyboardTypePhonePad];
                            qtyField.textAlignment = NSTextAlignmentCenter;
                            qtyField.borderStyle = UITextBorderStyleRoundedRect;
                            [bundleContainer addSubview:qtyField];
                            bundleInternalY += 50;
                        }
                    }
                    else{
                        if([bundleCollection[@"required"] isEqualToString:@"1"]){
                            titleStringWidth += 4;
                            UILabel *requiredStar = [[UILabel alloc] initWithFrame:CGRectMake(titleStringWidth, bundleInternalY, 10, 23)];
                            [requiredStar setText:@"*"];
                            [requiredStar setTextColor: [GlobalData colorWithHexString:@"df280a"]];
                            [bundleContainer addSubview:requiredStar];
                        }
                        bundleInternalY += 25;
                        pickerValue = @"Bundle";
                        UIPickerView *dataSelect = [[UIPickerView alloc] initWithFrame:CGRectMake(5, bundleInternalY,bundleContainer.frame.size.width-10, 50)];
                        dataSelect.tag = i+1;
                        dataSelect.showsSelectionIndicator = YES;
                        dataSelect.hidden = NO;
                        dataSelect.delegate = self;
                        [bundleContainer addSubview:dataSelect];
                        bundleInternalY += 60;
                        NSDictionary *qtyAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                        CGSize qtyStringSize = [@"QTY: " sizeWithAttributes:qtyAttributes];
                        CGFloat qtyStringWidth = qtyStringSize.width;
                        UILabel *qty = [[UILabel alloc] initWithFrame:CGRectMake(5, bundleInternalY, qtyStringWidth, 17)];
                        [qty setTextColor:[GlobalData colorWithHexString:@"000000"]];
                        [qty setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [qty setText:[globalObjectProduct.languageBundle localizedStringForKey:@"qty" value:@"" table:nil]];
                        [bundleContainer addSubview:qty];
                        CGSize labelSize = [qty.text sizeWithFont:qty.font constrainedToSize:qty.frame.size lineBreakMode:NSLineBreakByWordWrapping];
                        CGFloat labelHeight = labelSize.height;
                        UITextField *qtyField1 = [[UITextField alloc] initWithFrame:CGRectMake(qtyStringWidth+15, bundleInternalY-labelHeight+23, 40, 40)];
                        qtyField1.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
                        qtyField1.textColor = [UIColor blackColor];
                        qtyField1.text = @"0";
                        qtyField1.tag = i+4;
                        qtyField1.userInteractionEnabled = NO;
                        [qtyField1 setKeyboardType:UIKeyboardTypePhonePad];
                        qtyField1.textAlignment = NSTextAlignmentCenter;
                        qtyField1.borderStyle = UITextBorderStyleRoundedRect;
                        [bundleContainer addSubview:qtyField1];
                        bundleInternalY += 50;
                    }
                }
                
                CGRect newFrame1 = bundleContainer.frame;
                newFrame1.size.height = bundleInternalY;
                bundleContainer.frame = newFrame1;
                mainContainerY += (bundleInternalY + 21);
            }
            
            UILabel *descriptionHeading = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 30)];
            [descriptionHeading setTextColor:[GlobalData colorWithHexString:@"000000"]];
            [descriptionHeading setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
            [descriptionHeading setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
            [descriptionHeading setText:@"  Description"];
            [_mainView addSubview:descriptionHeading];
            
            mainContainerY += 30;
            UIView *descriptionContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 1)];
            descriptionContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            descriptionContainer.layer.borderWidth = 2.0f;
            [_mainView addSubview:descriptionContainer];
            UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, descriptionContainer.frame.size.width-10, 25)];
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[mainCollection[@"description"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            description.attributedText = attrStr;
            [description setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
            [description setLineBreakMode:NSLineBreakByWordWrapping];
            description.numberOfLines = 0;
            description.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
            description.textAlignment = NSTextAlignmentLeft;
            [description sizeToFit];
            description.preferredMaxLayoutWidth = _mainView.frame.size.width-10;
            [descriptionContainer addSubview:description];
            CGRect newFrame = descriptionContainer.frame;
            newFrame.size.height = description.frame.size.height+10;
            descriptionContainer.frame = newFrame;
            
            mainContainerY += descriptionContainer.frame.size.height+21;
            UILabel *addInfoHeading = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 30)];
            [addInfoHeading setTextColor:[GlobalData colorWithHexString:@"000000"]];
            [addInfoHeading setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
            [addInfoHeading setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
            [addInfoHeading setText:@"  Additional Information"];
            [_mainView addSubview:addInfoHeading];
            
            mainContainerY += 30;
            UIView *addInfoContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 35)];
            addInfoContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            addInfoContainer.layer.borderWidth = 2.0f;
            [_mainView addSubview:addInfoContainer];
            float internalY = 5;
            if([mainCollection[@"additionalInformation"] count] > 0){
                float addInfoLabelMaxWidth = 0,addInfoValueMaxWidth = 0;
                for(int i=0; i<[mainCollection[@"additionalInformation"] count]; i++) {
                    NSDictionary *additionalInfoDict = [mainCollection[@"additionalInformation"] objectAtIndex:i];
                    NSDictionary *addInfoLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                    CGSize addInfoLabelStringSize = [additionalInfoDict[@"label"] sizeWithAttributes:addInfoLabelAttributes];
                    CGFloat addInfoLabelStringWidth = addInfoLabelStringSize.width;
                    if(addInfoLabelStringWidth > addInfoLabelMaxWidth)
                        addInfoLabelMaxWidth = addInfoLabelStringWidth;
                    NSDictionary *addInfoValueAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};
                    CGSize addInfoValueStringSize = [additionalInfoDict[@"value"] sizeWithAttributes:addInfoValueAttributes];
                    CGFloat addInfoValueStringWidth = addInfoValueStringSize.width;
                    if(addInfoValueStringWidth > addInfoValueMaxWidth)
                        addInfoValueMaxWidth = addInfoValueStringWidth;
                }
                for(int i=0; i<[mainCollection[@"additionalInformation"] count]; i++) {
                    NSDictionary *additionalInfoDict = [mainCollection[@"additionalInformation"] objectAtIndex:i];
                    UIView *addInfoLabelContainer = [[UIView alloc]initWithFrame:CGRectMake(5, internalY, addInfoLabelMaxWidth+10, 25)];
                    addInfoLabelContainer.layer.borderColor = [GlobalData colorWithHexString:@"C8C6C6"].CGColor;
                    [addInfoLabelContainer setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
                    addInfoLabelContainer.layer.borderWidth = 2.0f;
                    [addInfoContainer addSubview:addInfoLabelContainer];
                    UILabel *addInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, addInfoLabelMaxWidth, 25)];
                    [addInfoLabel setTextColor:[GlobalData colorWithHexString:@"000000"]];
                    [addInfoLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                    [addInfoLabel setText:additionalInfoDict[@"label"]];
                    [addInfoLabelContainer addSubview:addInfoLabel];
                    
                    UIView *addInfoValueContainer = [[UIView alloc]initWithFrame:CGRectMake(addInfoLabelMaxWidth+13, internalY, addInfoValueMaxWidth+10, 25)];
                    addInfoValueContainer.layer.borderColor = [GlobalData colorWithHexString:@"C8C6C6"].CGColor;
                    addInfoValueContainer.layer.borderWidth = 2.0f;
                    [addInfoContainer addSubview:addInfoValueContainer];
                    UILabel *addInfoValue = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, addInfoValueMaxWidth, 25)];
                    [addInfoValue setTextColor:[GlobalData colorWithHexString:@"000000"]];
                    [addInfoValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                    [addInfoValue setText:additionalInfoDict[@"value"]];
                    [addInfoValueContainer addSubview:addInfoValue];
                    internalY += 23;
                }
                
                internalY += 7;
                CGRect newFrame = addInfoContainer.frame;
                newFrame.size.height = internalY;
                addInfoContainer.frame = newFrame;
                internalY += 5;
            }
            else{
                UILabel *emptyAddInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, _mainView.frame.size.width-10, 25)];
                [emptyAddInfoLabel setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [emptyAddInfoLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                [emptyAddInfoLabel setText:@"No Additional Information Available."];
                [addInfoContainer addSubview:emptyAddInfoLabel];
                internalY -= 5;
                mainContainerY += 30;
            }
            
            mainContainerY += internalY+21;
            UILabel *reviewHeading = [[UILabel alloc] initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 30)];
            [reviewHeading setTextColor:[GlobalData colorWithHexString:@"000000"]];
            [reviewHeading setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
            [reviewHeading setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
            [reviewHeading setText:[globalObjectProduct.languageBundle localizedStringForKey:@"reviews" value:@"" table:nil]];
            [_mainView addSubview:reviewHeading];
            
            mainContainerY += 30;
            UIView *reviewContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 35)];
            reviewContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            reviewContainer.layer.borderWidth = 2.0f;
            [_mainView addSubview:reviewContainer];
            float reviewInternalY = 5;
            if([mainCollection[@"reviewList"] count] > 0){
                mainContainerY += 5;
                UIView *hr1 = [[UIView alloc]initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 1)];
                [hr1 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                [reviewContainer addSubview:hr1];
                
                reviewInternalY += 6;
                NSDictionary *reviewSummaryAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                CGSize reviewSummaryStringSize = [[NSString stringWithFormat:@"CUSTOMER REVIEWS %d ITEM(S)", (int)[mainCollection[@"reviewList"] count]] sizeWithAttributes:reviewSummaryAttributes];
                CGFloat reviewSummaryStringWidth = reviewSummaryStringSize.width;
                UILabel *reviewSummary = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewSummaryStringWidth, 25)];
                [reviewSummary setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [reviewSummary setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                [reviewSummary setText:[NSString stringWithFormat:@"CUSTOMER REVIEWS %d ITEM(S)", (int)[mainCollection[@"reviewList"] count]]];
                [reviewContainer addSubview:reviewSummary];
                
                reviewInternalY += 30;
                UIView *hr2 = [[UIView alloc]initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 1)];
                [hr2 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                [reviewContainer addSubview:hr2];
                
                reviewInternalY += 6;
                for(int i=0; i<[mainCollection[@"reviewList"] count]; i++) {
                    NSDictionary *reviewDict = [mainCollection[@"reviewList"] objectAtIndex:i];
                    NSDictionary *reviewTitleAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                    CGSize reviewTitleStringSize = [reviewDict[@"title"] sizeWithAttributes:reviewTitleAttributes];
                    CGFloat reviewTitleStringWidth = reviewTitleStringSize.width;
                    UILabel *reviewTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewTitleStringWidth, 25)];
                    [reviewTitle setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [reviewTitle setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewTitle setText:reviewDict[@"title"]];
                    [reviewContainer addSubview:reviewTitle];
                    
                    reviewInternalY += 30;
                    UILabel *reviewDetails = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 25)];
                    [reviewDetails setText:reviewDict[@"details"]];
                    [reviewDetails setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewDetails setLineBreakMode:NSLineBreakByWordWrapping];
                    reviewDetails.numberOfLines = 0;
                    reviewDetails.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
                    reviewDetails.textAlignment = NSTextAlignmentLeft;
                    reviewDetails.preferredMaxLayoutWidth = reviewContainer.frame.size.width-10;
                    [reviewDetails sizeToFit];
                    [reviewContainer addSubview:reviewDetails];
                    
                    reviewInternalY += reviewDetails.frame.size.height+5;
                    float ratingLabelMaxWidth = 0;
                    for(int k=0; k<[reviewDict[@"ratings"] count]; k++) {
                        NSDictionary *ratingLabelDict = [reviewDict[@"ratings"] objectAtIndex:k];
                        NSDictionary *ratingLabelAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                        CGSize ratingLabelStringSize = [ratingLabelDict[@"label"] sizeWithAttributes:ratingLabelAttributes];
                        CGFloat ratingLabelStringWidth = ratingLabelStringSize.width;
                        if(ratingLabelStringWidth > ratingLabelMaxWidth)
                            ratingLabelMaxWidth = ratingLabelStringWidth;
                    }
                    
                    for(int j=0; j<[reviewDict[@"ratings"] count]; j++) {
                        NSDictionary *ratingDict = [reviewDict[@"ratings"] objectAtIndex:j];
                        UILabel *ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, ratingLabelMaxWidth, 25)];
                        [ratingLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [ratingLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                        [ratingLabel setText:ratingDict[@"label"]];
                        [reviewContainer addSubview:ratingLabel];
                        
                        UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(ratingLabelMaxWidth+10,reviewInternalY,120,24)];
                        UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
                        [ratingContainer addSubview:grayContainer];
                        UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                        gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI1];
                        UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                        gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI2];
                        UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                        gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI3];
                        UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                        gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI4];
                        UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                        gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
                        [grayContainer addSubview:gI5];
                        
                        double percent = 24 * [ratingDict[@"value"] doubleValue];
                        UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
                        blueContainer.clipsToBounds = YES;
                        [ratingContainer addSubview:blueContainer];
                        UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
                        bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI1];
                        UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
                        bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI2];
                        UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
                        bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI3];
                        UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
                        bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI4];
                        UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
                        bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
                        [blueContainer addSubview:bI5];
                        [reviewContainer addSubview:ratingContainer];
                        reviewInternalY += 29;
                    }
                    
                    NSDictionary *reviewByAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                    CGSize reviewByStringSize = [reviewDict[@"reviewBy"] sizeWithAttributes:reviewByAttributes];
                    CGFloat reviewByStringWidth = reviewByStringSize.width;
                    UILabel *reviewBy = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewByStringWidth, 25)];
                    [reviewBy setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [reviewBy setBackgroundColor:[UIColor clearColor]];
                    [reviewBy setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewBy setText:reviewDict[@"reviewBy"]];
                    [reviewContainer addSubview:reviewBy];
                    
                    reviewInternalY += 30;
                    NSDictionary *reviewOnAttributes = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]};
                    CGSize reviewOnStringSize = [reviewDict[@"reviewOn"] sizeWithAttributes:reviewOnAttributes];
                    CGFloat reviewOnStringWidth = reviewOnStringSize.width;
                    UILabel *reviewOn = [[UILabel alloc] initWithFrame:CGRectMake(5, reviewInternalY, reviewOnStringWidth, 25)];
                    [reviewOn setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [reviewOn setBackgroundColor:[UIColor clearColor]];
                    [reviewOn setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
                    [reviewOn setText:reviewDict[@"reviewOn"]];
                    [reviewContainer addSubview:reviewOn];
                    
                    reviewInternalY += 30;
                    UIView *hr3 = [[UIView alloc]initWithFrame:CGRectMake(5, reviewInternalY, reviewContainer.frame.size.width-10, 1)];
                    [hr3 setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                    [reviewContainer addSubview:hr3];
                    reviewInternalY += 11;
                }
                
                reviewInternalY += 7;
                CGRect newFrame = reviewContainer.frame;
                newFrame.size.height = reviewInternalY;
                reviewContainer.frame = newFrame;
                reviewInternalY += 5;
            }
            else{
                UILabel *emptyReviewLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, _mainView.frame.size.width-10, 25)];
                [emptyReviewLabel setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [emptyReviewLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                [emptyReviewLabel setText:@"Be the first to review this product"];
                emptyReviewLabel.userInteractionEnabled = YES;
                [reviewContainer addSubview:emptyReviewLabel];
                UITapGestureRecognizer *addReviewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addReview:)];
                addReviewGesture.numberOfTapsRequired = 1;
                [emptyReviewLabel addGestureRecognizer:addReviewGesture];
                internalY -= 5;
                mainContainerY += 30;
            }
            
            mainContainerY += reviewInternalY+5;
            if([_parentClass isEqualToString:@"home"] || [_parentClass isEqualToString:@"SearchProduct"] || [_parentClass isEqualToString:@"notification"])
                addToCartBlock = [[UIView alloc]initWithFrame:CGRectMake(0, _mainBaseView.frame.size.height-99, _mainBaseView.frame.size.width, 50)];
            else
                addToCartBlock = [[UIView alloc]initWithFrame:CGRectMake(0, _mainBaseView.frame.size.height-49, _mainBaseView.frame.size.width, 50)];
            [addToCartBlock setBackgroundColor:[GlobalData colorWithHexString:@"009688"]];
            [_mainBaseView addSubview:addToCartBlock];
            UITextField *qtyField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, (_mainBaseView.frame.size.width/2)-10, 40)];
            qtyField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
            qtyField.textColor = [UIColor blackColor];
            [qtyField setPlaceholder:@"Enter Qty"];
            qtyField.text = @"1";
            qtyField.tag = 2000;
            [qtyField setKeyboardType:UIKeyboardTypePhonePad];
            qtyField.textAlignment = NSTextAlignmentLeft;
            qtyField.borderStyle = UITextBorderStyleRoundedRect;
            [addToCartBlock addSubview:qtyField];
            UILabel *addToCartBtn = [[UILabel alloc] initWithFrame:CGRectMake((_mainBaseView.frame.size.width/2)+5, 5, (_mainBaseView.frame.size.width/2)-10,40)];
            [addToCartBtn setBackgroundColor: [GlobalData colorWithHexString:@"FF9800"]];
            [addToCartBtn setTextColor: [UIColor whiteColor]];
            [addToCartBtn setText:[globalObjectProduct.languageBundle localizedStringForKey:@"addCart" value:@"" table:nil]];
            addToCartBtn.userInteractionEnabled = YES;
            addToCartBtn.textAlignment = NSTextAlignmentCenter;
            [addToCartBlock addSubview:addToCartBtn];
            UITapGestureRecognizer *addToCartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addToCart:)];
            addToCartGesture.numberOfTapsRequired = 1;
            [addToCartBtn addGestureRecognizer:addToCartGesture];
            _mainViewHeightConstraint.constant = mainContainerY;
        }
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

- (void)linkSample:(UITapGestureRecognizer *)recognizer {
    int index = (int)recognizer.view.tag;
    NSDictionary *linkDataDict = [mainCollection[@"links"][@"linkData"] objectAtIndex:(index-1)];
    if([linkDataDict objectForKey:@"url"]) {
        NSURL *image_url = [NSURL URLWithString:linkDataDict[@"url"]];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:image_url]];
        ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
        [library writeImageToSavedPhotosAlbum:image.CGImage orientation:0 completionBlock:^(NSURL *assetURL, NSError *error) {}];
    }
}

- (void)dateChanged:(id)sender{
    // NSLog(@"value: %@",[sender date]);
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    
    UIView *parentViewTag = pickerView.superview;
    if(parentViewTag.tag == 7000){
        if(pickerView.tag == 1){
            NSMutableArray *labelData = [[NSMutableArray alloc] init];
            NSMutableArray *formatedPrice = [[NSMutableArray alloc] init];
            NSMutableArray *oldPrice = [[NSMutableArray alloc] init];
            NSMutableDictionary *labelDict = [[NSMutableDictionary alloc] init];
            NSDictionary *attributesvalues = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:0];
            for (int i = 0; i <[attributesvalues[@"options"] count] ; i++) {
                labelDict = [attributesvalues[@"options"] objectAtIndex:i];
                [labelData addObject:labelDict[@"label"]];
                [oldPrice addObject:labelDict[@"oldPrice"]];
                if(![[labelData objectAtIndex:i] isEqualToString:@"0"]){
                    [formatedPrice addObject:labelDict[@"formatedPrice"]];
                }
            }
            
            return [labelData count] +1;
        }
        else{
            return [configDataForRow count] + 1;
        }
    }
    if(parentViewTag.tag == 8000){
        NSDictionary *customOptionDict = [mainCollection[@"bundleOptions"] objectAtIndex:(pickerView.tag-1)];
        return [customOptionDict[@"optionValues"] count] + 1;
    }
    if(parentViewTag.tag == 4000){
        NSDictionary *customOptionDict = [mainCollection[@"customOptions"] objectAtIndex:(pickerView.tag-1)];
        return [customOptionDict[@"optionValues"] count];
    }
    return 1;
}

-(NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    UIView *parentViewTag = pickerView.superview;
    if(parentViewTag.tag == 7000){
        NSString *data=[globalObjectProduct.languageBundle localizedStringForKey:@"chooseSelection" value:@"" table:nil];
        if(pickerView.tag == 1){
            NSMutableArray *labelData = [[NSMutableArray alloc] init];
            NSMutableArray *formatedPrice = [[NSMutableArray alloc] init];
            NSMutableArray *oldPrice = [[NSMutableArray alloc] init];
            NSMutableDictionary *labelDict = [[NSMutableDictionary alloc] init];
            if(row == 0){
                return [NSString stringWithFormat:@"%@ ",data];
            }
            else{
                NSDictionary *attributesvalues = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:0];
                for (int i = 0; i <[attributesvalues[@"options"] count] ; i++) {
                    labelDict = [attributesvalues[@"options"] objectAtIndex:i];
                    [labelData addObject:labelDict[@"label"]];
                    [oldPrice addObject:labelDict[@"oldPrice"]];
                    if(![[oldPrice objectAtIndex:i] isEqualToString:@"0"]){
                        NSString *priceTitle = [NSString stringWithFormat:@"+%@",labelDict[@"formatedPrice"]];
                        [formatedPrice addObject:priceTitle];
                    }
                    else{
                        [formatedPrice addObject:@""];
                    }
                }
                return [NSString stringWithFormat:@"%@ %@ ",[labelData objectAtIndex:row-1],[formatedPrice objectAtIndex:row-1]];
            }
        }
        else if(row ==0){
            if(configRowValue == 0 ){
                UIView *configUIView = [_mainView viewWithTag:7000];
                for(long i = pickerView.tag; i < 4; i++){
                    UIPickerView * tempPicker = [configUIView viewWithTag:i+1];
                    [tempPicker setUserInteractionEnabled:NO];
                }
            }
            NSString *data=[globalObjectProduct.languageBundle localizedStringForKey:@"chooseSelection" value:@"" table:nil];
            return [NSString stringWithFormat:@"%@ ",data];
        }
        else{
            configtitleArray = [[NSMutableArray alloc] init];
            NSMutableDictionary *value =[configPickerDictionary objectForKey:[NSNumber numberWithInteger:pickerView.tag-1]];
            for( id key in value){
                [configtitleArray addObject:key];
            }
            return [NSString stringWithFormat:@"%@ ",[configtitleArray objectAtIndex:row-1]];
        }
    }
    
    if(parentViewTag.tag == 8000){
        NSMutableArray *title = [[NSMutableArray alloc] init];
        NSMutableArray *title1 = [[NSMutableArray alloc] init];
        NSDictionary *bundleCollection = [mainCollection[@"bundleOptions"] objectAtIndex:(pickerView.tag-1)];
        for(id key in bundleCollection[@"optionValues"]){
            title = bundleCollection[@"optionValues"][key][@"title"];
            [title1 addObject:title];
        }
        if(row == 0){
            NSString *data=[globalObjectProduct.languageBundle localizedStringForKey:@"chooseSelection" value:@"" table:nil];
            return [NSString stringWithFormat:@"%@ ",data];
        }
        else{
            return [NSString stringWithFormat:@"%@ ",[title1 objectAtIndex:row-1]];
        }
    }
    
    if(parentViewTag.tag == 4000){
        NSDictionary *customOptionDict = [mainCollection[@"customOptions"] objectAtIndex:(pickerView.tag-1)];
        NSString *key = [[optionValues objectAtIndex:(pickerView.tag-1)] objectAtIndex:row];
        id value = [customOptionDict[@"optionValues"] objectForKey:key];
        NSString *additionalPriceString = @"";
        if([value[@"price_type"] isEqualToString:@"fixed"])
            additionalPriceString = [NSString stringWithFormat:@"+%@", value[@"formated_price"]];
        else{
            float price = [mainCollection[@"price"] floatValue];
            price = (price/100)*[value[@"price"] floatValue];
            additionalPriceString = [NSString stringWithFormat:@"+%.02f", price];
        }
        if([value[@"price"] floatValue] > 0)
            return [NSString stringWithFormat:@"%@ %@", value[@"default_title"], additionalPriceString];
        else
            return value[@"default_title"];
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    UIView *parentViewTag = pickerView.superview;
    if(parentViewTag.tag == 7000){
        [configDataForRow removeAllObjects];
        if(row > 0){
            NSString *rowData=[NSString stringWithFormat:@"%li",(long)row];
            NSString *pickerInfoData=[configPickerInfo objectForKey:[NSNumber numberWithLong:pickerView.tag]];
            if([configPickerInfo objectForKey:[NSNumber numberWithInteger:pickerView.tag]]){
                if(![rowData isEqualToString:pickerInfoData]){
                    for(long i = pickerView.tag; i < configPickerCount ;i++){
                        UIView *configUIView1 = [_mainView viewWithTag:7000];
                        NSInteger tagValue1 = i+1;
                        UIPickerView * tempPicker1 = [configUIView1 viewWithTag:tagValue1];
                        if(tagValue1 == pickerView.tag+1){
                            [tempPicker1 setUserInteractionEnabled:YES];
                        }
                        else{
                            [tempPicker1 setUserInteractionEnabled:NO];
                        }
                        [tempPicker1 selectRow:0 inComponent:0 animated:NO];
                        [tempPicker1 reloadComponent:0];
                    }
                }
                
            }
        }
        
        if(pickerView.tag < configPickerCount && row > 0)
            [configPickerInfo setObject:[NSNumber numberWithInteger:row] forKey:[NSNumber numberWithInteger:pickerView.tag]];
        
        if(row == 0){
            for(long i = pickerView.tag; i < configPickerCount ;i++){
                UIView *configUIView1 = [_mainView viewWithTag:7000];
                NSInteger tagValue1 = i+1;
                UIPickerView * tempPicker = [configUIView1 viewWithTag:tagValue1];
                [tempPicker setUserInteractionEnabled:NO];
                [tempPicker selectRow:0 inComponent:0 animated:NO];
                [configDataForRow removeAllObjects];
                [tempPicker reloadComponent:0];
            }
        }
        configRowValue = row;
        if(row > 0){
            NSInteger tagValue = pickerView.tag;
            UIView *configUIView = [_mainView viewWithTag:7000];
            UIPickerView * tempPicker = [configUIView viewWithTag:tagValue+1];
            [tempPicker setUserInteractionEnabled:YES];
        }
        NSMutableDictionary *labelDict = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *configLabelDict = [[NSMutableDictionary alloc] init];
        configDataForRow = [[NSMutableArray alloc] init];
        if(pickerView.tag < configPickerCount && row > 0){
            
            if(pickerView.tag ==1){
                [firstConfigIdData removeAllObjects];
                [configIdData removeAllObjects];
                NSDictionary *attributesvalues = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:0];
                labelDict = [attributesvalues[@"options"] objectAtIndex:row - 1];
                for(int i = 0; i < [labelDict[@"products"] count]; i++)
                {
                    [firstConfigIdData addObject:[labelDict[@"products"] objectAtIndex:i]];
                }
            }
            else if(pickerView.tag ==2) {
                NSDictionary *attributesvalues5 = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:pickerView.tag-1];
                [configIdData removeAllObjects];
                
                NSMutableArray *rowArray =[[NSMutableArray alloc] init];
                NSMutableDictionary *rowDict = [configRowValueDict objectForKey:[NSNumber numberWithInteger:pickerView.tag-1]];
                for( id key in rowDict)
                {
                    [rowArray addObject:key];
                }
                int myInt = [[rowArray objectAtIndex:row-1] intValue];
                labelDict = [attributesvalues5[@"options"] objectAtIndex:myInt];
                for(int k = 0; k < [labelDict[@"products"] count]; k++){
                    for (int j = 0; j < [firstConfigIdData count]; j++) {
                        if([[firstConfigIdData objectAtIndex:j] isEqualToString:[labelDict[@"products"] objectAtIndex:k]]){
                            [configIdData addObject:[labelDict[@"products"] objectAtIndex:k]];
                        }
                    }
                }
            }
            [configStoreValue removeAllObjects];
            NSDictionary *attributesvalues1 = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:pickerView.tag];
            for (int i = 0; i <[attributesvalues1[@"options"] count] ; i++) {
                configLabelDict = [attributesvalues1[@"options"] objectAtIndex:i];
                for(int k = 0; k < [configLabelDict[@"products"] count]; k++){
                    if(pickerView.tag ==1){
                        for (int j = 0; j < [firstConfigIdData count]; j++) {
                            if([[firstConfigIdData objectAtIndex:j] isEqualToString:[configLabelDict[@"products"] objectAtIndex:k]]){
                                flag = 1;
                                break;
                            }
                            else{
                                flag = 0;
                            }
                        }
                    }
                    else{
                        for (int j = 0; j < [configIdData count]; j++) {
                            if([[configIdData objectAtIndex:j] isEqualToString:[configLabelDict[@"products"] objectAtIndex:k]]){
                                flag = 1;
                                break;
                            }
                            else{
                                flag = 0;
                            }
                            
                        }
                    }
                    if(flag  == 1){
                        [configStoreValue addObject:[NSNumber numberWithInt:i]];
                        if(!([configLabelDict[@"oldPrice"] isEqualToString:@"0"])) {
                            NSString *data = [NSString stringWithFormat:@"%@ +%@",configLabelDict[@"label"],configLabelDict[@"formatedPrice"]];
                            [configDataForRow addObject:data];
                        }
                        else{
                            [configDataForRow addObject:configLabelDict[@"label"]];
                            
                        }
                    }
                }
            }
            NSInteger tagValue = pickerView.tag;
            [configRowValueDict setObject:[NSMutableArray arrayWithArray:configStoreValue] forKey:[NSNumber numberWithInteger:pickerView.tag]];
            [configPickerDictionary setObject:[NSMutableArray arrayWithArray:configDataForRow] forKey:[NSNumber numberWithInteger:tagValue]];
        }
        
        if(row > 0){
            UIView *configUIView = [_mainView viewWithTag:7000];
            UIPickerView *tempPicker = [configUIView viewWithTag:pickerView.tag + 1];
            [tempPicker reloadComponent:0];
        }
    }
    
    if(parentViewTag.tag == 8000){
        bundleValueRow = (int)row;
        if(row == 0){
            UITextField *tempField = [bundleContainer viewWithTag:pickerView.tag+3];
            tempField.text = @"0";
            tempField.userInteractionEnabled = NO;
        }
        else{
            UITextField *tempField = [bundleContainer viewWithTag:pickerView.tag+3];
            tempField.text = @"1";
            tempField.userInteractionEnabled = YES;
        }
    }
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    if([addToCartMoved isEqualToString:@"0"]){
        addToCartMoved = @"1";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGRect newFrame = addToCartBlock.frame;
        _mainViewHeightConstraint.constant += keyboardSize.height;
        if([_parentClass isEqualToString:@"home"] || [_parentClass isEqualToString:@"SearchProduct" ])
            newFrame.origin.y = addToCartBlock.frame.origin.y - keyboardSize.height + 50;
        else
            newFrame.origin.y = addToCartBlock.frame.origin.y - keyboardSize.height;
        addToCartBlock.frame = newFrame;
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    if([addToCartMoved isEqualToString:@"1"]){
        addToCartMoved = @"0";
        NSDictionary* info = [notification userInfo];
        CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGRect newFrame = addToCartBlock.frame;
        _mainViewHeightConstraint.constant -= keyboardSize.height;
        if([_parentClass isEqualToString:@"home"] || [_parentClass isEqualToString:@"SearchProduct" ])
            newFrame.origin.y = addToCartBlock.frame.origin.y + keyboardSize.height - 50;
        else
            newFrame.origin.y = addToCartBlock.frame.origin.y + keyboardSize.height;
        addToCartBlock.frame = newFrame;
    }
}

-(void)addToCart:(UITapGestureRecognizer *)recognizer{
    selectedDownloadableProduct=[[NSMutableDictionary alloc]init];
    selectedGroupedProduct = [[NSMutableDictionary alloc]init];
    selectedCustomOption = [[NSMutableDictionary alloc]init];
    int isValid = 1;
    NSString *errorMessage = @"";
    int oneGroupdProductSelected = 0;
    if([mainCollection[@"groupedData"] count] > 0){
        UIView *groupedUIView = [_mainView viewWithTag:5000];
        for(int i=0; i<[mainCollection[@"groupedData"] count]; i++) {
            NSDictionary *groupedDataDict = [mainCollection[@"groupedData"] objectAtIndex:i];
            UITextField *tempField = [groupedUIView viewWithTag:i+1];
            if([[NSString stringWithFormat:@"%@",groupedDataDict[@"isAvailable"]] isEqualToString:@"1"]){
                if([tempField.text isEqualToString:@"0"] || [tempField.text isEqualToString:@""]){}
                else{
                    oneGroupdProductSelected = 1;
                    [selectedGroupedProduct setObject:tempField.text forKey:groupedDataDict[@"id"]];
                }
            }
        }
        if(oneGroupdProductSelected == 0){
            isValid = 0;
            errorMessage = @"Please enter quantity greater than zero to add to cart";
        }
    }
    if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"] ){
        int isSwitchOn = 0;
        UIView *downloadOptionUIView = [_mainView viewWithTag:6000];
        for(int i=0; i<[mainCollection[@"links"][@"linkData"] count]; i++){
            NSDictionary *linksOption = [mainCollection[@"links"][@"linkData"] objectAtIndex:i];
            UISwitch *switchValue = [downloadOptionUIView viewWithTag:i+1];
            if([switchValue isOn]){
                isSwitchOn = 1;
                [selectedDownloadableProduct setObject:linksOption[@"id"] forKey:linksOption[@"id"]];
            }
        }
        if(isSwitchOn == 0){
            isValid = 0;
            errorMessage = @"Select at Least One Option";
        }
    }
    if([mainCollection[@"customOptions"] count] > 0){
        UIView *customOptionUIView = [_mainView viewWithTag:4000];
        for(int i=0; i<[mainCollection[@"customOptions"] count]; i++) {
            NSDictionary *customOptionDict = [mainCollection[@"customOptions"] objectAtIndex:i];
            if([customOptionDict[@"is_require"] integerValue] == 1){
                if([customOptionDict[@"type"] isEqualToString:@"field"]){
                    UITextField *tempField = [customOptionUIView viewWithTag:i+1];
                    [tempField setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    if([tempField.text isEqual:@""]){
                        isValid = 0;
                        errorMessage = [NSString stringWithFormat:@"%@ is a required field", customOptionDict[@"title"]];
                        [tempField setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                    }
                    else
                        [selectedCustomOption setObject:tempField.text forKey:customOptionDict[@"option_id"]];
                }
                
                if([customOptionDict[@"type"] isEqualToString:@"area"]){
                    UITextView *tempArea = [customOptionUIView viewWithTag:i+1];
                    [tempArea setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    if([tempArea.text isEqual:@""]){
                        isValid = 0;
                        if([errorMessage isEqualToString:@""])
                            errorMessage = [NSString stringWithFormat:@"%@ is a required field", customOptionDict[@"title"]];
                        [tempArea setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                    }
                    else
                        [selectedCustomOption setObject:tempArea.text forKey:customOptionDict[@"option_id"]];
                }
                if([customOptionDict[@"type"] isEqualToString:@"checkbox"] || [customOptionDict[@"type"] isEqualToString:@"multiple"]){
                    UIView *tempSwitchArea = [customOptionUIView viewWithTag:i+1];
                    [tempSwitchArea setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    int isSwitchOn = 0;
                    NSMutableArray *selectedOption = [[NSMutableArray alloc]init];
                    for(id key in [optionValues objectAtIndex:i]){
                        UISwitch *tempSwitch = [tempSwitchArea viewWithTag:[key integerValue]];
                        if([tempSwitch isOn]){
                            isSwitchOn = 1;
                            [selectedOption addObject:key];
                        }
                    }
                    if(isSwitchOn == 0){
                        isValid = 0;
                        if([errorMessage isEqualToString:@""])
                            errorMessage = [NSString stringWithFormat:@"%@ is a required field", customOptionDict[@"title"]];
                        [tempSwitchArea setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                    }
                    else
                        [selectedCustomOption setObject:selectedOption forKey:customOptionDict[@"option_id"]];
                }
            }
            else{
                if([customOptionDict[@"type"] isEqualToString:@"field"]){
                    UITextField *tempField = [customOptionUIView viewWithTag:i+1];
                    [selectedCustomOption setObject:tempField.text forKey:customOptionDict[@"option_id"]];
                }
                if([customOptionDict[@"type"] isEqualToString:@"area"]){
                    UITextView *tempArea = [customOptionUIView viewWithTag:i+1];
                    [selectedCustomOption setObject:tempArea.text forKey:customOptionDict[@"option_id"]];
                }
                if([customOptionDict[@"type"] isEqualToString:@"checkbox"] || [customOptionDict[@"type"] isEqualToString:@"multiple"]){
                    UIView *tempSwitchArea = [customOptionUIView viewWithTag:i+1];
                    [tempSwitchArea setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                    int isSwitchOn = 0;
                    NSMutableArray *selectedOption = [[NSMutableArray alloc]init];
                    for(id key in [optionValues objectAtIndex:i]){
                        UISwitch *tempSwitch = [tempSwitchArea viewWithTag:[key integerValue]];
                        if([tempSwitch isOn]){
                            isSwitchOn = 1;
                            [selectedOption addObject:key];
                        }
                    }
                    [selectedCustomOption setObject:selectedOption forKey:customOptionDict[@"option_id"]];
                }
            }
            if([customOptionDict[@"type"] isEqualToString:@"radio"] || [customOptionDict[@"type"] isEqualToString:@"drop_down"]){
                UIPickerView *tempPicker = [customOptionUIView viewWithTag:i+1];
                int row = (int)[tempPicker selectedRowInComponent:0];
                [selectedCustomOption setObject:[[optionValues objectAtIndex:i] objectAtIndex:row] forKey:customOptionDict[@"option_id"]];
            }
            if([customOptionDict[@"type"] isEqualToString:@"date"] || [customOptionDict[@"type"] isEqualToString:@"date_time"] || [customOptionDict[@"type"] isEqualToString:@"time"]){
                UIDatePicker *tempDatePicker = [customOptionUIView viewWithTag:i+1];
                NSDate *date = tempDatePicker.date;
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:date];
                if([customOptionDict[@"type"] isEqualToString:@"date"]){
                    NSMutableDictionary *dateDict = [[NSMutableDictionary alloc]init];
                    [dateDict setObject:[NSString stringWithFormat:@"%ld", (long)[components month]] forKey:@"month"];
                    [dateDict setObject:[NSString stringWithFormat:@"%ld", (long)[components day]] forKey:@"day"];
                    [dateDict setObject:[NSString stringWithFormat:@"%ld", (long)[components year]] forKey:@"year"];
                    [selectedCustomOption setObject:dateDict forKey:customOptionDict[@"option_id"]];
                }
                if([customOptionDict[@"type"] isEqualToString:@"date_time"]){
                    NSMutableDictionary *dateTimeDict = [[NSMutableDictionary alloc]init];
                    [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components month]] forKey:@"month"];
                    [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components day]] forKey:@"day"];
                    [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components year]] forKey:@"year"];
                    [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components hour]] forKey:@"hour"];
                    [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components minute]] forKey:@"minute"];
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"a"];
                    [dateTimeDict setObject:[formatter stringFromDate:date] forKey:@"day_part"];
                    [selectedCustomOption setObject:dateTimeDict forKey:customOptionDict[@"option_id"]];
                }
                if([customOptionDict[@"type"] isEqualToString:@"time"]){
                    NSMutableDictionary *timeDict = [[NSMutableDictionary alloc]init];
                    [timeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components hour]] forKey:@"hour"];
                    [timeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components minute]] forKey:@"minute"];
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"a"];
                    [timeDict setObject:[formatter stringFromDate:date] forKey:@"day_part"];
                    [selectedCustomOption setObject:timeDict forKey:customOptionDict[@"option_id"]];
                }
            }
        }
    }
    
    selectedBundleProduct = [[NSMutableDictionary alloc]init];
    selectedBundleProductQuantity = [[NSMutableDictionary alloc]init];
    NSMutableArray *value =[[NSMutableArray alloc] init];
    NSMutableArray *optionIdData =[[NSMutableArray alloc] init];
    UITextField *tempField1;
    if([mainCollection[@"bundleOptions"] count] > 0){
        NSArray *arr=[[NSArray alloc] init];
        UIView *bundleUIView = [_mainView viewWithTag:8000];
        for(int i=0; i<[mainCollection[@"bundleOptions"] count]; i++) {
            NSDictionary *bundleDict = [mainCollection[@"bundleOptions"] objectAtIndex:i];
            bundlekeyCount = 0;
            for(id key in bundleDict[@"optionValues"]){
                value = bundleDict[@"optionValues"][key][@"optionValueId"];
                [optionIdData addObject:value];
                bundlekeyCount++;
            }
            tempField1 = [bundleUIView viewWithTag:i+4];
            int row = 0;
            UIPickerView *tempPicker;
            if([bundleDict[@"required"] integerValue] == 1 ){
                UITextField *tempField = [bundleUIView viewWithTag:i+4];
                [tempField setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                if([tempField.text isEqual:@"0"]){
                    isValid = 0;
                    errorMessage = [NSString stringWithFormat:@"%@ is a required field", bundleDict[@"title"]];
                    [tempField setBackgroundColor:[GlobalData colorWithHexString:@"e78d8d"]];
                }
                
            }
            if(![tempField1.text isEqualToString:@"0"] && isValid == 1){
                tempPicker = [bundleUIView viewWithTag:i+1];
                if(tempPicker.tag < [mainCollection[@"bundleOptions"] count])
                    row = (int)[tempPicker selectedRowInComponent:0];
                else
                    row = bundleValueRow;
                if([bundleDict[@"required"] isEqualToString:@"1"] && bundlekeyCount == 1){
                    [selectedBundleProduct setObject:[optionIdData objectAtIndex:0] forKey:bundleDict[@"option_id"]];
                }
                if(i < [mainCollection[@"bundleOptions"] count] && (tempPicker.tag > 0) && row > 0 && tempPicker.tag < [mainCollection[@"bundleOptions"] count]){
                    [selectedBundleProduct setObject:[optionIdData objectAtIndex:row-1] forKey:bundleDict[@"option_id"]];
                }
                else if(tempPicker.tag == [mainCollection[@"bundleOptions"] count]){
                    [selectedBundleProduct setObject:[optionIdData objectAtIndex:row-1] forKey:bundleDict[@"option_id"]];
                }
                [optionIdData removeAllObjects];
                [selectedBundleProductQuantity setObject:tempField1.text forKey:bundleDict[@"option_id"]];
            }
        }
    }
    
    selectedConfigProduct = [[NSMutableDictionary alloc]init];
    if([mainCollection[@"configurableData"] count] > 0){
        UIView *configUIView = [_mainView viewWithTag:7000];
        NSMutableArray *rowIndex = [[NSMutableArray alloc] init];
        UIPickerView *tempPicker = [configUIView viewWithTag:1];
        int row = (int)[tempPicker selectedRowInComponent:0];
        for(int i = 1; i <= configPickerCount; i++){
            UIPickerView *tempPicker1 = [configUIView viewWithTag:i];
            int row1 = (int)[tempPicker1 selectedRowInComponent:0];
            [rowIndex addObject:[NSNumber numberWithInt: row1]];
            if(row1 == 0 && isValid!=0){
                isValid = 0;
                NSDictionary *configLabel=[mainCollection[@"configurableData"][@"attributes"] objectAtIndex:i - 1];
                errorMessage = [NSString stringWithFormat:@"%@ is a required field", configLabel[@"label"]];
            }
        }
        if(isValid != 0){
            if(tempPicker.tag == 1 ){
                NSMutableDictionary *firstConfigOptionId = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:0];
                NSMutableDictionary *configOptionId = [firstConfigOptionId[@"options"] objectAtIndex:row-1];
                [selectedConfigProduct setObject:configOptionId[@"id"] forKey:firstConfigOptionId[@"id"]];
            }
            for(NSInteger j = 1 ;j < configPickerCount; j++){
                NSMutableDictionary *firstConfigOptionId = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:j];
                NSMutableDictionary *configDict = [configRowValueDict objectForKey:[NSNumber numberWithInteger:j]];
                NSMutableArray *configRowArray = [[NSMutableArray alloc] init];
                int index = [[rowIndex objectAtIndex:j] intValue];
                for( id key in configDict)
                {
                    
                    [configRowArray addObject:key];
                }
                int rowIndex = [[configRowArray objectAtIndex:index-1] intValue];
                NSMutableDictionary *configOptionId = [firstConfigOptionId[@"options"] objectAtIndex:rowIndex];
                [selectedConfigProduct setObject:configOptionId[@"id"] forKey:firstConfigOptionId[@"id"]];
            }
        }
    }
    if(isValid == 0){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    else{
        UITextField *qtyField = [_mainBaseView viewWithTag:2000];
        [qtyField resignFirstResponder];
        whichApiDataToprocess = @"addToCart";
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
    
}

-(void)addReview:(UITapGestureRecognizer *)recognizer{
    [self performSegueWithIdentifier:@"addReviewSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"addReviewSegue"]) {
        AddReview *destViewController = segue.destinationViewController;
        destViewController.productId = _productId;
        destViewController.productName = _productName;
    }
    if([segue.identifier isEqualToString:@"addtoWishlistLoginSegue"]) {
        CustomerLogin *destViewController = segue.destinationViewController;
        destViewController.catalogProductFlag = @"1";
    }
}

-(void)addToWishlist:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId == nil){
        UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectProduct.languageBundle localizedStringForKey:@"logInWishList" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          [self performSegueWithIdentifier:@"addtoWishlistLoginSegue" sender:self];
                                                      }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectProduct.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [AC addAction:okBtn];
        [AC addAction:noBtn];
        [self.parentViewController presentViewController:AC animated:YES completion:nil];
    }
    else{
        selectedDownloadableProduct=[[NSMutableDictionary alloc]init];
        selectedGroupedProduct = [[NSMutableDictionary alloc]init];
        selectedCustomOption = [[NSMutableDictionary alloc]init];
        NSString *errorMessage = @"";
        // grouped product checks
        if([mainCollection[@"groupedData"] count] > 0){
            UIView *groupedUIView = [_mainView viewWithTag:5000];
            for(int i=0; i<[mainCollection[@"groupedData"] count]; i++) {
                NSDictionary *groupedDataDict = [mainCollection[@"groupedData"] objectAtIndex:i];
                UITextField *tempField = [groupedUIView viewWithTag:i+1];
                if([tempField.text isEqualToString:@"0"])
                    [selectedGroupedProduct setObject:@"0" forKey:groupedDataDict[@"id"]];
                else
                    [selectedGroupedProduct setObject:tempField.text forKey:groupedDataDict[@"id"]];
            }
        }
        // downloadble data
        if([mainCollection[@"links"][@"linksPurchasedSeparately"] isEqualToString:@"1"] ){
            UIView *downloadOptionUIView = [_mainView viewWithTag:6000];
            for(int i=0; i<[mainCollection[@"links"][@"linkData"] count]; i++){
                NSDictionary *linksOption = [mainCollection[@"links"][@"linkData"] objectAtIndex:i];
                UISwitch *switchValue = [downloadOptionUIView viewWithTag:i+1];
                if([switchValue isOn]){
                    [selectedDownloadableProduct setObject:linksOption[@"id"] forKey:linksOption[@"id"]];
                }else{
                    [selectedDownloadableProduct setObject:@"" forKey:linksOption[@"id"]];
                }
            }
        }
        // custome option data
        if([mainCollection[@"customOptions"] count] > 0){
            UIView *customOptionUIView = [_mainView viewWithTag:4000];
            for(int i=0; i<[mainCollection[@"customOptions"] count]; i++) {
                NSDictionary *customOptionDict = [mainCollection[@"customOptions"] objectAtIndex:i];
                if([customOptionDict[@"is_require"] integerValue] == 1){
                    if([customOptionDict[@"type"] isEqualToString:@"field"]){
                        UITextField *tempField = [customOptionUIView viewWithTag:i+1];
                        if([tempField.text isEqual:@""]){
                            [selectedCustomOption setObject:@"" forKey:customOptionDict[@"option_id"]];
                        }
                        else
                            [selectedCustomOption setObject:tempField.text forKey:customOptionDict[@"option_id"]];
                    }
                    
                    if([customOptionDict[@"type"] isEqualToString:@"area"]){
                        UITextView *tempArea = [customOptionUIView viewWithTag:i+1];
                        if([tempArea.text isEqual:@""]){
                            [selectedCustomOption setObject:@"" forKey:customOptionDict[@"option_id"]];
                        }
                        else
                            [selectedCustomOption setObject:tempArea.text forKey:customOptionDict[@"option_id"]];
                    }
                    if([customOptionDict[@"type"] isEqualToString:@"checkbox"] || [customOptionDict[@"type"] isEqualToString:@"multiple"]){
                        UIView *tempSwitchArea = [customOptionUIView viewWithTag:i+1];
                        [tempSwitchArea setBackgroundColor:[GlobalData colorWithHexString:@"FFFFFF"]];
                        int isSwitchOn = 0;
                        NSMutableArray *selectedOption = [[NSMutableArray alloc]init];
                        for(id key in [optionValues objectAtIndex:i]){
                            UISwitch *tempSwitch = [tempSwitchArea viewWithTag:[key integerValue]];
                            if([tempSwitch isOn]){
                                isSwitchOn = 1;
                                [selectedOption addObject:key];
                            }
                        }
                        if(isSwitchOn == 0){
                            [selectedCustomOption setObject:selectedOption forKey:customOptionDict[@"option_id"]];
                        }
                        else
                            [selectedCustomOption setObject:selectedOption forKey:customOptionDict[@"option_id"]];
                    }
                }
                else{
                    if([customOptionDict[@"type"] isEqualToString:@"field"]){
                        UITextField *tempField = [customOptionUIView viewWithTag:i+1];
                        [selectedCustomOption setObject:tempField.text forKey:customOptionDict[@"option_id"]];
                    }
                    if([customOptionDict[@"type"] isEqualToString:@"area"]){
                        UITextView *tempArea = [customOptionUIView viewWithTag:i+1];
                        [selectedCustomOption setObject:tempArea.text forKey:customOptionDict[@"option_id"]];
                    }
                    if([customOptionDict[@"type"] isEqualToString:@"checkbox"] || [customOptionDict[@"type"] isEqualToString:@"multiple"]){
                        UIView *tempSwitchArea = [customOptionUIView viewWithTag:i+1];
                        int isSwitchOn = 0;
                        NSMutableArray *selectedOption = [[NSMutableArray alloc]init];
                        for(id key in [optionValues objectAtIndex:i]){
                            UISwitch *tempSwitch = [tempSwitchArea viewWithTag:[key integerValue]];
                            if([tempSwitch isOn]){
                                isSwitchOn = 1;
                                [selectedOption addObject:key];
                            }
                        }
                        [selectedCustomOption setObject:selectedOption forKey:customOptionDict[@"option_id"]];
                    }
                }
                if([customOptionDict[@"type"] isEqualToString:@"radio"] || [customOptionDict[@"type"] isEqualToString:@"drop_down"]){
                    UIPickerView *tempPicker = [customOptionUIView viewWithTag:i+1];
                    int row = (int)[tempPicker selectedRowInComponent:0];
                    [selectedCustomOption setObject:[[optionValues objectAtIndex:i] objectAtIndex:row] forKey:customOptionDict[@"option_id"]];
                }
                if([customOptionDict[@"type"] isEqualToString:@"date"] || [customOptionDict[@"type"] isEqualToString:@"date_time"] || [customOptionDict[@"type"] isEqualToString:@"time"]){
                    UIDatePicker *tempDatePicker = [customOptionUIView viewWithTag:i+1];
                    NSDate *date = tempDatePicker.date;
                    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:date];
                    if([customOptionDict[@"type"] isEqualToString:@"date"]){
                        NSMutableDictionary *dateDict = [[NSMutableDictionary alloc]init];
                        [dateDict setObject:[NSString stringWithFormat:@"%ld", (long)[components month]] forKey:@"month"];
                        [dateDict setObject:[NSString stringWithFormat:@"%ld", (long)[components day]] forKey:@"day"];
                        [dateDict setObject:[NSString stringWithFormat:@"%ld", (long)[components year]] forKey:@"year"];
                        [selectedCustomOption setObject:dateDict forKey:customOptionDict[@"option_id"]];
                    }
                    if([customOptionDict[@"type"] isEqualToString:@"date_time"]){
                        NSMutableDictionary *dateTimeDict = [[NSMutableDictionary alloc]init];
                        [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components month]] forKey:@"month"];
                        [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components day]] forKey:@"day"];
                        [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components year]] forKey:@"year"];
                        [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components hour]] forKey:@"hour"];
                        [dateTimeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components minute]] forKey:@"minute"];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"a"];
                        [dateTimeDict setObject:[formatter stringFromDate:date] forKey:@"day_part"];
                        [selectedCustomOption setObject:dateTimeDict forKey:customOptionDict[@"option_id"]];
                    }
                    if([customOptionDict[@"type"] isEqualToString:@"time"]){
                        NSMutableDictionary *timeDict = [[NSMutableDictionary alloc]init];
                        [timeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components hour]] forKey:@"hour"];
                        [timeDict setObject:[NSString stringWithFormat:@"%ld", (long)[components minute]] forKey:@"minute"];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"a"];
                        [timeDict setObject:[formatter stringFromDate:date] forKey:@"day_part"];
                        [selectedCustomOption setObject:timeDict forKey:customOptionDict[@"option_id"]];
                    }
                }
            }
        }
        
        selectedBundleProduct = [[NSMutableDictionary alloc]init];
        selectedBundleProductQuantity = [[NSMutableDictionary alloc]init];
        NSMutableArray *value =[[NSMutableArray alloc] init];
        NSMutableArray *optionIdData =[[NSMutableArray alloc] init];
        UITextField *tempField1;
        if([mainCollection[@"bundleOptions"] count] > 0){
            UIView *bundleUIView = [_mainView viewWithTag:8000];
            for(int i=0; i<[mainCollection[@"bundleOptions"] count]; i++) {
                NSDictionary *bundleDict = [mainCollection[@"bundleOptions"] objectAtIndex:i];
                bundlekeyCount = 0;
                for(id key in bundleDict[@"optionValues"]){
                    value = bundleDict[@"optionValues"][key][@"optionValueId"];
                    [optionIdData addObject:value];
                    bundlekeyCount++;
                }
                tempField1 = [bundleUIView viewWithTag:i+4];
                int row = 0;
                UIPickerView *tempPicker;
                if(![tempField1.text isEqualToString:@"0"]){
                    tempPicker = [bundleUIView viewWithTag:i+1];
                    if(tempPicker.tag < [mainCollection[@"bundleOptions"] count])
                        row = (int)[tempPicker selectedRowInComponent:0];
                    else
                        row = bundleValueRow;
                    
                    if([bundleDict[@"required"] isEqualToString:@"1"] && bundlekeyCount == 1){
                        [selectedBundleProduct setObject:[optionIdData objectAtIndex:0] forKey:bundleDict[@"option_id"]];
                    }
                    if(i < [mainCollection[@"bundleOptions"] count] && (tempPicker.tag > 0) && row > 0 && tempPicker.tag < [mainCollection[@"bundleOptions"] count]){
                        [selectedBundleProduct setObject:[optionIdData objectAtIndex:row-1] forKey:bundleDict[@"option_id"]];
                    }
                    else if(tempPicker.tag == [mainCollection[@"bundleOptions"] count]){
                        [selectedBundleProduct setObject:[optionIdData objectAtIndex:row-1] forKey:bundleDict[@"option_id"]];
                    }
                    [optionIdData removeAllObjects];
                    [selectedBundleProductQuantity setObject:tempField1.text forKey:bundleDict[@"option_id"]];
                }
                if([tempField1.text isEqualToString:@"0"]){
                    [selectedBundleProduct setObject:@"" forKey:bundleDict[@"option_id"]];
                }
                [optionIdData removeAllObjects];
            }
        }
        
        // configurable data
        selectedConfigProduct = [[NSMutableDictionary alloc]init];
        if([mainCollection[@"configurableData"] count] > 0){
            UIView *configUIView = [_mainView viewWithTag:7000];
            for(int i = 1; i <= configPickerCount; i++){
                UIPickerView *tempPicker = [configUIView viewWithTag:i];
                int row = (int)[tempPicker selectedRowInComponent:0];
                NSMutableDictionary *firstConfigOptionId = [mainCollection[@"configurableData"][@"attributes"] objectAtIndex:i-1];
                if(row == 0){
                    [selectedConfigProduct setObject:@"" forKey:firstConfigOptionId[@"id"]];
                }
                else{
                    NSMutableDictionary *configOptionId = [firstConfigOptionId[@"options"] objectAtIndex:row-1];
                    [selectedConfigProduct setObject:configOptionId[@"id"] forKey:firstConfigOptionId[@"id"]];
                }
            }}
        whichApiDataToprocess = @"addToWishlist";
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)contactUsWindow:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 888;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UIView *contactBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5)];
    [contactBlock setBackgroundColor : [UIColor whiteColor]];
    contactBlock.layer.cornerRadius = 4;
    
    float y = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 32)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [sortTitle setText : @"SELLER CONTACTS"];
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [contactBlock addSubview : sortTitle];
    
    y += 42;
    UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 1)];
    [hr setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [contactBlock addSubview : hr];
    y +=10;
    UITextField *nameField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 40)];
    nameField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    nameField.textColor = [UIColor blackColor];
    if([preferences objectForKey:@"customerName"]){
        nameField.text = [preferences objectForKey:@"customerName"];
    }else{
        [nameField setPlaceholder:@"Enter Name"];
    }
    nameField.layer.borderWidth = 1.0f;
    nameField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    nameField.tag = 1;
    [nameField setKeyboardType:UIKeyboardTypePhonePad];
    nameField.textAlignment = NSTextAlignmentLeft;
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:nameField];
    
    y += 50;
    UITextField *emailField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 40)];
    emailField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    emailField.textColor = [UIColor blackColor];
    if([preferences objectForKey:@"customerEmail"]){
        emailField.text = [preferences objectForKey:@"customerEmail"];
    }else{
        [emailField setPlaceholder:@"Enter Name"];
    }
    [emailField setPlaceholder:@"Email Address"];
    emailField.layer.borderWidth = 1.0f;
    emailField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    emailField.tag = 2;
    [emailField setKeyboardType:UIKeyboardTypePhonePad];
    emailField.textAlignment = NSTextAlignmentLeft;
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:emailField];
    
    y +=50;
    UITextField *subjectField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 40)];
    subjectField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    subjectField.textColor = [UIColor blackColor];
    [subjectField setPlaceholder:@"Enter Subject"];
    subjectField.layer.borderWidth = 1.0f;
    subjectField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    subjectField.tag = 3;
    [subjectField setKeyboardType:UIKeyboardTypePhonePad];
    subjectField.textAlignment = NSTextAlignmentLeft;
    subjectField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:subjectField];
    
    y += 50;
    
    UITextView *textArea = [[UITextView alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 100)];
    textArea.layer.borderWidth = 1.0f;
    textArea.textColor = [UIColor blackColor];
    textArea.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textArea.font = [UIFont fontWithName: @"Trebuchet MS" size: 16.0f];
    textArea.tag = 4;
    [contactBlock addSubview:textArea ];
    
    y += 120;
    
    UIView *requestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , contactBlock.frame.size.width, 30)];
    requestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    requestContainer.layer.borderWidth = 0.0f;
    [contactBlock addSubview:requestContainer];
    
    NSDictionary *reqAttributesforCancelButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforCancelButtonText = [@"Cancel" sizeWithAttributes:reqAttributesforCancelButtonText];
    CGFloat reqStringWidthVCancelButtonText = reqStringSizeforCancelButtonText.width;
    UIButton  *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(contactCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0,0,reqStringWidthVCancelButtonText+20, 40);
    [cancelButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [cancelButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [cancelButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    
    [requestContainer addSubview:cancelButton];
    
    NSDictionary *reqAttributesforSubmitButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforSubmitButtonText = [@"Submit" sizeWithAttributes:reqAttributesforSubmitButtonText];
    CGFloat reqStringWidthSubmitButtonText = reqStringSizeforSubmitButtonText.width;
    UIButton  *SubmitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [SubmitButton addTarget:self action:@selector(contactSubmitButton :) forControlEvents:UIControlEventTouchUpInside];
    [SubmitButton setTitle:@"Submit" forState:UIControlStateNormal];
    SubmitButton.frame = CGRectMake(reqStringWidthVCancelButtonText +20 +10,0,reqStringWidthSubmitButtonText+20, 40);
    [SubmitButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [SubmitButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [SubmitButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    
    [requestContainer addSubview:SubmitButton];
    
    CGRect  newFrame = requestContainer.frame;
    newFrame.size.width = reqStringWidthVCancelButtonText +20 +10 + reqStringWidthSubmitButtonText+20;
    requestContainer.frame = newFrame;
    
    UIView *finalrequestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , contactBlock.frame.size.width, 30)];
    finalrequestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    finalrequestContainer.layer.borderWidth = 0.0f;
    requestContainer.center = CGPointMake(finalrequestContainer.frame.size.width  / 2,
                                          finalrequestContainer.frame.size.height / 2);
    [finalrequestContainer addSubview:requestContainer];
    
    [contactBlock addSubview:finalrequestContainer];
    
    CGRect  contactNewFrame = contactBlock.frame;
    contactNewFrame.size.height = y +50;
    contactBlock.frame = contactNewFrame;
    contactBlock.center = blurEffectView.center;
    [blurEffectView addSubview:contactBlock];
    [self.view addSubview:blurEffectView];
}
-(void)contactCancelButton :(UIButton*)button{
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    [parent2.superview removeFromSuperview];
}
-(void)contactSubmitButton :(UIButton*)button{
    contactUsData = [[NSMutableDictionary alloc]init];
    NSString *errorMessage = @"Please Fill";
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    UITextField *nameField = [parent2 viewWithTag:1];
    UITextField *emailField = [parent2 viewWithTag:2];
    UITextField *subjectlField = [parent2 viewWithTag:3];
    UITextView *queryField = [parent2 viewWithTag:4];
    
    
    isValid = 1;
    if ([queryField.text isEqualToString:@""]){
        isValid =0;
        errorMessage = @"Please enter the Query";
    }
    if ([subjectlField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Subject";
    }
    if ([emailField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Email";
    }
    if ([nameField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Name";
    }
    
    
    if(isValid == 1){
        [contactUsData setObject:nameField.text forKey:@"name"];
        [contactUsData setObject:emailField.text forKey:@"email"];
        [contactUsData setObject:subjectlField.text forKey:@"subject"];
        [contactUsData setObject:queryField.text forKey:@"query"];
        isValid=2;
    }
    
    if(isValid == 0){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    if(isValid == 2) {
        [parent2.superview removeFromSuperview];
        whichApiDataToprocess = @"contactSeller";
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}




-(void)changeImage:(UITapGestureRecognizer *)recognizer{
    int index = (int)recognizer.view.tag;
    NSDictionary *productImageDict = [mainCollection[@"imageGallery"] objectAtIndex:index];
    UIImageView *productImage = [_mainView viewWithTag:1000];
    UIImage *largeImage = [imageCache objectForKey:productImageDict[@"largeImage"]];
    productImage.image = [UIImage imageNamed:@"placeholder.png"];
    if(largeImage)
        productImage.image = largeImage;
    else{
        [_queue addOperationWithBlock:^{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:productImageDict[@"largeImage"]]];
            UIImage *largeImage = [UIImage imageWithData: imageData];
            if(largeImage){
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    productImage.image = largeImage;
                }];
                [imageCache setObject:largeImage forKey:productImageDict[@"largeImage"]];
            }
        }];
    }
}

@end
