//
//  addReview.h
//  Mobikul
//
//  Created by Ratnesh on 26/04/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddReview : UIViewController <NSXMLParserDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id mainCollection, addReviewCollection,collection;
    NSCache *imageCache;
    NSString *whichApiDataToprocess, *sizeModified;
    NSMutableDictionary *ratingsDict;
    NSMutableArray *storeSubviewsTag;
    UIWindow *currentWindow;
    
    //New
    UIImageView *IV_SelectImg;
    UIImagePickerController *IMG_PickerController;
    
    BOOL SelectImageBool;
    NSString *Str_ReviewImageBase64;
    
    NSMutableData *responseData;
}

@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic) NSString *productId;
@property (nonatomic) NSString *productName;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraint;
@property (strong, nonatomic) IBOutlet UIView *mainBaseView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMarginConstraint;

@end
