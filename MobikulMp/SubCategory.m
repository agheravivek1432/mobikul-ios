//
//  SubCategory.m
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "SubCategory.h"
#import "GlobalData.h"
#import "ProductCategory.h"
GlobalData *globalObjectSubCat;
@implementation SubCategory

- (void)viewDidLoad {
    [super viewDidLoad];
    preferences = [NSUserDefaults standardUserDefaults];
    globalObjectSubCat=[[GlobalData alloc] init];
    globalObjectSubCat.delegate=self;
    [globalObjectSubCat language];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.title = [NSString stringWithFormat:@"%@'s Sub Category", _categoryName];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:@"00CBDF"];
    subCategories = [[NSMutableArray alloc] init];
    self.navigationItem.title = [globalObjectSubCat.languageBundle localizedStringForKey:@"subCategory" value:@"" table:nil];
    [subCategories addObject:[NSString stringWithFormat:@"View %@", _categoryName]];
    for(int i = 0; i < [_subCategoryData[@"children"] count]; i++) {
        NSDictionary *subCaterieDict = [_subCategoryData[@"children"] objectAtIndex:i];
        [subCategories addObject:subCaterieDict[@"name"]];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [subCategories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [subCategories objectAtIndex:[indexPath row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    if([indexPath row] == 0){
        categoryDict = [NSMutableDictionary dictionary];
        [categoryDict setObject:_subCategoryData[@"category_id"] forKey:@"category_id"];
        [categoryDict setObject:_subCategoryData[@"name"] forKey:@"name"];
    }
    else{
        int rowCount = (int)[indexPath row]-1;
        categoryDict = [_subCategoryData[@"children"] objectAtIndex:rowCount];
    }
    [self performSegueWithIdentifier:@"subCategorySegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"subCategorySegue"]) {
        //UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        ProductCategory *destViewController = segue.destinationViewController;
        destViewController.categoryId = categoryDict[@"category_id"];
        destViewController.categoryName = categoryDict[@"name"];
    }
}
@end
