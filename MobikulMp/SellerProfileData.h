//
//  SellerProfileData.h
//  MobikulMp
//
//  Created by kunal prasad on 21/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SellerProfileData : UIViewController
{
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    NSString *name,*entityId,*typeId;
    NSString *sessionId, *message, *dataFromApi ;
    UIAlertController *alert;
    NSCache *imageCache;
    id mainCollection ,addToWishListCollection, addToCartCollection,contactSellerCollection,collection;
    UIScrollView *recentCollectionScrollView;
    NSMutableArray *featuredProductCurtainOpenSignal;
    NSString *whichApiDataToprocess, *productIdForApiCall;
    NSMutableDictionary *contactUsData;
    NSInteger isValid;
    NSString *sellerId;
    UIWindow *currentWindow;
}
@property (nonatomic) NSString *profileUrl;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *socialIconsScrollView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *socialIconScrollViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *socialIconScrollViewHeight;
@property (nonatomic, strong) NSOperationQueue *queue;

@end
