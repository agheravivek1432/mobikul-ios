//
//  AllSellerList.h
//  MobikulMp
//
//  Created by kunal prasad on 29/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllSellerList : UIViewController<UIScrollViewDelegate>{
    
    NSCache *imageCache;
    NSInteger isAlertVisible;
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSString *sessionId, *dataFromApi;
    id mainCollection,collection;
    NSString *whichApiDataToProcess;
    NSMutableDictionary *shopTitle;
    NSString *profileUrl;
    UITapGestureRecognizer *sellorTitleTap ;
    UIWindow *currentWindow;
    
}
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;
@property (strong, nonatomic) IBOutlet UIView *mainBaseView;
@property (weak, nonatomic) IBOutlet UIScrollView *sellerListScrollView;

@end
