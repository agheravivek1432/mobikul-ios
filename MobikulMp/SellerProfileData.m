//
//  SellerProfileData.m
//  MobikulMp
//
//  Created by kunal prasad on 21/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "SellerProfileData.h"
#import "GlobalData.h"
#import "ToastView.h"
#import "CatalogProduct.h"
#import "MakeReviewData.h"
#import "BlogListViewController.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)





@interface SellerProfileData ()

@end

GlobalData *globalObjectSellerProfileData;

@implementation SellerProfileData

- (void)viewDidLoad {
    [super viewDidLoad];
    imageCache = [[NSCache alloc] init];
    globalObjectSellerProfileData = [[GlobalData alloc] init];
    globalObjectSellerProfileData.delegate = self;
    [globalObjectSellerProfileData language];
    isAlertVisible = 0;
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    whichApiDataToprocess = @"";
    isValid = 0;
    preferences = [NSUserDefaults standardUserDefaults];
     NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectSellerProfileData callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectSellerProfileData.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectSellerProfileData.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectSellerProfileData.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectSellerProfileData.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectSellerProfileData.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    NSString *quoteId = [preferences objectForKey:@"quoteId"];
    if([whichApiDataToprocess isEqual:@"addToWishlist"]){
        if(customerId != nil)
            [post appendFormat:@"customerId=%@&", customerId];
        else{
            [post appendFormat:@"quoteId=%@&", quoteId];
        }
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectSellerProfileData callHTTPPostMethod:post api:@"mobikulhttp/catalog/addtoWishlist" signal:@"HttpPostMetod"];
    }
    else if([whichApiDataToprocess isEqual: @"addToCart"]){
        if(customerId !=nil)
            [post appendFormat:@"customerId=%@&", customerId];
        NSString *quoteId = [preferences objectForKey:@"quoteId"];
        if(quoteId != nil)
            [post appendFormat:@"quoteId=%@&", quoteId];
        [post appendFormat:@"productId=%@&", productIdForApiCall];
        NSString *storeId = [preferences objectForKey:@"storeId"];
        [post appendFormat:@"storeId=%@", storeId];
        [globalObjectSellerProfileData callHTTPPostMethod:post api:@"mobikulhttp/checkout/addtoCart" signal:@"HttpPostMetod"];
        globalObjectSellerProfileData.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"contactSeller"]){
        NSString *sellerid = [preferences objectForKey:@"sellerId"];
        [post appendFormat:@"storeId=%@&", storeId];
        [post appendFormat:@"subject=%@&", [contactUsData objectForKey:@"subject"]];
        [post appendFormat:@"query=%@&", [contactUsData objectForKey:@"query"]];
        [post appendFormat:@"sellerId=%@&", sellerid];
        [post appendFormat:@"customerEmail=%@&",[contactUsData objectForKey:@"name"]];
        [post appendFormat:@"customerName=%@", [contactUsData objectForKey:@"email"]];
        [globalObjectSellerProfileData callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/contactSeller" signal:@"HttpPostMetod"];
        globalObjectSellerProfileData.delegate = self;
    }
    else{
         [post appendFormat:@"storeId=%@&", storeId];
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@&", screenWidth];
        [post appendFormat:@"profileUrl=%@", _profileUrl];
        [globalObjectSellerProfileData callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getsellerprofileData" signal:@"HttpPostMetod"];
        globalObjectSellerProfileData.delegate = self;

    }
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    if([whichApiDataToprocess isEqual: @"addToWishlist"]){
        addToWishListCollection = collection;
        if([addToWishListCollection[@"status"] boolValue])
            [ToastView showToastInParentView:self.view withText:@"Product Added To Wishlist Successfully." withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:@"Sorry!! Something went wrong please try again later." withStatus:@"error" withDuaration:5.0];
    }
    else
        if([whichApiDataToprocess isEqual: @"addToCart"]){
            addToCartCollection = collection;
            if([addToCartCollection objectForKey:@"quoteId"]){
                [preferences setObject:addToCartCollection[@"quoteId"] forKey:@"quoteId"];
                [preferences synchronize];
            }
            if([addToCartCollection objectForKey:@"cartCount"])
                [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", addToCartCollection[@"cartCount"]]];
            if([addToCartCollection[@"error"] boolValue])
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"error" withDuaration:5.0];
            else
                [ToastView showToastInParentView:self.view withText:addToCartCollection[@"message"] withStatus:@"success" withDuaration:5.0];
    }
        else if([whichApiDataToprocess isEqual:@"contactSeller"]){
            contactSellerCollection = collection;
            if([contactSellerCollection objectForKey:@"message"]){
                
                [ToastView showToastInParentView:self.view withText:contactSellerCollection[@"message"] withStatus:@"success" withDuaration:5.0];
            }else{
                [ToastView showToastInParentView:self.view withText:contactSellerCollection[@"message"] withStatus:@"error" withDuaration:5.0];
            }
            
        }
    else {
    
    mainCollection = collection;
    float mainContainerY = 0;
    float internalY = 0;
    
    [preferences setObject:mainCollection[@"sellerId"] forKey:@"sellerId"];
    // logo image and extra image
    
   UIView *bannerContainer = [[UIView alloc] initWithFrame:CGRectMake(5, 0 , _mainView.frame.size.width-10, SCREEN_WIDTH/2)];
    bannerContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    bannerContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:bannerContainer];
    
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10 , SCREEN_WIDTH/4, SCREEN_WIDTH/4)];
    logo.image = [UIImage imageNamed:@"ic_placeholder.png"];
    logo.userInteractionEnabled = YES;
    UIImage *image = [imageCache objectForKey:mainCollection[@"logo"]];
    if(image)
        logo.image = image;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"logo"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        logo.image = image;
        [imageCache setObject:image forKey:mainCollection[@"logo"]];
    }
    [bannerContainer addSubview:logo];
    
    UIImageView *worldMap = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4 +5, 10 ,bannerContainer.frame.size.width-logo.frame.size.width-5, SCREEN_WIDTH/4)];
        
    [worldMap setImage:[UIImage imageNamed:@"ic_world.png"]];
    [bannerContainer addSubview:worldMap];
    
    CGRect bannernewFrame = bannerContainer.frame;
    bannernewFrame.size.height = SCREEN_WIDTH /4 + 10;
    bannerContainer.frame = bannernewFrame;

    
   
    mainContainerY += SCREEN_WIDTH/4 + 10;
    
    // profile url ,map,address , social icon
    
    internalY = 5;
    UIView *sellerContainer = [[UIView alloc] initWithFrame:CGRectMake(5, mainContainerY +5 , _mainView.frame.size.width-10, 300)];
    sellerContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    sellerContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:sellerContainer];
    
    NSDictionary *reqAttributesforProfileTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]};
    CGSize reqStringSizeProfileTitle = [mainCollection[@"shopTitle"] sizeWithAttributes:reqAttributesforProfileTitle];
    CGFloat reqStringWidthProfileTitle = reqStringSizeProfileTitle.width;
    UILabel *profileTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, internalY , reqStringWidthProfileTitle, 30)];
    [profileTitleLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [profileTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [profileTitleLabel setText:mainCollection[@"shopTitle"]];
    
    [sellerContainer addSubview: profileTitleLabel];
    
    internalY += 35;
    
    UIImageView *countryFlag = [[UIImageView alloc] initWithFrame:CGRectMake(20, internalY , 2*SCREEN_WIDTH/16, SCREEN_WIDTH/16)];
    countryFlag.image = [UIImage imageNamed:@"ic_placeholder.png"];
    logo.userInteractionEnabled = YES;
    UIImage *countryImage = [imageCache objectForKey:mainCollection[@"countryflagUrl"]];
    if(image)
        countryFlag.image = countryImage;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"countryflagUrl"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        countryFlag.image = image;
        [imageCache setObject:image forKey:mainCollection[@"countryflagUrl"]];
    }
    [sellerContainer addSubview:countryFlag];
        
        
    NSDictionary *reqAttributesForContactUs = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeContactUs = [@" Contact Us" sizeWithAttributes:reqAttributesForContactUs];
    CGFloat reqStringWidthContactUs = reqStringSizeContactUs.width;
    UILabel *contactUsLabel = [[UILabel alloc] initWithFrame:CGRectMake(sellerContainer.frame.size.width -reqStringWidthContactUs -10 , internalY + 5, reqStringWidthContactUs, 20)];
    [contactUsLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
    [contactUsLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [contactUsLabel setText:@" Contact Us"];
    [contactUsLabel setUserInteractionEnabled:YES];
    [sellerContainer addSubview:contactUsLabel];
    
    UITapGestureRecognizer *openContactUsWindow = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contactUsWindow:)];
    openContactUsWindow.numberOfTapsRequired = 1;
    [contactUsLabel addGestureRecognizer:openContactUsWindow];

    internalY += SCREEN_WIDTH /16;
    
    NSDictionary *reqAttributesForAddress = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeAddress = [mainCollection[@"address"] sizeWithAttributes:reqAttributesForAddress];
    CGFloat reqStringWidthAddress = reqStringSizeAddress.width;
    CGFloat reqStringHeightaddress = reqStringSizeAddress.height ;
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, internalY + 5, reqStringWidthAddress, reqStringHeightaddress)];
    [addressLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [addressLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [addressLabel setText:mainCollection[@"address"]];
    
    [sellerContainer addSubview:addressLabel];

    internalY += reqStringHeightaddress + 10;
    
    // Scroll view for social icon
    
    UIScrollView *socialIconScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(20, internalY  ,sellerContainer.frame.size.width-20, 50)];
    //[socialIconScrollView setBackgroundColor: [UIColor grayColor]];
    socialIconScrollView.userInteractionEnabled = YES;
    [sellerContainer addSubview:socialIconScrollView];
    float shiftX = 0;
    NSInteger flag = 0;
    
    
    if([mainCollection[@"IGIsActive"] isEqualToString:@"1"]){
        UIImageView *instagram = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [instagram setImage:[UIImage imageNamed:@"ic_instagram.png"]];
        [socialIconScrollView addSubview:instagram];
        shiftX += 60;
        flag = 1;
    }
    
    
    if([mainCollection[@"FBIsActive"] isEqualToString:@"1"]){
        UIImageView *facebook = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [facebook setImage:[UIImage imageNamed:@"ic_facebook.png"]];
        [socialIconScrollView addSubview:facebook];
        shiftX += 60;
        flag = 1;
    }
    if([mainCollection[@"PIIsActive"] isEqualToString:@"1"]){
        UIImageView *pinterest = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [pinterest setImage:[UIImage imageNamed:@"ic_pinterest.png"]];
        [socialIconScrollView addSubview:pinterest];
        shiftX += 60;
        flag = 1;
    }
    
    if([mainCollection[@"TWIsActive"] isEqualToString:@"1"]){
        UIImageView *twitter = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [twitter setImage:[UIImage imageNamed:@"ic_twitter1.png"]];
        [socialIconScrollView addSubview:twitter];
         shiftX +=60;
        flag = 1;
    }
    
    if([mainCollection[@"GPIsActive"] isEqualToString:@"1"]){
        UIImageView *googleplus = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [googleplus setImage:[UIImage imageNamed:@"ic_g+.png"]];
        [socialIconScrollView addSubview:googleplus];
        shiftX +=60;
        flag = 1;
    }
    
    if([mainCollection[@"VIIsActive"] isEqualToString:@"1"]){
        UIImageView *vimeo = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [vimeo setImage:[UIImage imageNamed:@"ic_vimeo.png"]];
        [socialIconScrollView addSubview:vimeo];
        shiftX += 60;
        flag = 1;
    }
    if([mainCollection[@"YTIsActive"] isEqualToString:@"1"]){
        UIImageView *youtube = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [youtube setImage:[UIImage imageNamed:@"ic_youtube.png"]];
        [socialIconScrollView addSubview:youtube];
        shiftX += 60;
        flag = 1;
    }
    if([mainCollection[@"MSIsActive"] isEqualToString:@"1"]){
        UIImageView *moleskine = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX, 0, 50, 50)];
        [moleskine setImage:[UIImage imageNamed:@"ic_moleskine.jpg"]];
        [socialIconScrollView addSubview:moleskine];
        shiftX += 60;
        flag = 1;
    }
    
    if(flag == 0 ){
        socialIconScrollView.hidden = YES;
        [socialIconScrollView removeFromSuperview];
    }
    else{
    socialIconScrollView.contentSize = CGSizeMake(shiftX,50);
        internalY += 50;
    }
    
    CGRect sellerContainernewFrame = sellerContainer.frame;
    sellerContainernewFrame.size.height = internalY + 10;
    sellerContainer.frame = sellerContainernewFrame;
    
    mainContainerY += internalY +10;
    
    
    // description
    
    internalY = 0;
    UIView *descriptionContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY +10 ,  _mainView.frame.size.width-10, 200)];
    descriptionContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    descriptionContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:descriptionContainer];
    
    
    UILabel *descriptionHeading = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,descriptionContainer.frame.size.width, 30)];
    [descriptionHeading setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [descriptionHeading setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [descriptionHeading setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
    [descriptionHeading setText:@"  Description"];
    [descriptionContainer addSubview:descriptionHeading];

    internalY += 30;
    NSDictionary *reqAttributesforDesc = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:19.0f]};
    CGSize reqStringSizeDesc = [mainCollection[@"sellerDescription"] sizeWithAttributes:reqAttributesforDesc];
    UILabel *sellerDesc = [[UILabel alloc] initWithFrame:CGRectMake(5,30,  _mainView.frame.size.width-20, 25)];
    [sellerDesc setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [sellerDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.0f]];
    [sellerDesc setText:mainCollection[@"sellerDescription"]];
    sellerDesc.lineBreakMode = NSLineBreakByWordWrapping;
    sellerDesc.numberOfLines = 0;
    sellerDesc.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    sellerDesc.textAlignment = NSTextAlignmentLeft;
    [sellerDesc sizeToFit];
    sellerDesc.preferredMaxLayoutWidth = descriptionContainer.frame.size.width-10 ;
    [descriptionContainer addSubview:sellerDesc];

    internalY +=sellerDesc.frame.size.height +10;
    mainContainerY += internalY;
    
    CGRect descriptionrNewFrame = descriptionContainer.frame;
    descriptionrNewFrame.size.height = internalY ;
    descriptionContainer.frame = descriptionrNewFrame;
    
    // image scroller

    mainContainerY +=20;
        
    float scrollViewHeight;
    float productBlockHeight;
    if(SCREEN_WIDTH > 500){
    scrollViewHeight = (SCREEN_WIDTH/2.5)+117;
    productBlockHeight = (SCREEN_WIDTH/2.5)+110;
    }
    else{
    scrollViewHeight = (SCREEN_WIDTH/2.5)+80;
    productBlockHeight = (SCREEN_WIDTH/2.5)+70;
    }
        
        
    
    recentCollectionScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(5, mainContainerY , _mainView.frame.size.width-10, scrollViewHeight)];
   // recentCollectionScrollView.delegate = self;
    recentCollectionScrollView.tag = 2;
    //[recentCollectionScrollView setBackgroundColor: [UIColor grayColor]];
    recentCollectionScrollView.userInteractionEnabled = YES;
    [_mainView addSubview:recentCollectionScrollView];
    
    shiftX = 5;
    int y = 5;
    float subWish = 64;
    float subAdd = 32;
    
      featuredProductCurtainOpenSignal = [[NSMutableArray alloc] init];
    for(int i=0 ; i<[mainCollection[@"recentCollection"] count] ;i++){
    NSDictionary *recentCollectionDict = [mainCollection[@"recentCollection"] objectAtIndex:i];
    
    UIView *productBlock = [[UIView alloc]initWithFrame:CGRectMake(shiftX, y, (SCREEN_WIDTH/2.5)+10, productBlockHeight)];
    featuredProductCurtainOpenSignal[i] = @"0";
    shiftX +=SCREEN_WIDTH/2.5 +20;
    [productBlock setBackgroundColor:[UIColor whiteColor]];
    productBlock.layer.cornerRadius = 2;
    productBlock.layer.shadowOffset = CGSizeMake(0, 0);
    productBlock.layer.shadowRadius = 3;
    productBlock.layer.shadowOpacity = 0.5;
    [recentCollectionScrollView addSubview:productBlock];
        
        UIView *productImageBlock = [[UIView alloc]initWithFrame:CGRectMake(5, y, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
        productImageBlock.clipsToBounds = YES;
        UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
        productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
        productImage.userInteractionEnabled = YES;
        productImage.tag = i;
        UIImage *image = [imageCache objectForKey:recentCollectionDict[@"thumbNail"]];
        if(image)
            productImage.image = image;
        else{
            [_queue addOperationWithBlock:^{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:recentCollectionDict[@"thumbNail"]]];
                UIImage *image = [UIImage imageWithData: imageData];
                if(image){
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        productImage.image = image;
                    }];
                    [imageCache setObject:image forKey:recentCollectionDict[@"thumbNail"]];
                }
            }];
        }
        UITapGestureRecognizer *openFeaturedProductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recentViewProduct:)];
        openFeaturedProductGesture.numberOfTapsRequired = 1;
        [productImage addGestureRecognizer:openFeaturedProductGesture];
        
        [productImageBlock addSubview:productImage];
        UIView *productImageCurtain = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5,SCREEN_WIDTH/2.5)];
        [productImageCurtain setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        if(SCREEN_WIDTH < 500){
            subWish = 54;
            subAdd = 22;
        }
        
        
        UIImageView *wishlistButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)-subWish, (SCREEN_WIDTH/2.5)-40, 32, 32)];
        [wishlistButton setImage:[UIImage imageNamed:@"ic_addtowishlist.png"]];
        wishlistButton.tag = i;
        wishlistButton.userInteractionEnabled = YES;
        UITapGestureRecognizer *addtowishlistGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredAddtoWishlist:)];
        addtowishlistGesture.numberOfTapsRequired = 1;
        [wishlistButton addGestureRecognizer:addtowishlistGesture];
        [productImageCurtain addSubview:wishlistButton];
        if([recentCollectionDict[@"hasOptions"] isEqual: @"1"] || [recentCollectionDict[@"typeId"] isEqual: @"configurable"] || [recentCollectionDict[@"typeId"] isEqual: @"bundle"] || [recentCollectionDict[@"typeId"] isEqual: @"grouped"]){
            UIImageView *viewproductButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
            [viewproductButton setImage:[UIImage imageNamed:@"ic_viewproduct.png"]];
            viewproductButton.tag = i;
            viewproductButton.userInteractionEnabled = YES;
            UITapGestureRecognizer *viewproductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recentViewProduct:)];
            viewproductGesture.numberOfTapsRequired = 1;
            [viewproductButton addGestureRecognizer:viewproductGesture];
            [productImageCurtain addSubview:viewproductButton];
        }
        else{
            UIImageView *addtocartButton = [[UIImageView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH/2.5)/2)+subAdd, (SCREEN_WIDTH/2.5)-40, 32, 32)];
            [addtocartButton setImage:[UIImage imageNamed:@"ic_addtocart.png"]];
            addtocartButton.tag = i;
            addtocartButton.userInteractionEnabled = YES;
            UITapGestureRecognizer *addtocartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredAddtoCart:)];
            addtocartGesture.numberOfTapsRequired = 1;
            [addtocartButton addGestureRecognizer:addtocartGesture];
            [productImageCurtain addSubview:addtocartButton];
        }
        [productImageBlock addSubview:productImageCurtain];
        [productBlock addSubview:productImageBlock];
    
        y += (SCREEN_WIDTH/2.5)+5;
        UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake(10, y, (SCREEN_WIDTH/2.5)-50, 25)];
        [productName setTextColor:[GlobalData colorWithHexString:@"555555"]];
        [productName setBackgroundColor:[UIColor clearColor]];
        [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
        [productName setText:recentCollectionDict[@"name"]];
        [productBlock addSubview:productName];
        
        
        if(SCREEN_WIDTH > 500){
        
        y += 30;
        if([recentCollectionDict[@"typeId"] isEqual:@"grouped"]){
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 90, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [preString setText:@"Starting at:"];
            [productBlock addSubview:preString];
            
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(95, y, 70, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [price setText:recentCollectionDict[@"groupedPrice"]];
            [productBlock addSubview:price];
        }
        else
            if([recentCollectionDict[@"typeId"] isEqual:@"bundle"]){
                UILabel *from = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 50, 25)];
                [from setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [from setBackgroundColor:[UIColor clearColor]];
                [from setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [from setText:@"From:"];
                [productBlock addSubview:from];
                
                UILabel *minPrice = [[UILabel alloc] initWithFrame:CGRectMake(55, y, 60, 25)];
                [minPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [minPrice setBackgroundColor:[UIColor clearColor]];
                [minPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [minPrice setText:recentCollectionDict[@"formatedMinPrice"]];
                [productBlock addSubview:minPrice];
                
                UILabel *to = [[UILabel alloc] initWithFrame:CGRectMake(125, y, 25, 25)];
                [to setTextColor:[GlobalData colorWithHexString:@"555555"]];
                [to setBackgroundColor:[UIColor clearColor]];
                [to setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [to setText:@"To:"];
                [productBlock addSubview:to];
                
                UILabel *maxPrice = [[UILabel alloc] initWithFrame:CGRectMake(147, y, 60, 25)];
                [maxPrice setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                [maxPrice setBackgroundColor:[UIColor clearColor]];
                [maxPrice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                [maxPrice setText:recentCollectionDict[@"formatedMaxPrice"]];
                [productBlock addSubview:maxPrice];
            }
            else{
                if(![recentCollectionDict[@"specialPrice"] isEqual:[NSNull null]] && [recentCollectionDict[@"isInRange"] boolValue]){
                    UILabel *regularprice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 70, 25)];
                    [regularprice setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [regularprice setBackgroundColor:[UIColor clearColor]];
                    [regularprice setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:recentCollectionDict[@"formatedPrice"]];
                    [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@2 range:NSMakeRange(0, [attributeString length])];
                    [regularprice setAttributedText:attributeString];
                    [productBlock addSubview:regularprice];
                    
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(80, y, 85, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [price setText:recentCollectionDict[@"formatedSpecialPrice"]];
                    [productBlock addSubview:price];
                }
                else{
                    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 60, 25)];
                    [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [price setBackgroundColor:[UIColor clearColor]];
                    [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
                    [price setText:recentCollectionDict[@"formatedPrice"]];
                    [productBlock addSubview:price];
                }
            }
        if([recentCollectionDict[@"hasTierPrice"] isEqual:@"true"] && [recentCollectionDict[@"typeId"] isEqual: @"simple"]){
            UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake(70, y, 90, 25)];
            [preString setTextColor:[GlobalData colorWithHexString:@"cf5050"]];
            [preString setBackgroundColor:[UIColor clearColor]];
            [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [preString setText:@"AS LOW AS:"];
            [productBlock addSubview:preString];
            
            UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(155, y, 60, 25)];
            [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
            [price setBackgroundColor:[UIColor clearColor]];
            [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:17.0f]];
            [price setText:recentCollectionDict[@"tierPrice"]];
            [productBlock addSubview:price];
        }
       }
        float yProductOption;
        if(SCREEN_WIDTH > 500)
            yProductOption = (SCREEN_WIDTH/2.5)+65;
        else
            yProductOption = (SCREEN_WIDTH/2.5)+10;
        UIImageView *productOption = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)-25, yProductOption, 32, 32)];
        [productOption setImage:[UIImage imageNamed:@"ic_pro_option.png"]];
        productOption.userInteractionEnabled = YES;
        productOption.tag = i;
        [productBlock addSubview:productOption];
        UITapGestureRecognizer *showOptionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(featuredProductShowOptions:)];
        showOptionGesture.numberOfTapsRequired = 1;
        [productOption addGestureRecognizer:showOptionGesture];
        
        y += 30;
        UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(10,y,120,24)];
        UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
        [ratingContainer addSubview:grayContainer];
        UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI1];
        UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI2];
        UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI3];
        UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI4];
        UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI5];
        
        double percent = 24 * [recentCollectionDict[@"rating"] doubleValue];
        UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
        blueContainer.clipsToBounds = YES;
        [ratingContainer addSubview:blueContainer];
        UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI1];
        UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI2];
        UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI3];
        UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI4];
        UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI5];
        [productBlock addSubview:ratingContainer];
        
        y = 5;
    }
    recentCollectionScrollView.contentSize = CGSizeMake(shiftX -5 ,110);
    
    mainContainerY +=scrollViewHeight;
    
    // return policy
    
    internalY = 0;
    UIView *returnPolicyContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY +10 ,  _mainView.frame.size.width-10, 100)];
    returnPolicyContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    returnPolicyContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:returnPolicyContainer];
    
    
     UILabel *returnPolicy = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,returnPolicyContainer.frame.size.width, 30)];
     [returnPolicy setTextColor:[UIColor blackColor]];
     [returnPolicy setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
     [returnPolicy setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
     [returnPolicy setText:@"  Return Policy"];
     [returnPolicyContainer addSubview:returnPolicy];

    internalY += 30;
    UILabel *returnPolicyDesc = [[UILabel alloc] initWithFrame:CGRectMake(5,30,  _mainView.frame.size.width-20, 25)];
    NSAttributedString  *attrStringReturnPolicy = [[NSAttributedString alloc] initWithData:[mainCollection[@"returnPolicy"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //[returnPolicyDesc setTextColor:[GlobalData colorWithHexString:@"000000"]];
    //[returnPolicyDesc setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
       returnPolicyDesc.attributedText = attrStringReturnPolicy;
    [returnPolicyDesc setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    [returnPolicyDesc setLineBreakMode: NSLineBreakByWordWrapping];
    returnPolicyDesc.numberOfLines = 0;
    returnPolicyDesc.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    returnPolicyDesc.textAlignment = NSTextAlignmentLeft;
    [returnPolicyDesc sizeToFit];
    returnPolicyDesc.preferredMaxLayoutWidth = descriptionContainer.frame.size.width-10 ;
    [returnPolicyContainer addSubview:returnPolicyDesc];
    
    internalY +=returnPolicyDesc.frame.size.height +10;
    mainContainerY += internalY +10;
    
    CGRect returnPolicyNewFrame = returnPolicyContainer.frame;
    returnPolicyNewFrame.size.height = internalY ;
    returnPolicyContainer.frame = returnPolicyNewFrame;

    
    //shipping policy
    
    internalY = 0;
    UIView *shippingPolicyContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY +10 ,  _mainView.frame.size.width-10, 100)];
    shippingPolicyContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    shippingPolicyContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:shippingPolicyContainer];
    
    
    UILabel *shippingPolicy = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,returnPolicyContainer.frame.size.width, 30)];
    [shippingPolicy setTextColor:[UIColor blackColor]];
    [shippingPolicy setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [shippingPolicy setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
    [shippingPolicy setText:@"  Shipping Policy"];
    [shippingPolicyContainer addSubview:shippingPolicy];
    
    internalY += 30;
    UILabel *shippingPolicyDesc = [[UILabel alloc] initWithFrame:CGRectMake(5,30,  _mainView.frame.size.width-20, 25)];
    NSAttributedString  *attrStringShippingPolicy = [[NSAttributedString alloc] initWithData:[mainCollection[@"shippingPolicy"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    shippingPolicyDesc.attributedText = attrStringShippingPolicy;
    [shippingPolicyDesc setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [shippingPolicyDesc setFont:[UIFont fontWithName:@"Trebuchet MS" size:19.0f]];
    shippingPolicyDesc.lineBreakMode = NSLineBreakByWordWrapping;
    shippingPolicyDesc.numberOfLines = 0;
    shippingPolicyDesc.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    shippingPolicyDesc.textAlignment = NSTextAlignmentLeft;
    [shippingPolicyDesc sizeToFit];
    shippingPolicyDesc.preferredMaxLayoutWidth = descriptionContainer.frame.size.width-10 ;
    [shippingPolicyContainer addSubview:shippingPolicyDesc];
    
    internalY +=shippingPolicyDesc.frame.size.height +10;
    mainContainerY += internalY +10;
    
    CGRect  shippingPolicyNewFrame = shippingPolicyContainer.frame;
    shippingPolicyNewFrame.size.height = internalY ;
    shippingPolicyContainer.frame = shippingPolicyNewFrame;
    
    
    // avrerage rating container
    
    internalY = 0;
    float avgValue = 0;
    UIView *averageRatingContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY +10 ,  _mainView.frame.size.width-10, 200)];
    averageRatingContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    averageRatingContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:averageRatingContainer];
    
    
    UILabel *averageRatingHead = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,returnPolicyContainer.frame.size.width, 30)];
    [averageRatingHead setTextColor:[UIColor blackColor]];
    [averageRatingHead setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [averageRatingHead setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
    [averageRatingHead setText:@"  Rating"];
    [averageRatingContainer addSubview:averageRatingHead];
    
    internalY +=40;
    float initialYPositionForAverageRating = internalY;
    NSDictionary *reqAttributesforAvgRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeAvgratingTitle = [@"  Average Rating" sizeWithAttributes:reqAttributesforAvgRatingTitle];
    CGFloat reqStringWidthAvgRatingTitle = reqStringSizeAvgratingTitle.width;
    UILabel *averageRating = [[UILabel alloc] initWithFrame:CGRectMake(10, internalY,reqStringWidthAvgRatingTitle, 25)];
    [averageRating setTextColor:[UIColor blackColor]];
    [averageRating setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    [averageRating setText:@"Average Rating"];
    [averageRatingContainer addSubview:averageRating];
    
    internalY +=25;
    
    for(int i=0;i<[mainCollection[@"averageRatingData"] count];i++ ){
      NSDictionary *averageRatingDict = [mainCollection[@"averageRatingData"] objectAtIndex:i];
        avgValue +=[averageRatingDict[@"value"] doubleValue];
        NSDictionary *reqAttributesforAvgRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeAvgratingTitle = [averageRatingDict[@"label"] sizeWithAttributes:reqAttributesforAvgRatingTitle];
        CGFloat reqStringWidthAvgRatingTitle = reqStringSizeAvgratingTitle.width;
        UILabel *averageRating = [[UILabel alloc] initWithFrame:CGRectMake(10, internalY,reqStringWidthAvgRatingTitle, 25)];
        [averageRating setTextColor:[UIColor blackColor]];
        [averageRating setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
        [averageRating setText:averageRatingDict[@"label"]];
        [averageRatingContainer addSubview:averageRating];
        
        UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(100,internalY,120,24)];
        UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
        [ratingContainer addSubview:grayContainer];
        UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI1];
        UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI2];
        UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI3];
        UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI4];
        UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
        [grayContainer addSubview:gI5];
        
        double percent = 24 * [averageRatingDict[@"value"] doubleValue];
        
        UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
        blueContainer.clipsToBounds = YES;
        [ratingContainer addSubview:blueContainer];
        UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
        bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI1];
        UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
        bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI2];
        UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
        bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI3];
        UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
        bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI4];
        UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
        bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
        [blueContainer addSubview:bI5];
        [averageRatingContainer addSubview:ratingContainer];
        
        internalY +=25;
        
    }
    
    NSString *myStringAvgValue = [NSString stringWithFormat:@"%.2f", avgValue/[mainCollection[@"averageRatingData"] count]];
    NSDictionary *reqAttributesforAvgRatingValue = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeAvgratingValue = [myStringAvgValue sizeWithAttributes:reqAttributesforAvgRatingValue];
    CGFloat reqStringWidthAvgValue = reqStringSizeAvgratingValue.width;
    UILabel *averageValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringWidthAvgRatingTitle +20, initialYPositionForAverageRating,reqStringWidthAvgValue, 25)];
    [averageValue setTextColor:[UIColor blackColor]];
    [averageValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
    [averageValue setText:myStringAvgValue];
    [averageRatingContainer addSubview:averageValue];
    
    
    CGRect  averageContainerNewFrame = averageRatingContainer.frame;
    averageContainerNewFrame.size.height = internalY +10;
    averageRatingContainer.frame = averageContainerNewFrame;
    
    mainContainerY +=internalY+20;

    
    // review list container
    
    
    internalY = 0;
    UIView *reviewListContainer = [[UIView alloc] initWithFrame:CGRectMake(5,mainContainerY +10 ,  _mainView.frame.size.width-10, 300)];
    reviewListContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
    reviewListContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:reviewListContainer];
    
    
    UILabel *reviewDetails = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,returnPolicyContainer.frame.size.width, 30)];
    [reviewDetails setTextColor:[UIColor blackColor]];
    [reviewDetails setBackgroundColor:[GlobalData colorWithHexString:@"EEEEEE"]];
    [reviewDetails setFont:[UIFont fontWithName:@"Trebuchet MS" size:24.0f]];
    [reviewDetails setText:@"  Review List"];
    [reviewListContainer addSubview:reviewDetails];
    
    mainContainerY += 40;
    internalY = 40;
    float Y = 5;
    for(int i=0;i<[mainCollection[@"recentRating"] count];i++ ){
        NSDictionary *recentRating = [mainCollection[@"recentRating"] objectAtIndex:i];
        
        UIView *ratingDataContainer = [[UIView alloc] initWithFrame:CGRectMake(10,internalY ,reviewListContainer.frame.size.width-20 , 100)];
        ratingDataContainer.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        ratingDataContainer.layer.borderWidth = 2.0f;
        [reviewListContainer addSubview:ratingDataContainer];
        

        NSDictionary *reqAttributesforReviewRatingSummary = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeReviewratingSummary = [recentRating[@"summary"] sizeWithAttributes:reqAttributesforReviewRatingSummary];
        CGFloat reqStringWidthReviewRatingSummary = reqStringSizeReviewratingSummary.width;
        UILabel *reviewRatingSummary = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingSummary, 25)];
        [reviewRatingSummary setTextColor:[UIColor blackColor]];
        [reviewRatingSummary setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [reviewRatingSummary setText:recentRating[@"summary"]];
        [ratingDataContainer addSubview:reviewRatingSummary];

        
        
        Y = 35;
        for(int j=0;j<[recentRating[@"rating"] count];j++){
        NSDictionary *recentRatingValue = [recentRating[@"rating"] objectAtIndex:j];
            NSDictionary *reqAttributesforReviewRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
            CGSize reqStringSizeReviewratingTitle = [recentRatingValue[@"label"] sizeWithAttributes:reqAttributesforReviewRatingTitle];
            CGFloat reqStringWidthReviewRatingTitle = reqStringSizeReviewratingTitle.width;
            UILabel *reviewRating = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,reqStringWidthReviewRatingTitle, 25)];
            [reviewRating setTextColor:[UIColor blackColor]];
            [reviewRating setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
            [reviewRating setText:recentRatingValue[@"label"]];
            [ratingDataContainer addSubview:reviewRating];
            
            
            
            UIView *ratingContainer = [[UIView alloc]initWithFrame:CGRectMake(100,Y,120,24)];
            UIView *grayContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,120,24)];
            [ratingContainer addSubview:grayContainer];
            UIImageView *gI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            gI1.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI1];
            UIImageView *gI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            gI2.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI2];
            UIImageView *gI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            gI3.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI3];
            UIImageView *gI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            gI4.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI4];
            UIImageView *gI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            gI5.image = [UIImage imageNamed:@"ic_star_gray.png"];
            [grayContainer addSubview:gI5];
            
            double percent = 24 * [recentRatingValue[@"value"] doubleValue];
            
            UIView *blueContainer = [[UIView alloc]initWithFrame:CGRectMake(0,0,percent,24)];
            blueContainer.clipsToBounds = YES;
            [ratingContainer addSubview:blueContainer];
            UIImageView *bI1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,24,24)];
            bI1.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI1];
            UIImageView *bI2 = [[UIImageView alloc] initWithFrame:CGRectMake(24,0,24,24)];
            bI2.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI2];
            UIImageView *bI3 = [[UIImageView alloc] initWithFrame:CGRectMake(48,0,24,24)];
            bI3.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI3];
            UIImageView *bI4 = [[UIImageView alloc] initWithFrame:CGRectMake(72,0,24,24)];
            bI4.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI4];
            UIImageView *bI5 = [[UIImageView alloc] initWithFrame:CGRectMake(96,0,24,24)];
            bI5.image = [UIImage imageNamed:@"ic_star_blue.png"];
            [blueContainer addSubview:bI5];
            [ratingDataContainer addSubview:ratingContainer];

            
            
            Y +=25;
            
        }
        Y +=5;
        NSDictionary *reqAttributesforReviewRatingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeReviewratingTitle = [recentRating[@"title"] sizeWithAttributes:reqAttributesforReviewRatingTitle];
        CGFloat reqStringWidthReviewRatingTitle = reqStringSizeReviewratingTitle.width;
        UILabel *reviewRatingTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, Y,ratingDataContainer.frame.size.width-20, 25)];
        [reviewRatingTitle setTextColor:[UIColor blackColor]];
        [reviewRatingTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [reviewRatingTitle setText:recentRating[@"title"]];
        [ratingDataContainer addSubview:reviewRatingTitle];
        Y += 25;
        
        CGRect  ratingDataNewFrame = ratingDataContainer.frame;
        ratingDataNewFrame.size.height = Y +5 ;
        ratingDataContainer.frame = ratingDataNewFrame;
        
        internalY +=Y +10;
        
        Y = 5;
    }
    CGRect  reviewListNewFrame = reviewListContainer.frame;
    reviewListNewFrame.size.height = internalY +5 ;
    reviewListContainer.frame = reviewListNewFrame;
    mainContainerY += internalY;
    internalY = mainContainerY;
        
    UIButton *makeReviewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [makeReviewButton addTarget:self action:@selector(buttonHandlerForMakeReview :) forControlEvents:UIControlEventTouchUpInside];
    [makeReviewButton setTitle:@"MAKE A REVIEW" forState:UIControlStateNormal];
    makeReviewButton.frame = CGRectMake(8,internalY-20 , _mainView.frame.size.width-16, 50.0);
    [makeReviewButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [makeReviewButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [makeReviewButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:makeReviewButton ];
        
    mainContainerY += 45;
        
    UIButton *blogButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [blogButton addTarget:self action:@selector(blogbuttonPressed :) forControlEvents:UIControlEventTouchUpInside];
    [blogButton setTitle:@"Blogs" forState:UIControlStateNormal];
    blogButton.frame = CGRectMake(8,mainContainerY , _mainView.frame.size.width-16, 50.0);
    [blogButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [blogButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [blogButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:blogButton ];
        
    mainContainerY += 60;
        
        
        
    _mainViewHeightConstraint.constant = mainContainerY;
    
}
}


//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGFloat width = scrollView.frame.size.width;
//    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
//      NSLog(@"Did scroll  %ld",page);
//}


-(void)featuredProductShowOptions:(UITapGestureRecognizer *)recognizer{
    UIView *containerView = [recognizer.view.superview.subviews objectAtIndex:0];
    UIView *curtainView = [containerView.subviews objectAtIndex:1];
    if([featuredProductCurtainOpenSignal[recognizer.view.tag] isEqual: @"0"]) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             curtainView.frame = CGRectMake(0, 0, curtainView.frame.size.width, curtainView.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             if(finished)
                                 featuredProductCurtainOpenSignal[recognizer.view.tag] = @"1";
                         }
         ];
    }
    else{
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             curtainView.frame = CGRectMake(0, curtainView.frame.size.height, curtainView.frame.size.width, curtainView.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             if(finished)
                                 featuredProductCurtainOpenSignal[recognizer.view.tag] = @"0";
                         }
         ];
    }
}

-(void)featuredAddtoWishlist:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId == nil){
        UIAlertController * AC = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please Login To Add this Product to Wishlist" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action){
                                                          [self performSegueWithIdentifier:@"addtoWishlistLoginSegue" sender:self];
                                                      }];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [AC addAction:okBtn];
        [AC addAction:noBtn];
        [self.parentViewController presentViewController:AC animated:YES completion:nil];
    }
    else{
        NSDictionary *tempDictionary = [[NSDictionary alloc] init];
        tempDictionary = [mainCollection[@"recentCollection"] objectAtIndex:recognizer.view.tag];
        whichApiDataToprocess = @"addToWishlist";
        productIdForApiCall = tempDictionary[@"entityId"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}


-(void)featuredAddtoCart:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    NSDictionary *tempDictionary = [[NSDictionary alloc] init];
    tempDictionary = [mainCollection[@"recentCollection"] objectAtIndex:recognizer.view.tag];
    whichApiDataToprocess = @"addToCart";
    productIdForApiCall = tempDictionary[@"entityId"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void) recentViewProduct:(UITapGestureRecognizer *)recognizer{
    NSDictionary *recentViewCollection = [mainCollection[@"recentCollection"] objectAtIndex:recognizer.view.tag];
    name = recentViewCollection[@"name"];
    entityId = recentViewCollection[@"entityId"];
    typeId = recentViewCollection[@"typeId"];
  [self performSegueWithIdentifier:@"sellerprofileDataToProductSegue" sender:self];
}

-(void)buttonHandlerForMakeReview:(UIButton*)button{
    
  [self performSegueWithIdentifier:@"sellerProfileDataToMakeReviewDataSegeue" sender:self];
    
}

-(void)blogbuttonPressed:(UIButton*)button{
    
    [self performSegueWithIdentifier:@"blogListView" sender:self];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"sellerprofileDataToProductSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = entityId;
        destViewController.productName = name;
        destViewController.productType = typeId;
        destViewController.parentClass = @"SellerProfileData";
    }
    else if([segue.identifier isEqualToString:@"sellerProfileDataToMakeReviewDataSegeue"]) {
        MakeReviewData *destViewController = segue.destinationViewController;
        destViewController.profileUrl = _profileUrl;
        destViewController.popUpView = @"0";
    }
    else if([segue.identifier isEqualToString:@"blogListView"])
    {
        BlogListViewController *controller = segue.destinationViewController;
    }
}

-(void)contactUsWindow:(UITapGestureRecognizer *)recognizer{
    preferences = [NSUserDefaults standardUserDefaults];
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 888;
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UIView *contactBlock = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5, SCREEN_WIDTH/2+SCREEN_WIDTH/2.5)];
    [contactBlock setBackgroundColor : [UIColor whiteColor]];
    contactBlock.layer.cornerRadius = 4;
    
    float y = 10;
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 32)];
    [sortTitle setTextColor : [GlobalData colorWithHexString : @"268ED7"]];
    [sortTitle setBackgroundColor : [UIColor clearColor]];
    [sortTitle setFont : [UIFont fontWithName : @"AmericanTypewriter-Bold" size : 25.0f]];
    [sortTitle setText : @"SELLER CONTACTS"];
    sortTitle.textAlignment = NSTextAlignmentCenter;
    [contactBlock addSubview : sortTitle];
    
    y += 42;
    UIView *hr = [[UIView alloc]initWithFrame:CGRectMake(0, y, SCREEN_WIDTH/2 +SCREEN_WIDTH/2.5, 1)];
    [hr setBackgroundColor:[GlobalData colorWithHexString : @"268ED7"]];
    [contactBlock addSubview : hr];
    y +=10;
    UITextField *nameField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 40)];
    nameField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    nameField.textColor = [UIColor blackColor];
    if([preferences objectForKey:@"customerName"]){
        nameField.text = [preferences objectForKey:@"customerName"];
    }else{
    [nameField setPlaceholder:@"Enter Name"];
    }
    nameField.layer.borderWidth = 1.0f;
    nameField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    nameField.tag = 1;
    [nameField setKeyboardType:UIKeyboardTypePhonePad];
    nameField.textAlignment = NSTextAlignmentLeft;
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:nameField];
    
    y += 50;
    UITextField *emailField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 40)];
    emailField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    emailField.textColor = [UIColor blackColor];
    if([preferences objectForKey:@"customerEmail"]){
        emailField.text = [preferences objectForKey:@"customerEmail"];
    }else{
        [emailField setPlaceholder:@"Enter Name"];
    }
    [emailField setPlaceholder:@"Email Address"];
    emailField.layer.borderWidth = 1.0f;
    emailField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    emailField.tag = 2;
    [emailField setKeyboardType:UIKeyboardTypePhonePad];
    emailField.textAlignment = NSTextAlignmentLeft;
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:emailField];
    
    y +=50;
    UITextField *subjectField = [[UITextField alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 40)];
    subjectField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
    subjectField.textColor = [UIColor blackColor];
    [subjectField setPlaceholder:@"Enter Subject"];
    subjectField.layer.borderWidth = 1.0f;
    subjectField.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    subjectField.tag = 3;
    [subjectField setKeyboardType:UIKeyboardTypePhonePad];
    subjectField.textAlignment = NSTextAlignmentLeft;
    subjectField.borderStyle = UITextBorderStyleRoundedRect;
    [contactBlock addSubview:subjectField];
    
    y += 50;
    
    UITextView *textArea = [[UITextView alloc] initWithFrame:CGRectMake(10, y, contactBlock.frame.size.width -20, 100)];
    textArea.layer.borderWidth = 1.0f;
    textArea.textColor = [UIColor blackColor];
    textArea.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textArea.font = [UIFont fontWithName: @"Trebuchet MS" size: 16.0f];
    textArea.tag = 4;
    [contactBlock addSubview:textArea ];
    
    y += 120;
    
    UIView *requestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , contactBlock.frame.size.width, 30)];
    requestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    requestContainer.layer.borderWidth = 0.0f;
    [contactBlock addSubview:requestContainer];
    
    NSDictionary *reqAttributesforCancelButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforCancelButtonText = [@"Cancel" sizeWithAttributes:reqAttributesforCancelButtonText];
    CGFloat reqStringWidthVCancelButtonText = reqStringSizeforCancelButtonText.width;
    UIButton  *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [cancelButton addTarget:self action:@selector(contactCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0,0,reqStringWidthVCancelButtonText+20, 40);
    [cancelButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [cancelButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [cancelButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    
    [requestContainer addSubview:cancelButton];
    
    NSDictionary *reqAttributesforSubmitButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforSubmitButtonText = [@"Submit" sizeWithAttributes:reqAttributesforSubmitButtonText];
    CGFloat reqStringWidthSubmitButtonText = reqStringSizeforSubmitButtonText.width;
    UIButton  *SubmitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [SubmitButton addTarget:self action:@selector(contactSubmitButton :) forControlEvents:UIControlEventTouchUpInside];
    [SubmitButton setTitle:@"Submit" forState:UIControlStateNormal];
    SubmitButton.frame = CGRectMake(reqStringWidthVCancelButtonText +20 +10,0,reqStringWidthSubmitButtonText+20, 40);
    [SubmitButton setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [SubmitButton setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [SubmitButton setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    
    [requestContainer addSubview:SubmitButton];
    
    CGRect  newFrame = requestContainer.frame;
    newFrame.size.width = reqStringWidthVCancelButtonText +20 +10 + reqStringWidthSubmitButtonText+20;
    requestContainer.frame = newFrame;
    
    UIView *finalrequestContainer = [[UIView alloc]initWithFrame:CGRectMake(0,y , contactBlock.frame.size.width, 30)];
    finalrequestContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    finalrequestContainer.layer.borderWidth = 0.0f;
    requestContainer.center = CGPointMake(finalrequestContainer.frame.size.width  / 2,
                                          finalrequestContainer.frame.size.height / 2);
    [finalrequestContainer addSubview:requestContainer];
    
    [contactBlock addSubview:finalrequestContainer];

    CGRect  contactNewFrame = contactBlock.frame;
    contactNewFrame.size.height = y +50;
    contactBlock.frame = contactNewFrame;
    contactBlock.center = blurEffectView.center;
    [blurEffectView addSubview:contactBlock];
    [self.view addSubview:blurEffectView];
}

-(void)contactCancelButton :(UIButton*)button{
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    [parent2.superview removeFromSuperview];
    }
-(void)contactSubmitButton :(UIButton*)button{
    contactUsData = [[NSMutableDictionary alloc]init];
    NSString *errorMessage = @"Please Fill";
    UIView *parent = button.superview;
    UIView *parent1 = parent.superview;
    UIView *parent2 = parent1.superview;
    UITextField *nameField = [parent2 viewWithTag:1];
    UITextField *emailField = [parent2 viewWithTag:2];
    UITextField *subjectlField = [parent2 viewWithTag:3];
    UITextView *queryField = [parent2 viewWithTag:4];
    
    
    isValid = 1;
    if ([queryField.text isEqualToString:@""]){
        isValid =0;
        errorMessage = @"Please enter the Query";
    }
    if ([subjectlField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Subject";
    }
    if ([emailField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Email";
    }
    if ([nameField.text isEqual:@""]){
        isValid =0;
        errorMessage = @"Please enter the Name";
    }
    
    
    if(isValid == 1){
        [contactUsData setObject:nameField.text forKey:@"name"];
        [contactUsData setObject:emailField.text forKey:@"email"];
        [contactUsData setObject:subjectlField.text forKey:@"subject"];
        [contactUsData setObject:queryField.text forKey:@"query"];
        isValid=2;
    }
    
    if(isValid == 0){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:errorMessage message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    if(isValid == 2) {
        [parent2.superview removeFromSuperview];
        whichApiDataToprocess = @"contactSeller";
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}
@end
