//
//  SellerOrderDetails.m
//  MobikulMp
//
//  Created by kunal prasad on 27/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "SellerOrderDetails.h"
#import "GlobalData.h"
#import "SellerSoldOrderData.h"
#import "ToastView.h"
#import "InvoiceDetails.h"
#import "ShipMentDetails.h"
#import "CreditMemoList.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

@interface SellerOrderDetails ()

@end

GlobalData *globalObjectSellerOrderDetails;

@implementation SellerOrderDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectSellerOrderDetails = [[GlobalData alloc] init];
    globalObjectSellerOrderDetails.delegate = self;
    [globalObjectSellerOrderDetails language];
    isAlertVisible = 0;
    whichApiDataToprocess = @"";
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectSellerOrderDetails callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectSellerOrderDetails.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectSellerOrderDetails.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectSellerOrderDetails.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectSellerOrderDetails.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectSellerOrderDetails.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    if([whichApiDataToprocess isEqual:@"cancelOrder"]){
     [post appendFormat:@"customerId=%@&", _customerId];
     [post appendFormat:@"incrementId=%@", _incrementId];
     [globalObjectSellerOrderDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/cancelOrder" signal:@"HttpPostMetod"];
     globalObjectSellerOrderDetails.delegate = self;
    }
    else if( [whichApiDataToprocess isEqual:@"createInvoiceRequest"]){
     [post appendFormat:@"customerId=%@&", _customerId];
     [post appendFormat:@"incrementId=%@", _incrementId];
     [globalObjectSellerOrderDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/invoice" signal:@"HttpPostMetod"];
     globalObjectSellerOrderDetails.delegate = self;
    }
    else if([whichApiDataToprocess isEqual:@"sendEailToCustomer"]){
    [post appendFormat:@"incrementId=%@", _incrementId];
    [globalObjectSellerOrderDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/sendEmail" signal:@"HttpPostMetod"];
    globalObjectSellerOrderDetails.delegate = self;
    
    }
    else{
     [post appendFormat:@"customerId=%@&", _customerId];
     [post appendFormat:@"incrementId=%@&", _incrementId];
     [post appendFormat:@"storeId=%@&", storeId];
     [globalObjectSellerOrderDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getorderDetails" signal:@"HttpPostMetod"];
     globalObjectSellerOrderDetails.delegate = self;
    }
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    
    float mainContainerY = 0;
    float internalY = 0;
    float Y = 0;
    if([whichApiDataToprocess isEqual:@"cancelOrder"]){
        if([collection[@"success"] integerValue] == 1)
            [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:@"Sorry!! Something went wrong please try again later." withStatus:@"error" withDuaration:5.0];

    }
    
   else  if([whichApiDataToprocess isEqual: @"sendEailToCustomer"]){
      if([collection[@"success"] integerValue] == 1)
            [ToastView showToastInParentView:self.view withText:@"The order email has been sent." withStatus:@"success" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:@"Sorry!! Something went wrong please try again later." withStatus:@"error" withDuaration:5.0];
    }
     else if( [whichApiDataToprocess isEqual:@"createInvoiceRequest"]){
         if([collection[@"success"] integerValue] == 1)
             [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
         else
             [ToastView showToastInParentView:self.view withText:@"Cannot create Invoice for this order." withStatus:@"error" withDuaration:5.0];
         [self viewDidLoad];  // temporary i have set as wrong
     }
    else{
        for(UIView *subViews in _mainView.subviews){       // clear all subview
            [subViews removeFromSuperview];
        }
    internalY +=5;
    NSDictionary *reqAttributesHeading = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]};                 // main heading
    CGSize reqStringSizeHeading = [mainCollection[@"mainHeading"] sizeWithAttributes:reqAttributesHeading];
    UILabel *headingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY, reqStringSizeHeading.width, 35)];
    [headingLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [headingLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [headingLabelName setText:mainCollection[@"mainHeading"]];
    [_mainView addSubview:headingLabelName];
    
    internalY += 40;
//        
//    if([_statusOfOrder isEqualToString:@"COMPLETE"] || [_statusOfOrder isEqualToString:@"PROCESSING"]){
//    UIButton *buttonCreditMemo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [buttonCreditMemo addTarget:self action:@selector(buttonHandlerForCreditMemo :) forControlEvents:UIControlEventTouchUpInside];
//    [buttonCreditMemo setTitle:@"CREDIT MEMO" forState:UIControlStateNormal];
//    buttonCreditMemo.frame = CGRectMake(1,internalY ,SCREEN_WIDTH-2, 40.0);
//    [buttonCreditMemo setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
//    [buttonCreditMemo setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
//    [buttonCreditMemo setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
//    [_mainView addSubview:buttonCreditMemo ];
//    internalY +=45;
//    }
        
    if(mainCollection[@"sendmailAction"]){
    UIButton *buttonSendEmail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonSendEmail addTarget:self action:@selector(buttonHandlerForSendEmail :) forControlEvents:UIControlEventTouchUpInside];
    [buttonSendEmail setTitle:mainCollection[@"sendmailAction"] forState:UIControlStateNormal];
    buttonSendEmail.frame = CGRectMake(8,internalY ,_mainView.frame.size.width-16, 40.0);
    [buttonSendEmail setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buttonSendEmail setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [buttonSendEmail setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:buttonSendEmail ];
    internalY +=45;
    }
        
        
    if(mainCollection[@"invoiceAction"]){
    buttonSendInvoice = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonSendInvoice addTarget:self action:@selector(buttonHandlerForSendInvoice :) forControlEvents:UIControlEventTouchUpInside];
    [buttonSendInvoice setTitle:mainCollection[@"invoiceAction"] forState:UIControlStateNormal];
    buttonSendInvoice.tag = 1000;
    buttonSendInvoice.frame = CGRectMake(8,internalY ,_mainView.frame.size.width-16, 40.0);
    [buttonSendInvoice setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buttonSendInvoice setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [buttonSendInvoice setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:buttonSendInvoice ];
    internalY += 45;
    }
    
    if(mainCollection[@"cancelOrderAction"]){
    UIButton *buttonCancelOrder = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonCancelOrder addTarget:self action:@selector(buttonHandlerForCancelOrder :) forControlEvents:UIControlEventTouchUpInside];
    [buttonCancelOrder setTitle:mainCollection[@"cancelOrderAction"] forState:UIControlStateNormal];
    buttonCancelOrder.frame = CGRectMake(8,internalY ,_mainView.frame.size.width-16, 40.0);
    [buttonCancelOrder setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buttonCancelOrder setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [buttonCancelOrder setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [_mainView addSubview:buttonCancelOrder ];
    internalY +=45;
    }
    mainContainerY +=internalY;
    
    UIView *detailListContainer = [[UIView alloc] initWithFrame:CGRectMake(5, mainContainerY , _mainView.frame.size.width-10, SCREEN_WIDTH/2)];       // main container
    detailListContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    detailListContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:detailListContainer];
    
    internalY = 5;
    
    NSDictionary *reqAttributesSubHeading = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]};             // sub heading
    CGSize reqStringSizeSubHeading = [mainCollection[@"subHeading"] sizeWithAttributes:reqAttributesSubHeading];
    UILabel *subHeadingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY, _mainView.frame.size.width-20, 30)];
    [subHeadingLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subHeadingLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]];
    [subHeadingLabelName setText:mainCollection[@"subHeading"]];
    [detailListContainer addSubview:subHeadingLabelName];
   
    internalY += 35;
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,detailListContainer.frame.size.width-10 ,1)];         // line 1
    line1.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line1.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:line1];
    
    internalY +=4;
    
    NSDictionary *reqAttributesAboutOrder = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]};
    CGSize reqStringSizeAboutOrder = [@"About This Order:" sizeWithAttributes:reqAttributesAboutOrder];
    UILabel *aboutOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY,reqStringSizeAboutOrder.width, 20)];    //
    [aboutOrderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [aboutOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [aboutOrderLabelName setText:@"About This Order:"];
    [detailListContainer addSubview:aboutOrderLabelName];
        
    internalY +=30;
        
    int i = 0;
    NSDictionary *linkData = mainCollection[@"links"] ;
    for(id key in linkData){
    //NSLog(@"key=%@ value=%@", key, [data objectForKey:key]);
    //NSLog(@" got data %@",[value objectForKey:@"label"]);
    NSDictionary *value = [linkData objectForKey:key];
    UILabel *invoiceLink = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY,150, 20)];    //  invoice generate label
    [invoiceLink setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
    [invoiceLink setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [invoiceLink setText:[value objectForKey:@"label"]];
     invoiceLink.tag = i;
    [detailListContainer addSubview:invoiceLink];
    invoiceTitleTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callInvoiceTitleTap:)];
    invoiceLink.userInteractionEnabled = "YES";
    invoiceTitleTap.numberOfTapsRequired = 1;
    [invoiceLink addGestureRecognizer:invoiceTitleTap];
   // shiftX +=160;
    internalY +=30;
    i +=1;
        
    }
    
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,detailListContainer.frame.size.width-10 ,1)];           // line 2
    line2.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line2.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:line2];
    
    internalY += 4;
    
    
    
    NSDictionary *reqAttributesDateTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:22.0f]};             // order date title
    CGSize reqStringSizeDateTitle = [mainCollection[@"orderDateTitle"] sizeWithAttributes:reqAttributesDateTitle];
    UILabel *dateTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY, reqStringSizeDateTitle.width, 30)];
    [dateTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [dateTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:22.0f]];
    [dateTitleLabelName setText:mainCollection[@"orderDateTitle"]];
    [detailListContainer addSubview:dateTitleLabelName];
    
    
    NSDictionary *reqAttributesDateValue = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:22.0f]};             // order date value
    CGSize reqStringSizeDateValue = [mainCollection[@"orderDateValue"] sizeWithAttributes:reqAttributesDateValue];
    UILabel *dateValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeDateTitle.width+10 ,internalY, reqStringSizeDateValue.width, 30)];
    [dateValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [dateValueLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:22.0f]];
    [dateValueLabelName setText:mainCollection[@"orderDateValue"]];
    [detailListContainer addSubview:dateValueLabelName];
    
    internalY += 30;
    mainContainerY +=internalY;
    
    UIView *buyerContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // buyer data container
    buyerContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    buyerContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:buyerContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeBuyerTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerTitle = [mainCollection[@"buyerData"][@"title"] sizeWithAttributes:reqAttributeBuyerTitle];
    UILabel *buyerTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeBuyerTitle.width, 25)];
    [buyerTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buyerTitleLabelName setText:mainCollection[@"buyerData"][@"title"]];
    [buyerContainer addSubview:buyerTitleLabelName];
    
    
    Y +=30;
    
    
    
    NSDictionary *reqAttributeBuyerName = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerName = [mainCollection[@"buyerData"][@"name"] sizeWithAttributes:reqAttributeBuyerName];
    UILabel *buyerNameLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeBuyerName.width, 25)];
    [buyerNameLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerNameLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [buyerNameLabelName setText:mainCollection[@"buyerData"][@"name"]];
    [buyerContainer addSubview:buyerNameLabelName];
    
    Y +=30;
    
   
    NSDictionary *reqAttributeBuyerEmail = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerEmail = [mainCollection[@"buyerData"][@"email"] sizeWithAttributes:reqAttributeBuyerEmail];
    UILabel *buyerEmailLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeBuyerEmail.width, 25)];
    [buyerEmailLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerEmailLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [buyerEmailLabelName setText:mainCollection[@"buyerData"][@"email"]];
    [buyerContainer addSubview:buyerEmailLabelName];
    
    Y += 30;
    
    CGRect buyerContainerFrame = buyerContainer.frame;
    buyerContainerFrame.size.height = Y;
    buyerContainer.frame = buyerContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *shippingAddressContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // shipping address data container
    shippingAddressContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    shippingAddressContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:shippingAddressContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeShippingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeShippingTitle = [mainCollection[@"shippingAddressData"][@"title"] sizeWithAttributes:reqAttributeShippingTitle];
    UILabel *shippingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingTitle.width, 25)];
    [shippingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [shippingTitleLabelName setText:mainCollection[@"shippingAddressData"][@"title"]];
    [shippingAddressContainer addSubview:shippingTitleLabelName];
    
    Y +=30;
    
    for(int i=0;i<[mainCollection[@"shippingAddressData"][@"address"] count]; i++){
        NSDictionary *address = [mainCollection[@"shippingAddressData"][@"address"] objectAtIndex: i];
        NSString *addr = address;
        NSDictionary *reqAttributeShippingAddr = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping address
       CGSize reqStringSizeShippingAddr = [addr sizeWithAttributes:reqAttributeShippingAddr];
        UILabel *shippingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingAddr.width, 25)];
        [shippingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [shippingTitleLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [shippingTitleLabelName setText:addr];
        [shippingAddressContainer addSubview:shippingTitleLabelName];
        
        Y +=30;
        
    }
        
    CGRect shippingContainerFrame = shippingAddressContainer.frame;
    shippingContainerFrame.size.height = Y;
    shippingAddressContainer.frame = shippingContainerFrame;
   
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *shippingInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];// shipping information data container
    shippingInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    shippingInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:shippingInformationContainer];

    Y = 5;
    
    NSDictionary *reqAttributeShippingInfor = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeShippingInfor = [mainCollection[@"shippingMethodData"][@"title"] sizeWithAttributes:reqAttributeShippingInfor];
    UILabel *shippingInfoLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingInfor.width, 25)];
    [shippingInfoLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingInfoLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [shippingInfoLabelName setText:mainCollection[@"shippingMethodData"][@"title"]];
    [shippingInformationContainer addSubview:shippingInfoLabelName];
    
    Y += 30;
    
    
    NSDictionary *reqAttributeShippingMethod = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping method
    CGSize reqStringSizeShippingMethod = [mainCollection[@"shippingMethodData"][@"method"] sizeWithAttributes:reqAttributeShippingMethod];
    UILabel *shippingMethodLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingMethod.width, 25)];
    [shippingMethodLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingMethodLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingMethodLabelName setText:mainCollection[@"shippingMethodData"][@"method"]];
    [shippingInformationContainer addSubview:shippingMethodLabelName];

    Y +=30;
    
    
    CGRect shippingMethodContainerFrame = shippingInformationContainer.frame;
    shippingMethodContainerFrame.size.height = Y;
    shippingInformationContainer.frame = shippingMethodContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
   
    
    UIView *billingAddressContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // billing address data container
    billingAddressContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    billingAddressContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:billingAddressContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeBillingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // billing information heading
    CGSize reqStringSizeBillingTitle = [mainCollection[@"billingAddressData"][@"title"] sizeWithAttributes:reqAttributeBillingTitle];
    UILabel *billingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingTitle.width, 25)];
    [billingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [billingTitleLabelName setText:mainCollection[@"billingAddressData"][@"title"]];
    [billingAddressContainer addSubview:billingTitleLabelName];
    
    Y +=30;
    
    for(int i=0;i<[mainCollection[@"billingAddressData"][@"address"] count]; i++){
        NSDictionary *address = [mainCollection[@"billingAddressData"][@"address"] objectAtIndex: i];
        NSString *addr = address;
        NSDictionary *reqAttributeBillingAddr = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping address
        CGSize reqStringSizeBillingAddr = [addr sizeWithAttributes:reqAttributeBillingAddr];
        UILabel *billingAddrLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingAddr.width, 25)];
        [billingAddrLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [billingAddrLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [billingAddrLabelName setText:addr];
        [billingAddressContainer addSubview:billingAddrLabelName];
        
        Y +=30;
        
    }

    CGRect billingAddrContainerFrame = billingAddressContainer.frame;
    billingAddrContainerFrame.size.height = Y;
    billingAddressContainer.frame = billingAddrContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;

    
    UIView *billingInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];// billing information data container
    billingInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    billingInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:billingInformationContainer];
    
    
    Y = 5;
    
    NSDictionary *reqAttributeBillingInfor = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeBillingInfor = [mainCollection[@"paymentMethodData"][@"title"] sizeWithAttributes:reqAttributeBillingInfor];
    UILabel *billingInfoLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingInfor.width, 25)];
    [billingInfoLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingInfoLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [billingInfoLabelName setText:mainCollection[@"paymentMethodData"][@"title"]];
    [billingInformationContainer addSubview:billingInfoLabelName];
    
    
    Y += 30;
    
    
    NSDictionary *reqAttributeBillingMethod = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // billing method
    CGSize reqStringSizeBillingMethod = [mainCollection[@"paymentMethodData"][@"method"] sizeWithAttributes:reqAttributeBillingMethod];
    UILabel *billingMethodLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingMethod.width, 25)];
    [billingMethodLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingMethodLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [billingMethodLabelName setText:mainCollection[@"paymentMethodData"][@"method"]];
    [billingInformationContainer addSubview:billingMethodLabelName];

    Y +=30;
    
    
    CGRect billingMethodContainerFrame = billingInformationContainer.frame;
    billingMethodContainerFrame.size.height = Y;
    billingInformationContainer.frame = billingMethodContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *paymentInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,900)];// payment information data container
    paymentInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    paymentInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:paymentInformationContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeItemOrderTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]};             // payment heading information heading
    CGSize reqStringItemOrderTitle = [@"ITEM ORDERED" sizeWithAttributes:reqAttributeItemOrderTitle];
    UILabel *itemOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringItemOrderTitle.width, 35)];
    [itemOrderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [itemOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [itemOrderLabelName setText:@"ITEM ORDERED"];
    [paymentInformationContainer addSubview:itemOrderLabelName];

    Y +=40;
    
    NSDictionary *reqAttributeProduct = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]};             // payment subheading information heading
    CGSize reqStringProduct = [@"PRODUCT" sizeWithAttributes:reqAttributeProduct];
    UILabel *productLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringProduct.width, 30)];
    [productLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [productLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [productLabelName setText:@"PRODUCT"];
    [paymentInformationContainer addSubview:productLabelName];
    
    Y +=35;
    
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(5, Y ,paymentInformationContainer.frame.size.width-10 ,1)];           // line 3
    line3.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line3.layer.borderWidth = 2.0f;
    [paymentInformationContainer addSubview:line3];

    Y +=4;
    
    for(int i=0;i<[mainCollection[@"items"] count];i++){
        
        NSDictionary *productData  = [mainCollection[@"items"] objectAtIndex: i];
        
        NSDictionary *reqAttributeProductName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // product name subheading information heading
        CGSize reqStringProductName = [productData[@"productName"] sizeWithAttributes:reqAttributeProductName];
        UILabel *productLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringProductName.width, 25)];
        [productLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [productLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [productLabelName setText:productData[@"productName"]];
        [paymentInformationContainer addSubview:productLabelName];
        
        Y +=30;
        
        NSDictionary *reqAttributePriceLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // price lebel
        CGSize reqStringSizePriceLabel = [@"PRICE : " sizeWithAttributes:reqAttributePriceLebel];
        UILabel *priceLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizePriceLabel.width, 25)];
        [priceLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [priceLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [priceLabelName setText:@"PRICE : "];
        [paymentInformationContainer addSubview:priceLabelName];
        
        
        NSDictionary *reqAttributePriceValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // price value
        CGSize reqStringSizePriceValue = [productData[@"price"] sizeWithAttributes:reqAttributePriceValue];
        UILabel *priceValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizePriceLabel.width +15, Y , reqStringSizePriceValue.width, 25)];
        [priceValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [priceValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [priceValueLabelName setText:productData[@"price"]];
        [paymentInformationContainer addSubview:priceValueLabelName];
        
        Y +=30;
        
        NSDictionary *reqAttributeStatusLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // status lebel
        CGSize reqStringSizeStatusLabel = [@"STATUS : " sizeWithAttributes:reqAttributeStatusLebel];
        UILabel *statusLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeStatusLabel.width, 25)];
        [statusLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [statusLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [statusLabelName setText:@"STATUS : "];
        [paymentInformationContainer addSubview:statusLabelName];

        Y += 30;
        
        if([productData[@"qty"][@"Ordered"] integerValue] > 0){
        NSString *orderValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Ordered"] intValue]];
        NSDictionary *reqAttributeOrderLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order lebel
        CGSize reqStringSizeOrderLabel = [@"Oredered : " sizeWithAttributes:reqAttributeOrderLabel];
        UILabel *orderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeOrderLabel.width, 25)];
        [orderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [orderLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [orderLabelName setText:@"Oredered : "];
        [paymentInformationContainer addSubview:orderLabelName];
        
        NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order Value
        CGSize reqStringSizeOrderValue = [orderValue sizeWithAttributes:reqAttributeOrderValue];
        UILabel *orderLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ orderLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
        [orderLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [orderLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [orderLabelValue setText:orderValue];
        [paymentInformationContainer addSubview:orderLabelValue];
        
            
            
        Y+=30 ;
            
            
        }
        if([productData[@"qty"][@"Invoiced"] integerValue] > 0){
            NSString *invoiceValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Invoiced"] intValue]];
            NSDictionary *reqAttributeInvoiceLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // invoice  lebel
            CGSize reqStringSizeInvoiceLabel = [@"Invoiced : " sizeWithAttributes:reqAttributeInvoiceLabel];
            UILabel *invoiceLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeInvoiceLabel.width, 25)];
            [invoiceLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [invoiceLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [invoiceLabelName setText:@"Invoiced : "];
            [paymentInformationContainer addSubview:invoiceLabelName];
            
            NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // invoice value
            CGSize reqStringSizeOrderValue = [invoiceValue sizeWithAttributes:reqAttributeOrderValue];
            UILabel *invoiceLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ invoiceLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
            [invoiceLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [invoiceLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [invoiceLabelValue setText:invoiceValue];
            [paymentInformationContainer addSubview:invoiceLabelValue];
            
            Y+=30 ;
        }
        if([productData[@"qty"][@"Shipped"] integerValue] > 0){
            NSString *shipValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Shipped"] intValue]];
            NSDictionary *reqAttributeShipLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // ship  lebel
            CGSize reqStringSizeShipLabel = [@"Shipped : " sizeWithAttributes:reqAttributeShipLabel];
            UILabel *shipLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeShipLabel.width, 25)];
            [shipLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [shipLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [shipLabelName setText:@"Shipped : "];
            [paymentInformationContainer addSubview:shipLabelName];
            
            NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // ship value
            CGSize reqStringSizeOrderValue = [shipValue sizeWithAttributes:reqAttributeOrderValue];
            UILabel *shipLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ shipLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
            [shipLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [shipLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [shipLabelValue setText:shipValue];
            [paymentInformationContainer addSubview:shipLabelValue];
            
            Y+=30 ;
        }
        if([productData[@"qty"][@"Canceled"] integerValue] > 0){
            NSString *cancelValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Canceled"] intValue]];
            NSDictionary *reqAttributeCancelLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // cancel  lebel
            CGSize reqStringSizeCancelLabel = [@"Canceled : " sizeWithAttributes:reqAttributeCancelLabel];
            UILabel *cancelLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeCancelLabel.width, 25)];
            [cancelLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [cancelLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [cancelLabelName setText:@"Canceled : "];
            [paymentInformationContainer addSubview:cancelLabelName];
            
            NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // cancel value
            CGSize reqStringSizeOrderValue = [cancelValue sizeWithAttributes:reqAttributeOrderValue];
            UILabel *cancelLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ cancelLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
            [cancelLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [cancelLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [cancelLabelValue setText:cancelValue];
            [paymentInformationContainer addSubview:cancelLabelValue];
            
            Y+=30 ;
        }
        if([productData[@"qty"][@"Refunded"] integerValue] > 0){
            NSString *cancelValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Refunded"] intValue]];
            NSDictionary *reqAttributeCancelLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // refund  lebel
            CGSize reqStringSizeCancelLabel = [@"Refunded : " sizeWithAttributes:reqAttributeCancelLabel];
            UILabel *cancelLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeCancelLabel.width, 25)];
            [cancelLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [cancelLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [cancelLabelName setText:@"Refunded : "];
            [paymentInformationContainer addSubview:cancelLabelName];
            
            NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // refund value
            CGSize reqStringSizeOrderValue = [cancelValue sizeWithAttributes:reqAttributeOrderValue];
            UILabel *cancelLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ cancelLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
            [cancelLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [cancelLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [cancelLabelValue setText:cancelValue];
            [paymentInformationContainer addSubview:cancelLabelValue];
            
            Y+=30 ;
        }
        
        NSDictionary *reqAttributeAdminComm = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // Admin commision lebel
        CGSize reqStringSizeAdminComm = [@"ADMIN COMISSION : " sizeWithAttributes:reqAttributeAdminComm];
        UILabel *AdminLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeAdminComm.width, 25)];
        [AdminLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [AdminLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [AdminLabelName setText:@"ADMIN COMISSION : "];
        [paymentInformationContainer addSubview:AdminLabelName];
        
        
        NSDictionary *reqAttributeAdminValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // admin commision value value
        CGSize reqStringSizeAdmin = [productData[@"adminComission"] sizeWithAttributes:reqAttributeAdminValue];
        UILabel *adminCommLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeAdminComm.width +15, Y , reqStringSizeAdmin.width, 25)];
        [adminCommLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [adminCommLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [adminCommLabelName setText:productData[@"adminComission"]];
        [paymentInformationContainer addSubview:adminCommLabelName];
        
        Y +=30;
        
        NSDictionary *reqAttributeVendor = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // vendor commision lebel
        CGSize reqStringSizeVendor = [@"VENDOR TOTAL : " sizeWithAttributes:reqAttributeVendor];
        UILabel *VendorLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeVendor.width, 25)];
        [VendorLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [VendorLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [VendorLabelName setText:@"VENDOR TOTAL : "];
        [paymentInformationContainer addSubview:VendorLabelName];
        
        
        NSDictionary *reqAttributeVendorValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // vendor commision value value
        CGSize reqStringSizeVendorValue = [productData[@"vendorTotal"] sizeWithAttributes:reqAttributeVendorValue];
        UILabel *vendorValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeVendor.width +15, Y , reqStringSizeVendorValue.width, 25)];
        [vendorValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [vendorValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [vendorValueLabelName setText:productData[@"vendorTotal"]];
        [paymentInformationContainer addSubview:vendorValueLabelName];
        
        Y+=30;
        
        NSDictionary *reqAttributeSubTotal = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
        CGSize reqStringSizeSubTotal = [@"SUB TOTAL : " sizeWithAttributes:reqAttributeSubTotal];
        UILabel *subTotalLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeSubTotal.width, 25)];
        [subTotalLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [subTotalLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [subTotalLabelName setText:@"SUB TOTAL : "];
        [paymentInformationContainer addSubview:subTotalLabelName];
        
        NSDictionary *reqAttributeSubTotalValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
        CGSize reqStringSizeSubTotalValue = [productData[@"subTotal"] sizeWithAttributes:reqAttributeSubTotalValue];
        UILabel *subTotalValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeSubTotal.width +15 , Y , reqStringSizeSubTotalValue.width, 25)];
        [subTotalValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [subTotalValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [subTotalValueLabelName setText:productData[@"subTotal"]];
        [paymentInformationContainer addSubview:subTotalValueLabelName];
        
        
        Y +=30;
        
        if(i !=[mainCollection[@"items"] count]-1 ){
        UIView *line4 = [[UIView alloc] initWithFrame:CGRectMake(5, Y ,paymentInformationContainer.frame.size.width-10 ,1)];           // line 3
        line4.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
        line4.layer.borderWidth = 2.0f;
        [paymentInformationContainer addSubview:line4];

        Y +=4;
        }
        
        
    }
    
    CGRect paymentInformationContainerFrame = paymentInformationContainer.frame;
    paymentInformationContainerFrame.size.height = Y;
    paymentInformationContainer.frame = paymentInformationContainerFrame;
    
    mainContainerY +=Y+10;
    internalY +=Y +10;
    
    
    UIView *differentPaymentContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,500)];// payment information data container
    differentPaymentContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    differentPaymentContainer.layer.borderWidth = 2.0f;
    differentPaymentContainer.backgroundColor = [GlobalData colorWithHexString:@"CCCCCC"];
    [detailListContainer addSubview:differentPaymentContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeSubTotal = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
    CGSize reqStringSizeSubTotal = [mainCollection[@"subtotal"][@"title"] sizeWithAttributes:reqAttributeSubTotal];
    UILabel *subTotalLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeSubTotal.width, 25)];
    [subTotalLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subTotalLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [subTotalLabelName setText:mainCollection[@"subtotal"][@"title"]];
    [differentPaymentContainer addSubview:subTotalLabelName];
    
    UILabel *subTotalValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [subTotalValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subTotalValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [subTotalValueLabelName setText:mainCollection[@"subtotal"][@"value"]];
     subTotalValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:subTotalValueLabelName];

    
    Y += 30;
    
    NSDictionary *reqAttributeShipping = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             //  shipping lebel
    CGSize reqStringSizeShipping = [mainCollection[@"shipping"][@"title"] sizeWithAttributes:reqAttributeShipping];
    UILabel *shippingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShipping.width, 25)];
    [shippingLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingLabelName setText:mainCollection[@"shipping"][@"title"]];
    [differentPaymentContainer addSubview:shippingLabelName];
    
    UILabel *shippingValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [shippingValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingValueLabelName setText:mainCollection[@"shipping"][@"value"]];
    shippingValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:shippingValueLabelName];

    Y +=30;
    
    NSDictionary *reqAttributeTax = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // total lebel
    CGSize reqStringSizeTax = [mainCollection[@"tax"][@"title"] sizeWithAttributes:reqAttributeTax];
    UILabel *taxLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTax.width, 25)];
    [taxLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [taxLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [taxLabelName setText:mainCollection[@"tax"][@"title"]];
    [differentPaymentContainer addSubview:taxLabelName];
    
    UILabel *taxValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [taxValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [taxValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [taxValueLabelName setText:mainCollection[@"tax"][@"value"]];
    taxValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:taxValueLabelName];

    Y +=30;
    
    NSDictionary *reqAttributeTotalOrder = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};             //  total order lebel
    CGSize reqStringSizeTotalOrder = [mainCollection[@"totalOrderedAmount"][@"title"] sizeWithAttributes:reqAttributeTotalOrder];
    UILabel *totalOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalOrder.width, 25)];
    [totalOrderLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [totalOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    [totalOrderLabelName setText:mainCollection[@"totalOrderedAmount"][@"title"]];
    [differentPaymentContainer addSubview:totalOrderLabelName];
    
    UILabel *totalOrderValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totalOrderValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalOrderValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalOrderValueLabelName setText:mainCollection[@"totalOrderedAmount"][@"value"]];
    totalOrderValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totalOrderValueLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeTotalVendor = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};                 //  total vendor lebel
    CGSize reqStringSizeTotalVendor = [mainCollection[@"totalVendorAmount"][@"title"] sizeWithAttributes:reqAttributeTotalVendor];
    UILabel *totalVendorLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalVendor.width, 25)];
    [totalVendorLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalVendorLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalVendorLabelName setText:mainCollection[@"totalVendorAmount"][@"title"]];
    [differentPaymentContainer addSubview:totalVendorLabelName];
    
    UILabel *totaVendorlValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totaVendorlValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totaVendorlValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totaVendorlValueLabelName setText:mainCollection[@"totalVendorAmount"][@"value"]];
    totaVendorlValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totaVendorlValueLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeTotalAdmin = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};                //  total admin commission lebel
    CGSize reqStringSizeTotalAdmin = [mainCollection[@"totalAdminComission"][@"title"] sizeWithAttributes:reqAttributeTotalAdmin];
    UILabel *totalAdminCommLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalAdmin.width, 25)];
    [totalAdminCommLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalAdminCommLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalAdminCommLabelName setText:mainCollection[@"totalAdminComission"][@"title"]];
    [differentPaymentContainer addSubview:totalAdminCommLabelName];
    
    UILabel *totaAdminCommlValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totaAdminCommlValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totaAdminCommlValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totaAdminCommlValueLabelName setText:mainCollection[@"totalAdminComission"][@"value"]];
    totaAdminCommlValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totaAdminCommlValueLabelName];
    
    Y +=30;
    
    CGRect differentpaymentInformationContainerFrame = differentPaymentContainer.frame;
    differentpaymentInformationContainerFrame.size.height = Y;
    differentPaymentContainer.frame = differentpaymentInformationContainerFrame;
    
    mainContainerY +=Y+10;
    internalY +=Y +10;
        
    CGRect mainContainerFrame = detailListContainer.frame;
    mainContainerFrame.size.height = internalY;
    detailListContainer.frame = mainContainerFrame;
    _mainViewHeightConstraints.constant =mainContainerY+50 ;
    }
}

-(void)buttonHandlerForSendEmail:(UIButton*)button{
    
    whichApiDataToprocess = @"sendEailToCustomer";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)buttonHandlerForSendInvoice:(UIButton*)button{
    if(button.tag == 1000){
    whichApiDataToprocess = @"createInvoiceRequest";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];

    }
    
}
-(void) callInvoiceTitleTap:(UITapGestureRecognizer *)recognize {
     UILabel *result = [recognize.view viewWithTag:recognize.view.tag];
    if([result.text isEqualToString:@"Invoices"]){
        [self performSegueWithIdentifier:@"orderDeatilsToInvoiceDetails" sender:self];}
   else if([result.text isEqualToString:@"Refunds"]){
       [self performSegueWithIdentifier:@"sellerOrderDetailsToCreditMemoList" sender:self];
    }
   else if([result.text isEqualToString:@"Shipments"]){
       [self performSegueWithIdentifier:@"orderDetailsToShipmentDetails" sender:self];
   }
    
}




-(void)buttonHandlerForCancelOrder:(UIButton*)button{
    whichApiDataToprocess = @"cancelOrder";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"orderDeatilsToInvoiceDetails"]) {
        InvoiceDetails *destViewController = segue.destinationViewController;
        destViewController.incrementId = _incrementId;
        destViewController.customerId = _customerId;
        destViewController.invoiceId = mainCollection[@"links"][@"invoice"][@"invoiceId"];
    }
    else if([segue.identifier isEqualToString:@"sellerOrderDetailsToCreditMemoList"]) {
        CreditMemoList *destViewController = segue.destinationViewController;
        destViewController.incrementId = _incrementId;
        destViewController.customerId = _customerId;
    }
    else if([segue.identifier isEqualToString:@"orderDetailsToShipmentDetails"]) {
        ShipMentDetails *destViewController = segue.destinationViewController;
        destViewController.incrementId = _incrementId;
        destViewController.customerId = _customerId;
        destViewController.shipmentId = mainCollection[@"links"][@"shipment"][@"shipmentId"];
    }
    
}


@end
