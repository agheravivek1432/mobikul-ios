//
//  AddCategoryCell.h
//  MobikulMp
//
//  Created by Apple on 22/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCategoryCell : UITableViewCell

{
    IBOutlet UIView *viewAddCategory;
    IBOutlet UILabel *lblCategory;
    IBOutlet UITextField *txtFCategory;
    IBOutlet UIButton *btnAddCategory;
}

@property (nonatomic, retain) IBOutlet UITextField *txtFCategory;
@property (nonatomic, retain) IBOutlet UIButton *btnAddCategory;

-(void)reloadAddCategoryCell;

@end
