//
//  Category.h
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCategory : UIViewController<NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible, sortSignal;
    id mainCollection, addToWishListCollection, addToCartCollection,collection;
    NSMutableString *finalData;
    NSMutableArray *productCurtainOpenSignal, *sortDirection;
    NSCache *imageCache;
    NSString *sortItem, *sortDir;
    NSDictionary *productDictionary;
    NSString *whichApiDataToprocess, *productIdForApiCall,*reloadPageData;
    NSUserDefaults *languageCode;
    NSInteger flag;
    UIWindow *currentWindow;
    NSMutableArray *filterCodeValue,*filterIdValue,*filterCodeHeader,*filterItemValue;
    UIView *ratingContainerBlock;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSIndexPath *indexPathValue;
    NSArray *arrayMainCollection;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
}

@property (nonatomic) NSString *categoryId;
@property (nonatomic) NSString *categoryName;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UICollectionView *categoryProductCollectionView;
@property (weak, nonatomic) IBOutlet UIView *sortByView;

@end

