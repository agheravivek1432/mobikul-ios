//
//  AddEditAddress.m
//  Mobikul
//
//  Created by Ratnesh on 12/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "AddEditAddress.h"
#import "GlobalData.h"
#import "ToastView.h"

GlobalData *globalObjectAddEditAddress;

@implementation AddEditAddress

GlobalData *globalObjectEditAddress;

- (void)viewDidLoad {
    [super viewDidLoad];
    _statePicker.tag = 2;
    _picker.tag = 1;
    globalObjectEditAddress=[[GlobalData alloc] init];
    whichApiResultDataToProcess = @"formdata";
    [globalObjectEditAddress language];
    countryPickerData = [[NSMutableArray alloc] init];
    statePickerData = [[NSMutableArray alloc] init];
    // checking for new address or edit address
    if([_addOrEdit isEqual: @"1"]){
        self.navigationItem.title = [globalObjectEditAddress.languageBundle localizedStringForKey:@"newAddress" value:@"" table:nil];
        isEdit = 0;
    }
    else{
        self.navigationItem.title =[globalObjectEditAddress.languageBundle localizedStringForKey:@"editAddress" value:@"" table:nil];
        isEdit = 1;
    }
    
    globalObjectAddEditAddress = [[GlobalData alloc] init];
    globalObjectAddEditAddress.delegate = self;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //self.navigationItem.title = [globalObjectEditAddress.languageBundle localizedStringForKey:@"addEditAddress" value:@"" table:nil];
    _firstName.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"First Name" value:@"" table:nil];
    _lastName.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"lastName" value:@"" table:nil];
    _company.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"company" value:@"" table:nil];
    _telephone.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"telephone" value:@"" table:nil];
    _fax.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"fax" value:@"" table:nil];
    _streetAddress.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"streetAddress" value:@"" table:nil];
    _streetAddress2.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"streetAddress2" value:@"" table:nil];
    _city.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"city" value:@"" table:nil];
    _state.text = [globalObjectEditAddress.languageBundle localizedStringForKey:@"state" value:@"" table:nil];
    _stateFieldsContainerHeightConstraint.constant = 30;
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    customerName = [preferences objectForKey:@"customerName"];
    NSArray* customerNameArray = [customerName componentsSeparatedByString: @" "];
    _firstNameField.text = [customerNameArray objectAtIndex: 0];
    _lastNameField.text = [customerNameArray objectAtIndex: 1];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    [self LoginwithSeller];
}

-(void)LoginwithSeller
{
    STR_SellerStatus = [[NSUserDefaults standardUserDefaults]stringForKey:@"isSellerKey"];
    if (![STR_SellerStatus isEqualToString:@"1"]) {
        _LBL_defaultPickUpswitchTitle.hidden = YES;
        _defaultPickUpSwitch.hidden=YES;
        default_pickup=@"0";
    }
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 1)
        return countryPickerData.count;
    else
        return statePickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 1)
        return countryPickerData[row];
    else
        return statePickerData[row];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectEditAddress callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectEditAddress.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectEditAddress.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectEditAddress.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectEditAddress.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectEditAddress.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil] ];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if([whichApiResultDataToProcess isEqualToString:@"formdata"]){
        [post appendFormat:@"customerId=%@&", [preferences objectForKey:@"customerId"]];
        if(isEdit == 1)
            [post appendFormat:@"addressId=%@", _addressId];
        [globalObjectAddEditAddress callHTTPPostMethod:post api:@"mobikulhttp/customer/addressformData" signal:@"HttpPostMetod"];
        globalObjectEditAddress.delegate = self;
    }
    else if([whichApiResultDataToProcess isEqualToString:@"saveformdata"]){
        [self saveAddressApi];
        globalObjectEditAddress.delegate = self;
    }
    
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
        
    }
    if([whichApiResultDataToProcess isEqual: @"formdata"]) {
        for(int i = 0; i < [collection[@"countryData"] count]; i++){
            NSDictionary *dict = [collection[@"countryData"] objectAtIndex:i];
            NSString *decode = [dict[@"name"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [countryPickerData addObject:[decode stringByReplacingOccurrencesOfString:@"+" withString:@" "]];
        }
        _picker.dataSource = self;
        _picker.delegate = self;
        _statePicker.dataSource = self;
        _statePicker.delegate = self;
        if(isEdit == 1){
            _firstNameField.text = collection[@"addressData"][@"firstname"];
            _lastNameField.text = collection[@"addressData"][@"lastname"];
            _companyField.text = collection[@"addressData"][@"company"];
            _telephoneField.text = collection[@"addressData"][@"telephone"];
            _faxField.text = collection[@"addressData"][@"fax"];
            _street1Field.text = collection[@"addressData"][@"street"][0];
            if([collection[@"addressData"][@"street"] count] > 1)
                _street2Field.text = collection[@"addressData"][@"street"][1];
            _cityField.text = collection[@"addressData"][@"city"];
            _zipcodeField.text = collection[@"addressData"][@"postcode"];
            for(int i = 0; i < [collection[@"countryData"] count]; i++){
                NSDictionary *dict = [collection[@"countryData"] objectAtIndex:i];
                if(dict[@"country_id"] == collection[@"addressData"][@"country_id"]){
                    countryIndex = i;
                    countryValue = dict[@"country_id"];
                    [_picker selectRow:i inComponent:0 animated:NO];
                    [self pickerView:_picker didSelectRow:i inComponent:0];
                    break;
                }
            }
            NSString *regionId = collection[@"addressData"][@"region_id"];
            NSInteger flag = 0;
            for(int i = 0; i < [collection[@"countryData"] count]; i++){
                NSDictionary *dict = [collection[@"countryData"] objectAtIndex:i];
                if(dict[@"country_id"] == collection[@"addressData"][@"country_id"]){
                    if([dict[@"states"] count]>0){
                        for(int j=0;j<[dict[@"states"]count]; j++){
                            NSDictionary *stateDict = [dict[@"states"] objectAtIndex:j] ;
                            if([stateDict[@"region_id"] isEqualToString:regionId]){
                                _mainContainerHeightConstraint.constant = 980;
                                flag = 1;
                                break;
                            }
                        }
                        if(flag == 0){
                            _stateField.text = collection[@"addressData"][@"region"];
                            _mainContainerHeightConstraint.constant = 910;
                            break;
                        }
                        
                    }
                    else{
                        _stateField.text = collection[@"addressData"][@"region"];
                        _mainContainerHeightConstraint.constant = 910;
                    }
                }
            }
            
            if([collection[@"addressData"][@"region_id"] isEqual: @"0"])
                _stateField.text = collection[@"addressData"][@"region"];
            if([collection[@"addressData"][@"isDefaultBilling"]  isEqual: @"true"])
                [_defaultBillingSwitch setOn:YES animated:NO];
            else
                [_defaultBillingSwitch setOn:NO animated:NO];
            if([collection[@"addressData"][@"isDefaultShipping"]  isEqual: @"true"])
                [_defaultShippingSwitch setOn:YES animated:NO];
            else
                [_defaultShippingSwitch setOn:NO animated:NO];
        }
    }
    else
        if([whichApiResultDataToProcess isEqual: @"saveformdata"]){
            if([collection[@"status"] boolValue] == TRUE)
                [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
            else
                [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"error" withDuaration:5.0];
        }
    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    @try{
        if(pickerView.tag == 1){
            countryIndex = row;
            NSDictionary *dict = [collection[@"countryData"] objectAtIndex:row];
            if(dict[@"states"] != nil) {
                regionValue = @"";
                _stateField.text = @"";
                _stateField.hidden = YES;
                _stateFieldsContainerHeightConstraint.constant = 100;
                _mainContainerHeightConstraint.constant = 980;
                _statePicker.hidden = NO;
                [statePickerData removeAllObjects];
                for(int i=0; i<[dict[@"states"] count]; i++){
                    NSDictionary *stateDict = [dict[@"states"] objectAtIndex:i];
                    [statePickerData addObject : stateDict[@"name"]];
                }
                _statePicker.dataSource = self;
                _statePicker.delegate = self;
                NSDictionary *stateDict = [dict[@"states"] objectAtIndex:0];
                regionIdValue = stateDict[@"region_id"];
                [_statePicker selectRow:0 inComponent:0 animated:NO];
                [self pickerView:_statePicker didSelectRow:0 inComponent:0];
                if(isEdit == 1){
                    for(int i=0; i < [dict[@"states"] count]; i++){
                        NSDictionary *stateDict = [dict[@"states"] objectAtIndex:i];
                        if(stateDict[@"region_id"] == collection[@"addressData"][@"region_id"]){
                            regionIdValue = stateDict[@"region_id"];
                            [_statePicker selectRow:i inComponent:0 animated:NO];
                            [self pickerView:_statePicker didSelectRow:i inComponent:0];
                            break;
                        }
                    }
                }
            }
            else{
                regionIdValue = 0;
                _stateFieldsContainerHeightConstraint.constant = 30;
                _mainContainerHeightConstraint.constant = 910;
                _stateField.hidden = NO;
                _statePicker.hidden = YES;
            }
            countryValue = dict[@"country_id"];
        }
        else
            if(pickerView.tag == 2){
                NSDictionary *dict = [collection[@"countryData"] objectAtIndex:countryIndex];
                NSDictionary *stateDict = [dict[@"states"] objectAtIndex:row];
                regionIdValue = stateDict[@"region_id"];
            }
    }
    @catch (NSException *e) {
        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
    }
}

- (IBAction)saveAddressButton:(id)sender {
    @try{
        int isValid = 1;
        NSString *errorMessage;
        firstNameValue = _firstNameField.text;
        lastNameValue = _lastNameField.text;
        companyValue = _companyField.text;
        telephoneValue = _telephoneField.text;
        faxValue = _faxField.text;
        street1Value = _street1Field.text;
        street2Value = _street2Field.text;
        cityValue = _cityField.text;
        if(regionIdValue == 0)
            regionValue = _stateField.text;
        else
            regionValue = @"";
        zipValue = _zipcodeField.text;
        if(_defaultBillingSwitch.isOn)
            default_billing = @"1";
        else
            default_billing = @"0";
        if(_defaultShippingSwitch.isOn)
            default_shipping = @"1";
        else
            default_shipping = @"0";
        if(_defaultPickUpSwitch.isOn)
            default_pickup = @"1";
        else
            default_pickup = @"0";
        NSDictionary *dict = [collection[@"countryData"] objectAtIndex:countryIndex];
        if([firstNameValue isEqual: @""]){
            isValid = 0;
            errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"firstNameRequire" value:@"" table:nil];
        }
        else
            if([lastNameValue isEqual: @""]){
                isValid = 0;
                errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"lastNameRequire" value:@"" table:nil];
            }
            else
                if([telephoneValue isEqual: @""]){
                    isValid = 0;
                    errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"telephoneRequired" value:@"" table:nil];
                }
                else
                    if([street1Value isEqual: @""]){
                        isValid = 0;
                        errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"streetRequired" value:@"" table:nil];
                    }
                    else
                        if([cityValue isEqual: @""]){
                            isValid = 0;
                            errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"cityRequired" value:@"" table:nil];
                        }
                        else
                            if([zipValue isEqual: @""]){
                                isValid = 0;
                                errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"zipRequired" value:@"" table:nil];
                            }
                            else
                                if(countryValue.length == 0){
                                    isValid = 0;
                                    errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"countryRequired" value:@"" table:nil];
                                }
                                else
                                    if(dict[@"states"] != nil) {
                                        if([regionIdValue isEqual: 0]){
                                            isValid = 0;
                                            errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"stateRequired" value:@"" table:nil];
                                        }
                                    }
                                    else
                                        if([regionValue isEqual: @""]){
                                            isValid = 0;
                                            errorMessage = [globalObjectEditAddress.languageBundle localizedStringForKey:@"editAddress" value:@"" table:nil];
                                        }
        
        if(isValid == 0){
            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectEditAddress.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectEditAddress.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
            [errorAlert addAction:noBtn];
            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
        }
        else{
            preferences = [NSUserDefaults standardUserDefaults];
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            whichApiResultDataToProcess = @"saveformdata";
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self saveAddressApi];
        }
    }
    @catch (NSException *e) {
        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
    }
}

- (IBAction)defaultBillingSwitch:(id)sender {
    if(_defaultBillingSwitch.isOn)
        default_billing = @"1";
    else
        default_billing = @"0";
}

- (IBAction)defaultShippingSwitch:(id)sender {
    if(_defaultShippingSwitch.isOn)
        default_shipping = @"1";
    else
        default_shipping = @"0";
}

- (IBAction)defaultPickUpSwitch:(id)sender {
    if(_defaultPickUpSwitch.isOn)
        default_pickup = @"1";
    else
        default_pickup = @"0";
}


-(void)saveAddressApi{
    addressDataDictionary = [NSMutableDictionary dictionary];
    [addressDataDictionary setObject:firstNameValue forKey:@"firstname"];
    [addressDataDictionary setObject:lastNameValue forKey:@"lastname"];
    [addressDataDictionary setObject:companyValue forKey:@"company"];
    [addressDataDictionary setObject:telephoneValue forKey:@"telephone"];
    [addressDataDictionary setObject:faxValue forKey:@"fax"];
    [addressDataDictionary setObject:cityValue forKey:@"city"];
    if(![regionValue isEqual: @""])
        [addressDataDictionary setObject:regionValue forKey:@"region"];
    if(regionIdValue != 0)
        [addressDataDictionary setObject:regionIdValue forKey:@"region_id"];
    [addressDataDictionary setObject:zipValue forKey:@"postcode"];
    [addressDataDictionary setObject:countryValue forKey:@"country_id"];
    NSArray *street = @[street1Value, street2Value];
    [addressDataDictionary setObject:street forKey:@"street"];
    [addressDataDictionary setObject:default_billing forKey:@"default_billing"];
    [addressDataDictionary setObject:default_shipping forKey:@"default_shipping"];
    [addressDataDictionary setObject:default_pickup forKey:@"default_pickup"];
    //default_pickup //set 1 //New
    
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    if(isEdit == 1)
        [post appendFormat:@"addressId=%@&", _addressId];
    else
        [post appendFormat:@"addressId=%@&", @"0"];
    [post appendFormat:@"customerId=%@&", [preferences objectForKey:@"customerId"]];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSData *jsonAddressData = [NSJSONSerialization dataWithJSONObject:addressDataDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonAddressString = [[NSString alloc] initWithData:jsonAddressData encoding:NSUTF8StringEncoding];
    [post appendFormat:@"addressData=%@", jsonAddressString];
    [GlobalData alertController:currentWindow msg:[globalObjectAddEditAddress.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    [globalObjectAddEditAddress callHTTPPostMethod:post api:@"mobikulhttp/customer/addressSave" signal:@"HttpPostMetod"];
    globalObjectEditAddress.delegate = self;
}

@end
