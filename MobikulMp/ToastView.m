//
//  ToastView.m
//  Mobikul
//
//  Created by Ratnesh on 20/01/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "ToastView.h"

@interface ToastView ()
@property (strong, nonatomic, readonly) UILabel *textLabel;
@end
@implementation ToastView
@synthesize textLabel = _textLabel;

float ToastHeight = 50.0f;
float const ToastGap = 10.0f;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(UILabel *)textLabel{
    if(!_textLabel) {
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, self.frame.size.width - 10.0, self.frame.size.height - 10.0)];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:19];
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _textLabel.numberOfLines = 0;
        [self addSubview:_textLabel];
    }
    return _textLabel;
}

- (void)setText:(NSString *)text{
    _text = text;
    self.textLabel.text = text;
    [self.textLabel sizeToFit];
    ToastHeight = self.textLabel.frame.size.height;
    
}

+ (void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withStatus:(NSString*)status withDuaration:(float)duration;{
    //Count toast views are already showing on parent. Made to show several toasts one above another
    int toastsAlreadyInParent = 0;
    for (UIView *subView in [parentView subviews]) {
        if ([subView isKindOfClass:[ToastView class]])
            toastsAlreadyInParent++;
    }
    CGRect parentFrame = parentView.frame;
    float yOrigin = parentFrame.size.height - (110.0 + ToastHeight * toastsAlreadyInParent + ToastGap * toastsAlreadyInParent);
    CGRect selfFrame = CGRectMake(parentFrame.origin.x + 20.0, yOrigin, parentFrame.size.width - 40.0, ToastHeight);
    ToastView *toast = [[ToastView alloc] initWithFrame:selfFrame];
    if([status isEqual: @"error"]){
        NSString *cString = [[@"F16048" stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
        NSRange range;
        range.location = 0;
        range.length = 2;
        NSString *rString = [cString substringWithRange:range];
        range.location = 2;
        NSString *gString = [cString substringWithRange:range];
        range.location = 4;
        NSString *bString = [cString substringWithRange:range];
        unsigned int r, g, b;
        [[NSScanner scannerWithString:rString] scanHexInt:&r];
        [[NSScanner scannerWithString:gString] scanHexInt:&g];
        [[NSScanner scannerWithString:bString] scanHexInt:&b];
        toast.backgroundColor = [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
        
        cString = [[@"B71B03" stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
        range.location = 0;
        range.length = 2;
        rString = [cString substringWithRange:range];
        range.location = 2;
        gString = [cString substringWithRange:range];
        range.location = 4;
        bString = [cString substringWithRange:range];
        [[NSScanner scannerWithString:rString] scanHexInt:&r];
        [[NSScanner scannerWithString:gString] scanHexInt:&g];
        [[NSScanner scannerWithString:bString] scanHexInt:&b];
        toast.layer.borderColor = [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f].CGColor;
        toast.layer.borderWidth = 2.0;
    }
    else
        if([status isEqual: @"success"]){
            NSString *cString = [[@"9BD16C" stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
            NSRange range;
            range.location = 0;
            range.length = 2;
            NSString *rString = [cString substringWithRange:range];
            range.location = 2;
            NSString *gString = [cString substringWithRange:range];
            range.location = 4;
            NSString *bString = [cString substringWithRange:range];
            unsigned int r, g, b;
            [[NSScanner scannerWithString:rString] scanHexInt:&r];
            [[NSScanner scannerWithString:gString] scanHexInt:&g];
            [[NSScanner scannerWithString:bString] scanHexInt:&b];
            toast.backgroundColor = [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
            
            cString = [[@"4E8C19" stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
            range.location = 0;
            range.length = 2;
            rString = [cString substringWithRange:range];
            range.location = 2;
            gString = [cString substringWithRange:range];
            range.location = 4;
            bString = [cString substringWithRange:range];
            [[NSScanner scannerWithString:rString] scanHexInt:&r];
            [[NSScanner scannerWithString:gString] scanHexInt:&g];
            [[NSScanner scannerWithString:bString] scanHexInt:&b];
            toast.layer.borderColor = [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f].CGColor;
            toast.layer.borderWidth = 2.0;
        }
    toast.alpha = 0.0f;
    toast.layer.cornerRadius = 4.0;
    toast.text = text;
    
    CGRect newFrame = toast.frame;
    newFrame.size.height = ToastHeight+10;
    toast.frame = newFrame;
    float yOrigin1 = parentView.frame.size.height - (toast.frame.size.height + 60);
    CGRect newFrame1 = toast.frame;
    newFrame1.origin.y = yOrigin1;
    toast.frame = newFrame1;
    toast.textLabel.center = CGPointMake(toast.frame.size.width  / 2,
                                         toast.frame.size.height / 2);
    
    
    [parentView addSubview:toast];
    [UIView animateWithDuration:0.4 animations:^{
        toast.alpha = 0.9f;
        toast.textLabel.alpha = 0.9f;
    }completion:^(BOOL finished) {
        if(finished){}
    }];
    [toast performSelector:@selector(hideSelf) withObject:nil afterDelay:duration];
    
}

- (void)hideSelf{
    [UIView animateWithDuration:0.4 animations:^{
        self.alpha = 0.0;
        self.textLabel.alpha = 0.0;
    }completion:^(BOOL finished) {
        if(finished)
            [self removeFromSuperview];
    }];
}

@end