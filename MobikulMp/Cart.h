//
//  Cart.h
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cart : UIViewController <NSXMLParserDelegate>{
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *code, *dataFromApi;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id mainCollection, removeItemCollection, emptyCartCollection,applyCouponCollection,collection;
    NSMutableString *finalData;
    NSCache *imageCache;
    NSString *whichApiDataToprocess, *itemIdToRemove, *enteredCouponCode;
    NSDictionary *productDictionary;
    NSUserDefaults *languageCode;
    NSMutableArray *toUpdateItemQtys, *toUpdateItemIds;
    UIWindow *currentWindow;
    NSString *itemId;
}

@property (weak, nonatomic) IBOutlet UILabel *cartEmpty;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (nonatomic, strong) UIView *itemBlockToRemove;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UIView *cartMainView;
@property (weak, nonatomic) IBOutlet UIView *itemContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *actionButtonContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionButtonContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *applyCouponContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *applyCouponContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *summaryContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *summaryContainerHeightConstraint;
- (IBAction)proceedToCheckout:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *proceedToCheckoutName;

@end