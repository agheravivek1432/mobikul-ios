//
//  MyDownloads.h
//  Mobikul
//
//  Created by Ratnesh on 08/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDownloads : UIViewController <NSXMLParserDelegate,UIScrollViewDelegate>{
    NSUserDefaults *preferences;
    UIAlertController *alert;
    NSInteger isAlertVisible;
    id collection, downloadUrlCollection;
    NSString *whichApiResponseToHandle, *fileName,*dataFromApi;
    NSURLSessionDownloadTask *download;
    NSString *hash;
    UIWindow *currentWindow;
    NSArray *arrayMainCollection;
    NSInteger totalItemDisplayed,pageNumber,totalCount;
    NSInteger contentHeight;
    NSInteger loadPageRequestFlag;
    NSString *reloadPageData;
}

@property (nonatomic, strong)NSURLSession *backgroundSession;
@property (weak, nonatomic) IBOutlet UIView *downloadContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *downloadContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *myDownloadScrollView;

@end
