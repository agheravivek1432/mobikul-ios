//
//  SubCategory.h
//  Mobikul
//
//  Created by Ratnesh on 17/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategory : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    NSUserDefaults *preferences;
    NSMutableArray *subCategories;
    NSMutableDictionary *categoryDict;
}

@property (nonatomic) NSDictionary *subCategoryData;
@property (nonatomic) NSString *categoryName;
@property (weak, nonatomic) IBOutlet UITableView *subCategoryTable;

@end
