//
//  CustomerOrderDetails.m
//  MobikulMp
//
//  Created by kunal prasad on 15/07/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "CustomerOrderDetails.h"
#include "GlobalData.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
GlobalData *globalObjectOrderDetails;

@interface CustomerOrderDetails ()

@end

@implementation CustomerOrderDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectOrderDetails=[[GlobalData alloc] init];
    globalObjectOrderDetails.delegate = self;
    [globalObjectOrderDetails language];
    preferences = [NSUserDefaults standardUserDefaults];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectOrderDetails callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}
-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectOrderDetails.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectOrderDetails.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectOrderDetails.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectOrderDetails.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectOrderDetails.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    [post appendFormat:@"incrementId=%@", _incrementId];
    [globalObjectOrderDetails callHTTPPostMethod:post api:@"mobikulhttp/customer/getorderDetail" signal:@"HttpPostMetod"];
    globalObjectOrderDetails.delegate = self;
}


-(void)doFurtherProcessingWithResult{
    
    
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    float mainContainerY = 5;
    float  internalY = 0;
    float Y = 0;
    float space;
    if(SCREEN_WIDTH>500){
        
        space = 10;
    }
    else{
        space = 4;
    }
    UIView *detailListContainer = [[UIView alloc] initWithFrame:CGRectMake(5, mainContainerY , SCREEN_WIDTH-space, 900)];       // main container
    detailListContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    detailListContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:detailListContainer];
    
    
    NSString *heading1 = [@"ORDER #" stringByAppendingString:collection[@"incrementId"]];
    NSString *heading2 = [heading1 stringByAppendingString:@" - "];
    NSString *finalHeading = [heading2 stringByAppendingString:collection[@"statusLabel"]];
    
    //    NSDictionary *reqAttributesHeading = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]};             //  heading
    //    CGSize reqStringSizeHeading = [finalHeading sizeWithAttributes:reqAttributesHeading];
    UILabel *headingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY, SCREEN_WIDTH-15, 30)];
    [headingLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [headingLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]];
    headingLabelName.adjustsFontSizeToFitWidth = YES;
    [headingLabelName setText:finalHeading];
    [detailListContainer addSubview:headingLabelName];
    
    internalY +=35;
    
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,detailListContainer.frame.size.width-10 ,1)];         // line 1
    line1.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line1.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:line1];
    
    internalY +=4;
    
    NSDictionary *reqAttributesAboutOrder = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]};
    CGSize reqStringSizeAboutOrder = [@"About This Order:" sizeWithAttributes:reqAttributesAboutOrder];
    UILabel *aboutOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY,reqStringSizeAboutOrder.width, 20)];    //
    [aboutOrderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [aboutOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [aboutOrderLabelName setText:@"About This Order:"];
    [detailListContainer addSubview:aboutOrderLabelName];
    
    
    internalY +=30;
    
    
    
    NSDictionary *reqAttributesOrderDate = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]};
    CGSize reqStringSizeOrderDate = [[@"ORDER DATE :" stringByAppendingString:collection[@"orderDate"]] sizeWithAttributes:reqAttributesOrderDate];
    UILabel *orderDateLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY,SCREEN_WIDTH-15, 20)];    //
    [orderDateLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [orderDateLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [orderDateLabelName setText:[@"ORDER DATE :" stringByAppendingString:collection[@"orderDate"]]];
    orderDateLabelName.adjustsFontSizeToFitWidth = YES;
    [detailListContainer addSubview:orderDateLabelName];
    
    internalY +=30;
    
    UIView *shippingAddressContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // shipping address data container
    shippingAddressContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    shippingAddressContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:shippingAddressContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeShippingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeShippingTitle = [@"SHIPPING ADDRESS :" sizeWithAttributes:reqAttributeShippingTitle];
    UILabel *shippingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingTitle.width, 25)];
    [shippingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [shippingTitleLabelName setText:@"SHIPPING ADDRESS :"];
    [shippingAddressContainer addSubview:shippingTitleLabelName];
    
    Y +=30;
    
    for(int i=0;i<[collection[@"shippingAddress"] count]; i++){
        NSDictionary *address = [collection[@"shippingAddress"] objectAtIndex: i];
        NSString *addr = address;
        NSDictionary *reqAttributeShippingAddr = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping address
        CGSize reqStringSizeShippingAddr = [addr sizeWithAttributes:reqAttributeShippingAddr];
        UILabel *shippingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingAddr.width, 25)];
        [shippingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [shippingTitleLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [shippingTitleLabelName setText:addr];
        [shippingAddressContainer addSubview:shippingTitleLabelName];
        
        Y +=30;
        
    }
    
    CGRect shippingContainerFrame = shippingAddressContainer.frame;
    shippingContainerFrame.size.height = Y;
    shippingAddressContainer.frame = shippingContainerFrame;
    
    internalY +=Y+5;
    
    
    UIView *shippingInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];// shipping information method container
    shippingInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    shippingInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:shippingInformationContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeShippingMethod = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping method
    CGSize reqStringSizeShippingMethod = [[@"SHIPPING METHOD :" stringByAppendingString:collection[@"shippingMethod"]] sizeWithAttributes:reqAttributeShippingMethod];
    UILabel *shippingMethodLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , SCREEN_WIDTH-45, 25)];
    [shippingMethodLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingMethodLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingMethodLabelName setText:[@"SHIPPING METHOD :" stringByAppendingString:collection[@"shippingMethod"]]];
    // shippingMethodLabelName.adjustsFontSizeToFitWidth = YES;
    [shippingInformationContainer addSubview:shippingMethodLabelName];
    
    Y +=30;
    
    CGRect shippingMethodContainerFrame = shippingInformationContainer.frame;
    shippingMethodContainerFrame.size.height = Y;
    shippingInformationContainer.frame = shippingMethodContainerFrame;
    
    internalY += Y+5;
    
    
    UIView *billingAddressContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // billing address data container
    billingAddressContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    billingAddressContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:billingAddressContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeBillingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // billing information heading
    CGSize reqStringSizeBillingTitle = [@"BILLING ADDRESS :" sizeWithAttributes:reqAttributeBillingTitle];
    UILabel *billingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingTitle.width, 25)];
    [billingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [billingTitleLabelName setText:@"BILLING ADDRESS :"];
    [billingAddressContainer addSubview:billingTitleLabelName];
    
    Y +=30;
    
    for(int i=0;i<[collection[@"billingAddress"] count]; i++){
        NSDictionary *address = [collection[@"billingAddress"] objectAtIndex: i];
        NSString *addr = address;
        NSDictionary *reqAttributeBillingAddr = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping address
        CGSize reqStringSizeBillingAddr = [addr sizeWithAttributes:reqAttributeBillingAddr];
        UILabel *billingAddrLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingAddr.width, 25)];
        [billingAddrLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [billingAddrLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [billingAddrLabelName setText:addr];
        [billingAddressContainer addSubview:billingAddrLabelName];
        
        Y +=30;
        
    }
    
    
    CGRect billingAddrContainerFrame = billingAddressContainer.frame;
    billingAddrContainerFrame.size.height = Y;
    billingAddressContainer.frame = billingAddrContainerFrame;
    
    internalY +=Y +5;
    
    UIView *billingInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];// billing information data container
    billingInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    billingInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:billingInformationContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeBillingMethod = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // billing method
    CGSize reqStringSizeBillingMethod = [[@"PAYMENT METHOD :" stringByAppendingString:collection[@"billingMethod"]] sizeWithAttributes:reqAttributeBillingMethod];
    UILabel *billingMethodLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , SCREEN_WIDTH-45, 25)];
    [billingMethodLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingMethodLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [billingMethodLabelName setText:[@"PAYMENT METHOD :" stringByAppendingString:collection[@"billingMethod"]]];
    billingMethodLabelName.adjustsFontSizeToFitWidth = YES;
    [billingInformationContainer addSubview:billingMethodLabelName];
    
    Y +=30;
    CGRect billingMethodContainerFrame = billingInformationContainer.frame;
    billingMethodContainerFrame.size.height = Y;
    billingInformationContainer.frame = billingMethodContainerFrame;
    
    
    
    internalY +=Y +5;
    
    NSDictionary *reqAttributeItemOrderTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]};             // payment heading information heading
    CGSize reqStringItemOrderTitle = [@"ITEM ORDERED" sizeWithAttributes:reqAttributeItemOrderTitle];
    UILabel *itemOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, internalY , reqStringItemOrderTitle.width, 35)];
    [itemOrderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [itemOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [itemOrderLabelName setText:@"ITEM ORDERED"];
    [detailListContainer addSubview:itemOrderLabelName];
    
    internalY +=40;
    
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,detailListContainer.frame.size.width-10 ,1)];           // line 3
    line3.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line3.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:line3];
    
    internalY += 5;
    
    UIView *paymentInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,400)];// payment information data container
    paymentInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    paymentInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:paymentInformationContainer];
    
    Y = 5;
    
    for(int i=0;i<[collection[@"items"] count];i++){
        
        NSDictionary *productData  = [collection[@"items"] objectAtIndex: i];
        
        NSDictionary *reqAttributeProductName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // product name subheading information heading
        CGSize reqStringProductName = [[@"PRODUCT :" stringByAppendingString:productData[@"name"]] sizeWithAttributes:reqAttributeProductName];
        UILabel *productLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , SCREEN_WIDTH-45, 25)];
        [productLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [productLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        productLabelName.adjustsFontSizeToFitWidth = YES;
        [productLabelName setText:[@"PRODUCT :" stringByAppendingString:productData[@"name"]]];
        [paymentInformationContainer addSubview:productLabelName];
        
        Y += 30;
        
        NSDictionary *reqAttributePriceLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // price lebel
        CGSize reqStringSizePriceLabel = [[@"PRICE : " stringByAppendingString:productData[@"price"]]  sizeWithAttributes:reqAttributePriceLebel];
        UILabel *priceLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizePriceLabel.width, 25)];
        [priceLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [priceLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [priceLabelName setText:[@"PRICE : " stringByAppendingString:productData[@"price"]]];
        [paymentInformationContainer addSubview:priceLabelName];
        
        Y +=20;
        
        NSDictionary *reqAttributeLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};                             // sku lebel
        CGSize reqStringSizeLabel = [[@"SKU : " stringByAppendingString:productData[@"sku"]]  sizeWithAttributes:reqAttributeLebel];
        UILabel *skuLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeLabel.width, 25)];
        [skuLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [skuLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [skuLabelName setText:[@"SKU : " stringByAppendingString:productData[@"sku"]]];
        [paymentInformationContainer addSubview:skuLabelName];
        
        Y +=20;
        
        NSDictionary *reqAttributeStatusLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // status lebel
        CGSize reqStringSizeStatusLabel = [@"QTY : " sizeWithAttributes:reqAttributeStatusLebel];
        UILabel *statusLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeStatusLabel.width, 25)];
        [statusLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [statusLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [statusLabelName setText:@"QTY : "];
        [paymentInformationContainer addSubview:statusLabelName];
        
        
        Y +=20;
        
        if([productData[@"qty"][@"Ordered"] integerValue] > 0){
            NSString *orderValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Ordered"] intValue]];
            NSDictionary *reqAttributeOrderLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order lebel
            CGSize reqStringSizeOrderLabel = [[@"Ordered: " stringByAppendingString:orderValue] sizeWithAttributes:reqAttributeOrderLabel];
            UILabel *orderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeOrderLabel.width, 25)];
            [orderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [orderLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [orderLabelName setText:[@"Ordered: " stringByAppendingString:orderValue]];
            [paymentInformationContainer addSubview:orderLabelName];
            
            Y +=20;
        }
        if([productData[@"qty"][@"Shipped"] integerValue] > 0){
            NSString *shippedValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Shipped"] intValue]];
            NSDictionary *reqAttributeShippedLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order lebel
            CGSize reqStringSizeShippedLabel = [[@"Shipped : " stringByAppendingString:shippedValue] sizeWithAttributes:reqAttributeShippedLabel];
            UILabel *shipLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeShippedLabel.width, 25)];
            [shipLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [shipLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [shipLabelName setText:[@"Shipped : " stringByAppendingString:shippedValue]];
            [paymentInformationContainer addSubview:shipLabelName];
            
            Y +=20;
        }
        if([productData[@"qty"][@"Canceled"] integerValue] > 0){
            NSString *cancelValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Canceled"] intValue]];
            NSDictionary *reqAttributeCancelLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order lebel
            CGSize reqStringSizeCancelLabel = [[@"Canceled : " stringByAppendingString:cancelValue] sizeWithAttributes:reqAttributeCancelLabel];
            UILabel *cancelLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeCancelLabel.width, 25)];
            [cancelLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [cancelLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [cancelLabelName setText:[@"Canceled : " stringByAppendingString:cancelValue]];
            [paymentInformationContainer addSubview:cancelLabelName];
            
            Y +=20;
        }
        
        if([productData[@"qty"][@"Refunded"] integerValue] > 0){
            NSString *refundedValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Refunded"] intValue]];
            NSDictionary *reqAttributeRefundedlLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order lebel
            CGSize reqStringSizeRefundedLabel = [[@"Refunded : " stringByAppendingString:refundedValue] sizeWithAttributes:reqAttributeRefundedlLabel];
            UILabel *refundLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeRefundedLabel.width, 25)];
            [refundLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [refundLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
            [refundLabelName setText:[@"Refunded : " stringByAppendingString:refundedValue]];
            [paymentInformationContainer addSubview:refundLabelName];
            
            Y +=20;
        }
        
        Y +=5;
        if(i !=[collection[@"items"] count]-1 ){
            UIView *line4 = [[UIView alloc] initWithFrame:CGRectMake(5, Y ,paymentInformationContainer.frame.size.width-10 ,1)];           // line 3
            line4.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
            line4.layer.borderWidth = 2.0f;
            [paymentInformationContainer addSubview:line4];
        }
        
        Y +=10;
    }
    
    CGRect paymentInformationContainerFrame = paymentInformationContainer.frame;
    paymentInformationContainerFrame.size.height = Y;
    paymentInformationContainer.frame = paymentInformationContainerFrame;
    
    internalY += Y +5;
    
    UIView *differentPaymentContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,200)];// payment information data container
    differentPaymentContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    differentPaymentContainer.layer.borderWidth = 2.0f;
    differentPaymentContainer.backgroundColor = [GlobalData colorWithHexString:@"CCCCCC"];
    [detailListContainer addSubview:differentPaymentContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeSubTotal = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
    CGSize reqStringSizeSubTotal = [collection[@"totals"][@"subtotal"][@"label"] sizeWithAttributes:reqAttributeSubTotal];
    UILabel *subTotalLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeSubTotal.width, 25)];
    [subTotalLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subTotalLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [subTotalLabelName setText:collection[@"totals"][@"subtotal"][@"label"]];
    [differentPaymentContainer addSubview:subTotalLabelName];
    
    UILabel *subTotalValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [subTotalValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subTotalValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [subTotalValueLabelName setText:collection[@"totals"][@"subtotal"][@"value"]];
    subTotalValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:subTotalValueLabelName];
    
    
    Y += 30;
    
    NSDictionary *reqAttributeShipping = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             //  shipping lebel
    CGSize reqStringSizeShipping = [collection[@"totals"][@"shipping"][@"label"]  sizeWithAttributes:reqAttributeShipping];
    UILabel *shippingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShipping.width, 25)];
    [shippingLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingLabelName setText:collection[@"totals"][@"shipping"][@"label"]];
    [differentPaymentContainer addSubview:shippingLabelName];
    
    UILabel *shippingValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [shippingValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingValueLabelName setText:collection[@"totals"][@"shipping"][@"value"]];
    shippingValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:shippingValueLabelName];
    
    
    Y +=30;
    
    NSDictionary *reqAttributeTax = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // total lebel
    CGSize reqStringSizeTax = [collection[@"totals"][@"tax"][@"label"] sizeWithAttributes:reqAttributeTax];
    UILabel *taxLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTax.width, 25)];
    [taxLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [taxLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [taxLabelName setText:collection[@"totals"][@"tax"][@"label"]];
    [differentPaymentContainer addSubview:taxLabelName];
    
    UILabel *taxValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [taxValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [taxValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [taxValueLabelName setText:collection[@"totals"][@"tax"][@"value"]];
    taxValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:taxValueLabelName];
    
    
    Y +=30;
    
    NSDictionary *reqAttributeTotalOrder = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};             //  total order lebel
    CGSize reqStringSizeTotalOrder = [collection[@"totals"][@"grandTotal"][@"label"] sizeWithAttributes:reqAttributeTotalOrder];
    UILabel *totalOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalOrder.width, 25)];
    [totalOrderLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [totalOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    [totalOrderLabelName setText:collection[@"totals"][@"grandTotal"][@"label"]];
    [differentPaymentContainer addSubview:totalOrderLabelName];
    
    UILabel *totalOrderValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totalOrderValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalOrderValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalOrderValueLabelName setText:collection[@"totals"][@"grandTotal"][@"value"]];
    totalOrderValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totalOrderValueLabelName];
    
    Y +=30;
    
    CGRect differentpaymentInformationContainerFrame = differentPaymentContainer.frame;
    differentpaymentInformationContainerFrame.size.height = Y;
    differentPaymentContainer.frame = differentpaymentInformationContainerFrame;
    
    internalY +=Y +5;
    
    CGRect mainContainerFrame = detailListContainer.frame;
    mainContainerFrame.size.height = internalY;
    detailListContainer.frame = mainContainerFrame;
    
    _mainViewHeightConstraints.constant = internalY+5;
}

@end
