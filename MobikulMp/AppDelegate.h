//
//  AppDelegate.h
//  Mobikul
//
//  Created by Ratnesh on 02/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    UIAlertController *alert;
    NSMutableString *finalData;
    NSMutableData *webData;
    NSString *gotSessionId, *gotMessage, *gotCode, *gotDataFromApi;
    NSString *sessionId, *message, *code, *dataFromApi;
    NSString *token;
    NSUserDefaults *preferences;
    id collection;
}
@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) NSString *dataStore;
@property (weak, nonatomic) NSString *homeRun;
@property (nonatomic, readwrite) int firstRun;

@end

