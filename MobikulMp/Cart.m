//
//  Cart.m
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "Cart.h"
#import "GlobalData.h"
#import "ToastView.h"
#import "CatalogProduct.h"
#import "AppDelegate.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

GlobalData *globalObjectCart;

@implementation Cart

- (void)viewDidLoad {
    [super viewDidLoad];
    whichApiDataToprocess = @"";
    languageCode = [NSUserDefaults standardUserDefaults];
    [self.tabBarController setSelectedIndex:4];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    globalObjectCart = [[GlobalData alloc] init];
    globalObjectCart.delegate = self;
    [globalObjectCart language];
    _emptyView.layer.cornerRadius = 2;
    _emptyView.layer.shadowOffset = CGSizeMake(0, 0);
    _emptyView.layer.shadowRadius = 3;
    _emptyView.layer.shadowOpacity = 0.5;
    [_proceedToCheckoutName setTitle: [globalObjectCart.languageBundle localizedStringForKey:@"proceedCheckout" value:@"" table:nil] forState: UIControlStateNormal];
    _cartEmpty.text = [globalObjectCart.languageBundle localizedStringForKey:@"cartEmpty" value:@"" table:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    isAlertVisible = 0;
    whichApiDataToprocess = @"";
    finalData = [[NSMutableString alloc] initWithString:@""];
    imageCache = [[NSCache alloc] init];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 4;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectCart.languageBundle localizedStringForKey:@"cart" value:@"" table:nil];
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    NSString *quoteId = [preferences objectForKey:@"quoteId"];
    if(customerId != nil || quoteId != nil){
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
    else{
        [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
        [_emptyView setValue:@"NO" forKeyPath:@"hidden"];
        [_cartMainView setValue:@"YES" forKeyPath:@"hidden"];
    }
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}
-(void)loginRequestCall{
    [self loginRequest];
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectCart.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectCart.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId != nil)
        [post appendFormat:@"customerId=%@&", customerId];
    NSString *quoteId = [preferences objectForKey:@"quoteId"];
    if(quoteId != nil)
        [post appendFormat:@"quoteId=%@&", quoteId];
    
    if([whichApiDataToprocess isEqualToString:@"removeItem"]){
        [post appendFormat:@"itemId=%@", itemIdToRemove];
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/removecartItem" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
    else if([whichApiDataToprocess isEqualToString:@"updateCart"]){
        NSData *jsontoUpdateItemIds = [NSJSONSerialization dataWithJSONObject:toUpdateItemIds options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonUpdateItemIdString = [[NSString alloc] initWithData:jsontoUpdateItemIds encoding:NSUTF8StringEncoding];
        NSData *jsonItemQty = [NSJSONSerialization dataWithJSONObject:toUpdateItemQtys options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonItemQtysString = [[NSString alloc] initWithData:jsonItemQty encoding:NSUTF8StringEncoding];
        [post appendFormat:@"itemIds=%@&", jsonUpdateItemIdString];
        [post appendFormat:@"itemQtys=%@", jsonItemQtysString];
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/updateCart" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
    else if([whichApiDataToprocess isEqualToString:@"cancelCoupon"]){
        [post appendFormat:@"couponCode=%@&", @""];
        [post appendFormat:@"removeCoupon=%@", @"1"];
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/applyCoupon" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
    else if([whichApiDataToprocess isEqualToString:@"applyCoupon"]){
        [post appendFormat:@"couponCode=%@&", enteredCouponCode];
        [post appendFormat:@"removeCoupon=%@", @"0"];
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/applyCoupon" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
    else if([whichApiDataToprocess isEqualToString:@"emptyCart"]){
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/emptyCart" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
    else if([whichApiDataToprocess isEqualToString: @"wishlistfromcartaction"]){
        [post appendFormat:@"itemId=%@", itemId];
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/wishlistfromCart" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
    else{
        NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
        [post appendFormat:@"width=%@", screenWidth];
        [globalObjectCart callHTTPPostMethod:post api:@"mobikulhttp/checkout/getcartDetails" signal:@"HttpPostMetod"];
        globalObjectCart.delegate = self;
    }
}


-(void)doFurtherProcessingWithResult
{
    if(isAlertVisible == 1)
    {
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    if([whichApiDataToprocess isEqualToString:@"updateCart"])
    {
        isAlertVisible = 0;
        whichApiDataToprocess = @"";
        finalData = [[NSMutableString alloc] initWithString:@""];
        imageCache = [[NSCache alloc] init];
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 4;
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:@"00CBDF"];
        preferences = [NSUserDefaults standardUserDefaults];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
    else if([whichApiDataToprocess isEqualToString: @"wishlistfromcartaction"])
    {
        if([collection[@"status"] boolValue] == TRUE)
            [self viewDidAppear:TRUE];
        
    }
    else if([whichApiDataToprocess isEqualToString:@"applyCoupon"] || [whichApiDataToprocess isEqualToString:@"cancelCoupon"]){
        applyCouponCollection = collection;
        if([applyCouponCollection[@"error"] boolValue])
            [ToastView showToastInParentView:self.view withText:applyCouponCollection[@"message"] withStatus:@"error" withDuaration:5.0];
        else
            [ToastView showToastInParentView:self.view withText:applyCouponCollection[@"message"] withStatus:@"success" withDuaration:5.0];
        isAlertVisible = 0;
        whichApiDataToprocess = @"";
        finalData = [[NSMutableString alloc] initWithString:@""];
        imageCache = [[NSCache alloc] init];
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 4;
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:@"00CBDF"];
        preferences = [NSUserDefaults standardUserDefaults];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
    else
        if([whichApiDataToprocess isEqualToString:@"emptyCart"]){
            emptyCartCollection = collection;
            if([emptyCartCollection[@"status"] boolValue]){
                [self viewDidAppear:TRUE];
                // [_emptyView setValue:@"NO" forKeyPath:@"hidden"];
                //                    [_cartMainView setValue:@"YES" forKeyPath:@"hidden"];
                //                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                //                        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:nil];
                //                    else
                //                        [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:nil];
            }
            else
                [ToastView showToastInParentView:self.view withText:[globalObjectCart.languageBundle localizedStringForKey:@"somethingWrong" value:@"" table:nil] withStatus:@"error" withDuaration:5.0];
            [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
        }
        else
            if([whichApiDataToprocess isEqualToString:@"removeItem"]){
                removeItemCollection = collection;
                [_itemBlockToRemove removeFromSuperview];
                for(UIView *subViews in _itemContainer.subviews){
                    if(subViews.tag > _itemBlockToRemove.tag){
                        CGRect newFrame = subViews.frame;
                        newFrame.origin.y = newFrame.origin.y - (_itemBlockToRemove.frame.size.height)-10;
                        subViews.frame = newFrame;
                    }
                }
                _itemContainerHeightConstraint.constant = _itemContainerHeightConstraint.constant - (_itemBlockToRemove.frame.size.height)-10;
                float prevHeightOfSummaryView = _summaryContainerHeightConstraint.constant;
                for(UIView *subViews in _summaryContainer.subviews)
                    [subViews removeFromSuperview];
                float summaryY = 5;
                if([removeItemCollection objectForKey:@"subtotal"]){
                    UILabel *preSubTotal = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                    [preSubTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [preSubTotal setBackgroundColor:[UIColor clearColor]];
                    [preSubTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [preSubTotal setText:removeItemCollection[@"subtotal"][@"title"]];
                    [_summaryContainer addSubview:preSubTotal];
                    UILabel *subTotal = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                    [subTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [subTotal setBackgroundColor:[UIColor clearColor]];
                    [subTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [subTotal setText:removeItemCollection[@"subtotal"][@"value"]];
                    subTotal.textAlignment = NSTextAlignmentRight;
                    [_summaryContainer addSubview:subTotal];
                    summaryY += 30;
                }
                if([removeItemCollection objectForKey:@"discount"]){
                    UILabel *preDiscount = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                    [preDiscount setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [preDiscount setBackgroundColor:[UIColor clearColor]];
                    [preDiscount setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [preDiscount setText:removeItemCollection[@"discount"][@"title"]];
                    [_summaryContainer addSubview:preDiscount];
                    UILabel *discount = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                    [discount setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [discount setBackgroundColor:[UIColor clearColor]];
                    [discount setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [discount setText:removeItemCollection[@"discount"][@"value"]];
                    discount.textAlignment = NSTextAlignmentRight;
                    [_summaryContainer addSubview:discount];
                    summaryY += 30;
                }
                if([removeItemCollection objectForKey:@"tax"]){
                    UILabel *preTax = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                    [preTax setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [preTax setBackgroundColor:[UIColor clearColor]];
                    [preTax setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [preTax setText:removeItemCollection[@"tax"][@"title"]];
                    [_summaryContainer addSubview:preTax];
                    UILabel *tax = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                    [tax setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [tax setBackgroundColor:[UIColor clearColor]];
                    [tax setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [tax setText:removeItemCollection[@"tax"][@"value"]];
                    tax.textAlignment = NSTextAlignmentRight;
                    [_summaryContainer addSubview:tax];
                    summaryY += 30;
                }
                if([removeItemCollection objectForKey:@"shipping"]){
                    UILabel *preShipping = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                    [preShipping setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [preShipping setBackgroundColor:[UIColor clearColor]];
                    [preShipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [preShipping setText:removeItemCollection[@"shipping"][@"title"]];
                    [_summaryContainer addSubview:preShipping];
                    UILabel *shipping = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                    [shipping setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [shipping setBackgroundColor:[UIColor clearColor]];
                    [shipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                    [shipping setText:removeItemCollection[@"shipping"][@"value"]];
                    shipping.textAlignment = NSTextAlignmentRight;
                    [_summaryContainer addSubview:shipping];
                    summaryY += 30;
                }
                if([removeItemCollection objectForKey:@"grandtotal"]){
                    UILabel *preShipping = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 30)];
                    [preShipping setTextColor:[GlobalData colorWithHexString:@"000000"]];
                    [preShipping setBackgroundColor:[UIColor clearColor]];
                    [preShipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:23.0f]];
                    [preShipping setText:removeItemCollection[@"grandtotal"][@"title"]];
                    [_summaryContainer addSubview:preShipping];
                    UILabel *shipping = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 30)];
                    [shipping setTextColor:[GlobalData colorWithHexString:@"000000"]];
                    [shipping setBackgroundColor:[UIColor clearColor]];
                    [shipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:23.0f]];
                    [shipping setText:removeItemCollection[@"grandtotal"][@"value"]];
                    shipping.textAlignment = NSTextAlignmentRight;
                    [_summaryContainer addSubview:shipping];
                    summaryY += 35;
                }
                _summaryContainerHeightConstraint.constant = summaryY;
                if([removeItemCollection[@"itemCount"] boolValue]){
                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", removeItemCollection[@"itemCount"]]];
                    else
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", removeItemCollection[@"itemCount"]]];
                }
                else{
                    [_emptyView setValue:@"NO" forKeyPath:@"hidden"];
                    [_cartMainView setValue:@"YES" forKeyPath:@"hidden"];
                    [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:nil];
                }
                [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
            }
            else{
                mainCollection = collection;
                if([mainCollection[@"cartCount"] boolValue]){
                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%@", mainCollection[@"cartCount"]]];
                    else
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:[NSString stringWithFormat:@"%@", mainCollection[@"cartCount"]]];
                    [_emptyView setValue:@"YES" forKeyPath:@"hidden"];
                    [_cartMainView setValue:@"NO" forKeyPath:@"hidden"];
                    
                    [_itemContainer setBackgroundColor:[UIColor whiteColor]];
                    for(UIView *subViews in _itemContainer.subviews)
                        [subViews removeFromSuperview];
                    float mainContainerHeight = 0;
                    float x = 0, y = 0;
                    for(int i=0; i<[mainCollection[@"items"] count]; i++) {
                        NSDictionary *cartItemDict = [mainCollection[@"items"] objectAtIndex:i];
                        float boxHeight = (SCREEN_WIDTH/2.5)+10;
                        float heightCalculation = 0;
                        if([cartItemDict objectForKey:@"options"]) {
                            for(int j=0; j<[cartItemDict[@"options"] count]; j++) {
                                NSDictionary *customOptions = [cartItemDict[@"options"] objectAtIndex:j];
                                heightCalculation += 30;
                                if([customOptions[@"value"] isKindOfClass:[NSArray class]]){
                                    for(int k=0; k<[customOptions[@"value"] count]; k++) {
                                        if(k < [customOptions[@"value"] count]-1)
                                            heightCalculation += 25;
                                    }
                                }
                                heightCalculation += 25;
                            }
                        }
                        heightCalculation += 161;
                        if(heightCalculation > boxHeight)
                            boxHeight = heightCalculation +50;
                        
                        UIView *itemBlock = [[UIView alloc]initWithFrame:CGRectMake(x, y, _itemContainer.frame.size.width, boxHeight)];
                        [itemBlock setBackgroundColor:[UIColor whiteColor]];
                        itemBlock.layer.cornerRadius = 2;
                        itemBlock.layer.shadowOffset = CGSizeMake(0, 0);
                        itemBlock.layer.shadowRadius = 3;
                        itemBlock.layer.shadowOpacity = 0.5;
                        itemBlock.tag = (i+1)*1000;
                        float internalX = 5;
                        float internalY = 5;
                        
                        UIImageView *productImage = [[UIImageView alloc] initWithFrame:CGRectMake(internalX, internalY, SCREEN_WIDTH/2.5, SCREEN_WIDTH/2.5)];
                        productImage.userInteractionEnabled = YES;
                        productImage.tag = i;
                        UIImage *image = [imageCache objectForKey:cartItemDict[@"image"]];
                        productImage.image = [UIImage imageNamed:@"ic_placeholder.png"];
                        if(image)
                            productImage.image = image;
                        else{
                            [_queue addOperationWithBlock:^{
                                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cartItemDict[@"image"]]];
                                UIImage *image = [UIImage imageWithData: imageData];
                                if(image){
                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                        productImage.image = image;
                                    }];
                                    [imageCache setObject:image forKey:cartItemDict[@"image"]];
                                }
                            }];
                        }
                        UITapGestureRecognizer *viewProductGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewProduct:)];
                        viewProductGesture.numberOfTapsRequired = 1;
                        [productImage addGestureRecognizer:viewProductGesture];
                        [itemBlock addSubview:productImage];
                        float widthAfterProductImage = _itemContainer.frame.size.width - ((SCREEN_WIDTH/2.5)+15);
                        UILabel *productName = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10, internalY, widthAfterProductImage-25, 25)];
                        [productName setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [productName setBackgroundColor:[UIColor clearColor]];
                        [productName setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [productName setText:cartItemDict[@"name"]];
                        [itemBlock addSubview:productName];
                        
                        if([cartItemDict objectForKey:@"options"]) {
                            internalY += 30;
                            for(int j=0; j<[cartItemDict[@"options"] count]; j++) {
                                NSDictionary *customOptions = [cartItemDict[@"options"] objectAtIndex:j];
                                UILabel *optionLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+15, internalY, widthAfterProductImage-5, 25)];
                                [optionLabel setTextColor:[GlobalData colorWithHexString:@"4A4848"]];
                                [optionLabel setBackgroundColor:[UIColor clearColor]];
                                [optionLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                                [optionLabel setText:[NSString stringWithFormat:@"%@ : ", customOptions[@"label"]]];
                                [itemBlock addSubview:optionLabel];
                                internalY += 30;
                                if([customOptions[@"value"] isKindOfClass:[NSArray class]]){
                                    for(int k=0; k<[customOptions[@"value"] count]; k++) {
                                        UILabel *optionValue = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+20, internalY, widthAfterProductImage-10, 20)];
                                        [optionValue setTextColor:[GlobalData colorWithHexString:@"767373"]];
                                        [optionValue setBackgroundColor:[UIColor clearColor]];
                                        [optionValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]];
                                        [optionValue setText:[NSString stringWithFormat:@"%@", [customOptions[@"value"] objectAtIndex:k]]];
                                        [itemBlock addSubview:optionValue];
                                        if(k < [customOptions[@"value"] count]-1)
                                            internalY += 25;
                                    }
                                }
                                else{
                                    UILabel *optionValue = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+20, internalY, widthAfterProductImage-10, 20)];
                                    [optionValue setTextColor:[GlobalData colorWithHexString:@"767373"]];
                                    [optionValue setBackgroundColor:[UIColor clearColor]];
                                    [optionValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:16.0f]];
                                    [optionValue setText:[NSString stringWithFormat:@"%@", customOptions[@"value"]]];
                                    [itemBlock addSubview:optionValue];
                                }
                                internalY += 25;
                            }
                        }
                        else
                            internalY += 30;
                        
                        UILabel *SKU = [[UILabel alloc]initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10, internalY, 45, 25)];
                        [SKU setTextColor:[GlobalData colorWithHexString:@"767373"]];
                        [SKU setBackgroundColor:[UIColor clearColor]];
                        [SKU setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [SKU setText:@"SKU : "];
                        [itemBlock addSubview:SKU];
                        
                        UILabel *skuValue = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+55, internalY, widthAfterProductImage-45, 25)];
                        [skuValue setTextColor:[GlobalData colorWithHexString:@"767373"]];
                        [skuValue setBackgroundColor:[UIColor clearColor]];
                        [skuValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [skuValue setText:cartItemDict[@"sku"]];
                        [itemBlock addSubview:skuValue];
                        
                        internalY += 30;
                        UILabel *preString = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10, internalY, 55, 25)];
                        [preString setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preString setBackgroundColor:[UIColor clearColor]];
                        [preString setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [preString setText:[globalObjectCart.languageBundle localizedStringForKey:@"price" value:@"" table:nil]];
                        [itemBlock addSubview:preString];
                        
                        UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+65, internalY, widthAfterProductImage-55, 25)];
                        [price setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [price setBackgroundColor:[UIColor clearColor]];
                        [price setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [price setText:cartItemDict[@"price"]];
                        [itemBlock addSubview:price];
                        
                        internalY += 30;
                        UITextField* qtyField = [[UITextField alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10, internalY, 80, 25)];
                        qtyField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
                        qtyField.textColor = [UIColor blackColor];
                        [qtyField setKeyboardType:UIKeyboardTypePhonePad];
                        qtyField.text = [NSString stringWithFormat:@"%@", cartItemDict[@"qty"]];
                        qtyField.textAlignment = NSTextAlignmentCenter;
                        qtyField.borderStyle = UITextBorderStyleRoundedRect;
                        [itemBlock addSubview:qtyField];
                        
                        internalY += 30;
                        
                        UIView *hr = [[UIView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10, internalY, widthAfterProductImage, 1)];
                        [hr setBackgroundColor:[GlobalData colorWithHexString:@"C8C4C4"]];
                        [itemBlock addSubview:hr];
                        
                        internalY += 6;
                        UILabel *preSubTotal = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10, internalY, 90, 25)];
                        [preSubTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preSubTotal setBackgroundColor:[UIColor clearColor]];
                        [preSubTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [preSubTotal setText:[globalObjectCart.languageBundle localizedStringForKey:@"subtotal" value:@"" table:nil]];
                        [itemBlock addSubview:preSubTotal];
                        
                        UILabel *subTotal = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+100, internalY, widthAfterProductImage-90, 25)];
                        [subTotal setTextColor:[GlobalData colorWithHexString:@"268ED7"]];
                        [subTotal setBackgroundColor:[UIColor clearColor]];
                        [subTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                        [subTotal setText:cartItemDict[@"subTotal"]];
                        [itemBlock addSubview:subTotal];
                        
                        internalY += 30;
                        
                        if( [preferences objectForKey:@"customerId"] != NULL){
                            
                            UILabel *wishListfromCart = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH/2.5)+10 , internalY, widthAfterProductImage, 20)];
                            [wishListfromCart setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
                            [wishListfromCart setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
                            [wishListfromCart setText:@"Move to wishlist"];
                            [wishListfromCart setUserInteractionEnabled:YES];
                            wishListfromCart.tag = i;
                            [itemBlock addSubview:wishListfromCart];
                            internalY +=30;
                            UITapGestureRecognizer *movetoWishListTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(wishlistFromCart:)];
                            movetoWishListTap.numberOfTapsRequired = 1;
                            [wishListfromCart addGestureRecognizer:movetoWishListTap];
                        }
                        
                        
                        UIImageView *removeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_itemContainer.frame.size.width-31 ,5, 26, 26)];
                        removeIcon.image = [UIImage imageNamed:@"ic_remove.png"];
                        removeIcon.tag = i;
                        removeIcon.userInteractionEnabled = YES;
                        [itemBlock addSubview:removeIcon];
                        UITapGestureRecognizer *removeItemGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeItem:)];
                        removeItemGesture.numberOfTapsRequired = 1;
                        [removeIcon addGestureRecognizer:removeItemGesture];
                        
                        
                        
                        
                        if(i < [mainCollection[@"items"] count]-1)
                            y += boxHeight+10;
                        else
                            y += boxHeight;
                        [_itemContainer addSubview:itemBlock];
                    }
                    _itemContainerHeightConstraint.constant = y;
                    mainContainerHeight += y+8;
                    
                    for(UIView *subViews in _actionButtonContainer.subviews)
                        [subViews removeFromSuperview];
                    [_actionButtonContainer setBackgroundColor:[UIColor whiteColor]];
                    _actionButtonContainer.layer.cornerRadius = 2;
                    _actionButtonContainer.layer.shadowOffset = CGSizeMake(0, 0);
                    _actionButtonContainer.layer.shadowRadius = 3;
                    _actionButtonContainer.layer.shadowOpacity = 0.5;
                    UILabel *emptyCartBtn = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, (_actionButtonContainer.frame.size.width/2)-10, 50)];
                    [emptyCartBtn setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [emptyCartBtn setTextColor: [UIColor whiteColor]];
                    [emptyCartBtn setText:[globalObjectCart.languageBundle localizedStringForKey:@"emptyCart" value:@"" table:nil]];
                    emptyCartBtn.userInteractionEnabled = YES;
                    emptyCartBtn.textAlignment = NSTextAlignmentCenter;
                    [_actionButtonContainer addSubview:emptyCartBtn];
                    UITapGestureRecognizer *emptyCartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emptyCart:)];
                    emptyCartGesture.numberOfTapsRequired = 1;
                    [emptyCartBtn addGestureRecognizer:emptyCartGesture];
                    
                    UILabel *updateCartLabel = [[UILabel alloc] initWithFrame:CGRectMake((_actionButtonContainer.frame.size.width/2), 5, (_actionButtonContainer.frame.size.width/2)-5, 50)];
                    [updateCartLabel setBackgroundColor:[GlobalData colorWithHexString:@"268ED7"]];
                    [updateCartLabel setTextColor: [UIColor whiteColor]];
                    [updateCartLabel setText:[globalObjectCart.languageBundle localizedStringForKey:@"updateCart" value:@"" table:nil]];
                    updateCartLabel.userInteractionEnabled = YES;
                    updateCartLabel.textAlignment = NSTextAlignmentCenter;
                    [_actionButtonContainer addSubview:updateCartLabel];
                    UITapGestureRecognizer *updateCartGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateCart:)];
                    updateCartGesture.numberOfTapsRequired = 1;
                    [updateCartLabel addGestureRecognizer:updateCartGesture];
                    _actionButtonContainerHeightConstraint.constant = 60;
                    mainContainerHeight += 68;
                    
                    for(UIView *subViews in _applyCouponContainer.subviews)
                        [subViews removeFromSuperview];
                    [_applyCouponContainer setBackgroundColor:[UIColor whiteColor]];
                    _applyCouponContainer.layer.cornerRadius = 2;
                    _applyCouponContainer.layer.shadowOffset = CGSizeMake(0, 0);
                    _applyCouponContainer.layer.shadowRadius = 3;
                    _applyCouponContainer.layer.shadowOpacity = 0.5;
                    UILabel *couponLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, _applyCouponContainer.frame.size.width-10, 25)];
                    [couponLabel setTextColor:[GlobalData colorWithHexString:@"555555"]];
                    [couponLabel setBackgroundColor:[UIColor clearColor]];
                    [couponLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
                    [couponLabel setText:[globalObjectCart.languageBundle localizedStringForKey:@"discount" value:@"" table:nil]];
                    [_applyCouponContainer addSubview:couponLabel];
                    UITextField *couponCodeField = [[UITextField alloc] initWithFrame:CGRectMake(5, 35, ((_applyCouponContainer.frame.size.width-10)/2)-5, 50)];
                    couponCodeField.font = [UIFont fontWithName:@"Trebuchet MS" size:18.0f];
                    couponCodeField.textColor = [UIColor blackColor];
                    [couponCodeField setPlaceholder:@"Enter Coupon Code"];
                    if(mainCollection[@"couponCode"] != [NSNull null])
                        couponCodeField.text = [NSString stringWithFormat:@"%@", mainCollection[@"couponCode"]];
                    couponCodeField.textAlignment = NSTextAlignmentLeft;
                    couponCodeField.borderStyle = UITextBorderStyleRoundedRect;
                    [_applyCouponContainer addSubview:couponCodeField];
                    if([mainCollection[@"couponCode"] isEqual:[NSNull null]]){
                        UILabel *applyCouponBtn = [[UILabel alloc] initWithFrame:CGRectMake(((_applyCouponContainer.frame.size.width-10)/2)+5, 35, (_applyCouponContainer.frame.size.width-10)/2, 50)];
                        [applyCouponBtn setBackgroundColor: [GlobalData colorWithHexString:@"268ED7"]];
                        [applyCouponBtn setTextColor: [UIColor whiteColor]];
                        [applyCouponBtn setText:[globalObjectCart.languageBundle localizedStringForKey:@"apply" value:@"" table:nil]];
                        applyCouponBtn.userInteractionEnabled = YES;
                        applyCouponBtn.textAlignment = NSTextAlignmentCenter;
                        [_applyCouponContainer addSubview:applyCouponBtn];
                        UITapGestureRecognizer *applyCouponGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(applyCoupon:)];
                        applyCouponGesture.numberOfTapsRequired = 1;
                        [applyCouponBtn addGestureRecognizer:applyCouponGesture];
                    }
                    else{
                        UILabel *applyCouponBtn = [[UILabel alloc] initWithFrame:CGRectMake(((_applyCouponContainer.frame.size.width-10)/2)+5, 35, (((_applyCouponContainer.frame.size.width-10)/2)/2)-5, 50)];
                        [applyCouponBtn setBackgroundColor: [GlobalData colorWithHexString:@"268ED7"]];
                        [applyCouponBtn setTextColor: [UIColor whiteColor]];
                        [applyCouponBtn setText:[globalObjectCart.languageBundle localizedStringForKey:@"apply" value:@"" table:nil]];
                        applyCouponBtn.userInteractionEnabled = YES;
                        applyCouponBtn.textAlignment = NSTextAlignmentCenter;
                        [_applyCouponContainer addSubview:applyCouponBtn];
                        UITapGestureRecognizer *applyCouponGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(applyCoupon:)];
                        applyCouponGesture.numberOfTapsRequired = 1;
                        [applyCouponBtn addGestureRecognizer:applyCouponGesture];
                        UILabel *cancelCouponBtn = [[UILabel alloc] initWithFrame:CGRectMake((((_applyCouponContainer.frame.size.width-10)/2)+5)+((_applyCouponContainer.frame.size.width-10)/2)/2, 35, ((_applyCouponContainer.frame.size.width-10)/2)/2, 50)];
                        [cancelCouponBtn setBackgroundColor: [GlobalData colorWithHexString:@"268ED7"]];
                        [cancelCouponBtn setTextColor: [UIColor whiteColor]];
                        [cancelCouponBtn setText:[globalObjectCart.languageBundle localizedStringForKey:@"cancel" value:@"" table:nil]];
                        cancelCouponBtn.userInteractionEnabled = YES;
                        cancelCouponBtn.textAlignment = NSTextAlignmentCenter;
                        [_applyCouponContainer addSubview:cancelCouponBtn];
                        UITapGestureRecognizer *cancelCouponGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelCoupon:)];
                        cancelCouponGesture.numberOfTapsRequired = 1;
                        [cancelCouponBtn addGestureRecognizer:cancelCouponGesture];
                    }
                    _applyCouponContainerHeightConstraint.constant = 90;
                    mainContainerHeight += 98;
                    
                    for(UIView *subViews in _summaryContainer.subviews)
                        [subViews removeFromSuperview];
                    [_summaryContainer setBackgroundColor:[UIColor whiteColor]];
                    _summaryContainer.layer.cornerRadius = 2;
                    _summaryContainer.layer.shadowOffset = CGSizeMake(0, 0);
                    _summaryContainer.layer.shadowRadius = 3;
                    _summaryContainer.layer.shadowOpacity = 0.5;
                    float summaryY = 5;
                    if([mainCollection objectForKey:@"subtotal"]){
                        UILabel *preSubTotal = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                        [preSubTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preSubTotal setBackgroundColor:[UIColor clearColor]];
                        [preSubTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preSubTotal setText:mainCollection[@"subtotal"][@"title"]];
                        [_summaryContainer addSubview:preSubTotal];
                        UILabel *subTotal = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                        [subTotal setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [subTotal setBackgroundColor:[UIColor clearColor]];
                        [subTotal setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [subTotal setText:mainCollection[@"subtotal"][@"value"]];
                        subTotal.textAlignment = NSTextAlignmentRight;
                        [_summaryContainer addSubview:subTotal];
                        summaryY += 30;
                    }
                    if([mainCollection objectForKey:@"discount"]){
                        UILabel *preDiscount = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                        [preDiscount setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preDiscount setBackgroundColor:[UIColor clearColor]];
                        [preDiscount setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preDiscount setText:mainCollection[@"discount"][@"title"]];
                        [_summaryContainer addSubview:preDiscount];
                        UILabel *discount = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                        [discount setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [discount setBackgroundColor:[UIColor clearColor]];
                        [discount setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [discount setText:mainCollection[@"discount"][@"value"]];
                        discount.textAlignment = NSTextAlignmentRight;
                        [_summaryContainer addSubview:discount];
                        summaryY += 30;
                    }
                    if([mainCollection objectForKey:@"tax"]){
                        UILabel *preTax = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                        [preTax setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preTax setBackgroundColor:[UIColor clearColor]];
                        [preTax setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preTax setText:mainCollection[@"tax"][@"title"]];
                        [_summaryContainer addSubview:preTax];
                        UILabel *tax = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                        [tax setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [tax setBackgroundColor:[UIColor clearColor]];
                        [tax setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [tax setText:mainCollection[@"tax"][@"value"]];
                        tax.textAlignment = NSTextAlignmentRight;
                        [_summaryContainer addSubview:tax];
                        summaryY += 30;
                    }
                    if([mainCollection objectForKey:@"shipping"]){
                        UILabel *preShipping = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 25)];
                        [preShipping setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [preShipping setBackgroundColor:[UIColor clearColor]];
                        [preShipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [preShipping setText:mainCollection[@"shipping"][@"title"]];
                        [_summaryContainer addSubview:preShipping];
                        UILabel *shipping = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 25)];
                        [shipping setTextColor:[GlobalData colorWithHexString:@"555555"]];
                        [shipping setBackgroundColor:[UIColor clearColor]];
                        [shipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:20.0f]];
                        [shipping setText:mainCollection[@"shipping"][@"value"]];
                        shipping.textAlignment = NSTextAlignmentRight;
                        [_summaryContainer addSubview:shipping];
                        summaryY += 30;
                    }
                    if([mainCollection objectForKey:@"grandtotal"]){
                        UILabel *preShipping = [[UILabel alloc] initWithFrame:CGRectMake(5, summaryY, (((_summaryContainer.frame.size.width-10)/3)*2), 30)];
                        [preShipping setTextColor:[GlobalData colorWithHexString:@"000000"]];
                        [preShipping setBackgroundColor:[UIColor clearColor]];
                        [preShipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:23.0f]];
                        [preShipping setText:mainCollection[@"grandtotal"][@"title"]];
                        [_summaryContainer addSubview:preShipping];
                        UILabel *shipping = [[UILabel alloc] initWithFrame:CGRectMake((((_summaryContainer.frame.size.width-10)/3)*2)+5, summaryY, ((_summaryContainer.frame.size.width-10)/3), 30)];
                        [shipping setTextColor:[GlobalData colorWithHexString:@"000000"]];
                        [shipping setBackgroundColor:[UIColor clearColor]];
                        [shipping setFont:[UIFont fontWithName:@"Trebuchet MS" size:23.0f]];
                        [shipping setText:mainCollection[@"grandtotal"][@"value"]];
                        shipping.textAlignment = NSTextAlignmentRight;
                        [_summaryContainer addSubview:shipping];
                        summaryY += 35;
                    }
                    _summaryContainerHeightConstraint.constant = summaryY;
                    mainContainerHeight += summaryY+20;
                    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
                }
                else{
                    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"])
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:nil];
                    else
                        [[[[[self tabBarController] tabBar] items] objectAtIndex:4] setBadgeValue:nil];
                    [_emptyView setValue:@"NO" forKeyPath:@"hidden"];
                    [_cartMainView setValue:@"YES" forKeyPath:@"hidden"];
                    [self.view.subviews setValue:@"NO" forKeyPath:@"hidden"];
                }
            }
}

-(void)viewProduct:(UITapGestureRecognizer *)recognizer{
    productDictionary = [[NSDictionary alloc] init];
    productDictionary = [mainCollection[@"items"] objectAtIndex:recognizer.view.tag];
    [self performSegueWithIdentifier:@"openProductFromCartSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openProductFromCartSegue"]) {
        CatalogProduct *destViewController = segue.destinationViewController;
        destViewController.productId = productDictionary[@"productId"];
        destViewController.productName = productDictionary[@"name"];
        destViewController.productType = productDictionary[@"typeId"];
    }
}

-(void)removeItem:(UITapGestureRecognizer *)recognizer{
    UIAlertController * confirmDialogue = [UIAlertController alertControllerWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"pleaseConfirm" value:@"" table:nil] message:[globalObjectCart.languageBundle localizedStringForKey:@"sureRemove" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      whichApiDataToprocess = @"removeItem";
                                                      [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
                                                      int index = (int)recognizer.view.tag;
                                                      NSDictionary *itemDict = [mainCollection[@"items"] objectAtIndex:index];
                                                      itemIdToRemove = itemDict[@"id"];
                                                      int itemToDelete = (index+1)*1000;
                                                      _itemBlockToRemove = [_itemContainer viewWithTag:itemToDelete];
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [confirmDialogue addAction:okBtn];
    [confirmDialogue addAction:noBtn];
    [self.parentViewController presentViewController:confirmDialogue animated:YES completion:nil];
}

-(void)emptyCart:(UITapGestureRecognizer *)recognizer{
    UIAlertController * confirmDialogue = [UIAlertController alertControllerWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"pleaseConfirm" value:@"" table:nil] message:[globalObjectCart.languageBundle localizedStringForKey:@"sureEmpty" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action){
                                                      whichApiDataToprocess = @"emptyCart";
                                                      [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [confirmDialogue addAction:okBtn];
    [confirmDialogue addAction:noBtn];
    [self.parentViewController presentViewController:confirmDialogue animated:YES completion:nil];
}

-(void)updateCart:(UITapGestureRecognizer *)recognizer{
    toUpdateItemQtys = [[NSMutableArray alloc] init];
    toUpdateItemIds = [[NSMutableArray alloc] init];
    int i = 0;
    for(UIView *subViews in _itemContainer.subviews){
        UITextField *qtyField = (UITextField*)[subViews.subviews objectAtIndex:[subViews.subviews count]-6];
        if([qtyField.text boolValue])
            [toUpdateItemQtys addObject:qtyField.text];
        else
            [toUpdateItemQtys addObject:@"1"];
        NSDictionary *cartItemDict = [mainCollection[@"items"] objectAtIndex:i];
        [toUpdateItemIds addObject:cartItemDict[@"id"]];
        i++;
    }
    whichApiDataToprocess = @"updateCart";
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)applyCoupon:(UITapGestureRecognizer *)recognizer{
    UITextField *couponField = (UITextField*)[_applyCouponContainer.subviews objectAtIndex:1];
    enteredCouponCode = couponField.text;
    if([enteredCouponCode isEqual: @""]){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:[globalObjectCart.languageBundle localizedStringForKey:@"enterCoupon" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    else{
        whichApiDataToprocess = @"applyCoupon";
        [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)cancelCoupon:(UITapGestureRecognizer *)recognizer{
    UITextField *couponField = (UITextField*)[_applyCouponContainer.subviews objectAtIndex:1];
    enteredCouponCode = couponField.text;
    if([enteredCouponCode isEqual: @""]){
        UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"error" value:@"" table:nil] message:[globalObjectCart.languageBundle localizedStringForKey:@"nothingCancel" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectCart.languageBundle localizedStringForKey:@"ok" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
        [errorAlert addAction:noBtn];
        [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
    }
    else{
        whichApiDataToprocess = @"cancelCoupon";
        [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }
}

-(void)wishlistFromCart:(UITapGestureRecognizer *)recognizer{
    NSDictionary *cartItemDict = [mainCollection[@"items"] objectAtIndex:recognizer.view.tag];
    itemId = cartItemDict[@"id"];
    whichApiDataToprocess = @"wishlistfromcartaction";
    [self.view.subviews setValue:@"YES" forKeyPath:@"hidden"];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
    
}



- (IBAction)proceedToCheckout:(id)sender {
    [self performSegueWithIdentifier:@"checkoutSegue" sender:self];
}

@end