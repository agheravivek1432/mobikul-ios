//
//  Newsletter.m
//  Mobikul
//
//  Created by Ratnesh on 09/03/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "Newsletter.h"
#import "GlobalData.h"
#import "ToastView.h"
GlobalData *globalObjectNewsLetter;

@implementation Newsletter

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectNewsLetter=[[GlobalData alloc] init];
    globalObjectNewsLetter.delegate=self;
    [globalObjectNewsLetter language];
    isAlertVisible = 0;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    preferences = [NSUserDefaults standardUserDefaults];
    _subscribe.backgroundColor = [GlobalData colorWithHexString:GLOBAL_COLOR];
    self.navigationItem.title = [globalObjectNewsLetter.languageBundle localizedStringForKey:@"newsletterTitle" value:@"" table:nil];
    self.navigationController.title = [globalObjectNewsLetter.languageBundle localizedStringForKey:@"newsletterTitle" value:@"" table:nil];
    [_subscribe setTitle: [globalObjectNewsLetter.languageBundle localizedStringForKey:@"subscribe" value:@"" table:nil] forState: UIControlStateNormal];
    _subscriberEmailId.placeholder = [globalObjectNewsLetter.languageBundle localizedStringForKey:@"yourEmail" value:@"" table:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    _subscriberEmailId.layer.cornerRadius = 2;
    _subscribeButton.layer.cornerRadius = 4;
    currentWindow = [UIApplication sharedApplication].keyWindow;
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)subscribeClicked:(id)sender {
    @try{
        int isValid = 1;
        NSString *errorMessage;
        subscriberEmailId = _subscriberEmailId.text;
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if(![emailTest evaluateWithObject:subscriberEmailId]){
            isValid = 0;
            errorMessage = [globalObjectNewsLetter.languageBundle localizedStringForKey:@"validEmail" value:@"" table:nil];
        }
        if(isValid == 0){
            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"validationError" value:@"" table:nil] message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
            [errorAlert addAction:noBtn];
            [self.parentViewController presentViewController:errorAlert animated:YES completion:nil];
        }
        else{
            NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
            if(savedSessionId == nil)
                [self loginRequest];
            else
                [self callingHttppApi];
        }
    }
    @catch (NSException *e) {
        //NSLog(@"catching %@ reason %@", [e name], [e reason]);
    }
}

-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectNewsLetter callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action){
                                                      NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
                                                      if(savedSessionId == nil)
                                                          [self loginRequest];
                                                      else
                                                          [self callingHttppApi];
                                                  }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}
-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectNewsLetter.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    NSMutableString *post = [NSMutableString string];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    [post appendFormat:@"websiteId=%@&", websiteId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    [post appendFormat:@"storeId=%@&", storeId];
    [post appendFormat:@"email=%@&", subscriberEmailId];
    NSString *isLoggedIn = [preferences objectForKey:@"isLoggedIn"];
    [post appendFormat:@"isLoggedIn=%@&", isLoggedIn];
    NSString *customerId = [preferences objectForKey:@"customerId"];
    if(customerId!=NULL)
        [post appendFormat:@"customerId=%@", customerId];
    [globalObjectNewsLetter callHTTPPostMethod:post api:@"mobikulhttp/extra/subscribetoNewsletter" signal:@"HttpPostMetod"];
    globalObjectNewsLetter.delegate = self;
}

-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
        
    }
    NSInteger errorCode = [collection[@"errorCode"] integerValue];
    if(errorCode == 0)
        [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"success" withDuaration:5.0];
    else
        [ToastView showToastInParentView:self.view withText:collection[@"message"] withStatus:@"error" withDuaration:5.0];
    [_subscriberEmailId resignFirstResponder];
}



@end