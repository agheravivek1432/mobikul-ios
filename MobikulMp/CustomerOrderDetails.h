//
//  CustomerOrderDetails.h
//  MobikulMp
//
//  Created by kunal prasad on 15/07/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerOrderDetails : UIViewController{
    NSUserDefaults *preferences;
    id collection;
    UIWindow *currentWindow;
    NSInteger isAlertVisible;
    
}
@property (nonatomic) NSString *incrementId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraints;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@end
