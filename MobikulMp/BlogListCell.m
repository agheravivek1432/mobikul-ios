//
//  BlogListCell.m
//  MobikulMp
//
//  Created by Apple on 12/10/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "BlogListCell.h"

@implementation BlogListCell

@synthesize dictBlog;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reloadBlogListCell
{
    lblBlogName.text = [dictBlog valueForKey:@"subject"];
    
    lblNameAndDate.text = [NSString stringWithFormat:@"By %@ at %@",[dictBlog valueForKey:@"cname"],[dictBlog valueForKey:@"created_at"]];
    
    NSString * htmlString = [dictBlog valueForKey:@"content"];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    lblContent.attributedText = attrStr;
    
    lblTag.text = [dictBlog valueForKey:@"tag"];
}
@end
