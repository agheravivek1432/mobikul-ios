//
//  MarketPlace.m
//  MobikulMp
//
//  Created by kunal prasad on 14/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "MarketPlace.h"
#import "GlobalData.h"
#import "CatalogProduct.h"
#import "SellerProducts.h"
#import "SellerProfileData.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)



@interface MarketPlace ()

@end

GlobalData *globalObjectMarketPlace;

@implementation MarketPlace

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectMarketPlace = [[GlobalData alloc] init];
    globalObjectMarketPlace.delegate = self;
    [globalObjectMarketPlace language];
    isAlertVisible = 0;
    imageCache = [[NSCache alloc] init];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
    
}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectMarketPlace callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectMarketPlace.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *websiteId = [preferences objectForKey:@"websiteId"];
    if(websiteId == nil){
        [post appendFormat:@"websiteId=%@&", websiteId];
        [preferences setObject:DEFAULT_WEBSITE_ID forKey:@"websiteId"];
        [preferences synchronize];
    }
    else
    [post appendFormat:@"storeId=%@&", storeId];
    NSString *screenWidth = [NSString stringWithFormat:@"%f", SCREEN_WIDTH];
    [post appendFormat:@"width=%@", screenWidth];
    [globalObjectMarketPlace callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/getlandingpageData" signal:@"HttpPostMetod"];
    globalObjectMarketPlace.delegate = self;
}


-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
     UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectMarketPlace.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectMarketPlace.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectMarketPlace.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectMarketPlace.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    
    mainCollection = collection;
    float mainContainerY = 0;
    
    // banner Image
    
    if(mainCollection[@"bannerImage"]){
    
    UIView *bannerContainer = [[UIView alloc] initWithFrame:CGRectMake(5, 5 , _mainView.frame.size.width-10, SCREEN_WIDTH/2)];
    [_mainView addSubview:bannerContainer];

    UIImageView *banner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 , bannerContainer.frame.size.width, bannerContainer.frame.size.height)];
    banner.image = [UIImage imageNamed:@"placeholder_marketplace_home_banner.png"];
    banner.userInteractionEnabled = YES;
    UIImage *image = [imageCache objectForKey:mainCollection[@"bannerImage"]];
    
    if(image)
        banner.image = image;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:mainCollection[@"bannerImage"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        banner.image = image;
        if(image != nil)
        [imageCache setObject:image forKey:mainCollection[@"bannerImage"]];
    }
    [bannerContainer addSubview:banner];
    
    //banner image head text
    
    NSString *bannerHeadText = [mainCollection[@"banner"] objectAtIndex:0];
    UILabel *bannerHead = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, bannerContainer.frame.size.width, 25)];
    [bannerHead setTextColor:[GlobalData colorWithHexString:@"ffffff"]];
    [bannerHead setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [bannerHead setText:bannerHeadText];
    bannerHead.textAlignment = NSTextAlignmentCenter;
    [bannerContainer addSubview:bannerHead];
   
     //banner image description
    
    NSString *bannerDescText = [mainCollection[@"banner"] objectAtIndex:1];
    NSDictionary *reqAttributesforDesc = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};
    CGSize reqStringSizeDesc = [bannerDescText sizeWithAttributes:reqAttributesforDesc];
    CGFloat reqStringHeightDesc = reqStringSizeDesc.height;
    UILabel *bannerDesc = [[UILabel alloc] initWithFrame:CGRectMake(0,(SCREEN_WIDTH-4)/16 + 26, SCREEN_WIDTH, reqStringHeightDesc)];
    [bannerDesc setTextColor:[GlobalData colorWithHexString:@"ffffff"]];
    [bannerDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    //[bannerDesc setBackgroundColor:[UIColor blueColor]];
    [bannerDesc setText:bannerDescText];
    bannerDesc.lineBreakMode = NSLineBreakByWordWrapping;
    bannerDesc.numberOfLines = 0;
    bannerDesc.textAlignment = NSTextAlignmentCenter;
    [bannerContainer addSubview:bannerDesc];
    
     NSInteger internnalHeight = (SCREEN_WIDTH-4)/16 + 26 + reqStringHeightDesc;
    
    // banner image attached button
    
    internnalHeight +=10;
    bannerImageView = [[UIView alloc]initWithFrame:CGRectMake(0,internnalHeight , bannerContainer.frame.size.width, 30)];
    bannerImageView.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    bannerImageView.layer.borderWidth = 0.0f;
    [bannerContainer addSubview:bannerImageView ];
    
    
    NSDictionary *reqAttributesforButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};
    CGSize reqStringSizeforButtonText = [bannerHeadText sizeWithAttributes:reqAttributesforButtonText];
    CGFloat reqStringWidthButtonText = reqStringSizeforButtonText.width;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(buttonHandlerForBannerImage) forControlEvents:UIControlEventAllEvents];
    [button setTitle:bannerHeadText forState:UIControlStateNormal];
    button.frame = CGRectMake(0,0,reqStringWidthButtonText +20, 40.0);
    [button setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    [button setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"]];
    [button setTitleColor:[GlobalData colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    button.center = CGPointMake(bannerImageView.frame.size.width  / 2,
                                     bannerImageView.frame.size.height / 2);
    
    [bannerImageView addSubview:button];
    mainContainerY+=(SCREEN_WIDTH-4)/2;
    mainContainerY+=10;
    }
    
    //banner title
    if([mainCollection[@"label1"] isEqualToString:@""] == false){
    
    UILabel *requiredLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, mainContainerY+10,SCREEN_WIDTH, 25)];
    [requiredLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
    [requiredLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [requiredLabel setText:mainCollection[@"label1"]];
    requiredLabel.textAlignment = NSTextAlignmentCenter;
    [_mainView addSubview:requiredLabel];
    mainContainerY += 35;
    }
    // processing icons image
    groupIconImageContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY+10, _mainView.frame.size.width-10, 300)];
    groupIconImageContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    groupIconImageContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:groupIconImageContainer];
    NSInteger iconImage = [mainCollection[@"icons"] count];
    NSInteger shiftX = SCREEN_WIDTH/16;
    NSInteger shiftXforLabel = 0;
    
    if(SCREEN_WIDTH >500){
    if(iconImage>0){
    for (int i = 0;i<iconImage ; i++) {
    NSDictionary *iconOption = [mainCollection[@"icons"] objectAtIndex:i];
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX,10,(SCREEN_WIDTH)/8,SCREEN_WIDTH/8)];
    icon.image = [UIImage imageNamed:@"ic_placeholder.png"];
    UIImage *image = [imageCache objectForKey:iconOption[@"image"]];
    if(image)
        icon.image = image;
    else{
        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:iconOption[@"image"]]];
        UIImage *image = [UIImage imageWithData: imageData];
        if(image != nil){
        icon.image = image;
        [imageCache setObject:image forKey:iconOption[@"image"]];}
    }
    UILabel *requiredLabel = [[UILabel alloc] initWithFrame:CGRectMake(shiftXforLabel, 10+SCREEN_WIDTH/8, SCREEN_WIDTH/4, 20)];
    [requiredLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [requiredLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
    [requiredLabel setText:iconOption[@"label"]];
    requiredLabel.adjustsFontSizeToFitWidth = YES;
    requiredLabel.textAlignment = NSTextAlignmentCenter;
    shiftX += (SCREEN_WIDTH)/4;
    shiftXforLabel += (SCREEN_WIDTH)/4;
    [groupIconImageContainer addSubview:icon];
    [groupIconImageContainer addSubview:requiredLabel];
    }
    }
    else{
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, _mainView.frame.size.width-10,SCREEN_WIDTH/8+45)];
    icon.image = [UIImage imageNamed:@"marketplacebanner.png"];
    [groupIconImageContainer addSubview:icon];
    }
    CGRect newFrame = groupIconImageContainer.frame;
    newFrame.size.height = 45+SCREEN_WIDTH/8;
    groupIconImageContainer.frame = newFrame;
    mainContainerY +=SCREEN_WIDTH/8 + 45;
    }
    
    
    if(SCREEN_WIDTH<500){
        shiftX = SCREEN_WIDTH/32;
        shiftXforLabel = SCREEN_WIDTH/32;
        float Y = 10;
        if(iconImage>0){
       for (int i = 0;i<iconImage ; i++) {
        if(i==2){
            Y +=10+14*(SCREEN_WIDTH)/32 +10;
            shiftX = SCREEN_WIDTH/32;
            shiftXforLabel = SCREEN_WIDTH/32;
        }
        
        NSDictionary *iconOption = [mainCollection[@"icons"] objectAtIndex:i];
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX,Y,14*(SCREEN_WIDTH)/32,14*SCREEN_WIDTH/32)];
        icon.image = [UIImage imageNamed:@"ic_placeholder.png"];
        UIImage *image = [imageCache objectForKey:iconOption[@"image"]];
        if(image)
            icon.image = image;
        else{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:iconOption[@"image"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            if(image){
            icon.image = image;
            [imageCache setObject:image forKey:iconOption[@"image"]];
            }
        }
        
        
        UILabel *requiredLabel = [[UILabel alloc] initWithFrame:CGRectMake(shiftXforLabel, Y+14*(SCREEN_WIDTH)/32, 14*(SCREEN_WIDTH)/32, 20)];
        [requiredLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [requiredLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
        [requiredLabel setText:iconOption[@"label"]];
        requiredLabel.adjustsFontSizeToFitWidth = YES;
        requiredLabel.textAlignment = NSTextAlignmentCenter;
        shiftX = 17*SCREEN_WIDTH/32;
        shiftXforLabel =17 *(SCREEN_WIDTH)/32;
        [groupIconImageContainer addSubview:icon];
        [groupIconImageContainer addSubview:requiredLabel];
        
        
    }
    CGRect newFrame = groupIconImageContainer.frame;
    newFrame.size.height = 45+Y+14*(SCREEN_WIDTH)/32;
    groupIconImageContainer.frame = newFrame;
    mainContainerY +=Y+ 14*(SCREEN_WIDTH/32) + 45;
   }
    else{
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, _mainView.frame.size.width-10,SCREEN_WIDTH/8+45)];
        icon.image = [UIImage imageNamed:@"marketplacebanner.png"];
        [groupIconImageContainer addSubview:icon];
    CGRect newFrame = groupIconImageContainer.frame;
    newFrame.size.height = 45+SCREEN_WIDTH/8;
    groupIconImageContainer.frame = newFrame;
    mainContainerY +=SCREEN_WIDTH/8 + 45;
    }
  }
    
    
    
       // sellers head title
    if([mainCollection[@"label2"] isEqualToString:@""] == false ){
    NSDictionary *reqAttributesforSellerHead = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:24.0f]};
    CGSize reqStringSizSeller = [mainCollection[@"label2"] sizeWithAttributes:reqAttributesforSellerHead];
    CGFloat reqStringWidthSeller = reqStringSizSeller.width;
    UILabel *requiredSellerLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH)/2 -(reqStringWidthSeller/2), mainContainerY+20, reqStringWidthSeller, 25)];
    [requiredSellerLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
    [requiredSellerLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [requiredSellerLabel setText:mainCollection[@"label2"]];
    [_mainView addSubview:requiredSellerLabel];
     mainContainerY += 55;
    }else{
        mainContainerY +=20;
    }
    
    // sellers contains
    shiftX = 0;
    float internalY = 10;
    
    if(SCREEN_WIDTH >600){
    
    for(int i=0; i< [mainCollection[@"sellers"] count]; i++){
        sellerGroupContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 300)];
        sellerGroupContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
        sellerGroupContainer.layer.borderWidth = 2.0f;
        sellerGroupContainer.tag = i;
        [_mainView addSubview:sellerGroupContainer];
        
        NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:i];
        
        UIImageView *sellerIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/16,internalY,(SCREEN_WIDTH)/4,SCREEN_WIDTH/4)];
        if(sellerOption[@"pro1id"] != [NSNull null] ){
        sellerIcon1.image = [UIImage imageNamed:@"ic_placeholder.png"];
        UIImage *image = [imageCache objectForKey:sellerOption[@"pro1thumbnail"]];
        if(image)
            sellerIcon1.image = image;
        else{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"pro1thumbnail"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            sellerIcon1.image = image;
            if(image)
                [imageCache setObject:image forKey:sellerOption[@"pro1thumbnail"]];
        }
        }
    
        sellerIcon1.tag = 1;
        sellerIcon1.layer.borderWidth = 2.0;
        sellerIcon1.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        sellerImage1Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callFirstSellerIcon:)];
        sellerIcon1.userInteractionEnabled = "YES";
        sellerImage1Touch.numberOfTapsRequired = 1;
        [sellerIcon1 addGestureRecognizer:sellerImage1Touch];
        [sellerGroupContainer addSubview:sellerIcon1];
        shiftX +=(6*SCREEN_WIDTH)/16;
        
        UIImageView *sellerIcon2 = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX,internalY,(SCREEN_WIDTH)/4,SCREEN_WIDTH/4)];
        if(sellerOption[@"pro2id"] != [NSNull null] ){
        sellerIcon2.image = [UIImage imageNamed:@"ic_placeholder.png"];
        UIImage *image = [imageCache objectForKey:sellerOption[@"pro2thumbnail"]];
        if(image)
            sellerIcon2.image = image;
        else{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"pro2thumbnail"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            sellerIcon2.image = image;
            if(image)
                [imageCache setObject:image forKey:sellerOption[@"pro2thumbnail"]];
        }
        }
        sellerIcon2.tag = 2;
        sellerIcon2.layer.borderWidth = 2.0;
        sellerIcon2.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        sellerImage2Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callSecondSellerIcon:)];
        sellerIcon2.userInteractionEnabled = "YES";
        sellerImage2Touch.numberOfTapsRequired = 1;
        [sellerIcon2 addGestureRecognizer:sellerImage2Touch];
        [sellerGroupContainer addSubview:sellerIcon2];
        shiftX +=(5*SCREEN_WIDTH)/16;
        
        UIImageView *sellerIcon3 = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX,internalY,(SCREEN_WIDTH)/4,SCREEN_WIDTH/4)];
        if(sellerOption[@"pro3id"] != [NSNull null] ){
        sellerIcon3.image = [UIImage imageNamed:@"ic_placeholder.png"];
        UIImage *image = [imageCache objectForKey:sellerOption[@"pro3thumbnail"]];
        if(image)
            sellerIcon3.image = image;
        else{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"pro3thumbnail"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            sellerIcon3.image = image;
            if(image)
                [imageCache setObject:image forKey:sellerOption[@"pro3thumbnail"]];
        }
        }
        sellerIcon3.layer.borderWidth = 2.0;
        sellerIcon3.tag = 3;
        sellerIcon3.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
        sellerImage3Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callThirdSellerIcon:)];
        sellerIcon3.userInteractionEnabled = "YES";
        sellerImage3Touch.numberOfTapsRequired = 1;
        [sellerIcon3 addGestureRecognizer:sellerImage3Touch];
        [sellerGroupContainer addSubview:sellerIcon3];
        internalY +=SCREEN_WIDTH/4 +20;
        
        UIImageView *sellerIcon4 = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/16,internalY,(SCREEN_WIDTH)/8,SCREEN_WIDTH/8)];
        sellerIcon4.image = [UIImage imageNamed:@"ic_placeholder.png"];
        UIImage *image = [imageCache objectForKey:sellerOption[@"sellerIcon"]];
        if(image)
            sellerIcon4.image = image;
        else{
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"sellerIcon"]]];
            UIImage *image = [UIImage imageWithData: imageData];
            sellerIcon4.image = image;
            if(image)
                [imageCache setObject:image forKey:sellerOption[@"sellerIcon"]];
        }
        sellerIcon4.tag = 4;
        sellerImage4Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callFourthSellerIcon:)];
        sellerIcon4.userInteractionEnabled = "YES";
        sellerImage4Touch.numberOfTapsRequired = 1;
        [sellerIcon4 addGestureRecognizer:sellerImage4Touch];
        [sellerGroupContainer addSubview:sellerIcon4];
        
        
        NSDictionary *reqAttributesforShopTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:24.0f]};
        CGSize reqStringSizeShopTitle = [sellerOption[@"shopTitle"] sizeWithAttributes:reqAttributesforShopTitle];
        CGFloat reqStringWidthShopTitle = reqStringSizeShopTitle.width;
        UILabel *requiredShopTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/16 + (SCREEN_WIDTH)/8 +20, internalY, reqStringWidthShopTitle, 25)];
        [requiredShopTitleLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
        [requiredShopTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [requiredShopTitleLabel setText:sellerOption[@"shopTitle"]];
        sellorTitleTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callSellerTitleTap:)];
        requiredShopTitleLabel.userInteractionEnabled = "YES";
        sellorTitleTap.numberOfTapsRequired = 1;
        [requiredShopTitleLabel addGestureRecognizer:sellorTitleTap];
        [sellerGroupContainer addSubview:requiredShopTitleLabel];
        
        NSString *inStr = [NSString stringWithFormat: @"%d", [sellerOption[@"sellerProductCount"] intValue]];
        NSString *sellerCount = [inStr stringByAppendingString:@" PRODUCTS"];
        NSDictionary *reqAttributesforSellerCounnt = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:24.0f]};
        CGSize reqStringSellerSize = [sellerCount sizeWithAttributes:reqAttributesforSellerCounnt];
        CGFloat reqStringWidthSellerCount = reqStringSellerSize.width;
        UILabel *requiredSellerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/16 + (SCREEN_WIDTH)/8 +20, internalY + (SCREEN_WIDTH/24), reqStringWidthSellerCount, 25)];
        [requiredSellerCountLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [requiredSellerCountLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [requiredSellerCountLabel setText:sellerCount];
        [sellerGroupContainer addSubview:requiredSellerCountLabel];
        
        NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
        CGSize reqStringSizeforViewAllButtonText = [@"View All" sizeWithAttributes:reqAttributesforViewAllButtonText];
        CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
        UIButton  *buttonViewAll = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [buttonViewAll addTarget:self action:@selector(buttonHandlerForGroupIconViewAll :) forControlEvents:UIControlEventTouchUpInside];
        [buttonViewAll setTitle:@"View All" forState:UIControlStateNormal];
        buttonViewAll.frame = CGRectMake(SCREEN_WIDTH/16 + (SCREEN_WIDTH)/8 +20,internalY  + SCREEN_WIDTH/12,reqStringWidthViewAllButtonText+20, SCREEN_WIDTH/24);
        [buttonViewAll setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [buttonViewAll setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
        [buttonViewAll setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [sellerGroupContainer addSubview:buttonViewAll];
        
        
        internalY += SCREEN_WIDTH/8 +40;
        shiftX = 0;
        mainContainerY += internalY;
        CGRect newFrame = sellerGroupContainer.frame;
        newFrame.size.height = internalY;
        internalY = 10;
        sellerGroupContainer.frame = newFrame;
        mainContainerY +=5;
    }}
    // Screen width less than 600
    else {
        for(int i=0; i< [mainCollection[@"sellers"] count]; i++){
            
            sellerGroupContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY, _mainView.frame.size.width-10, 300)];
            sellerGroupContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
            sellerGroupContainer.layer.borderWidth = 2.0f;
            sellerGroupContainer.tag = i;
            [_mainView addSubview:sellerGroupContainer];
            
            shiftX = SCREEN_WIDTH /16;
            NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:i];
            
            UIImageView *sellerIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/32,internalY,(14*SCREEN_WIDTH)/32,(14*SCREEN_WIDTH)/32)];
            if(sellerOption[@"pro1id"] != [NSNull null] ){
            sellerIcon1.image = [UIImage imageNamed:@"ic_placeholder.png"];
            UIImage *image = [imageCache objectForKey:sellerOption[@"pro1thumbnail"]];
            if(image)
                sellerIcon1.image = image;
            else{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"pro1thumbnail"]]];
                UIImage *image = [UIImage imageWithData: imageData];
                sellerIcon1.image = image;
                if(image)
                    [imageCache setObject:image forKey:sellerOption[@"pro1thumbnail"]];
            }
            }
            sellerIcon1.tag = 1;
            sellerIcon1.layer.borderWidth = 2.0;
            sellerIcon1.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            sellerImage1Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callFirstSellerIcon:)];
            sellerIcon1.userInteractionEnabled = "YES";
            sellerImage1Touch.numberOfTapsRequired = 1;
            [sellerIcon1 addGestureRecognizer:sellerImage1Touch];
            [sellerGroupContainer addSubview:sellerIcon1];
            
            shiftX =(17*SCREEN_WIDTH)/32;
            
            UIImageView *sellerIcon2 = [[UIImageView alloc] initWithFrame:CGRectMake(shiftX,internalY,(14*SCREEN_WIDTH)/32-10,(14*SCREEN_WIDTH)/32)];
            if(sellerOption[@"pro2id"] != [NSNull null] ){
            sellerIcon2.image = [UIImage imageNamed:@"ic_placeholder.png"];
            UIImage *image = [imageCache objectForKey:sellerOption[@"pro2thumbnail"]];
            if(image)
                sellerIcon2.image = image;
            else{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"pro2thumbnail"]]];
                UIImage *image = [UIImage imageWithData: imageData];
                sellerIcon2.image = image;
                if(image)
                    [imageCache setObject:image forKey:sellerOption[@"pro2thumbnail"]];
            }
            }
            sellerIcon2.tag = 2;
            sellerIcon2.layer.borderWidth = 2.0;
            sellerIcon2.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            sellerImage2Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callSecondSellerIcon:)];
            sellerIcon2.userInteractionEnabled = "YES";
            sellerImage2Touch.numberOfTapsRequired = 1;
            [sellerIcon2 addGestureRecognizer:sellerImage2Touch];
            [sellerGroupContainer addSubview:sellerIcon2];
            
            internalY += (14*SCREEN_WIDTH)/32 +5;
            
            UIImageView *sellerIcon3 = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH /16+40,internalY,14*(SCREEN_WIDTH)/16-80,SCREEN_WIDTH/4 + SCREEN_WIDTH/8 +10 )];
            if(sellerOption[@"pro3id"] != [NSNull null] ){
            sellerIcon3.image = [UIImage imageNamed:@"ic_placeholder.png"];
            UIImage *image = [imageCache objectForKey:sellerOption[@"pro3thumbnail"]];
            if(image)
                sellerIcon3.image = image;
            else{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"pro3thumbnail"]]];
                UIImage *image = [UIImage imageWithData: imageData];
                sellerIcon3.image = image;
                if(image)
                    [imageCache setObject:image forKey:sellerOption[@"pro3thumbnail"]];
            }
            }
            sellerIcon3.layer.borderWidth = 2.0;
            sellerIcon3.tag = 3;
            sellerIcon3.layer.borderColor = [GlobalData colorWithHexString:@"EEEEEE"].CGColor;
            sellerImage3Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callThirdSellerIcon:)];
            sellerIcon3.userInteractionEnabled = "YES";
            sellerImage3Touch.numberOfTapsRequired = 1;
            [sellerIcon3 addGestureRecognizer:sellerImage3Touch];
            [sellerGroupContainer addSubview:sellerIcon3];
            
            internalY +=SCREEN_WIDTH/4 + SCREEN_WIDTH /8 +20 ;
            
            UIImageView *sellerIcon4 = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/16,internalY,(SCREEN_WIDTH)/8 + SCREEN_WIDTH/16 ,SCREEN_WIDTH/8 + SCREEN_WIDTH/16+15 )];
            sellerIcon4.image = [UIImage imageNamed:@"ic_placeholder.png"];
            UIImage *image = [imageCache objectForKey:sellerOption[@"sellerIcon"]];
            if(image)
                sellerIcon4.image = image;
            else{
                NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:sellerOption[@"sellerIcon"]]];
                UIImage *image = [UIImage imageWithData: imageData];
                sellerIcon4.image = image;
                if(image)
                    [imageCache setObject:image forKey:sellerOption[@"sellerIcon"]];
            }
            sellerIcon4.tag = 4;
            sellerImage4Touch =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callFourthSellerIcon:)];
            sellerIcon4.userInteractionEnabled = "YES";
            sellerImage4Touch.numberOfTapsRequired = 1;
            [sellerIcon4 addGestureRecognizer:sellerImage4Touch];
            [sellerGroupContainer addSubview:sellerIcon4];
            
            
            NSDictionary *reqAttributesforShopTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]};
            CGSize reqStringSizeShopTitle = [sellerOption[@"shopTitle"] sizeWithAttributes:reqAttributesforShopTitle];
            CGFloat reqStringWidthShopTitle = reqStringSizeShopTitle.width;
            UILabel *requiredShopTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4 +20, internalY, reqStringWidthShopTitle, 25)];
            [requiredShopTitleLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
            [requiredShopTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
            [requiredShopTitleLabel setText:sellerOption[@"shopTitle"]];
            sellorTitleTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callSellerTitleTap:)];
            requiredShopTitleLabel.userInteractionEnabled = "YES";
            sellorTitleTap.numberOfTapsRequired = 1;
            [requiredShopTitleLabel addGestureRecognizer:sellorTitleTap];
            [sellerGroupContainer addSubview:requiredShopTitleLabel];
            
            NSString *inStr = [NSString stringWithFormat: @"%d", [sellerOption[@"sellerProductCount"] intValue]];
            NSString *sellerCount = [inStr stringByAppendingString:@" PRODUCTS"];
            NSDictionary *reqAttributesforSellerCounnt = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]};
            CGSize reqStringSellerSize = [sellerCount sizeWithAttributes:reqAttributesforSellerCounnt];
            CGFloat reqStringWidthSellerCount = reqStringSellerSize.width;
            UILabel *requiredSellerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/4  +20, internalY + (SCREEN_WIDTH/24)+ SCREEN_WIDTH /48 +5, reqStringWidthSellerCount, 25)];
            [requiredSellerCountLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
            [requiredSellerCountLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
            [requiredSellerCountLabel setText:sellerCount];
            [sellerGroupContainer addSubview:requiredSellerCountLabel];
            
            NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]};
            CGSize reqStringSizeforViewAllButtonText = [@"View All" sizeWithAttributes:reqAttributesforViewAllButtonText];
            CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
            UIButton  *buttonViewAll = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [buttonViewAll addTarget:self action:@selector(buttonHandlerForGroupIconViewAll :) forControlEvents:UIControlEventTouchUpInside];
            [buttonViewAll setTitle:@"View All" forState:UIControlStateNormal];
            buttonViewAll.frame = CGRectMake(SCREEN_WIDTH/4 +20,internalY  + SCREEN_WIDTH/12 + SCREEN_WIDTH /24 +10,reqStringWidthViewAllButtonText+20, SCREEN_WIDTH/24 + SCREEN_WIDTH /48 +20);
            [buttonViewAll setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
            [buttonViewAll setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
            [buttonViewAll setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
            [sellerGroupContainer addSubview:buttonViewAll];
            
            
            internalY += SCREEN_WIDTH/8 + SCREEN_WIDTH/16+ 40;
            shiftX = 0;
            mainContainerY += internalY;
            CGRect newFrame = sellerGroupContainer.frame;
            newFrame.size.height = internalY;
            internalY = 10;
            sellerGroupContainer.frame = newFrame;
            mainContainerY +=5;
        }
        
    }
    
    
    
    //  last banner view all
    
    if(mainCollection[@"label3"] != NULL){
    internalY = 10;
    viewAllContainer = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY +10, _mainView.frame.size.width-10, 300)];
    viewAllContainer.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    viewAllContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:viewAllContainer];
     
    if([mainCollection[@"label3"] isEqualToString:@""] == false){
    UILabel *viewAllLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, internalY , SCREEN_WIDTH, 20)];
    [viewAllLabel setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [viewAllLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [viewAllLabel setText:mainCollection[@"label3"]];
    viewAllLabel.adjustsFontSizeToFitWidth = YES;
    viewAllLabel.textAlignment = NSTextAlignmentCenter;
    [viewAllContainer addSubview:viewAllLabel];
    internalY +=30;
    }
    NSDictionary *reqAttributesforViewAllButtonText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};
    CGSize reqStringSizeforViewAllButtonText = [@"View All" sizeWithAttributes:reqAttributesforViewAllButtonText];
    CGFloat reqStringWidthViewAllButtonText = reqStringSizeforViewAllButtonText.width;
    UIButton *buttonViewAllDesc = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [buttonViewAllDesc addTarget:self action:@selector(buttonHandlerForViewAll :) forControlEvents:UIControlEventTouchUpInside];
    [buttonViewAllDesc setTitle:@"View All" forState:UIControlStateNormal];
    buttonViewAllDesc.frame = CGRectMake((SCREEN_WIDTH)/2-(reqStringWidthViewAllButtonText/2),internalY ,reqStringWidthViewAllButtonText + 20, 40.0);
    [buttonViewAllDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buttonViewAllDesc setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
    [buttonViewAllDesc setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
    [viewAllContainer addSubview:buttonViewAllDesc];
    
        
    internalY += 50;
    mainContainerY += internalY;
    CGRect newFrame = viewAllContainer.frame;
    newFrame.size.height = internalY;
    viewAllContainer.frame = newFrame;
        
    }
    
    // last description
    internalY = 10;
    if ([mainCollection[@"label4"] isEqualToString:@""] == false) {
        
    viewAllDescription = [[UIView alloc]initWithFrame:CGRectMake(5, mainContainerY +20, _mainView.frame.size.width-10, 300)];
    viewAllDescription.layer.borderColor = [GlobalData colorWithHexString:@"555555"].CGColor;
    viewAllDescription.layer.borderWidth = 2.0f;
    [_mainView addSubview:viewAllDescription];
    
    NSDictionary *reqAttributesforHeadText = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]};
    CGSize reqStringSizeHeadText = [mainCollection[@"label4"] sizeWithAttributes:reqAttributesforHeadText];
    CGFloat reqStringWidthHeadText = reqStringSizeHeadText.width;
    UILabel *headTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, internalY , SCREEN_WIDTH, 30)];
    [headTextLabel setTextColor:[GlobalData colorWithHexString:@"3399cc"]];
    [headTextLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [headTextLabel setText:mainCollection[@"label4"]];
    headTextLabel.textAlignment = NSTextAlignmentCenter;
    [viewAllDescription addSubview:headTextLabel];
    
    NSString *aboutImage = mainCollection[@"aboutImage"];
     if ( aboutImage.length >0  ) {
    internalY += 30;
         
     NSDictionary *reqAttributesforDesc = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:19.0f]};
     CGSize reqStringSizeDesc = [mainCollection[@"sellerDescription"] sizeWithAttributes:reqAttributesforDesc];
     UILabel *sellerDesc = [[UILabel alloc] initWithFrame:CGRectMake(5,internalY,  _mainView.frame.size.width-20, 25)];
     [sellerDesc setTextColor:[GlobalData colorWithHexString:@"000000"]];
     [sellerDesc setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.0f]];
     [sellerDesc setText:mainCollection[@"aboutImage"]];
     sellerDesc.lineBreakMode = NSLineBreakByWordWrapping;
     sellerDesc.numberOfLines = 0;
     sellerDesc.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
     sellerDesc.textAlignment = NSTextAlignmentLeft;
     [sellerDesc sizeToFit];
     sellerDesc.preferredMaxLayoutWidth = viewAllDescription.frame.size.width-10 ;
     [viewAllDescription addSubview:sellerDesc];
         
     internalY +=sellerDesc.frame.size.height +10;
    }
        
        
    internalY += 40;
    mainContainerY += internalY;
    CGRect newFrame = viewAllDescription.frame;
    newFrame.size.height = internalY;
    viewAllDescription.frame = newFrame;
    }
    _mainViewHeightConstraints.constant = mainContainerY +25;
}


-(void)buttonHandlerForGroupIconViewAll :(UIButton*)button{
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:button.superview.tag];
    profileurl = sellerOption[@"profileurl"];
    [self performSegueWithIdentifier:@"groupProductMarketPlaceSegue" sender:self];
}


-(void)buttonHandlerForBannerImage{
}

-(void)buttonHandlerForViewAll:(UIButton*)button{
    [self performSegueWithIdentifier:@"marketplaceToSellerListSegue" sender:self];
 }


-(void)callFirstSellerIcon:(UITapGestureRecognizer *)recognize {
    
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:recognize.view.superview.tag];
    if(sellerOption[@"pro1id"] != [NSNull null] ){
    entityId = sellerOption[@"pro1id"];
    name = sellerOption[@"pro1name"];
    typeId = sellerOption[@"pro1type"];
    [self performSegueWithIdentifier:@"marketPlaceToProductSegue" sender:self];
    }
}

-(void)callSecondSellerIcon:(UITapGestureRecognizer *)recognize {
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:recognize.view.superview.tag];
    if(sellerOption[@"pro2id"] != [NSNull null] ){
    entityId = sellerOption[@"pro2id"];
    name = sellerOption[@"pro2name"];
    typeId = sellerOption[@"pro2type"];
    [self performSegueWithIdentifier:@"marketPlaceToProductSegue" sender:self];
    }
  }

- (void)callThirdSellerIcon:(UITapGestureRecognizer *)recognize {
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:recognize.view.superview.tag];
    if(sellerOption[@"pro3id"] != [NSNull null] ){
    entityId = sellerOption[@"pro3id"];
    name = sellerOption[@"pro3name"];
    typeId = sellerOption[@"pro3type"];
    [self performSegueWithIdentifier:@"marketPlaceToProductSegue" sender:self];
    }
}
- (void)callFourthSellerIcon:(UITapGestureRecognizer *)recognize {
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:recognize.view.superview.tag];
    profileurl = sellerOption[@"profileurl"];
    [self performSegueWithIdentifier:@"marketPlaceToSellerProfileData" sender:self];
}

-(void) callSellerTitleTap:(UITapGestureRecognizer *)recognize {
    NSDictionary *sellerOption = [mainCollection[@"sellers"] objectAtIndex:recognize.view.superview.tag];
    profileurl = sellerOption[@"profileurl"];
    [self performSegueWithIdentifier:@"marketPlaceToSellerProfileData" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"marketPlaceToProductSegue"]) {
    CatalogProduct *destViewController = segue.destinationViewController;
    destViewController.productId = entityId;
    destViewController.productName = name;
    destViewController.productType = typeId;
    destViewController.parentClass = @"marketplace";
     }
    else if([segue.identifier isEqualToString:@"groupProductMarketPlaceSegue"]){
    SellerProducts *destViewController = segue.destinationViewController;
    destViewController.profileUrl = profileurl;
    }
    else if([segue.identifier isEqualToString:@"marketPlaceToSellerProfileData"]){
        SellerProfileData *destViewController = segue.destinationViewController;
        destViewController.profileUrl = profileurl;
    }
}


@end
