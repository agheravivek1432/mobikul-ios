//
//  ListMenu.h
//  Mobikul
//
//  Created by Ratnesh on 12/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home.h"

@interface ListMenu : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    NSUserDefaults *preferences;
    NSMutableArray *menuListData,*headerTitle,*marketPlaceData;
    NSMutableArray *allData;
    UIAlertController *alert;
    NSMutableDictionary *questionAskToAdminData;
    NSInteger isValid;
    NSString *whichApiDataToprocess,*dataFromApi;
    UIVisualEffectView  *blurEffectView;
    UIView *requestContainerView;
    NSInteger isAlertVisible ;
    UITapGestureRecognizer *tapGestureRecognizer;
    NSInteger keyboardSetFlag , containerYPosition;
    NSUserDefaults *languageCode;
    id collection;
   UIWindow *currentWindow ;
    
}

@property (weak, nonatomic) IBOutlet UITableView *menuListTable;

@end
