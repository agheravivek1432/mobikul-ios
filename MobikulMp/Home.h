//
//  Home.h
//  Mobikul
//
//  Created by Ratnesh on 10/02/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalData.h"
#import "AppDelegate.h"
@interface Home : UIViewController <NSXMLParserDelegate,SampleProtocolDelegate>{
    
    NSUserDefaults *preferences;
    NSString *sessionId, *message, *dataFromApi;
    UIAlertController *alert;
    NSInteger imageCount, isAlertVisible;
    id mainCollection, addToWishListCollection, addToCartCollection,collection;
    NSMutableArray *categoryData, *storeData, *categoryId, *categoryImagesId, *categoryImages;
    NSMutableArray *featuredProductCurtainOpenSignal, *newProductCurtainOpenSignal;
    NSCache *imageCache,*catalogCache;
    NSDictionary *featuredCategoryDictionary, *productDictionary, *bannerProductDictionary;
    NSString *whichApiDataToprocess, *productIdForApiCall, *gotCode, *gotMessage, *gotSessionId, *gotDataFromApi, *code;
    NSMutableString *finalData;
    NSMutableData *webData;
    NSUserDefaults *languageCode;
    AppDelegate *appDelegate;
    UIWindow *currentWindow;
}

@property (weak, nonatomic) IBOutlet UILabel *featuredCategoriesar;
@property (weak, nonatomic) IBOutlet UILabel *featuredProductsar;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productHeightLabelConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *featureProductHeightConstraints;
@property (weak, nonatomic) IBOutlet UILabel *featureProductLabel;
@property (weak, nonatomic) IBOutlet UILabel *featuredCategories;
@property (weak, nonatomic) IBOutlet UILabel *productsar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *listMenuButtonar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *categoryArabic;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *listArabic;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *categotyMenuButtonar;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *categotyMenuButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *listMenuButton;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroller;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageScrollerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageScrollerWidthConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *featuredProductContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *featuredProductHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *nwProContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nwProHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feturedCategoryContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *featuredCategoryContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContainer;
@property (weak, nonatomic) IBOutlet UILabel *featuredCategoryTitle;
@property (nonatomic, strong) NSString *apiName;
@property (nonatomic, strong) NSString *categoryCustomerId;
@property (nonatomic, strong) NSCache *cache;


@end