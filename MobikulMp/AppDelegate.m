//
//  AppDelegate.m
//  Mobikul
//
//  Created by Ratnesh on 02/12/15.
//  Copyright © 2015 Webkul. All rights reserved.
//

#import "AppDelegate.h"
#import "Home.h"
#import "GlobalData.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


GlobalData *globalAppDelegateObject;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    globalAppDelegateObject = [[GlobalData alloc] init];
    globalAppDelegateObject.delegate = self;
    [globalAppDelegateObject language];
    _homeRun = @"runHome";
    _firstRun++;
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    //   arabic
    NSUserDefaults *languageCode = [NSUserDefaults standardUserDefaults];
    if([[languageCode stringForKey:@"language" ] isEqualToString:@"ar"]){
        _dataStore = @"otherLanguage";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Arabic" bundle:nil];
        UIViewController *initViewController = [storyBoard instantiateInitialViewController];
        [self.window setRootViewController:initViewController];
        [self.window makeKeyAndVisible];
    }
    else{
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
        UIViewController *initViewController = [storyBoard instantiateInitialViewController];
        [self.window setRootViewController:initViewController];
        [self.window makeKeyAndVisible];
    }
    
    GlobalData *obj = [GlobalData getInstance];
    obj.catalogMenuFlag = @"1";
    
    return YES;
}

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    token =[NSString stringWithFormat:@"%@",deviceToken];
    NSString *removeSpace = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *removeData = [removeSpace stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *removeData2 = [removeData stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = removeData2;
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}



#pragma mark - Sample protocol delegate
-(void)finalDataForRegisterDeviceCompleted:(id)collectionData
{
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalAppDelegateObject callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
}

-(void) callingHttppApi{
    if(![[preferences stringForKey:@"isDeviceToken"] isEqualToString:token]){
        NSMutableString *post = [NSMutableString string];
        preferences = [NSUserDefaults standardUserDefaults];
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        [post appendFormat:@"sessionId=%@&", savedSessionId];
        [post appendFormat:@"token=%@", token];
        [globalAppDelegateObject callHTTPPostMethod:post api:@"mobikulhttp/extra/registerDevice" signal:@"HttpPostRegisterDevice"];
        globalAppDelegateObject.delegate = self;
    }
}
-(void)doFurtherProcessingWithResult{
    [preferences setObject:token forKey:@"isDeviceToken"];
    [preferences synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    //    NSLog(@"app icon value %ld",[UIApplication sharedApplication].applicationIconBadgeNumber);   // current app badge value
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive){        // when we tap on notification bar
        NSDictionary *dict=[userInfo valueForKeyPath:@"aps"];
        if([dict[@"notification_type"] isEqualToString:@"1"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotificationforProduct" object:nil userInfo:userInfo];
        }
        else if([dict[@"notification_type"] isEqualToString:@"2"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotificationforCategory" object:nil userInfo:userInfo];
        }
    }
    else if(application.applicationState == UIApplicationStateBackground){                   // when application is closed (background check)
        NSDictionary *dict=[userInfo valueForKeyPath:@"aps"];
        //        NSLog(@" data received via push noti in background   %@",dict[@"badge"]);
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[dict[@"badge"] integerValue]];
    }
    else if([UIApplication sharedApplication].applicationState ==UIApplicationStateActive){  // when application is running and notification come to application (foreground check)
        
    }
    completionHandler(UIBackgroundFetchResultNewData);
    
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        GlobalData *obj = [GlobalData getInstance];
        for(int i=0;i<[obj.categoryImages count] ; i++){
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[obj.categoryImages objectAtIndex:i]]]];
            if(image !=nil)
            [obj.gloablCatalogIconCache setObject:image forKey:[obj.categoryImages objectAtIndex:i]];
        }
        
    });
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
