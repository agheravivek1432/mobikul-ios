//
//  InvoiceDetails.m
//  MobikulMp
//
//  Created by kunal prasad on 30/06/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

#import "InvoiceDetails.h"
#import "GlobalData.h"
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

@interface InvoiceDetails ()
@end

GlobalData *globalObjectInvoiceDetails;

@implementation InvoiceDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    globalObjectInvoiceDetails = [[GlobalData alloc] init];
    globalObjectInvoiceDetails.delegate = self;
    [globalObjectInvoiceDetails language];
    whichApiDataToProcess = @"";
    isAlertVisible = 0;
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    self.navigationController.navigationBar.barTintColor =  [GlobalData colorWithHexString:GLOBAL_COLOR];
    currentWindow = [UIApplication sharedApplication].keyWindow;
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingHttppApi];

}


#pragma mark - Sample protocol delegate
-(void)finalHttpDataprocessCompleted:(id)collectionData{
    isAlertVisible = 1;
    collection = collectionData ;
    if([collection[@"success"] integerValue] == 5){
        [self loginRequest];
    }
    else{
        [self doFurtherProcessingWithResult];
    }
}

-(void)finalCallingApiCompleted{
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    [self callingHttppApi];
}

-(void)connectionErorWindow{
    [self performSelector: @selector(showConnectionErrorDialogue) withObject: nil afterDelay: 1];
}

-(void)loginRequestCall{
    [self loginRequest];
}
-(void)loginRequest{
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"password=%@&",API_KEY];
    [post appendFormat:@"username=%@", API_USER_NAME];
    [globalObjectInvoiceDetails callHTTPPostMethod:post api:@"mobikulhttp/extra/soaplogin" signal:@"HttpLoginPostMetod"];
    
}

-(void)showConnectionErrorDialogue{
    [self.view setUserInteractionEnabled:YES];
    if([currentWindow viewWithTag:121212])
        [[currentWindow viewWithTag:121212] removeFromSuperview];
    UIAlertController * AC = [UIAlertController alertControllerWithTitle:[globalObjectInvoiceDetails.languageBundle localizedStringForKey:@"warning" value:@"" table:nil] message:[globalObjectInvoiceDetails.languageBundle localizedStringForKey:@"errorConnection" value:@"" table:nil] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:[globalObjectInvoiceDetails.languageBundle localizedStringForKey:@"retry" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
        if(savedSessionId == nil)
            [self loginRequest];
        else
            [self callingHttppApi];
    }];
    UIAlertAction* noBtn = [UIAlertAction actionWithTitle:[globalObjectInvoiceDetails.languageBundle localizedStringForKey:@"dismiss" value:@"" table:nil] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [AC addAction:okBtn];
    [AC addAction:noBtn];
    [self.parentViewController presentViewController:AC animated:YES completion:nil];
}


-(void) callingHttppApi{
    [GlobalData alertController:currentWindow msg:[globalObjectInvoiceDetails.languageBundle localizedStringForKey:@"pleaseWait" value:@"" table:nil]];
    preferences = [NSUserDefaults standardUserDefaults];
    NSMutableString *post = [NSMutableString string];
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    [post appendFormat:@"sessionId=%@&", savedSessionId];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *customerid = [preferences objectForKey:@"customerId"];
    [post appendFormat:@"customerId=%@&", _customerId];
    [post appendFormat:@"storeId=%@&", storeId];
    [post appendFormat:@"incrementId=%@&", _incrementId];
    [post appendFormat:@"invoiceId=%@&", _invoiceId];
    if([whichApiDataToProcess isEqual:@"sendinvoiceEmail"]){
    [globalObjectInvoiceDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/sendinvoiceMail" signal:@"HttpPostMetod"];
    globalObjectInvoiceDetails.delegate = self;
    }
    else{
    [globalObjectInvoiceDetails callHTTPPostMethod:post api:@"mobikulmphttp/marketplace/viewInvoice" signal:@"HttpPostMetod"];
    globalObjectInvoiceDetails.delegate = self;
    }


}

-(void)callingApi{
    NSString *prefSessionId = [preferences objectForKey:@"sessionId"];
    NSString *apiName = @"";
    NSString *nameSpace = @"urn:Magento";
    NSError *error;
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSString *storeId = [preferences objectForKey:@"storeId"];
    NSString *customerid = [preferences objectForKey:@"customerId"];
    [dictionary setObject:_customerId forKey:@"customerId"];
    [dictionary setObject:storeId forKey:@"storeId"];
    [dictionary setObject:_incrementId forKey:@"incrementId"];
    [dictionary setObject:_invoiceId forKey:@"invoiceId"];
    if([whichApiDataToProcess isEqual:@"sendinvoiceEmail"]){
        apiName = @"mobikulmpMarketplaceSendinvoiceMail";
    }
    else{
    apiName = @"mobikulmpMarketplaceViewInvoice";
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString;
    if(!jsonData){
//        NSLog(@"Got an error: %@", error);
    }
    else
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *parameters = [NSString stringWithFormat:@"<sessionId xsi:type=\"xsd:string\">%@</sessionId><attributes xsi:type=\"xsd:string\">%@</attributes>", prefSessionId, jsonString];
    alert = [UIAlertController alertControllerWithTitle: @"Please Wait..." message: nil preferredStyle: UIAlertControllerStyleAlert];
    NSString *envelope = [GlobalData createEnvelope:apiName  forNamespace:nameSpace forParameters:parameters currentView:self isAlertVisiblef:0 alertf:alert];
    isAlertVisible = 1;
    [globalObjectInvoiceDetails sendingApiData:envelope];
    globalObjectInvoiceDetails.delegate = self;
}



-(void)doFurtherProcessingWithResult{
    if(isAlertVisible == 1){
        isAlertVisible = 0;
        [self.view setUserInteractionEnabled:YES];
        if([currentWindow viewWithTag:121212])
            [[currentWindow viewWithTag:121212] removeFromSuperview];
    }
    mainCollection = collection;
    float mainContainerY = 0;
    float internalY = 5;
    float Y = 0;
    if([whichApiDataToProcess isEqual:@"sendinvoiceEmail"]){
    }
    else{
    internalY +=5;
    NSDictionary *reqAttributesHeading = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]};                 // main heading
    CGSize reqStringSizeHeading = [mainCollection[@"mainHeading"] sizeWithAttributes:reqAttributesHeading];
    UILabel *headingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY, reqStringSizeHeading.width, 35)];
    [headingLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [headingLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [headingLabelName setText:mainCollection[@"mainHeading"]];
    [_mainView addSubview:headingLabelName];
    
    internalY += 40;
    if(mainCollection[@"sendmailAction"]){
        UIButton *buttonSendEmail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [buttonSendEmail addTarget:self action:@selector(buttonHandlerForSendEmail :) forControlEvents:UIControlEventTouchUpInside];
        [buttonSendEmail setTitle:mainCollection[@"sendmailAction"] forState:UIControlStateNormal];
        buttonSendEmail.frame = CGRectMake(8,internalY ,_mainView.frame.size.width-16, 40.0);
        [buttonSendEmail setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [buttonSendEmail setBackgroundColor:[GlobalData colorWithHexString:@"3399cc"] ];
        [buttonSendEmail setTitleColor:[GlobalData colorWithHexString:@"ffffff"]  forState:UIControlStateNormal];
        [_mainView addSubview:buttonSendEmail ];
        internalY +=45;
    }
    mainContainerY +=internalY;
    
    
    UIView *detailListContainer = [[UIView alloc] initWithFrame:CGRectMake(5, mainContainerY , _mainView.frame.size.width-10, SCREEN_WIDTH/2)];       // main container
    detailListContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    detailListContainer.layer.borderWidth = 2.0f;
    [_mainView addSubview:detailListContainer];
    
    internalY = 5;
    
    NSDictionary *reqAttributesSubHeading = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]};             // sub heading
    CGSize reqStringSizeSubHeading = [mainCollection[@"subHeading"] sizeWithAttributes:reqAttributesSubHeading];
    UILabel *subHeadingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,internalY, _mainView.frame.size.width-10, 30)];
    [subHeadingLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subHeadingLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:26.0f]];
    [subHeadingLabelName setText:mainCollection[@"subHeading"]];
    [detailListContainer addSubview:subHeadingLabelName];
    
    internalY += 35;
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(5, internalY ,detailListContainer.frame.size.width-10 ,1)];         // line 1
    line1.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line1.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:line1];
    
    internalY +=4;
    
    mainContainerY +=internalY;
    
    UIView *orderContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,130)];           // order data container
    orderContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    orderContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:orderContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeOrderTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // order information heading
    CGSize reqStringSizeOrderTitle = [mainCollection[@"orderData"][@"title"] sizeWithAttributes:reqAttributeOrderTitle];
    UILabel *orderTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeOrderTitle.width, 25)];
    [orderTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [orderTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [orderTitleLabelName setText:mainCollection[@"orderData"][@"title"]];
    [orderContainer addSubview:orderTitleLabelName];

    Y +=30;
    
    NSDictionary *reqAttributeOrderName = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order information heading
    CGSize reqStringSizeOrderName = [mainCollection[@"orderData"][@"label"] sizeWithAttributes:reqAttributeOrderName];
    UILabel *orderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeOrderName.width, 25)];
    [orderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [orderLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [orderLabelName setText:mainCollection[@"orderData"][@"label"]];
    [orderContainer addSubview:orderLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeOrderStatus = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order information status heading
    CGSize reqStringSizeOrderStatus = [[mainCollection[@"orderData"][@"statusLabel"] stringByAppendingString:@" : "] sizeWithAttributes:reqAttributeOrderStatus];
    UILabel *orderStatusLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeOrderStatus.width, 25)];
    [orderStatusLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [orderStatusLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [orderStatusLabelName setText:[mainCollection[@"orderData"][@"statusLabel"] stringByAppendingString:@" : "]];
    [orderContainer addSubview:orderStatusLabelName];

    NSDictionary *reqAttributeOrderStatusValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order information status heading
    CGSize reqStringSizeOrderStatusValue = [mainCollection[@"orderData"][@"statusValue"] sizeWithAttributes:reqAttributeOrderStatusValue];
    UILabel *orderStatusValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeOrderStatus.width +10,Y, reqStringSizeOrderStatusValue.width, 25)];
    [orderStatusValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [orderStatusValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [orderStatusValueLabelName setText:mainCollection[@"orderData"][@"statusValue"]];
    [orderContainer addSubview:orderStatusValueLabelName];
    
    Y += 30;
    
    NSDictionary *reqAttributeDateStatus = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order information status heading
    CGSize reqStringSizeDateStatus = [[mainCollection[@"orderData"][@"dateLabel"] stringByAppendingString:@" :"] sizeWithAttributes:reqAttributeDateStatus];
    UILabel *dateStatusLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeDateStatus.width, 25)];
    [dateStatusLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [dateStatusLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [dateStatusLabelName setText:[mainCollection[@"orderData"][@"dateLabel"] stringByAppendingString:@" :"]];
    [orderContainer addSubview:dateStatusLabelName];
    
    
    
    NSDictionary *reqAttributeDateStatusValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order information status heading
    CGSize reqStringSizeDateStatusValue = [mainCollection[@"orderData"][@"dateValue"] sizeWithAttributes:reqAttributeDateStatusValue];
    UILabel *dateStatusValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeDateStatus.width +10,Y, reqStringSizeDateStatusValue.width, 25)];
    [dateStatusValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [dateStatusValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [dateStatusValueLabelName setText:mainCollection[@"orderData"][@"dateValue"]];
    [orderContainer addSubview:dateStatusValueLabelName];
    
    
    
    mainContainerY +=140;
    internalY +=140;
    
    UIView *buyerContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // buyer data container
    buyerContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    buyerContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:buyerContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeBuyerTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerTitle = [mainCollection[@"buyerData"][@"title"] sizeWithAttributes:reqAttributeBuyerTitle];
    UILabel *buyerTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeBuyerTitle.width, 25)];
    [buyerTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [buyerTitleLabelName setText:mainCollection[@"buyerData"][@"title"]];
    [buyerContainer addSubview:buyerTitleLabelName];
    
    
    Y +=30;
    
    
    
    NSDictionary *reqAttributeBuyerName = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerName = [mainCollection[@"buyerData"][@"nameLabel"] sizeWithAttributes:reqAttributeBuyerName];
    UILabel *buyerNameLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeBuyerName.width, 25)];
    [buyerNameLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerNameLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [buyerNameLabelName setText:mainCollection[@"buyerData"][@"nameLabel"]];
    [buyerContainer addSubview:buyerNameLabelName];
    
    
    NSDictionary *reqAttributeBuyerNameValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerNameValue = [mainCollection[@"buyerData"][@"nameValue"] sizeWithAttributes:reqAttributeBuyerNameValue];
    UILabel *buyerNameLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeBuyerName.width+10,Y, reqStringSizeBuyerNameValue.width, 25)];
    [buyerNameLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerNameLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [buyerNameLabelValue setText:mainCollection[@"buyerData"][@"nameValue"]];
    [buyerContainer addSubview:buyerNameLabelValue];
    
    Y +=30;
    
    
    NSDictionary *reqAttributeBuyerEmail = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerEmail = [mainCollection[@"buyerData"][@"emailLabel"] sizeWithAttributes:reqAttributeBuyerEmail];
    UILabel *buyerEmailLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10,Y, reqStringSizeBuyerEmail.width, 25)];
    [buyerEmailLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerEmailLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [buyerEmailLabelName setText:mainCollection[@"buyerData"][@"emailLabel"]];
    [buyerContainer addSubview:buyerEmailLabelName];
    
    
    NSDictionary *reqAttributeBuyerEmailValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // buyer information heading
    CGSize reqStringSizeBuyerEmailValue = [mainCollection[@"buyerData"][@"emailValue"] sizeWithAttributes:reqAttributeBuyerEmailValue];
    UILabel *buyerEmailLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeBuyerEmail.width + 10,Y, reqStringSizeBuyerEmailValue.width, 25)];
    [buyerEmailLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [buyerEmailLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [buyerEmailLabelValue setText:mainCollection[@"buyerData"][@"emailValue"]];
    [buyerContainer addSubview:buyerEmailLabelValue];
    
    Y += 30;
    
    CGRect buyerContainerFrame = buyerContainer.frame;
    buyerContainerFrame.size.height = Y;
    buyerContainer.frame = buyerContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *shippingAddressContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // shipping address data container
    shippingAddressContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    shippingAddressContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:shippingAddressContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeShippingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeShippingTitle = [mainCollection[@"shippingAddressData"][@"title"] sizeWithAttributes:reqAttributeShippingTitle];
    UILabel *shippingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingTitle.width, 25)];
    [shippingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [shippingTitleLabelName setText:mainCollection[@"shippingAddressData"][@"title"]];
    [shippingAddressContainer addSubview:shippingTitleLabelName];
    
    Y +=30;
    
    for(int i=0;i<[mainCollection[@"shippingAddressData"][@"address"] count]; i++){
        NSDictionary *address = [mainCollection[@"shippingAddressData"][@"address"] objectAtIndex: i];
        NSString *addr = address;
        NSDictionary *reqAttributeShippingAddr = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping address
        CGSize reqStringSizeShippingAddr = [addr sizeWithAttributes:reqAttributeShippingAddr];
        UILabel *shippingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingAddr.width, 25)];
        [shippingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [shippingTitleLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [shippingTitleLabelName setText:addr];
        [shippingAddressContainer addSubview:shippingTitleLabelName];
        
        Y +=30;
        
    }
    
    CGRect shippingContainerFrame = shippingAddressContainer.frame;
    shippingContainerFrame.size.height = Y;
    shippingAddressContainer.frame = shippingContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *shippingInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];// shipping information data container
    shippingInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    shippingInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:shippingInformationContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeShippingInfor = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeShippingInfor = [mainCollection[@"shippingMethodData"][@"title"] sizeWithAttributes:reqAttributeShippingInfor];
    UILabel *shippingInfoLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingInfor.width, 25)];
    [shippingInfoLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingInfoLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [shippingInfoLabelName setText:mainCollection[@"shippingMethodData"][@"title"]];
    [shippingInformationContainer addSubview:shippingInfoLabelName];
    
    Y += 30;
    
    
    NSDictionary *reqAttributeShippingMethod = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping method
    CGSize reqStringSizeShippingMethod = [mainCollection[@"shippingMethodData"][@"method"] sizeWithAttributes:reqAttributeShippingMethod];
    UILabel *shippingMethodLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShippingMethod.width, 25)];
    [shippingMethodLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingMethodLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingMethodLabelName setText:mainCollection[@"shippingMethodData"][@"method"]];
    [shippingInformationContainer addSubview:shippingMethodLabelName];
    
    Y +=30;
    
    
    CGRect shippingMethodContainerFrame = shippingInformationContainer.frame;
    shippingMethodContainerFrame.size.height = Y;
    shippingInformationContainer.frame = shippingMethodContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    
    UIView *billingAddressContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];           // billing address data container
    billingAddressContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    billingAddressContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:billingAddressContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeBillingTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // billing information heading
    CGSize reqStringSizeBillingTitle = [mainCollection[@"billingAddressData"][@"title"] sizeWithAttributes:reqAttributeBillingTitle];
    UILabel *billingTitleLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingTitle.width, 25)];
    [billingTitleLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingTitleLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [billingTitleLabelName setText:mainCollection[@"billingAddressData"][@"title"]];
    [billingAddressContainer addSubview:billingTitleLabelName];
    
    Y +=30;
    
    for(int i=0;i<[mainCollection[@"billingAddressData"][@"address"] count]; i++){
        NSDictionary *address = [mainCollection[@"billingAddressData"][@"address"] objectAtIndex: i];
        NSString *addr = address;
        NSDictionary *reqAttributeBillingAddr = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // shipping address
        CGSize reqStringSizeBillingAddr = [addr sizeWithAttributes:reqAttributeBillingAddr];
        UILabel *billingAddrLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingAddr.width, 25)];
        [billingAddrLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [billingAddrLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [billingAddrLabelName setText:addr];
        [billingAddressContainer addSubview:billingAddrLabelName];
        
        Y +=30;
        
    }
    
    CGRect billingAddrContainerFrame = billingAddressContainer.frame;
    billingAddrContainerFrame.size.height = Y;
    billingAddressContainer.frame = billingAddrContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *billingInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,100)];// billing information data container
    billingInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    billingInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:billingInformationContainer];
    
    
    Y = 5;
    
    NSDictionary *reqAttributeBillingInfor = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // shipping information heading
    CGSize reqStringSizeBillingInfor = [mainCollection[@"paymentMethodData"][@"title"] sizeWithAttributes:reqAttributeBillingInfor];
    UILabel *billingInfoLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingInfor.width, 25)];
    [billingInfoLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingInfoLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [billingInfoLabelName setText:mainCollection[@"paymentMethodData"][@"title"]];
    [billingInformationContainer addSubview:billingInfoLabelName];
    
    
    Y += 30;
    
    
    NSDictionary *reqAttributeBillingMethod = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // billing method
    CGSize reqStringSizeBillingMethod = [mainCollection[@"paymentMethodData"][@"method"] sizeWithAttributes:reqAttributeBillingMethod];
    UILabel *billingMethodLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeBillingMethod.width, 25)];
    [billingMethodLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [billingMethodLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [billingMethodLabelName setText:mainCollection[@"paymentMethodData"][@"method"]];
    [billingInformationContainer addSubview:billingMethodLabelName];
    
    Y +=30;
    
    
    CGRect billingMethodContainerFrame = billingInformationContainer.frame;
    billingMethodContainerFrame.size.height = Y;
    billingInformationContainer.frame = billingMethodContainerFrame;
    
    mainContainerY +=Y;
    internalY +=Y +10;
    
    
    UIView *paymentInformationContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,900)];// payment information data container
    paymentInformationContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    paymentInformationContainer.layer.borderWidth = 2.0f;
    [detailListContainer addSubview:paymentInformationContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeItemOrderTitle = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]};             // payment heading information heading
    CGSize reqStringItemOrderTitle = [@"ITEM ORDERED" sizeWithAttributes:reqAttributeItemOrderTitle];
    UILabel *itemOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringItemOrderTitle.width, 35)];
    [itemOrderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [itemOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f]];
    [itemOrderLabelName setText:@"ITEM ORDERED"];
    [paymentInformationContainer addSubview:itemOrderLabelName];
    
    Y +=40;
    
    NSDictionary *reqAttributeProduct = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]};             // payment subheading information heading
    CGSize reqStringProduct = [@"PRODUCT" sizeWithAttributes:reqAttributeProduct];
    UILabel *productLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringProduct.width, 30)];
    [productLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [productLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25.0f]];
    [productLabelName setText:@"PRODUCT"];
    [paymentInformationContainer addSubview:productLabelName];
    
    Y +=35;
    
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(5, Y ,paymentInformationContainer.frame.size.width-10 ,1)];           // line 3
    line3.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
    line3.layer.borderWidth = 2.0f;
    [paymentInformationContainer addSubview:line3];
    
    Y +=4;
    
    for(int i=0;i<[mainCollection[@"items"] count];i++){
        
        NSDictionary *productData  = [mainCollection[@"items"] objectAtIndex: i];
        
        NSDictionary *reqAttributeProductName = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]};             // product name subheading information heading
        CGSize reqStringProductName = [productData[@"productName"] sizeWithAttributes:reqAttributeProductName];
        UILabel *productLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringProductName.width, 25)];
        [productLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [productLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
        [productLabelName setText:productData[@"productName"]];
        [paymentInformationContainer addSubview:productLabelName];
        
        Y +=30;
        
        NSDictionary *reqAttributePriceLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // price lebel
        CGSize reqStringSizePriceLabel = [@"PRICE : " sizeWithAttributes:reqAttributePriceLebel];
        UILabel *priceLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizePriceLabel.width, 25)];
        [priceLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [priceLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [priceLabelName setText:@"PRICE : "];
        [paymentInformationContainer addSubview:priceLabelName];
        
        
        NSDictionary *reqAttributePriceValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // price value
        CGSize reqStringSizePriceValue = [productData[@"price"] sizeWithAttributes:reqAttributePriceValue];
        UILabel *priceValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizePriceLabel.width +15, Y , reqStringSizePriceValue.width, 25)];
        [priceValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [priceValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [priceValueLabelName setText:productData[@"price"]];
        [paymentInformationContainer addSubview:priceValueLabelName];
        
        Y +=30;
        
        NSDictionary *reqAttributeStatusLebel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // status lebel
        CGSize reqStringSizeStatusLabel = [@"STATUS : " sizeWithAttributes:reqAttributeStatusLebel];
        UILabel *statusLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeStatusLabel.width, 25)];
        [statusLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [statusLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [statusLabelName setText:@"STATUS : "];
        [paymentInformationContainer addSubview:statusLabelName];
        
        Y += 30;
        
        if([productData[@"qty"][@"Ordered"] integerValue] > 0){
        NSString *orderValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Ordered"] intValue]];
        NSDictionary *reqAttributeOrderLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order lebel
        CGSize reqStringSizeOrderLabel = [@"Oredered : " sizeWithAttributes:reqAttributeOrderLabel];
        UILabel *orderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeOrderLabel.width, 25)];
        [orderLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [orderLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [orderLabelName setText:@"Oredered : "];
        [paymentInformationContainer addSubview:orderLabelName];
        
        NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // order Value
        CGSize reqStringSizeOrderValue = [orderValue sizeWithAttributes:reqAttributeOrderValue];
        UILabel *orderLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ orderLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
        [orderLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [orderLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [orderLabelValue setText:orderValue];
        [paymentInformationContainer addSubview:orderLabelValue];
        Y+=30 ;
            
            
        }
        if([productData[@"qty"][@"Invoiced"] integerValue] > 0){
        NSString *invoiceValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Invoiced"] intValue]];
        NSDictionary *reqAttributeInvoiceLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // invoice  lebel
        CGSize reqStringSizeInvoiceLabel = [@"Invoiced : " sizeWithAttributes:reqAttributeInvoiceLabel];
        UILabel *invoiceLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeInvoiceLabel.width, 25)];
        [invoiceLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [invoiceLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [invoiceLabelName setText:@"Invoiced : "];
        [paymentInformationContainer addSubview:invoiceLabelName];
        
        NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // invoice value
        CGSize reqStringSizeOrderValue = [invoiceValue sizeWithAttributes:reqAttributeOrderValue];
        UILabel *invoiceLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ invoiceLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
        [invoiceLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [invoiceLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [invoiceLabelValue setText:invoiceValue];
        [paymentInformationContainer addSubview:invoiceLabelValue];
         Y+=30 ;
        }
        if([productData[@"qty"][@"Shipped"] integerValue] > 0){
        NSString *shipValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Shipped"] intValue]];
        NSDictionary *reqAttributeShipLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // ship  lebel
        CGSize reqStringSizeShipLabel = [@"Shipped : " sizeWithAttributes:reqAttributeShipLabel];
        UILabel *shipLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeShipLabel.width, 25)];
        [shipLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [shipLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [shipLabelName setText:@"Shipped : "];
        [paymentInformationContainer addSubview:shipLabelName];
        
        NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // ship value
        CGSize reqStringSizeOrderValue = [shipValue sizeWithAttributes:reqAttributeOrderValue];
        UILabel *shipLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ shipLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
        [shipLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [shipLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [shipLabelValue setText:shipValue];
        [paymentInformationContainer addSubview:shipLabelValue];
        
        Y+=30 ;
        }
        if([productData[@"qty"][@"Canceled"] integerValue] > 0){
        NSString *cancelValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Canceled"] intValue]];
        NSDictionary *reqAttributeCancelLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // cancel  lebel
        CGSize reqStringSizeCancelLabel = [@"Canceled : " sizeWithAttributes:reqAttributeCancelLabel];
        UILabel *cancelLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeCancelLabel.width, 25)];
        [cancelLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [cancelLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [cancelLabelName setText:@"Canceled : "];
        [paymentInformationContainer addSubview:cancelLabelName];
        
        NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // cancel value
        CGSize reqStringSizeOrderValue = [cancelValue sizeWithAttributes:reqAttributeOrderValue];
        UILabel *cancelLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ cancelLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
        [cancelLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [cancelLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [cancelLabelValue setText:cancelValue];
        [paymentInformationContainer addSubview:cancelLabelValue];
         Y+=30 ;
        }
        if([productData[@"qty"][@"Refunded"] integerValue] > 0){
        NSString *cancelValue= [NSString stringWithFormat:@"%d",[productData[@"qty"][@"Refunded"] intValue]];
        NSDictionary *reqAttributeCancelLabel = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // refund  lebel
        CGSize reqStringSizeCancelLabel = [@"Refunded : " sizeWithAttributes:reqAttributeCancelLabel];
        UILabel *cancelLabelName = [[UILabel alloc] initWithFrame:CGRectMake(20 , Y , reqStringSizeCancelLabel.width, 25)];
        [cancelLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [cancelLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [cancelLabelName setText:@"Refunded : "];
        [paymentInformationContainer addSubview:cancelLabelName];
        
        NSDictionary *reqAttributeOrderValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // refund value
        CGSize reqStringSizeOrderValue = [cancelValue sizeWithAttributes:reqAttributeOrderValue];
        UILabel *cancelLabelValue = [[UILabel alloc] initWithFrame:CGRectMake(30+ cancelLabelName.frame.size.width , Y , reqStringSizeOrderValue.width, 25)];
        [cancelLabelValue setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [cancelLabelValue setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [cancelLabelValue setText:cancelValue];
        [paymentInformationContainer addSubview:cancelLabelValue];
        Y+=30 ;
        }
        
        NSDictionary *reqAttributeAdminComm = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // Admin commision lebel
        CGSize reqStringSizeAdminComm = [@"ADMIN COMISSION : " sizeWithAttributes:reqAttributeAdminComm];
        UILabel *AdminLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeAdminComm.width, 25)];
        [AdminLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [AdminLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [AdminLabelName setText:@"ADMIN COMISSION : "];
        [paymentInformationContainer addSubview:AdminLabelName];
        
        
        NSDictionary *reqAttributeAdminValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // admin commision value value
        CGSize reqStringSizeAdmin = [productData[@"adminComission"] sizeWithAttributes:reqAttributeAdminValue];
        UILabel *adminCommLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeAdminComm.width +15, Y , reqStringSizeAdmin.width, 25)];
        [adminCommLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [adminCommLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [adminCommLabelName setText:productData[@"adminComission"]];
        [paymentInformationContainer addSubview:adminCommLabelName];
        
        Y +=30;
        
        NSDictionary *reqAttributeVendor = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // vendor commision lebel
        CGSize reqStringSizeVendor = [@"VENDOR TOTAL : " sizeWithAttributes:reqAttributeVendor];
        UILabel *VendorLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeVendor.width, 25)];
        [VendorLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [VendorLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [VendorLabelName setText:@"VENDOR TOTAL : "];
        [paymentInformationContainer addSubview:VendorLabelName];
        
        
        NSDictionary *reqAttributeVendorValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // vendor commision value value
        CGSize reqStringSizeVendorValue = [productData[@"vendorTotal"] sizeWithAttributes:reqAttributeVendorValue];
        UILabel *vendorValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeVendor.width +15, Y , reqStringSizeVendorValue.width, 25)];
        [vendorValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [vendorValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [vendorValueLabelName setText:productData[@"vendorTotal"]];
        [paymentInformationContainer addSubview:vendorValueLabelName];
        
        Y+=30;
        
        NSDictionary *reqAttributeSubTotal = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
        CGSize reqStringSizeSubTotal = [@"SUB TOTAL : " sizeWithAttributes:reqAttributeSubTotal];
        UILabel *subTotalLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeSubTotal.width, 25)];
        [subTotalLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [subTotalLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [subTotalLabelName setText:@"SUB TOTAL : "];
        [paymentInformationContainer addSubview:subTotalLabelName];
        
        NSDictionary *reqAttributeSubTotalValue = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
        CGSize reqStringSizeSubTotalValue = [productData[@"subTotal"] sizeWithAttributes:reqAttributeSubTotalValue];
        UILabel *subTotalValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(reqStringSizeSubTotal.width +15 , Y , reqStringSizeSubTotalValue.width, 25)];
        [subTotalValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
        [subTotalValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
        [subTotalValueLabelName setText:productData[@"subTotal"]];
        [paymentInformationContainer addSubview:subTotalValueLabelName];
        
        
        Y +=30;
        
        if(i !=[mainCollection[@"items"] count]-1 ){
        UIView *line4 = [[UIView alloc] initWithFrame:CGRectMake(5, Y ,paymentInformationContainer.frame.size.width-10 ,1)];           // line 3
        line4.layer.borderColor = [GlobalData colorWithHexString:@"636363"].CGColor;
        line4.layer.borderWidth = 2.0f;
        [paymentInformationContainer addSubview:line4];
        
            Y +=4;
        }
}
    
    CGRect paymentInformationContainerFrame = paymentInformationContainer.frame;
    paymentInformationContainerFrame.size.height = Y;
    paymentInformationContainer.frame = paymentInformationContainerFrame;
    
    mainContainerY +=Y+10;
    internalY +=Y +10;
    
    UIView *differentPaymentContainer = [[UIView alloc] initWithFrame:CGRectMake(10, internalY ,detailListContainer.frame.size.width-20 ,500)];// payment information data container
    differentPaymentContainer.layer.borderColor = [GlobalData colorWithHexString:@"CCCCCC"].CGColor;
    differentPaymentContainer.layer.borderWidth = 2.0f;
    differentPaymentContainer.backgroundColor = [GlobalData colorWithHexString:@"CCCCCC"];
    [detailListContainer addSubview:differentPaymentContainer];
    
    Y = 5;
    
    NSDictionary *reqAttributeSubTotal = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // sub total lebel
    CGSize reqStringSizeSubTotal = [mainCollection[@"subtotal"][@"title"] sizeWithAttributes:reqAttributeSubTotal];
    UILabel *subTotalLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeSubTotal.width, 25)];
    [subTotalLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subTotalLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [subTotalLabelName setText:mainCollection[@"subtotal"][@"title"]];
    [differentPaymentContainer addSubview:subTotalLabelName];
    
    UILabel *subTotalValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [subTotalValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [subTotalValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [subTotalValueLabelName setText:mainCollection[@"subtotal"][@"value"]];
    subTotalValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:subTotalValueLabelName];
    
    
    Y += 30;
    
    NSDictionary *reqAttributeShipping = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             //  shipping lebel
    CGSize reqStringSizeShipping = [mainCollection[@"shipping"][@"title"] sizeWithAttributes:reqAttributeShipping];
    UILabel *shippingLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeShipping.width, 25)];
    [shippingLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingLabelName setText:mainCollection[@"shipping"][@"title"]];
    [differentPaymentContainer addSubview:shippingLabelName];
    
    UILabel *shippingValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [shippingValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [shippingValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [shippingValueLabelName setText:mainCollection[@"shipping"][@"value"]];
    shippingValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:shippingValueLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeTax = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};             // total lebel
    CGSize reqStringSizeTax = [mainCollection[@"tax"][@"title"] sizeWithAttributes:reqAttributeTax];
    UILabel *taxLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTax.width, 25)];
    [taxLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [taxLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [taxLabelName setText:mainCollection[@"tax"][@"title"]];
    [differentPaymentContainer addSubview:taxLabelName];
    
    UILabel *taxValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [taxValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [taxValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [taxValueLabelName setText:mainCollection[@"tax"][@"value"]];
    taxValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:taxValueLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeTotalOrder = @{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]};             //  total order lebel
    CGSize reqStringSizeTotalOrder = [mainCollection[@"totalOrderedAmount"][@"title"] sizeWithAttributes:reqAttributeTotalOrder];
    UILabel *totalOrderLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalOrder.width, 25)];
    [totalOrderLabelName setTextColor:[GlobalData colorWithHexString:@"000000"]];
    [totalOrderLabelName setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18.0f]];
    [totalOrderLabelName setText:mainCollection[@"totalOrderedAmount"][@"title"]];
    [differentPaymentContainer addSubview:totalOrderLabelName];
    
    UILabel *totalOrderValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totalOrderValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalOrderValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalOrderValueLabelName setText:mainCollection[@"totalOrderedAmount"][@"value"]];
    totalOrderValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totalOrderValueLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeTotalVendor = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};                 //  total vendor lebel
    CGSize reqStringSizeTotalVendor = [mainCollection[@"totalVendorAmount"][@"title"] sizeWithAttributes:reqAttributeTotalVendor];
    UILabel *totalVendorLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalVendor.width, 25)];
    [totalVendorLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalVendorLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalVendorLabelName setText:mainCollection[@"totalVendorAmount"][@"title"]];
    [differentPaymentContainer addSubview:totalVendorLabelName];
    
    UILabel *totaVendorlValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totaVendorlValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totaVendorlValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totaVendorlValueLabelName setText:mainCollection[@"totalVendorAmount"][@"value"]];
    totaVendorlValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totaVendorlValueLabelName];
    
    Y +=30;
    
    NSDictionary *reqAttributeTotalAdmin = @{NSFontAttributeName:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]};                //  total admin commission lebel
    CGSize reqStringSizeTotalAdmin = [mainCollection[@"totalAdminComission"][@"title"] sizeWithAttributes:reqAttributeTotalAdmin];
    UILabel *totalAdminCommLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10, Y , reqStringSizeTotalAdmin.width, 25)];
    [totalAdminCommLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totalAdminCommLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totalAdminCommLabelName setText:mainCollection[@"totalAdminComission"][@"title"]];
    [differentPaymentContainer addSubview:totalAdminCommLabelName];
    
    UILabel *totaAdminCommlValueLabelName = [[UILabel alloc] initWithFrame:CGRectMake(0 , Y , differentPaymentContainer.frame.size.width-10, 25)];
    [totaAdminCommlValueLabelName setTextColor:[GlobalData colorWithHexString:@"636363"]];
    [totaAdminCommlValueLabelName setFont:[UIFont fontWithName:@"Trebuchet MS" size:18.0f]];
    [totaAdminCommlValueLabelName setText:mainCollection[@"totalAdminComission"][@"value"]];
    totaAdminCommlValueLabelName.textAlignment =   NSTextAlignmentRight;
    [differentPaymentContainer addSubview:totaAdminCommlValueLabelName];
    
    Y +=30;
    
    CGRect differentpaymentInformationContainerFrame = differentPaymentContainer.frame;
    differentpaymentInformationContainerFrame.size.height = Y;
    differentPaymentContainer.frame = differentpaymentInformationContainerFrame;
    
    mainContainerY +=Y+10;
    internalY +=Y +10;
    
    CGRect mainContainerFrame = detailListContainer.frame;
    mainContainerFrame.size.height = internalY;
    detailListContainer.frame = mainContainerFrame;
    
    _mainViewHeightConstraints.constant =mainContainerY+50 ;
    }
}

-(void)buttonHandlerForSendEmail:(UIButton*)button{
    whichApiDataToProcess = @"sendinvoiceEmail";
    NSString *savedSessionId = [preferences objectForKey:@"sessionId"];
    if(savedSessionId == nil)
        [self loginRequest];
    else
        [self callingApi];
}


@end
